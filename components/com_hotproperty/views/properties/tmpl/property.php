<?php
/**
 * @version		$Id: property.php 945 2010-02-01 10:39:52Z CY $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Property Layout
 * 
 * Displays a single Property.
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */
$hotproperty =& MosetsApplication::getInstance('hotproperty');
?>

<div id="hotproperty" class="property <?php echo $hotproperty->getCfg('pageclass_sfx'); ?>">
<?php if ($hotproperty->getCfg('show_page_title', 1)) : ?>
	<?php echo MosetsHTML::_('content.header', 1, $this->escape($this->pageTitle)); ?>
<?php endif; ?>
	
	<p class="buttons">
		<?php echo MosetsHTML::_('hotproperty.icon.edit', 'property', $this->row); ?>
	<?php if ($hotproperty->getCfg('show_pdficon', 1)) : ?>
		<?php echo MosetsHTML::_('hotproperty.icon.pdf', $this->row); ?>
	<?php endif; ?>
	</p>
	
	<?php echo $this->row->event->afterDisplayTitle; ?>
	
	<dl class="attributes">
	<?php foreach ($this->extrafields as $extrafield) : ?>
		<?php
		$extrafield_value = MosetsHTML::_('hotproperty.content.extrafield', $extrafield, $this->row);
		if (!empty($extrafield_value)) : ?>
			<?php if (!$extrafield->hideCaption && !empty($extrafield->caption)) : ?>
				<dt class="caption <?php echo $extrafield->name; ?>"><?php echo $this->escape($extrafield->iscore ? JText::_($extrafield->caption) : $extrafield->caption); ?></dt>
			<?php endif; ?>
			<dd class="value <?php echo $extrafield->name; ?>"><?php echo $extrafield_value; ?></dd>
		<?php endif; ?>
	<?php endforeach; ?>
	</dl>
	
<?php if ($hotproperty->getCfg('show_agentdetails')) : ?>
	<div class="agent">
		<?php echo MosetsHTML::_('content.header', 2, MosetsHTML::_('link', JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'agents', 'layout' => 'properties', 'id' => $this->row->Agent->id))), $this->escape($this->row->Agent->name)), $this->row->Agent->name); ?>
		<?php $this->agent = $this->row->Agent; ?>
		<?php echo $this->loadTemplate('agent'); ?>
	</div>
<?php endif; ?>

<?php if ($hotproperty->getCfg('show_enquiryform')) : ?>
	<?php echo MosetsHTML::_('content.header', 2, JText::_('Send Enquiry')); ?>
	<?php $this->agent = $this->row->Agent; ?>
	<?php echo $this->loadTemplate('contact'); ?>
<?php endif; ?>
</div>