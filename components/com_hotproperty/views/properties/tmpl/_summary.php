<?php
/**
 * @version		$Id: _summary.php 945 2010-02-01 10:39:52Z CY $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * 
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

$hotproperty =& MosetsApplication::getInstance('hotproperty');
?>

<?php echo MosetsHTML::_('content.header', $this->headerLevel,
	MosetsHTML::_('link', JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'properties', 'layout' => 'property', 'id' => $this->property->id))),
		($hotproperty->getCfg('show_thumb') ? '<span class="thumbnail">' . MosetsHTML::_('hotproperty.image.photo', 'thumbnail', @$this->property->Photo[0]->thumb) . '</span>' : '') . $this->escape($this->property->name)
	),
	$this->property->name,
	null,
	array('class' => 'contentheading')
); ?>
<?php
$buttons = '';
$buttons .= MosetsHTML::_('hotproperty.icon.edit', 'property', $this->property);
if ($buttons) : ?>
<p class="buttons">
	<?php echo $buttons; ?>
</p>
<?php endif; ?>
<?php if (!empty($this->extrafields)) : ?>
<dl class="attributes">
<?php
foreach ($this->extrafields as $extrafield) :
	$extrafield_value = MosetsHTML::_('hotproperty.content.extrafield', $extrafield, $this->property);
	if (!empty($extrafield_value)) :
		if (!$extrafield->hideCaption && !empty($extrafield->caption)) :
			?>
	<dt class="caption <?php echo $extrafield->name; ?>"><?php echo $this->escape($extrafield->iscore ? JText::_($extrafield->caption) : $extrafield->caption); ?></dt>
				<?php
			endif;
			?>
	<dd class="value <?php echo $extrafield->name; ?>"><?php echo $extrafield_value; ?></dd>
			<?php
		endif;
	endforeach;
	?>
</dl>
<?php endif; ?>
<?php if ($hotproperty->getCfg('show_moreinfo')) : ?>
<p class="more"><?php echo MosetsHTML::_('link', JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'properties', 'layout' => 'property', 'id' => $this->property->id))), JText::_('Show More Information'), array('title' => $this->escape($this->property->name), 'class' => 'readon')); ?></p>
<?php endif; ?>
