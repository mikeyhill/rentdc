<?php
/**
 * @version		$Id: property_contact.php 914 2009-11-11 23:32:02Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Company Contact Layout
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

$hotproperty =& MosetsApplication::getInstance('hotproperty');
?>

<?php MosetsHTML::_('behavior.formvalidation'); ?>

<form method="post" action="<?php echo JRoute::_($this->get('action')); ?>" class="form-validate">
	<fieldset>
		<ol>
			<li>
				<?php echo MosetsHTML::_('form.label', JText::_('Name'), 'name', true); ?>
				<?php echo MosetsHTML::_('form.input', 'text', 'name', '', false, array('class' => 'required')); ?>
			</li>
			<li>
				<?php echo MosetsHTML::_('form.label', JText::_('Email'), 'email', true); ?>
				<?php echo MosetsHTML::_('form.input', 'text', 'email', '', false, array('class' => 'required validate-email')); ?>
			</li>
			<li>
				<?php echo MosetsHTML::_('form.label', JText::_('Contact Number'), 'contactnumber'); ?>
				<?php echo MosetsHTML::_('form.input', 'text', 'contactnumber', ''); ?>
			</li>
			<li>
				<?php echo MosetsHTML::_('form.label', JText::_('Enquiry'), 'enquiry', true); ?>
				<?php echo MosetsHTML::_('form.textarea', 'enquiry', '', false, 30, 5, array('class' => 'required')); ?>
			</li>
		</ol>
		<?php echo MosetsHTML::_('form.input', 'submit', 'contact', JText::_('Send'))?>
	</fieldset>

	<input type="hidden" name="option" value="com_hotproperty" />
	<input type="hidden" name="controller" value="properties" />
	<input type="hidden" name="task" value="contact" />
	<?php echo MosetsHTML::_('form.token'); ?>
</form>