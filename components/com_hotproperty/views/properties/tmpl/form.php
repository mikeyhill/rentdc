<?php
/**
 * @version		$Id: form.php 929 2009-11-18 02:45:14Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Properties Form Template
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

$hotproperty =& MosetsApplication::getInstance('hotproperty');
?>

<?php
$document =& JFactory::getDocument();
$document->addScriptDeclaration("
	window.addEvent('domready', function() {
		newPhotoId = 0;
	});
");
?>

<div id="hotproperty" class="property form <?php echo $hotproperty->getCfg('pageclass_sfx'); ?>">
<?php if ($hotproperty->getCfg('show_page_title', 1)) : ?>
	<?php echo MosetsHTML::_('content.header', 1, $this->escape($this->pageTitle)); ?>
<?php endif; ?>

	<form action="<?php echo JRoute::_($this->get('action')); ?>" method="post" name="adminForm" class="form-validate" enctype="multipart/form-data">
		<p class="buttons">
			<?php echo MosetsHTML::_('form.input', 'submit', 'save', JText::_('Save')); ?>
			<?php echo MosetsHTML::_('link', base64_decode($this->get('referer')), JText::_('Cancel')); ?>
		</p>
		<fieldset id="general<?php echo $this->row->id; ?>" class="general">				
			<legend><?php echo JText::_('General'); ?></legend>
			<ol class="attributes">
				<li class="name">
					<?php echo MosetsHTML::_('form.label', JText::_('Name'), 'Property[' . $this->row->id . '][name]', true, JText::_('TOOLTIP NAME')); ?>
					<?php echo MosetsHTML::_('form.input', 'text', 'Property[' . $this->row->id . '][name]', $this->row->name, false, array('class' => 'required', 'maxlength' => '255')); ?>
				</li>
			<?php if (!$this->agent->need_approval) : ?>
				<?php echo MosetsHTML::_('form.input', 'hidden', 'Property[' . $this->row->id . '][approved]', 1); ?>
			<?php endif; ?>
				<?php echo MosetsHTML::_('form.input', 'hidden', 'Property[' . $this->row->id . '][agent]', $this->agent->id); ?>
				<li class="type">
					<?php echo MosetsHTML::_('form.label', JText::_('Type'), 'Property[' . $this->row->id . '][type]', true, JText::_('TOOLTIP TYPE')); ?>
					<?php echo MosetsHTML::_('hotproperty.list.types', 'Property[' . $this->row->id . '][type]', array('class' => 'required'), $this->row->type, false, false, true); ?>
				</li>
				<li class="published">
					<fieldset class="radio">
						<legend><span class="hasTip" title="<?php echo JText::_('Published'); ?>::<?php echo JText::_('TOOLTIP PUBLISHED'); ?>"><?php echo JText::_('Published'); ?></span></legend>
						<?php echo MosetsHTML::_('form.booleanlist', 'Property[' . $this->row->id . '][published]', null, $this->row->published); ?>
					</fieldset>
				</li>
			</ol>
		</fieldset>
		<fieldset id="extrafields<?php echo $this->row->id; ?>" class="extrafields">	
			<legend><?php echo JText::_('Extrafields'); ?></legend>
			<?php echo MosetsHTML::_('interface.advice', JText::_('Only published fields are listed below.'), 'info'); ?>
		<?php if (!empty($this->extrafields)) : ?>
			<ol class="attributes">
			<?php
			$exclude = array('company', 'name', 'agent', 'type', 'created', 'modified', 'published', 'featured', 'hits');
			foreach ($this->extrafields as $extrafield)
			{
				// Only display published extrafields
				if (!in_array($extrafield->name, $exclude)) {
					?>
					<li class="<?php echo $extrafield->name; ?>">
						<?php echo MosetsHTML::_('hotproperty.form.extrafield', $extrafield, $this->row, true); ?>
					</li>
					<?php
				}
			}
			?>
			</ol>
		<?php endif; ?>
		</fieldset>
		<fieldset id="publishing<?php echo $this->row->id; ?>" class="publishing">
			<legend><?php echo JText::_('Publishing'); ?></legend>
			<ol class="attributes">
				<li class="created">
					<?php echo MosetsHTML::_('form.label', JText::_('Created Date'), 'Property[' . $this->row->id . '][created]', false, JText::_('Creation date of the property.')); ?>
					<?php echo MosetsHTML::_('calendar', $this->row->created, 'Property[' . $this->row->id . '][created]'); ?>
				</li>
				<li class="publish_up">
					<?php echo MosetsHTML::_('form.label', JText::_('Start Publishing'), 'Property[' . $this->row->id . '][publish_up]', false, JText::_('Start publishing date/time.')); ?>
					<?php echo MosetsHTML::_('calendar', $this->row->publish_up, 'Property[' . $this->row->id . '][publish_up]'); ?>
				</li>
				<li class="publish_down">
					<?php echo MosetsHTML::_('form.label', JText::_('End Publishing'), 'Property[' . $this->row->id . '][publish_down]', false, JText::_('End publishing date/time.')); ?>
					<?php echo MosetsHTML::_('calendar', $this->row->publish_down, 'Property[' . $this->row->id . '][publish_down]'); ?>
				</li>
				<li class="metadesc">
					<?php echo MosetsHTML::_('form.label', JText::_('Description'), 'Property[' . $this->row->id . '][metadesc]', false, JText::_('Meta description.')); ?>
					<?php echo MosetsHTML::_('form.textarea', 'Property[' . $this->row->id . '][metadesc]', $this->row->metadesc); ?>
				</li>
				<li class="metakey">
					<?php echo MosetsHTML::_('form.label', JText::_('Keywords'), 'Property[' . $this->row->id . '][metakey]', false, JText::_('Meta keywords.')); ?>
					<?php echo MosetsHTML::_('form.textarea', 'Property[' . $this->row->id . '][metakey]', $this->row->metakey); ?>
				</li>
			</ol>
		</fieldset>
		<fieldset id="photos<?php echo $this->row->id; ?>" class="photos">
			<legend><?php echo JText::_('Photos'); ?></legend>
			<?php echo MosetsHTML::_('interface.advice', sprintf(JText::_('According to your configuration, new photos will be resized to %spx and to %spx for thumbnails.'), $hotproperty->getCfg('imgsize_standard'), $hotproperty->getCfg('imgsize_thumb')), 'info'); ?>
			<ol>	
		<?php
		if (!empty($this->row->Photo)) :
			for ($i=0, $n = count($this->row->Photo); $i < $n; $i++) :
				$photo =& $this->row->Photo[$i];
				?>
				<li>
					<fieldset id="photo<?php echo $photo->id; ?>" class="photo">
						<legend><span><?php echo JText::_('Photo #') . ($i+1); ?></span></legend>
						<ol class="attributes">
							<li class="photo">
								<fieldset class="file">
									<legend><span class="hasTip" title="<?php echo JText::_('Image'); ?>::<?php echo JText::_('TOOLTIP IMAGE'); ?>"><?php echo JText::_('Image'); ?></span></legend>
									<ol>
										<li>
											<?php echo MosetsHTML::_('form.label', JText::_('File'), 'Photo[' . $photo->id . '][original]'); ?>
											<?php echo MosetsHTML::_('form.input', 'file', 'Photo[' . $photo->id . '][original]', false); ?>
										</li>
										<li class="preview">
											<?php echo MosetsHTML::_('hotproperty.image.photo', 'thumbnail', $photo->thumb, $photo->thumb); ?>
											<?php echo MosetsHTML::_('form.label', MosetsHTML::_('form.checkbox', 'Photo[' . $photo->id . '][remove]') . JText::_('remove')); ?>
										</li>
									</ol>
								</fieldset>
							</li>
							<li class="title">
								<?php echo MosetsHTML::_('form.label', JText::_('Title'), 'Photo[' . $photo->id . '][title]', false, JText::_('Give a title for the photo. If none provided, the filename will be assumed.')); ?>
								<?php echo MosetsHTML::_('form.input', 'text', 'Photo[' . $photo->id . '][title]', $photo->title); ?>
							</li>
							<li class="description">
								<?php echo MosetsHTML::_('form.label', JText::_('Description'), 'Photo[' . $photo->id . '][desc]', false, JText::_('Give a description for that photo.')); ?>
								<?php echo MosetsHTML::_('form.textarea', 'Photo[' . $photo->id . '][desc]', $photo->desc); ?>
							</li>
							<li class="ordering">
								<?php echo MosetsHTML::_('form.label', JText::_('Ordering'), 'Photo[' . $photo->id . '][ordering]', false, JText::_('')); ?>
								<?php echo MosetsHTML::_('hotproperty.list.ordering', 'Photo[' . $photo->id . '][ordering]', 'photos', $photo, 'ordering', 'title'); ?>
							</li>
						</ol>
						<?php echo MosetsHTML::_('form.input', 'hidden', 'Photo[' . $photo->id . '][property]', $this->row->id); ?>
					</fieldset>
				</li>
				<?php
			endfor;
		endif;
		?>
				<li>
					<fieldset id="photo0" class="photo new">
						<legend><span><?php echo JText::_('New Photo'); ?></span></legend>
						<?php echo MosetsHTML::_('form.input', 'hidden', 'Photo[0][property]', $this->row->id); ?>
						<ol class="attributes">
							<li class="photo">
								<?php echo MosetsHTML::_('form.label', JText::_('Photo'), 'Photo[0][original]'); ?>
								<?php echo MosetsHTML::_('form.input', 'file', 'Photo[0][original]', false); ?>
							</li>
							<li class="title">
								<?php echo MosetsHTML::_('form.label', JText::_('Title'), 'Photo[0][title]', false, JText::_('Give a title for the photo. If none provided, the filename will be assumed.')); ?>
								<?php echo MosetsHTML::_('form.input', 'text', 'Photo[0][title]', ''); ?>
							</li>
							<li class="description">
								<?php echo MosetsHTML::_('form.label', JText::_('Description'), 'Photo[0][desc]', false, JText::_('Give a description for that photo.')); ?>
								<?php echo MosetsHTML::_('form.textarea', 'Photo[0][desc]', ''); ?>
							</li>
							<li class="ordering">
								<?php echo MosetsHTML::_('form.label', JText::_('Ordering'), 'Photo[0][ordering]', false, JText::_('Ordering')); ?>
								<p><?php echo JText::_('DESCNEWITEMSLAST') ?></p>
							</li>
						</ol>
					</fieldset>
				</li>
			</ol>

		<?php
		$document =& JFactory::getDocument();
		$document->addScriptDeclaration("
		window.addEvent('domready', function() {
			// Create the 'Add Photo' button
			new Element('p', {
				'id': 'addphoto" . $this->row->id . "',
				'class' : 'addphoto'
			}).setHTML(
				'<a href=\"javascript:void(0);\">" . JText::_('Add Photo') . "</a>'
			).injectInside($('photos" . $this->row->id . "'));
			
			// Remove the New Photo fieldset set in case Javascript was disabled
			$('photo0').getParent().remove();
			
			$('addphoto" . $this->row->id . "').addEvent('click', function(ev) {
				newPhotoId--;
				new Element('li').setHTML(
					'<fieldset id=\"photo' + newPhotoId + '\" class=\"photo new\">' +
						'<legend><span>New Photo</span></legend>' +
						'<a class=\"close\" href=\"javascript:void(0);\" title=\"" . JText::_('Close') . "\" onclick=\"javascript:$(this).getParent().getParent().remove();\"></a>' +
						'<ol class=\"attributes\">' +
							'<li class=\"photo\">' +
								'<label for=\"Photo' + newPhotoId + 'original\">" . JText::_('File') . "</label>' +
								'<input type=\"file\" value=\"\" id=\"Photo' + newPhotoId + 'original\" name=\"Photo[' + newPhotoId + '][original]\"/>' +
							'</li>' +
							'<li class=\"title\">' +
								'<label for=\"Photo' + newPhotoId + 'title\">" . JText::_('Title') . "</label>' +
								'<input type=\"text\" value=\"\" id=\"Photo' + newPhotoId + 'title\" name=\"Photo[' + newPhotoId + '][title]\"/>' +
							'</li>' +
							'<li class=\"description\">' +
								'<label for=\"Photo' + newPhotoId + 'desc\">" . JText::_('Description') . "</label>' +
								'<textarea id=\"Photo' + newPhotoId + 'desc\" name=\"Photo[' + newPhotoId + '][desc]\" cols=\"30\" rows=\"5\"></textarea>' +
							'</li>' +
							'<li class=\"ordering\">' +
								'<label>" . JText::_('Ordering') . "</label>' +
								'<p>" . JText::_('DESCNEWITEMSLAST') . "</p>' +
							'</li>' +
						'</ol>' +
						'<input type=\"hidden\" value=\"' + newPhotoId + '\" id=\"Photo' + newPhotoId + 'id\" name=\"Photo[' + newPhotoId + '][id]\"/>' +
						'<input type=\"hidden\" value=\"" . $this->row->id . "\" id=\"Photo' + newPhotoId + 'property\" name=\"Photo[' + newPhotoId + '][property]\"/>' +
					'</fieldset>'
				).injectInside($$('#photos" . $this->row->id . " ol')[0]);
			});
		});
		");
		?>
		</fieldset>
		<p class="buttons">
			<?php echo MosetsHTML::_('form.input', 'submit', 'save', JText::_('Save')); ?>
			<?php echo MosetsHTML::_('link', base64_decode($this->get('referer')), JText::_('Cancel')); ?>
		</p>

		<input type="hidden" name="option" value="com_hotproperty" />
		<input type="hidden" name="controller" value="properties" />
		<input type="hidden" name="task" value="save" />
		<?php echo MosetsHTML::_('form.token'); ?>
	</form>
</div>
