<?php
/**
 * @version		$Id: manage.php 945 2010-02-01 10:39:52Z CY $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Manage Template
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

$hotproperty =& MosetsApplication::getInstance('hotproperty');
?>

<div id="hotproperty" class="agent manage <?php echo $hotproperty->getCfg('pageclass_sfx'); ?>">
<?php if ($hotproperty->getCfg('show_page_title', 1)) : ?>
	<?php echo MosetsHTML::_('content.header', 1, $this->escape($this->pageTitle)); ?>
<?php endif; ?>

<form action="<?php echo JRoute::_($this->get('action')); ?>" method="post" name="adminForm" class="form-validate">

	<p class="buttons">
		<?php echo MosetsHTML::_('form.input', 'button', 'publish', JText::_('Publish'), false, array('onclick' => "submitbutton('publish');")); ?>
		<?php echo MosetsHTML::_('form.input', 'button', 'unpublish', JText::_('Unpublish'), false, array('onclick' => "submitbutton('unpublish');")); ?>
		<?php echo MosetsHTML::_('form.input', 'button', 'remove', JText::_('Remove'), false, array('onclick' => "submitbutton('remove');")); ?>
		<?php echo MosetsHTML::_('form.input', 'button', 'add', JText::_('Add'), false, array('onclick' => "submitbutton('add');")); ?>
	</p>

	<div class="arrange">
	<?php if ($this->pagination && $this->get('total') > 5 && $hotproperty->getCfg('use_displaynum')) : ?>
		<div class="limit">
			<span class="label"><?php echo JText::_('Display #:'); ?></span>
			<?php echo $this->pagination->getLimitBox(); ?>
		</div>
	<?php endif; ?>
		<fieldset class="filters">
			<legend><span>Filters</span></legend>
			<ul>
				<li class="search">
					<fieldset>
						<legend><span>Search</span></legend>
						<ul class="search">
						<?php foreach ($this->filters['search'] as $field => $value) : ?>
							<li><?php echo MosetsHTML::_('filter.search', $field, $this->escape($value)); ?></li>
						<?php endforeach; ?>
						</ul>
					</fieldset>
				</li>
				<li class="list">
					<fieldset>
						<legend><span>List</span></legend>
						<ul>
							<li><?php echo MosetsHTML::_('grid.state', 'filter[list][Property.published]', $this->filters['list']['Property.published']); ?></li>
							<li><?php echo MosetsHTML::_('hotproperty.list.types', 'filter[list][Property.type]', array('onchange' => 'document.adminForm.submit();'), $this->filters['list']['Property.type'], false, false, true); ?></li>
						</ul>
					</fieldset>
				</li>
			</ul>
			<p>
				<?php echo MosetsHTML::_('form.input', 'submit', '', JText::_('Go'), null, array('class' => 'button'));; ?>
				<?php //echo MosetsHTML::_('form.input', 'reset', '', JText::_('Reset'), null, array('class' => 'button'));; ?>
			</p>
		</fieldset>
	</div>
	
	<table class="results">
		<thead>
			<tr class="ordering">
				<th width="1%">
					<?php echo JText::_('#'); ?>
				</th>
				<th width="1%">
					<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->rows); ?>);" />
				</th>			
				<th>
					<?php echo MosetsHTML::_('hotproperty.content.sort', JText::_('Name'), 'Property.name', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
				</th>
				<th nowrap="nowrap" width="11%">
					<?php echo MosetsHTML::_('hotproperty.content.sort', JText::_('Published'), 'Property.published', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
				</th>
				<th width="11%">
					<?php echo MosetsHTML::_('hotproperty.content.sort', JText::_('Type'), 'Type.name', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
				</th>
				<th width="11%">
					<?php echo MosetsHTML::_('hotproperty.content.sort', JText::_('Location'), 'Property.suburb', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
				</th>
				<th nowrap="nowrap" width="10">
					<?php echo MosetsHTML::_('hotproperty.content.sort', JText::_('Date'), 'Property.creation', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
				</th>
				<th nowrap="nowrap" width="1%">
					<?php echo MosetsHTML::_('hotproperty.content.sort', JText::_('Hits'), 'Property.hits', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
				</th>
				<th nowrap="nowrap" width="1%">
				<?php echo MosetsHTML::_('hotproperty.content.sort', JText::_('Id'), 'Property.id', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
				</th>
			</tr>			
		</thead>
		<tbody>
		<?php if ($this->rows) : ?>
			<?php
			$k = 0;
			for ($i=0, $n = count($this->rows); $i < $n; $i++)
			{
				$row =& $this->rows[$i];
				?>
				<tr<?php echo ($k%2 ? ' class="zebra"' : ''); ?>>
					<td align="center">
						<?php echo $this->pagination->getRowOffset($i); ?>
					</td>
					<td>
						<?php echo MosetsHTML::_('grid.id', $i, $row->id); ?>
					</td>
					<td>
					<?php
					$href = '';
					// Has this property photo(s) ?
					if (isset($row->Photo)) {
						$href .= MosetsHTML::_('image', MosetsApplication::getPath('assets_images_url', 'hotproperty') . 'photo.png', JText::_('This property has an image defined.'), array('align' => 'absmiddle'));
					}
					$href .= MosetsHTML::_('link', JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'properties', 'layout' => 'form', 'id' => $row->id, 'referer' => base64_encode(JRequest::getVar('REQUEST_URI', null, 'server', 'string'))))), $this->escape($row->name));

					echo $href;
					?>
					</td>
				<?php if ($row->approved) : ?>
					<td align="center">
						<?php echo MosetsHTML::_('grid.published', $row, $i); ?>
					</td>
				<?php else : ?>
					<td align="center">
						[ Pending ]
					</td>
				<?php endif; ?>
					<td>
						<?php echo $row->Type->name; ?>
					</td>
					<td>
						<?php echo (!empty($row->suburb) && !empty($row->state)) ? $row->suburb . ", " . $row->state : $row->suburb . $row->state; ?>
					</td>
					<td align="center">
						<?php echo MosetsHTML::_('date',  $row->created, JText::_('DATE_FORMAT_LC4')); ?>
					</td>
					<td align="center">
						<?php echo $row->hits; ?>
					</td>
					<td align="center">
						<?php echo $row->id; ?>
					</td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
		<?php else : ?>
				<tr>
					<td colspan="9">No result</td>
				</tr>
		<?php endif; ?>
		</tbody>
	</table>
	<?php MosetsHTML::_('interface.legend'); ?>

	<input type="hidden" name="option" value="com_hotproperty" />
	<input type="hidden" name="controller" value="properties" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="ordering" value="<?php echo $this->get('ordering'); ?>" />
	<input type="hidden" name="ordering_dir" value="<?php echo $this->get('ordering_dir'); ?>" />
	<?php echo MosetsHTML::_('form.token'); ?>
</form>
	
<?php if ($this->pagination->{'pages.total'} > 1) : ?>
	<div class="paging">
		<?php echo $this->pagination->getPagesLinks(); ?>
		<span class="counter">
			<?php echo $this->pagination->getPagesCounter(); ?>
		</span>
	</div>
<?php endif; ?>
</div>