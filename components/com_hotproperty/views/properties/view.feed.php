<?php
/**
 * @version		$Id: view.feed.php 889 2009-10-28 22:50:14Z abernier $
 * @package		Hotproperty
 * @subpackage	View
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Properties View Feed
 *
 * @package		Hotproperty
 * @subpackage	View
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyViewProperties extends HotpropertyView
{
	/**
	 * Constructor
	 * 
	 * @access	public
	 * @param	array	Associative configuration array
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Properties',
			'name_singular' => 'Property'
		), $config));
	}
	
	/**
	 * Type's properties listing
	 * 
	 * @return void
	 */
	function displayDefault()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		/**
		 * Retreive datas
		 */
		
		$db			=& MosetsFactory::getDBO();
		$nullDate	= $db->getNullDate(); 
		$now		= JFactory::getDate();
		
		$rows = $this->get('data', null, array('all', array(
			'where'	=> array(
				'Property.approved' => 1,
				'Property.published' => 1,
				array(
					'OR' => array(
						'Property.publish_up' => $nullDate,
						'Property.publish_up <=' => $now->toMySQL()
					)
				),
				array(
					'OR' => array(
						'Property.publish_down' => $nullDate,
						'Property.publish_down >=' => $now->toMySQL()
					)
				)
			),
			'order' => array('Property.created' => 'desc'),
			'limit' => $mainframe->getCfg('feed_limit'),
			'contain' => array(
				'Agent' => array(),
				'Type' => array(),
				'Photo' => array(
					'limit' => 1,
					'order' => array('Photo.ordering' => 'ASC')
				)
			)
		)));
		$this->assignRef('rows', $rows);
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle(JText::_('Properties'));
		
		/**
		 * Output datas
		 */
		
		foreach ($rows as $property)
		{
			$item = new JFeedItem();
			
			$item->author		= $property->Agent->name;
			$item->category		= $property->Type->name;
			$item->date			= ($property->created ? date('r', strtotime($property->created)) : null);
			$item->description	= (@$property->Photo[0]->thumb ? MosetsHTML::_('hotproperty.image.photo', 'thumbnail', $property->Photo[0]->thumb) . '<br />' : '') . $property->intro_text . $property->full_text;
			$item->guid			= $property->id;
			$item->link			= JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'properties', 'layout' => 'property', 'id' => $property->id)));
			$item->pubDate		= ($property->publish_up ? date('r', strtotime($property->publish_up)) : null);
			$item->title 		= $this->escape($property->name);

			// loads item info into rss array
			$document =& JFactory::getDocument();
			$document->addItem($item);
		}
	}
}
?>