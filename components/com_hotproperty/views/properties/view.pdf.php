<?php
/**
 * @version		$Id: view.pdf.php 945 2010-02-01 10:39:52Z CY $
 * @package		Hotproperty
 * @subpackage	View
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Properties PDF View
 *
 * @package		Hotproperty
 * @subpackage	View
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyViewProperties extends HotpropertyView
{
	/**
	 * Constructor
	 * 
	 * @access	public
	 * @param	array	Associative configuration array
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Properties',
			'name_singular' => 'Property'
		), $config));
	}
	
	/**
	 * Display default layout
	 * 
	 * @return void
	 */
	function displayProperty()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');

		/**
		 * Retreive datas
		 */

		$db			=& MosetsFactory::getDBO();
		$nullDate	= $db->getNullDate(); 
		$now		= JFactory::getDate();
		
		$id = JRequest::getInt('id', null);

		$row = $this->get('data', null, array('first', array(
			'where'	=> array(
				'Property.id' => $id,
				'Property.approved' => 1,
				'Property.published' => 1,
				array(
					'OR' => array(
						'Property.publish_up' => $nullDate,
						'Property.publish_up <=' => $now->toMySQL()
					)
				),
				array(
					'OR' => array(
						'Property.publish_down' => $nullDate,
						'Property.publish_down >=' => $now->toMySQL()
					)
				)
			),
			'contain' => array(
				'PropertyField' => array(),
				'Photo' => array(
					'order' => array('Photo.ordering' => 'ASC')
				),
				'Agent' => array(
					'contain' => array(
						'Company' => array()
					)
				),
				'Type' => array()
			)
		)));
		
		// Make sure the requested property exists (and is published/approved)
		if (empty($row)) {
			JError::raiseError(404, sprintf(JText::_('%s #%s not found.'), JText::_('Property'), $id));
			return false;
		}
		
		$this->setModel($hotproperty->getModel('Field'));
		$fields = $this->get('data', 'Field', array('all', array(
			'where'		=> array(
				'Field.name NOT IN' => array('name', 'notes', 'full_text', 'modified', 'created'), // Disable 'name' and 'full_text'
				'Field.published' => 1,
				'Field.hidden' => 0
			),	
			'order'		=> array('Field.ordering' => 'ASC')
		)));
		
		/**
		 * Set Page title
		 */
		
		$document	=& JFactory::getDocument();
		$menus		=& JSite::getMenu();
		$menu		= $menus->getActive();
		
		// If the property we are displaying has an menu item, let's use its menu item 'page_title' param
		if (is_object($menu) && (isset($menu->query['view']) && $menu->query['view'] == MosetsInflector::underscore($this->getName())) && (isset($menu->query['id']) && $menu->query['id'] == $row->id)) {
			$menu_params = new JParameter($menu->params);
			if ($menu_params->get('page_title')) {
				$hotproperty->setCfg('page_title', $menu_params->get('page_title'));
			}
		// Otherwise, let's use the property's name
		} else {
			$hotproperty->setCfg('page_title', $row->name);
		}
		$document->setTitle($hotproperty->getCfg('page_title'));
		
		/**
		 * Set Meta tags (description, keywords, title, author)
		 */
		
		if ($row->metadesc) {
			$document->setDescription($row->metadesc);
		}
		if ($row->metakey) {
			$document->setMetadata('keywords', $row->metakey);
		}

		if ($mainframe->getCfg('MetaTitle') == '1') {
			$mainframe->addMetaTag('title', $row->name);
		}
		if ($mainframe->getCfg('MetaAuthor') == '1') {
			$mainframe->addMetaTag('author', $row->Agent->name);
		}
		
		$document->setName('hotproperty');
		$document->setHeader($this->_getHeaderText($row));
		
		/**
		 * PDF generation
		 */
		
		$pdf =& $document->_engine;
		
		$photo_margin_bottom	= $this->_px2mm(15);
		$thumb_margin_bottom	= $this->_px2mm(15/2);
		$details_offset			= array('x' => 0, 'y' => 0);
		$details_margin_left	= $this->_px2mm(15);
		
		// Set PDF Header and Footer fonts
		$lang = &JFactory::getLanguage();
		$font = $lang->getPdfFontName();
		$font = ($font) ? $font : 'freesans';

		$pdf->setRTL($lang->isRTL());

		$pdf->setHeaderFont(array($font, '', 10));
		$pdf->setFooterFont(array($font, '', 8));
		
		// Set PDF Metadata
		$pdf->SetCreator($document->getGenerator());
		$pdf->SetTitle($document->getTitle());
		$pdf->SetSubject($document->getDescription());
		$pdf->SetKeywords($document->getMetaData('keywords'));

		// Set PDF Header data
		$pdf->setHeaderData('', 0, $document->getTitle(), $document->getHeader());

		// Initialize PDF Document
		$pdf->AliasNbPages();
		$pdf->AddPage();
		
		// Photos
		if (@$row->Photo) {
			// Standard photo
			$photo = MosetsApplication::getPath('media_images_standard', 'hotproperty') . DS . $row->Photo[0]->standard;
			$photo_size = GetImageSize($photo);
			$w = $this->_px2mm($photo_size[0]);
			$h = $this->_px2mm($photo_size[1]);
			$x = ($pdf->getPageWidth() - $w) / 2;
			$y = $pdf->GetY();
			$pdf->Image($photo, $x, $y, $w, $h, '', '', 'N');
			$pdf->SetFillColor(246, 246, 246);
			$pdf->SetFont($font, 'B');
			// $w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0
			$pdf->MultiCell($w, 0, $row->Photo[0]->title, 0, 'C', 1, 1, $x);
			$pdf->SetFont($font, 'I');
			$pdf->MultiCell($w, 0, $row->Photo[0]->desc, 0, 'C', 1, 1, $x);
			
			$pdf->setY($pdf->getY() + $photo_margin_bottom);
			$details_offset['y'] = $pdf->getY();
			
			// Thumbnails
			$max_width = 0;
			for ($i = 1; $i < count($row->Photo); $i++)
			{
				$photo = MosetsApplication::getPath('media_images_thumbnail', 'hotproperty') . DS . $row->Photo[$i]->thumb;
				$photo_size = GetImageSize($photo);
				$w = $this->_px2mm($photo_size[0]);
				$max_width = max($w, $max_width);
				$h = $this->_px2mm($photo_size[1]);

				$pdf->Image($photo, $pdf->getX(), $pdf->getY(), $w, $h, '', '', 'N');
				$pdf->SetFillColor(246, 246, 246);
				$pdf->SetFont($font, 'B');
				$pdf->MultiCell($w, 0, $row->Photo[$i]->title, 0, 'C', 1);
				
				$pdf->setY($pdf->getY() + $thumb_margin_bottom);
			}
			$details_offset['x'] = $max_width;
		}
		
		// Fields
		$pdf->setPage(1);
		$pdf->setY($details_offset['y'] ? $details_offset['y'] : $pdf->getY());
		foreach ($fields as $field)
		{
			$field_value = $this->_getFieldValue($field, $row);
			
			if (!empty($field_value)) {
				$x = $pdf->lMargin + ($details_offset['x'] ? $details_offset['x'] + $details_margin_left : 0);
				$w = $pdf->getPageWidth() - $x - $pdf->rMargin;
				$pdf->writeHTMLCell($w, 0, $x, $pdf->getY(), '<strong>' . $field->caption . ':</strong> ' . $field_value, 0, 1);
			}
		}
	}
	
	function _getHeaderText(&$row)
	{
		$text = '';

		// Display Author name
		$text .= "\n";
		$text .= $row->Agent->name . ($row->Agent->email ? ' <' . $row->Agent->email . '>' : '') . ($row->Agent->mobile ? ' - ' . $row->Agent->mobile : '');

		// Display Created Date
		if (intval($row->created)) {
			$text .= "\n";
			$text .= MosetsHTML::_('date', $row->created, JText::_('DATE_FORMAT_LC2'));
		}
		
		// Display Modified Date
		if (intval($row->modified)) {
			$text .= " - ";
			$text .= JText::_('Last Updated') . ' ' . MosetsHTML::_('date', $row->modified, JText::_('DATE_FORMAT_LC2'));
		}
		
		return $text;
	}
	
	function _getFieldValue(&$field, &$row)
	{
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		$html = null;
		
		/**
		 * field's value
		 */

		$field_value = null;

		// Either from $row if iscore
		if ($field->iscore) {
			// Notes are private
			if ($field->name == 'notes') {
				//return null;
			}

			switch ($field->name)
			{
				case 'company':
					$field_value = @$row->Agent->Company->name;
					break;
				case 'agent':
					$field_value = @$row->Agent->name;
					break;
				case 'type':
					$field_value = @$row->Type->name;
					break;
				default:
					$field_value = @$row->{$field->name};
					break;
			}
		// Or from $row->PropertyField otherwise
		} else {
			$property2 = null;
			if (!empty($row->PropertyField)) {
				foreach ($row->PropertyField as $p2)
				{
					if ($p2->field == $field->id) {
						$property2 = $p2;
						break;
					}
				}
			}

			$field_value = @$property2->value;
		}

		// Set default value if empty
		if (empty($field_value)) {
			$field_value = @$field->default_value;
		}

		/**
		 * Render the field depending of its type
		 */

		if (!empty($field_value)) {
			switch ($field->field_type)
			{
				case 'text':
				case 'multitext':
					switch ($field->name)
					{
						case 'intro_text':
							if (strlen(trim($row->full_text)))
								$field_value .= $row->full_text;
						default:
							break;
					}
					$html .= $field->prefix_text . ' ' . $field_value . ' ' . $field->append_text;
					/*if ($field->field_type == 'multitext') {
						$html = '<p>' . $html . '</p>';
					}*/
					break;
				case 'link':
					$html .= $field->prefix_text . ' <a href="' . $field_value . '" target="' . ($hotproperty->getCfg('link_open_newwin') ? '_blank' : '_self') . '">' . $field_value . '</a> ' . $field->append_text;
					break;
				case 'selectlist':
				case 'radiobutton':
					switch ($field->name)
					{
						// Core 'type' field
						case 'type':
							$html .= '<a href="' . JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'types', 'layout' => 'properties', 'id' => $row->type))) . '">' . $field_value . '</a>';
							break;
						// Core 'agent' field
						case 'agent':
							$html .= '<a href="' . JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'agents', 'layout' => 'properties', 'id' => $row->agent))) . '">' . $field_value . '</a>';
							break;
						// Core 'company' field
						case 'company':
							$html .= '<a href="' . JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'companies', 'layout' => 'agents', 'id' => $row->Agent->Company->id))) . '">' . $field_value . '</a>';
							break;
						// Otherwise
						default:
							$html .= $field_value;
							break;
					}
					break;
				case 'selectmultiple':
				case 'checkbox':
					$html .= implode(', ', explode('|', $field_value));
					break;
				// Special cases (no field_type)
				default:
					switch ($field->name)
					{
						case 'price':
							$html .= $field->prefix_text . $hotproperty->getCfg('currency') . ' ' . number_format($field_value, $hotproperty->getCfg('dec_point'), $hotproperty->getCfg('dec_string'), ($hotproperty->getCfg('thousand_sep')) ? $hotproperty->getCfg('thousand_string') : '') . $field->append_text;
							break;
						/*case 'featured':
							$html .= ;
							break;*/
						case 'created':
						case 'modified':
							$html .= MosetsHTML::_('date',  $field_value,  JText::_('DATE_FORMAT_LC2'));
							break;
						default:
							$html .= $field_value;
							break;
					}
					break;
			}
		}
		
		return $html;
	}
	
	function _px2mm($px)
	{
		return $px * 25.4 / 72;
	}
}
?>