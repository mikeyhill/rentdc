<?php
/**
 * @version		$Id: view.html.php 945 2010-02-01 10:39:52Z CY $
 * @package		Hotproperty
 * @subpackage	View
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Properties View
 *
 * @package		Hotproperty
 * @subpackage	View
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyViewProperties extends HotpropertyView
{
	/**
	 * Constructor
	 * 
	 * @access	public
	 * @param	array	Associative configuration array
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Properties',
			'name_singular' => 'Property'
		), $config));
	}
	
	function displayDefault()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		// Set view ordering		
		$this->setOrdering($hotproperty->getCfg('ordering'), $hotproperty->getCfg('ordering_dir'));
		// Set view limit
		$this->setLimit($hotproperty->getCfg('limit'));
		
		/**
		 * Retreive datas
		 */
		
		$db			=& MosetsFactory::getDBO();
		$nullDate	= $db->getNullDate(); 
		$now		= JFactory::getDate();
		
		$rows = $this->get('data', null, array('all', array(
			'where'	=> array(
				'Property.approved' => 1,
				'Property.published' => 1,
				array(
					'OR' => array(
						'Property.publish_up' => $nullDate,
						'Property.publish_up <=' => $now->toMySQL()
					)
				),
				array(
					'OR' => array(
						'Property.publish_down' => $nullDate,
						'Property.publish_down >=' => $now->toMySQL()
					)
				)
			),
			'contain' => array(
				'Type' => array(),
				'Agent' => array(
					'contain' => array(
						'Company' => array()
					)
				),
				'PropertyField' => array(),
				'Photo' => array(
					'limit' => 1,
					'order' => array('Photo.ordering' => 'ASC')
				)
			),
			'order' => array($this->get('ordering') => $this->get('ordering_dir')),
			'limitstart' => $this->get('limitstart'),
			'limit' => $this->get('limit')
		)));
		$this->assignRef('rows', $rows);
		
		$this->setModel($hotproperty->getModel('Field'));
		$extrafields = $this->get('data', 'Field', array('all', array(
			'where' => array(
				'Field.name NOT IN' => array('name', 'full_text', !empty($id) ? 'type' : null), // Disable 'name', 'full_text' and 'type'
				'Field.published' => 1,
				'Field.hidden' => 0,
				'Field.listing' => 1
			),	
			'order' => array('Field.ordering' => 'ASC')
		)));
		$this->assignRef('extrafields', $extrafields);
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle(JText::_('Properties'));
		
		/**
		 * Set pathway
		 */
		
		$menus		=& JSite::getMenu();
		$menu		= $menus->getActive();
		
		if ($menu && $menu->query['view'] != MosetsInflector::underscore($this->getName())) {
			$pathway =& $mainframe->getPathway();
			
			switch ($menu->query['view'])
			{
				default:
					$pathway->addItem(JText::_('Properties'));
					break;
			}			
		}
		
		/**
		 * Syndication Feeds
		 */
		
		$this->addFeed('rss');
		$this->addFeed('atom');
	}
	
	function displayProperty()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		/**
		 * Retreive datas
		 */
		
		$db			=& MosetsFactory::getDBO();
		$nullDate	= $db->getNullDate(); 
		$now		= JFactory::getDate();
		$dispatcher	=& JDispatcher::getInstance();
		
		$id = JRequest::getInt('id', null);
		
		// Increase hits
		$model =& $this->getModel();
		$model->hit($id);
		
		$row = $this->get('data', null, array('first', array(
			'where'	=> array(
				'Property.id' => $id,
				'Property.approved' => 1,
				'Property.published' => 1,
				array(
					'OR' => array(
						'Property.publish_up' => $nullDate,
						'Property.publish_up <=' => $now->toMySQL()
					)
				),
				array(
					'OR' => array(
						'Property.publish_down' => $nullDate,
						'Property.publish_down >=' => $now->toMySQL()
					)
				)
			),
			'contain' => array(
				'PropertyField' => array(),
				'Photo' => array(
					'order' => array('Photo.ordering' => 'ASC')
				),
				'Agent' => array(
					'contain' => array(
						'Company' => array()
					)
				),
				'Type' => array()
			)
		)));
		$this->assignRef('row', $row);
		
		// Make sure the requested property exists (and is published/approved)
		if (empty($row)) {
			JError::raiseError(404, sprintf(JText::_('%s #%s not found.'), JText::_('Property'), $id));
			return false;
		}
		
		$this->setModel($hotproperty->getModel('Field'));
		$extrafields = $this->get('data', 'Field', array('all', array(
			'where'		=> array(
				'Field.name NOT IN' => array('name', 'full_text'), // Disable 'name' and 'full_text'
				'Field.published' => 1,
				'Field.hidden' => 0
			),	
			'order'		=> array('Field.ordering' => 'ASC')
		)));
		$this->assignRef('extrafields', $extrafields);
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle($row->name);

		/**
		 * Set Meta tags (description, keywords, title, author)
		 */
		
		$document =& JFactory::getDocument();
		
		if ($row->metadesc) {
			$document->setDescription($row->metadesc);
		}
		if ($row->metakey) {
			$document->setMetadata('keywords', $row->metakey);
		}

		if ($mainframe->getCfg('MetaTitle') == '1') {
			$mainframe->addMetaTag('title', $row->name);
		}
		if ($mainframe->getCfg('MetaAuthor') == '1') {
			$mainframe->addMetaTag('author', $row->Agent->name);
		}
		
		/**
		 * Set pathway
		 */
		
		$menus		=& JSite::getMenu();
		$menu		= $menus->getActive();
		
		if ($menu && $menu->query['view'] != MosetsInflector::underscore($this->getName())) {
			$pathway =& $mainframe->getPathway();
			
			switch ($menu->query['view'])
			{
				case 'types':
				case 'agents':
					$pathway->addItem($this->escape($row->name));
					break;
				case 'companies':
					$pathway->addItem($this->escape($row->Agent->name), MosetsRoute::getLink('hotproperty', array('view' => 'agents', 'layout' => 'properties', 'id' => $row->agent)));
					$pathway->addItem($this->escape($row->name));
					break;
				default:
					$pathway->addItem($this->escape($row->Type->name), MosetsRoute::getLink('hotproperty', array('view' => 'types', 'layout' => 'properties', 'id' => $row->type)));
					$pathway->addItem($this->escape($row->name));
					break;
			}			
		}
		
		// Process the property preparation plugins
		JPluginHelper::importPlugin('hotproperty');
		
		$row->event = new stdClass();
		$results = $dispatcher->trigger('onAfterDisplayTitle', array(&$row));
		$row->event->afterDisplayTitle = trim(implode("\n", $results));
	}
	
	/**
	 * Display form layout
	 * 
	 * @return void
	 */
	function displayForm()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		$db		= MosetsFactory::getDBO();
		$user	=& JFactory::getUser();
		
		/**
		 * Retrieve Property
		 */
		
		$ids = JRequest::getVar('id', array(), 'default', 'array');
		if (empty($ids)) {
			$row = $this->get('data', null, array('new'));
			$row = $row[0];
		} else {
			$row = $this->get('data', null, array('first', array(
				'where'	=> array('Property.' . $this->get('primaryKey', null) => $ids[0]),
				'contain' => array(
					'PropertyField' => array(),
					'Photo' => array(
						'order' => array('ordering' => 'ASC')
					),
					'Agent' => array(),
					'Type' => array()
				)
			)));
			
			// Make sure the requested property exists
			if (empty($row)) {
				JError::raiseError(404, sprintf(JText::_('%s #%s not found.'), JText::_('Property'), $id));
				return false;
			}
			
			// Make sure the current user own this property
			if ($user->get('id') != $row->Agent->user) {
				JError::raiseError(401, JText::_('ALERTNOTAUTH'));
				return false;
			}
		}
		$this->assignRef('row', $row);
		
		/**
		 * Retrieve Agent
		 */
		
		$this->setModel($hotproperty->getModel('Agent'));
		$agent = $this->get('data', 'Agent', array('first', array(
			'where' => array(
				'Agent.user' => $user->get('id')
			)
		)));
		$this->assignRef('agent', $agent);
		
		// Make sure the current user is member of Agent's group defined in configuration
		if (empty($agent) || $user->get('gid') < $hotproperty->getCfg('agent_groupid')) {
			JError::raiseError(401, JText::_('ALERTNOTAUTH'));
			return false;
		}
		
		/**
		 * Retrieve Extrafields
		 */
		
		$this->setModel($hotproperty->getModel('Field'));
		$extrafields = $this->get('data', 'Field', array('all', array(
			'where'		=> array(
				'Field.published' => 1,
				'Field.name <>' => 'full_text'	// Disable 'full_text'
			),
			'order'		=> array('Field.ordering' => 'ASC'),
			'recursive'	=> 0
		)));
		$this->assignRef('extrafields', $extrafields);
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle(empty($ids) ? JText::_('Add') : sprintf(JText::_('Edit %s'), $row->name));
		
		/**
		 * Set pathway
		 */
		
		$menus		=& JSite::getMenu();
		$menu		= $menus->getActive();
		
		if ($menu && $menu->query['view'] != MosetsInflector::underscore($this->getName())) {
			$pathway =& $mainframe->getPathway();
			
			switch ($menu->query['view'])
			{
				default:
					if (empty($ids)) {
						$pathway->addItem(JText::_('Add'));
					} else {
						$pathway->addItem($this->escape($row->Type->name), MosetsRoute::getLink('hotproperty', array('view' => 'types', 'layout' => 'properties', 'id' => $row->type)));
						$pathway->addItem($this->escape($row->name), MosetsRoute::getLink('hotproperty', array('view' => 'properties', 'layout' => 'property', 'id' => $row->id)));
						$pathway->addItem(JText::_('Edit'));
					}
					break;
			}			
		}
	}
	
	/**
	 * Display manage layout
	 * 
	 * @return void
	 */
	function displayManage()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		$this->setFilters(array(
			'search' => array(
				'Property.name' => ''
			),
			'list' => array(
				'Property.type'			=> '',
				'Property.published'	=> ''
			)
		));
		$this->setOrdering('Property.name');
		
		// Make sure the current user is allowed to manage properties
		$user =& JFactory::getUser();
		$this->setModel($hotproperty->getModel('Agent'));
		$agent = $this->get('data', 'Agent', array('first', array(
			'where' => array('Agent.user' => $user->get('id')),
			'contain' => array(
				'Company' => array()
			)
		)));
		$this->assignRef('agent', $agent);
		if (empty($agent) || $user->get('gid') < $hotproperty->getCfg('agent_groupid')) {
			JError::raiseError(401, JText::_('ALERTNOTAUTH'));
			return false;
		}
		
		$rows = $this->get('data', null, array('all', array(
			'where' => array_merge(
				$this->getFiltersConditions(),
				array(
					'Property.agent' => $agent->id,
					//'Property.approved >=' => ($agent->need_approval ? 1 : -1),
					'Property.published >=' => 0
				)
			),
			'contain' => array(
				'Type' => array(),
				'Photo' => array()
			),
			'order' => array('Property.approved' => 'ASC', $this->get('ordering') => $this->get('ordering_dir')),
			'limit' => $this->get('limit'),
			'limitstart' => $this->get('limitstart')
		)));
		$this->assignRef('rows', $rows);
		$this->set('total', $this->get('total', null));
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle(JText::_('Manage Properties'));
		
		/**
		 * Set pathway
		 */
		
		$menus		=& JSite::getMenu();
		$menu		= $menus->getActive();
		
		if ($menu && $menu->query['view'] != MosetsInflector::underscore($this->getName())) {
			$pathway =& $mainframe->getPathway();
			
			switch ($menu->query['view'])
			{
				default:
					$pathway->addItem(JText::_('Manage'));
					break;
			}			
		}
	}
}
?>
