<?php
/**
 * @version		$Id: view.html.php 945 2010-02-01 10:39:52Z CY $
 * @package		Hotproperty
 * @subpackage	View
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Agents View
 *
 * @package		Hotproperty
 * @subpackage	View
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyViewCompanies extends HotpropertyView
{
	/**
	 * Constructor
	 * 
	 * @access	public
	 * @param	array	Associative configuration array
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Companies',
			'name_singular' => 'Company'
		), $config));
	}
	
	function displayDefault()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		// Set view ordering		
		$this->setOrdering('Company.name', 'asc');
		// Set view limit
		$this->setLimit($hotproperty->getCfg('limit'));
		
		/**
		 * Retreive datas
		 */
		
		$db			=& MosetsFactory::getDBO();
		$nullDate	= $db->getNullDate(); 
		$now		= JFactory::getDate();
		
		$rows = $this->get('data', null, array('all', array(
			'contain' => array(
				'Agent' => array(
					'fields' => array('id'),
					'contain' => array(
						'Property' => array(
							'fields' => array('id'),
							'where'	=> array(
								'Property.approved' => 1,
								'Property.published' => 1,
								array(
									'OR' => array(
										'Property.publish_up' => $nullDate,
										'Property.publish_up <=' => $now->toMySQL()
									)
								),
								array(
									'OR' => array(
										'Property.publish_down' => $nullDate,
										'Property.publish_down >=' => $now->toMySQL()
									)
								)
							)
						)
					)
				)
			),
			'order' => array($this->get('ordering') => $this->get('ordering_dir')),
			'limitstart' => $this->get('limitstart'),
			'limit' => $this->get('limit')
		)));
		$this->assignRef('rows', $rows);
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle(JText::_('Companies'));
		
		/**
		 * Set pathway
		 */
		
		$menus		=& JSite::getMenu();
		$menu		= $menus->getActive();
		
		if ($menu && $menu->query['view'] != MosetsInflector::underscore($this->getName())) {
			$pathway =& $mainframe->getPathway();
			
			switch ($menu->query['view'])
			{
				default:
					$pathway->addItem(JText::_('Companies'));
					break;
			}
		}
	}
	
	/**
	 * Agents layout
	 * 
	 * @return void
	 */
	function displayAgents()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		// Set view limit
		$this->setLimit($hotproperty->getCfg('limit'));
		
		/**
		 * Retreive datas
		 */
		
		$id = JRequest::getInt('id');
		
		$row = $this->get('data', null, array('first', array(
			'where' => array('Company.id' => $id)
		)));
		$this->assignRef('row', $row);
		
		// Make sure the requested company exists
		if (empty($row)) {
			JError::raiseError(404, sprintf(JText::_('%s #%s not found.'), JText::_('Company'), $id));
			return false;
		}
		
		$db			=& MosetsFactory::getDBO();
		$nullDate	= $db->getNullDate(); 
		$now		= JFactory::getDate();
		
		$this->setModel($hotproperty->getModel('Agent'));
		$agents = $this->get('data', 'Agent', array('all', array(
			'where' => array(
				'Agent.company' => $id
			),
			'contain' => array(
				'Property' => array(
					'where'	=> array(
						'Property.approved' => 1,
						'Property.published' => 1,
						array(
							'OR' => array(
								'Property.publish_up' => $nullDate,
								'Property.publish_up <=' => $now->toMySQL()
							)
						),
						array(
							'OR' => array(
								'Property.publish_down' => $nullDate,
								'Property.publish_down >=' => $now->toMySQL()
							)
						)
					)
				)
			),
			'order' => array('Agent.name' => 'ASC'),
			'limitstart' => $this->get('limitstart'),
			'limit' => $this->get('limit')
		)));
		$this->total = $this->get('total', 'Agent');
		$this->assignRef('agents', $agents);
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle($row->name);
		
		/**
		 * Set pathway
		 */
		
		$menus		=& JSite::getMenu();
		$menu		= $menus->getActive();
		
		if ($menu && $menu->query['view'] != MosetsInflector::underscore($this->getName())) {
			$pathway =& $mainframe->getPathway();
			
			switch ($menu->query['view'])
			{
				default:
					$pathway->addItem(JText::_('Companies'), MosetsRoute::getLink('hotproperty', array('view' => 'companies')));
					$pathway->addItem($this->escape($row->name));
					break;
			}			
		}
	}
	
	/**
	 * Display default layout
	 * 
	 * @return void
	 */
	function displayProperties()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		// Set view limit
		$this->setLimit($hotproperty->getCfg('limit'));
		
		/**
		 * Retreive datas
		 */
		
		$id = JRequest::getInt('id');
		
		$db			=& MosetsFactory::getDBO();
		$nullDate	= $db->getNullDate(); 
		$now		= JFactory::getDate();
		
		$row = $this->get('data', null, array('first', array(
			'where' => array('Company.id' => $id),
			'contain' => array(
				'Agent' => array(
					'fields' => array('Agent.id')
				)
			)
		)));
		$this->assignRef('row', $row);
		
		// Make sure the requested company exists
		if (empty($row)) {
			JError::raiseError(404, sprintf(JText::_('%s #%s not found.'), JText::_('Company'), $id));
			return false;
		}
		
		$this->setModel($hotproperty->getModel('Property'));
		$properties = $this->get('data', 'Property', array('all', array(
			'where' => array(
				'Company.id' => $row->id,
				'Property.approved' => 1,
				'Property.published' => 1,
				array(
					'OR' => array(
						'Property.publish_up' => $nullDate,
						'Property.publish_up <=' => $now->toMySQL()
					)
				),
				array(
					'OR' => array(
						'Property.publish_down' => $nullDate,
						'Property.publish_down >=' => $now->toMySQL()
					)
				)
			),
			'contain' => array(
				'Type' => array(),
				'Agent' => array(
					'contain' => array(
						'Company' => array()
					)
				),
				'PropertyField' => array(),
				'Photo' => array(
					'limit' => 1,
					'order' => array('Photo.ordering' => 'ASC')
				)
			),
			'order' => array($this->get('ordering') => $this->get('ordering_dir')),
			'limitstart' => $this->get('limitstart'),
			'limit' => $this->get('limit')
		)));
		$this->assignRef('properties', $properties);
		$this->total = $this->get('total', 'Property');
		
		$this->setModel($hotproperty->getModel('Field'));
		$extrafields = $this->get('data', 'Field', array('all', array(
			'where' => array(
				'Field.name NOT IN' => array('name', 'full_text', 'company'), // Disable 'name', 'full_text' and 'company'
				'Field.published' => 1,
				'Field.hidden' => 0,
				'Field.listing' => 1
			),	
			'order' => array('Field.ordering' => 'ASC')
		)));
		$this->assignRef('extrafields', $extrafields);
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle(sprintf("%s %s", $row->name, JText::_('Properties')));
		
		/**
		 * Set pathway
		 */
		
		$menus		=& JSite::getMenu();
		$menu		= $menus->getActive();
		
		if ($menu && $menu->query['view'] != MosetsInflector::underscore($this->getName())) {
			$pathway =& $mainframe->getPathway();
			
			switch ($menu->query['view'])
			{
				default:
					$pathway->addItem(JText::_('Companies'), MosetsRoute::getLink('hotproperty', array('view' => 'companies')));
					$pathway->addItem($this->escape($row->name), MosetsRoute::getLink('hotproperty', array('view' => 'companies', 'layout' => 'agents', 'id' => $row->id)));
					$pathway->addItem(JText::_('Properties'));
					break;
			}			
		}
		
		/**
		 * Syndication Feeds
		 */
		
		$this->addFeed('rss');
		$this->addFeed('atom');
	}
	
	/**
	 * Display Contact layout
	 * 
	 * @return void
	 */
	function displayContact()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		$id = JRequest::getInt('id');
		
		$db	=& MosetsFactory::getDBO();
		
		$row = $this->get('data', null, array('first', array(
			'where' => array('Company.id' => $id)
		)));
		$this->assignRef('row', $row);
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle(sprintf(JText::_("Contact %s"), $row->name));
		
		/**
		 * Set pathway
		 */
		
		$menus		=& JSite::getMenu();
		$menu		= $menus->getActive();
		
		if ($menu && $menu->query['view'] != MosetsInflector::underscore($this->getName())) {
			$pathway =& $mainframe->getPathway();
			
			switch ($menu->query['view'])
			{
				default:
					$pathway->addItem(JText::_('Companies'), MosetsRoute::getLink('hotproperty', array('view' => 'companies')));
					$pathway->addItem($this->escape($row->name), MosetsRoute::getLink('hotproperty', array('view' => 'companies', 'layout' => 'agents', 'id' => $row->id)));
					$pathway->addItem(JText::_('Contact'));
					break;
			}			
		}
	}
}
?>