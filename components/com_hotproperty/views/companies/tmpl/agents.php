<?php
/**
 * @version		$Id: agents.php 945 2010-02-01 10:39:52Z CY $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Company listing Layout
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

$hotproperty =& MosetsApplication::getInstance('hotproperty');
?>

<div id="hotproperty" class="company vcard <?php echo $hotproperty->getCfg('pageclass_sfx'); ?>">
<?php if ($hotproperty->getCfg('show_page_title', 1)) : ?>
	<?php echo MosetsHTML::_('content.header', 1, '<span class="fn org">' . $this->escape($this->pageTitle) . '</span>', $hotproperty->getCfg('page_title')); ?>
<?php endif; ?>
	<?php
	$this->company = $this->row;
	echo $this->loadTemplate('info');
	?>
	<p class="contact"><?php echo MosetsHTML::_('link', MosetsRoute::getLink('hotproperty', array('view' => 'companies', 'layout' => 'contact', 'id' => $this->row->id, 'referer' => base64_encode(JRequest::getVar('REQUEST_URI', null, 'server', 'string')))), JText::_('Contact this Company')); ?></p>

<?php
$doWeNeedLimit = $this->pagination && $this->get('total') > 5 && $hotproperty->getCfg('use_displaynum');
if ($doWeNeedLimit) :
?>
	<div class="arrange">
		<div class="limit">
			<span class="label"><?php echo JText::_('Display #:'); ?></span>
			<?php echo $this->pagination->getLimitBox(); ?>
		</div>
	</div>
<?php endif; ?>
<?php if ($this->agents) : ?>
	<ul class="results agents">
	<?php foreach ($this->agents as $agent) : ?>
		<li class="agent vcard">
			<?php
			$this->agent = $agent;
			$this->headerLevel = 2;
			echo $this->loadTemplate('agent');
			?>
			<p class="contact"><?php echo MosetsHTML::_('link', MosetsRoute::getLink('hotproperty', array('view' => 'agents', 'layout' => 'contact', 'id' => $agent->id, 'referer' => base64_encode(JRequest::getVar('REQUEST_URI', null, 'server', 'string')))), JText::_('Contact this Agent'))?></p>
		</li>
	<?php endforeach; ?>
	</ul>
<?php endif; ?>

<?php if ($this->pagination->{'pages.total'} > 1) : ?>
	<div class="paging">
		<?php echo $this->pagination->getPagesLinks(); ?>
		<span class="counter">
			<?php echo $this->pagination->getPagesCounter(); ?>
		</span>
	</div>
<?php endif; ?>
</div>