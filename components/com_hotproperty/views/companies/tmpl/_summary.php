<?php
/**
 * @version		$Id$
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Company Summary Layout
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

$hotproperty =& MosetsApplication::getInstance('hotproperty');
?>

<?php echo MosetsHTML::_('content.header', $this->headerLevel,
	MosetsHTML::_('link', JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'companies', 'layout' => 'agents', 'id' => $this->company->id))),
		'<span class="thumbnail">' . MosetsHTML::_('hotproperty.image.photo', 'company', @$this->company->photo, JText::_("Company's photo"), array('class' => 'photo')) . '</span><span class="fn">' . $this->escape($this->company->name) . '</span>'
	),
	$this->company->name
); ?>
<?php if ($this->company->address || $this->company->suburb || $this->company->state || $this->company->postcode || $this->company->country || $this->company->telephone || $this->company->fax || $this->company->website || $this->company->desc) : ?>
<dl class="attributes">
<?php if ($this->company->address || $this->company->suburb || $this->company->state || $this->company->postcode || $this->company->country) : ?>
	<dd class="value address adr">			
	<?php if ($this->company->address) : ?>
		<span class="street-address"><?php echo $this->escape($this->company->address); ?></span><br />
	<?php endif; ?>
	<?php if ($this->company->suburb && $this->company->state && $this->company->postcode) : ?>
		<span class="locality"><?php echo $this->escape($this->company->suburb); ?></span>,
		<span class="region"><?php echo $this->escape($this->company->state); ?></span>,
		<span class="postal-code"><?php echo $this->escape($this->company->postcode); ?><span><br />
	<?php elseif ($this->company->suburb && $this->company->state) : ?>
		<span class="locality"><?php echo $this->escape($this->company->suburb); ?></span>,
		<span class="region"><?php echo $this->escape($this->company->state); ?></span><br />
	<?php elseif ($this->company->suburb && $this->company->postcode) : ?>
		<span class="locality"><?php echo $this->escape($this->company->suburb); ?></span>,
		<span class="postal-code"><?php echo $this->escape($this->company->postcode); ?></span><br />
	<?php elseif ($this->company->state && $this->company->postcode) : ?>
		<span class="region"><?php echo $this->escape($this->company->state); ?></span>,
		<span class="postal-code"><?php echo $this->escape($this->company->postcode); ?></span><br />
	<?php elseif ($this->company->suburb) : ?>
		<span class="locality"><?php echo $this->escape($this->company->suburb); ?></span><br />
	<?php elseif ($this->company->state) : ?>
		<span class="region"><?php echo $this->escape($this->company->state); ?></span><br />
	<?php elseif ($this->company->postcode) : ?>
		<span class="postal-code"><?php echo $this->escape($this->company->postcode); ?><br />
	<?php endif; ?>
	<?php if ($this->company->country) : ?>
		<span class="country-name"><?php echo $this->escape($this->company->country); ?></span>
	<?php endif; ?>
	</dd>
<?php endif; ?>
<?php if ($this->company->telephone) : ?>
	<dt class="caption telephone"><?php echo JText::_('Telephone'); ?></dt>
	<dd class="value telephone tel">
		<abbr class="type" title="work"></abbr>
		<span class="value"><?php echo $this->escape($this->company->telephone); ?></span>
	</dd>
<?php endif; ?>
<?php if ($this->company->fax) : ?>
	<dt class="caption fax"><?php echo JText::_('Fax'); ?></dt>
	<dd class="value fax tel">
		<abbr class="type" title="fax"></abbr>
		<span class="value"><?php echo $this->escape($this->company->fax); ?></span>
	</dd>
<?php endif; ?>
<?php if ($this->company->website) : ?>
	<dt class="caption website"><?php echo JText::_('Website'); ?></dt>
	<dd class="value website"><?php echo MosetsHTML::_('link', $this->company->website, $this->escape($this->company->website), array('class' => 'url')); ?></dd>
<?php endif; ?>
<?php if ($this->company->desc) : ?>
	<dd class="value description"><p class="note"><?php echo $this->company->desc; ?></p></dd>
<?php endif; ?>
<?php if (@$this->agents) : ?>
	<?php
	$properties_num = 0;
	foreach ($this->agents as $agent)
	{
		$properties_num += count(@$agent->Property);
	}
	?>
	<dt class="caption properties"><?php echo JText::_('Properties'); ?></dt>
	<dd class="value properties"><?php echo MosetsHTML::_('link', MosetsRoute::getLink('hotproperty', array('view' => 'companies', 'layout' => 'properties', 'id' => $this->company->id)), $properties_num); ?></dd>
<?php endif; ?>
<?php if (@$this->properties) : ?>
	<dt class="caption agents"><?php echo JText::_('Agents'); ?></dt>
	<dd class="value agents"><?php echo MosetsHTML::_('link', MosetsRoute::getLink('hotproperty', array('view' => 'companies', 'layout' => 'agents', 'id' => $this->company->id)), count($this->company->Agent)); ?></dd>
<?php endif; ?>
</dl>
<?php endif; ?>
