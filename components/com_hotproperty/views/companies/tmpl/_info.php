<?php
/**
 * @version		$Id: _info.php 945 2010-02-01 10:39:52Z CY $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * 
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

$hotproperty =& MosetsApplication::getInstance('hotproperty');
?>

<?php echo MosetsHTML::_('hotproperty.image.photo', 'company', @$this->company->photo, JText::_("Company's logo"), array('class' => 'logo')); ?>

<?php if ($this->row->address || $this->row->suburb || $this->row->state || $this->row->postcode || $this->row->country || $this->row->telephone || $this->row->fax || $this->row->website || $this->row->desc) : ?>
<dl class="attributes">
<?php if ($this->row->address || $this->row->suburb || $this->row->state || $this->row->postcode || $this->row->country) : ?>
	<dd class="value address adr">			
	<?php if ($this->row->address) : ?>
		<span class="street-address"><?php echo $this->escape($this->row->address); ?></span><br />
	<?php endif; ?>
	<?php if ($this->row->suburb && $this->row->state && $this->row->postcode) : ?>
		<span class="locality"><?php echo $this->escape($this->row->suburb); ?></span>,
		<span class="region"><?php echo $this->escape($this->row->state); ?></span>,
		<span class="postal-code"><?php echo $this->escape($this->row->postcode); ?><span><br />
	<?php elseif ($this->row->suburb && $this->row->state) : ?>
		<span class="locality"><?php echo $this->escape($this->row->suburb); ?></span>,
		<span class="region"><?php echo $this->escape($this->row->state); ?></span><br />
	<?php elseif ($this->row->suburb && $this->row->postcode) : ?>
		<span class="locality"><?php echo $this->escape($this->row->suburb); ?></span>,
		<span class="postal-code"><?php echo $this->escape($this->row->postcode); ?></span><br />
	<?php elseif ($this->row->state && $this->row->postcode) : ?>
		<span class="region"><?php echo $this->escape($this->row->state); ?></span>,
		<span class="postal-code"><?php echo $this->escape($this->row->postcode); ?></span><br />
	<?php elseif ($this->row->suburb) : ?>
		<span class="locality"><?php echo $this->escape($this->row->suburb); ?></span><br />
	<?php elseif ($this->row->state) : ?>
		<span class="region"><?php echo $this->escape($this->row->state); ?></span><br />
	<?php elseif ($this->row->postcode) : ?>
		<span class="postal-code"><?php echo $this->escape($this->row->postcode); ?><br />
	<?php endif; ?>
	<?php if ($this->row->country) : ?>
		<span class="country-name"><?php echo $this->escape($this->row->country); ?></span>
	<?php endif; ?>
	</dd>
<?php endif; ?>
<?php if ($this->row->telephone) : ?>
	<dt class="caption telephone"><?php echo JText::_('Telephone'); ?></dt>
	<dd class="value telephone tel">
		<abbr class="type" title="work"></abbr>
		<span class="value"><?php echo $this->escape($this->row->telephone); ?></span>
	</dd>
<?php endif; ?>
<?php if ($this->row->fax) : ?>
	<dt class="caption fax"><?php echo JText::_('Fax'); ?></dt>
	<dd class="value fax tel">
		<abbr class="type" title="fax"></abbr>
		<span class="value"><?php echo $this->escape($this->row->fax); ?></span>
	</dd>
<?php endif; ?>
<?php if ($this->row->website) : ?>
	<dt class="caption website"><?php echo JText::_('Website'); ?></dt>
	<dd class="value website"><?php echo MosetsHTML::_('link', $this->row->website, $this->escape($this->row->website), array('class' => 'url')); ?></dd>
<?php endif; ?>
<?php if ($this->row->desc) : ?>
	<dd class="value description"><p class="note"><?php echo $this->row->desc; ?></p></dd>
<?php endif; ?>
<?php if (@$this->agents) : ?>
	<?php
	$properties_num = 0;
	foreach ($this->agents as $agent)
	{
		$properties_num += count(@$agent->Property);
	}
	?>
	<dt class="caption properties"><?php echo JText::_('Properties'); ?></dt>
	<dd class="value properties"><?php echo MosetsHTML::_('link', MosetsRoute::getLink('hotproperty', array('view' => 'companies', 'layout' => 'properties', 'id' => $this->row->id)), $properties_num); ?></dd>
<?php endif; ?>
<?php if (@$this->properties) : ?>
	<dt class="caption agents"><?php echo JText::_('Agents'); ?></dt>
	<dd class="value agents"><?php echo MosetsHTML::_('link', MosetsRoute::getLink('hotproperty', array('view' => 'companies', 'layout' => 'agents', 'id' => $this->row->id)), count($this->row->Agent)); ?></dd>
<?php endif; ?>
</dl>
<?php endif; ?>