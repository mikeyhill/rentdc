<?php
/**
 * @version		$Id: agents_info.php 893 2009-10-29 01:36:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Company's info Layout
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

require(JPath::clean(MosetsApplication::getPath('views', 'hotproperty') . '/companies/tmpl/_info.php'));
?>