<?php
/**
 * @version		$Id$
 * @package		Hotproperty
 * @subpackage	View
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Featured View feed
 *
 * @package		Hotproperty
 * @subpackage	View
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyViewFeatured extends HotpropertyView
{
	/**
	 * Constructor
	 * 
	 * @access	public
	 * @param	array	Associative configuration array
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Featured',
			'name_singular' => 'Featured'
		), $config));
	}
	
	/**
	 * Display default layout
	 * 
	 * @return void
	 */
	function displayDefault()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		// Set view ordering		
		$this->setOrdering($hotproperty->getCfg('ordering'), $hotproperty->getCfg('ordering_dir'));
		// Set view limit
		$this->setLimit($hotproperty->getCfg('limit'));
		
		/**
		 * Retreive datas
		 */
		
		$db			=& MosetsFactory::getDBO();
		$nullDate	= $db->getNullDate(); 
		$now		= JFactory::getDate();
		
		$this->setModel($hotproperty->getModel('Property'));
		$properties = $this->get('data', 'Property', array('all', array(
			'where'	=> array(
				'Property.featured' => 1,
				'Property.approved' => 1,
				'Property.published' => 1,
				array(
					'OR' => array(
						'Property.publish_up' => $nullDate,
						'Property.publish_up <=' => $now->toMySQL()
					)
				),
				array(
					'OR' => array(
						'Property.publish_down' => $nullDate,
						'Property.publish_down >=' => $now->toMySQL()
					)
				)
			),
			'contain' => array(
				'Featured' => array(),
				'PropertyField' => array(),
				'Photo' => array(
					'order' => array('Photo.ordering' => 'ASC'),
					'limit' => 1
				),
				'Agent' => array(),
				'Type' => array()
			),
			'order' => array('Property.created' => 'desc'),
			'limit' => $mainframe->getCfg('feed_limit')
		)));
		$this->assignRef('properties', $properties);
		$this->total = $this->get('total', 'Property');
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle('Featured Properties');
		
		/**
		 * Output datas
		 */
		
		foreach ($properties as $property)
		{
			$item = new JFeedItem();
			
			$item->author		= $property->Agent->name;
			$item->category		= $property->Type->name;
			$item->date			= ($property->created ? date('r', strtotime($property->created)) : null);
			$item->description	= (@$property->Photo[0]->thumb ? MosetsHTML::_('hotproperty.image.photo', 'thumbnail', $property->Photo[0]->thumb) . '<br />' : '') . $property->intro_text . $property->full_text;
			$item->guid			= $property->id;
			$item->link			= JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'properties', 'layout' => 'property', 'id' => $property->id)));
			$item->pubDate		= ($property->publish_up ? date('r', strtotime($property->publish_up)) : null);
			$item->title 		= $this->escape($property->name);

			// loads item info into rss array
			$document =& JFactory::getDocument();
			$document->addItem($item);
		}
	}
}
?>