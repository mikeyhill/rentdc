<?php
/**
 * @version		$Id: default.php 884 2009-10-09 01:23:31Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Featured listing Layout
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

$hotproperty =& MosetsApplication::getInstance('hotproperty');
?>

<div id="hotproperty" class="type <?php echo $hotproperty->getCfg('pageclass_sfx'); ?>">
<?php if ($hotproperty->getCfg('show_page_title', 1)) : ?>
	<?php echo MosetsHTML::_('content.header', 1, $this->escape($this->pageTitle)); ?>
<?php endif; ?>

<?php
$doWeNeedLimit = $this->pagination && $this->get('total') > 5 && $hotproperty->getCfg('use_displaynum');
$doWeNeedOrdering = $this->get('total') > 1 && $hotproperty->getCfg('user_ordering');
if ($doWeNeedLimit || $doWeNeedOrdering) :
?>
	<div class="arrange">
	<?php if ($doWeNeedLimit) : ?>
		<div class="limit">
			<span class="label"><?php echo JText::_('Display #:'); ?></span>
			<?php echo $this->pagination->getLimitBox(); ?>
		</div>
	<?php endif; ?>
	<?php if ($doWeNeedOrdering) : ?>
		<div class="ordering">
			<span class="label"><?php echo JText::_('Order by:'); ?></span>
			<ul>
			<?php foreach ((array) $hotproperty->getCfg('user_ordering') as $ordering) : ?>
			<?php
			preg_match('/([a-z0-9_]+\.)?([a-z0-9_]+)/i', $ordering, $matches);
			$orderingText = ($matches[1] == 'Property.' ? $matches[2] : substr($matches[1], 0, -1));
			?>
				<li class="<?php echo $orderingText; ?>"><?php echo MosetsHTML::_('hotproperty.content.sort', JText::_($orderingText), $ordering, @$this->get('ordering_dir'), @$this->get('ordering')); ?></li>
			<?php endforeach; ?>
			</ul>
		</div>
	<?php endif; ?>
	</div>
<?php endif; ?>

<?php if ($this->properties) : ?>
	<ul class="results properties">
	<?php foreach ($this->properties as $property) : ?>
		<li class="property">
		<?php
		$this->property = $property;
		$this->headerLevel = 2;
		?>
			<?php echo $this->loadTemplate('property'); ?>
		</li>
	<?php endforeach; ?>
	</ul>
<?php else : ?>
	<p class="results properties empty"><?php echo JText::_('No result.'); ?></p>
<?php endif; ?>
	
<?php if ($this->pagination->{'pages.total'} > 1) : ?>
	<div class="paging">
		<?php echo $this->pagination->getPagesLinks(); ?>
		<span class="counter">
			<?php echo $this->pagination->getPagesCounter(); ?>
		</span>
	</div>
<?php endif; ?>
</div>