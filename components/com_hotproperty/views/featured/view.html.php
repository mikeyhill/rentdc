<?php
/**
 * @version		$Id: view.html.php 945 2010-02-01 10:39:52Z CY $
 * @package		Hotproperty
 * @subpackage	View
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Featured View
 *
 * @package		Hotproperty
 * @subpackage	View
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyViewFeatured extends HotpropertyView
{
	/**
	 * Constructor
	 * 
	 * @access	public
	 * @param	array	Associative configuration array
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Featured',
			'name_singular' => 'Featured'
		), $config));
	}
	
	/**
	 * Display default layout
	 * 
	 * @return void
	 */
	function displayDefault()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		// Set view ordering		
		$this->setOrdering($hotproperty->getCfg('ordering'), $hotproperty->getCfg('ordering_dir'));
		// Set view limit
		$this->setLimit($hotproperty->getCfg('limit'));
		
		/**
		 * Retreive datas
		 */
		
		$db			=& MosetsFactory::getDBO();
		$nullDate	= $db->getNullDate(); 
		$now		= JFactory::getDate();
		
		$this->setModel($hotproperty->getModel('Property'));
		$properties = $this->get('data', 'Property', array('all', array(
			'where'	=> array(
				'Property.featured' => 1,
				'Property.approved' => 1,
				'Property.published' => 1,
				array(
					'OR' => array(
						'Property.publish_up' => $nullDate,
						'Property.publish_up <=' => $now->toMySQL()
					)
				),
				array(
					'OR' => array(
						'Property.publish_down' => $nullDate,
						'Property.publish_down >=' => $now->toMySQL()
					)
				)
			),
			'contain' => array(
				'Featured' => array(),
				'PropertyField' => array(),
				'Photo' => array(
					'order' => array('Photo.ordering' => 'ASC')
				),
				'Agent' => array(
					'contain' => array(
						'Company' => array()
					)
				),
				'Type' => array()
			),
			'order' => array($this->get('ordering') => $this->get('ordering_dir')),
			'limitstart' => $this->get('limitstart'),
			'limit' => $this->get('limit')
		)));
		$this->assignRef('properties', $properties);
		$this->total = $this->get('total', 'Property');
		
		$this->setModel($hotproperty->getModel('Field'));
		$extrafields = $this->get('data', 'Field', array('all', array(
			'where' => array(
				'Field.name NOT IN' => array('name', 'full_text', 'featured'), // Disable 'name', 'full_text' and 'featured'
				'Field.published' => 1,
				'Field.hidden' => 0,
				'Field.featured' => 1
			),	
			'order' => array('Field.ordering' => 'ASC')
		)));
		$this->assignRef('extrafields', $extrafields);
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle('Featured Properties');
		
		/**
		 * Set pathway
		 */
		
		$menus		=& JSite::getMenu();
		$menu		= $menus->getActive();
		
		if ($menu && $menu->query['view'] != MosetsInflector::underscore($this->getName())) {
			$pathway =& $mainframe->getPathway();
			
			switch ($menu->query['view'])
			{
				default:
					$pathway->addItem(JText::_('Featured'));
					break;
			}			
		}
		
		/**
		 * Syndication Feeds
		 */
		
		$this->addFeed('rss');
		$this->addFeed('atom');
	}
}
?>