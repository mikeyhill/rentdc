<?php
/**
 * @version		$Id: view.html.php 946 2010-02-01 10:44:31Z CY $
 * @package		Hotproperty
 * @subpackage	View
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Agents View
 *
 * @package		Hotproperty
 * @subpackage	View
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyViewAgents extends HotpropertyView
{
	/**
	 * Constructor
	 * 
	 * @access	public
	 * @param	array	Associative configuration array
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Agents',
			'name_singular' => 'Agent'
		), $config));
	}
	
	/**
	 * Display default layout
	 * 
	 * @return void
	 */
	function displayDefault()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		// Set view ordering		
		$this->setOrdering('Agent.name', 'asc');
		// Set view limit
		$this->setLimit($hotproperty->getCfg('limit'));
		
		/**
		 * Retreive datas
		 */
		
		$db			=& MosetsFactory::getDBO();
		$nullDate	= $db->getNullDate(); 
		$now		= JFactory::getDate();
		
		$rows = $this->get('data', null, array('all', array(
			'contain' => array(
				'Property' => array(
					'fields' => array('id'),
					'where'	=> array(
						'Property.approved' => 1,
						'Property.published' => 1,
						array(
							'OR' => array(
								'Property.publish_up' => $nullDate,
								'Property.publish_up <=' => $now->toMySQL()
							)
						),
						array(
							'OR' => array(
								'Property.publish_down' => $nullDate,
								'Property.publish_down >=' => $now->toMySQL()
							)
						)
					)
				),
				'Company' => array()
			),
			'order' => array($this->get('ordering') => $this->get('ordering_dir')),
			'limitstart' => $this->get('limitstart'),
			'limit' => $this->get('limit')
		)));
		$this->assignRef('rows', $rows);
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle(JText::_('Agents'));
		
		/**
		 * Set pathway
		 */
		
		$menus		=& JSite::getMenu();
		$menu		= $menus->getActive();
		
		if ($menu && $menu->query['view'] != MosetsInflector::underscore($this->getName())) {
			$pathway =& $mainframe->getPathway();
			
			switch ($menu->query['view'])
			{
				default:
					$pathway->addItem(JText::_('Agents'));
					break;
			}
		}
	}
	
	/**
	 * Display default layout
	 * 
	 * @return void
	 */
	function displayProperties()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		// Set view ordering		
		$this->setOrdering($hotproperty->getCfg('ordering'), $hotproperty->getCfg('ordering_dir'));
		// Set view limit
		$this->setLimit($hotproperty->getCfg('limit'));
		
		/**
		 * Retreive datas
		 */
		
		$id = JRequest::getInt('id');
		
		$db			=& MosetsFactory::getDBO();
		$nullDate	= $db->getNullDate(); 
		$now		= JFactory::getDate();
		
		$row = $this->get('data', null, array('first', array(
			'where' => array('Agent.id' => $id),
			'contain' => array(
				'Company' => array()
			)
		)));
		$this->assignRef('row', $row);
		
		// Make sure the requested type exists
		if (empty($row)) {
			JError::raiseError(404, sprintf(JText::_('%s #%s not found.'), JText::_('Agent'), $id));
			return false;
		}
		
		$this->setModel($hotproperty->getModel('Property'));
		$properties = $this->get('data', 'Property', array('all', array(
			'where' => array(
				'Property.agent' => $id,
				'Property.approved' => 1,
				'Property.published' => 1,
				array(
					'OR' => array(
						'Property.publish_up' => $nullDate,
						'Property.publish_up <=' => $now->toMySQL()
					)
				),
				array(
					'OR' => array(
						'Property.publish_down' => $nullDate,
						'Property.publish_down >=' => $now->toMySQL()
					)
				)
			),
			'contain' => array(
				'Type' => array(),
				'PropertyField' => array(),
				'Photo' => array(
					'limit' => 1,
					'order' => array('Photo.ordering' => 'ASC')
				),
				'Agent' => array(
					'contain' => array(
						'Company' => array()
					)
				)
			),
			'order' => array($this->get('ordering') => $this->get('ordering_dir')),
			'limitstart' => $this->get('limitstart'),
			'limit' => $this->get('limit')
		)));
		$this->total = $this->get('total', 'Property');
		$this->assignRef('properties', $properties);
		
		$this->setModel($hotproperty->getModel('Field'));
		$extrafields = $this->get('data', 'Field', array('all', array(
			'where' => array(
				'Field.name NOT IN' => array('name', 'full_text', 'agent'), // Disable 'name', 'full_text' and 'agent'
				'Field.published' => 1,
				'Field.hidden' => 0,
				'Field.listing' => 1
			),	
			'order' => array('Field.ordering' => 'ASC')
		)));
		$this->assignRef('extrafields', $extrafields);
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle($row->name);
		
		/**
		 * Set pathway
		 */
		
		$menus		=& JSite::getMenu();
		$menu		= $menus->getActive();
		
		if ($menu && $menu->query['view'] != MosetsInflector::underscore($this->getName())) {
			$pathway =& $mainframe->getPathway();
			
			switch ($menu->query['view'])
			{
				default:
					$pathway->addItem(JText::_('Companies'), MosetsRoute::getLink('hotproperty', array('view' => 'companies')));
					$pathway->addItem($this->escape($row->Company->name), MosetsRoute::getLink('hotproperty', array('view' => 'companies', 'layout' => 'agents', 'id' => $row->company)));
					$pathway->addItem($this->escape($row->name));
					break;
			}			
		}
		
		/**
		 * Syndication Feeds
		 */
		
		$this->addFeed('rss');
		$this->addFeed('atom');
	}
	
	/**
	 * Display form layout
	 * 
	 * @return void
	 */
	function displayForm()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		/**
		 * Retreive datas
		 */
		
		$user =& JFactory::getUser();
		$row = $this->get('data', null, array('first', array(
			'where'	=> array('Agent.user' => $user->get('id')),
			'contain' => array(
				'Company' => array()
			)
		)));
		$this->assignRef('row', $row);
		
		// Make sure the current user is allowed to edit this property
		if (empty($row) || $user->get('gid') < $hotproperty->getCfg('agent_groupid')) {
			JError::raiseError(401, JText::_('ALERTNOTAUTH'));
			return false;
		}
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle(sprintf(JText::_('Edit %s'), $row->name));
		
		/**
		 * Set pathway
		 */
		
		$menus		=& JSite::getMenu();
		$menu		= $menus->getActive();
		
		if ($menu && $menu->query['view'] != MosetsInflector::underscore($this->getName())) {
			$pathway =& $mainframe->getPathway();
			
			switch ($menu->query['view'])
			{
				default:
					$pathway->addItem(JText::_('Profile'));
					break;
			}			
		}
	}
	
	/**
	 * Display Contact layout
	 * 
	 * @return void
	 */
	function displayContact()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		$id = JRequest::getInt('id');
		
		$db	=& MosetsFactory::getDBO();
		
		$row = $this->get('data', null, array('first', array(
			'where' => array('Agent.id' => $id),
			'contain' => array(
				'Company' => array()
			)
		)));
		$this->assignRef('row', $row);
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle(sprintf(JText::_("Contact %s"), $row->name));
		
		/**
		 * Set pathway
		 */
		
		$menus		=& JSite::getMenu();
		$menu		= $menus->getActive();
		
		if ($menu && $menu->query['view'] != MosetsInflector::underscore($this->getName())) {
			$pathway =& $mainframe->getPathway();
			
			switch ($menu->query['view'])
			{
				default:
					$pathway->addItem(JText::_('Companies'), MosetsRoute::getLink('hotproperty', array('view' => 'companies')));
					$pathway->addItem($this->escape($row->Company->name), MosetsRoute::getLink('hotproperty', array('view' => 'companies', 'layout' => 'agents', 'id' => $row->company)));
					$pathway->addItem($this->escape($row->name), MosetsRoute::getLink('hotproperty', array('view' => 'agents', 'layout' => 'properties', 'id' => $row->id)));
					$pathway->addItem(JText::_('Contact'));
					break;
			}			
		}
	}
}
?>