<?php
/**
 * @version		$Id: _summary.php 945 2010-02-01 10:39:52Z CY $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Agent Summary Layout
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

$hotproperty =& MosetsApplication::getInstance('hotproperty');
?>

<?php echo MosetsHTML::_('content.header', $this->headerLevel,
	MosetsHTML::_('link', JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'agents', 'layout' => 'properties', 'id' => $this->agent->id))),
		'<span class="thumbnail">' . MosetsHTML::_('hotproperty.image.photo', 'agent', @$this->agent->photo, JText::_("Agent's photo"), array('class' => 'photo')) . '</span><span class="fn">' . $this->escape($this->agent->name) . '</span>'
	),
	$this->agent->name
); ?>
<p class="buttons">
	<?php echo MosetsHTML::_('hotproperty.icon.edit', 'agent', $this->agent); ?>
</p>
<?php if (@$this->agent->Company || $this->agent->mobile || @$this->agent->Property || $this->agent->desc) : ?>
<dl class="attributes">
<?php if (@$this->agent->Company) : ?>
	<dt class="caption company"><?php echo JText::_('Company') ?></dt>
	<dd class="value company"><?php echo MosetsHTML::_('link', JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'companies', 'layout' => 'agents', 'id' => $this->agent->company))), $this->escape($this->agent->Company->name), array('class' => 'org')); ?></dd>
<?php endif; ?>
<?php if ($this->agent->mobile) : ?>
	<dt class="caption mobile"><?php echo JText::_('Mobile') ?></dt>
	<dd class="value mobile tel">
		<abbr class="type" title="work"></abbr>
		<?php echo $this->escape($this->agent->mobile); ?>
	</dd>
<?php endif; ?>
<?php if (@$this->agent->Property) : ?>
	<dt class="caption properties"><?php echo JText::_('Properties'); ?></dt>
	<dd class="value properties"><?php echo count($this->agent->Property); ?></dd>
<?php endif; ?>
<?php if ($this->agent->desc) : ?>
	<dd class="value description"><p class="note"><?php echo $this->agent->desc; ?></p></dd>
<?php endif; ?>
</dl>
<?php endif; ?>
