<?php
/**
 * @version		$Id: form.php 920 2009-11-15 19:56:40Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Agent Form Template
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

$hotproperty =& MosetsApplication::getInstance('hotproperty');
?>

<div id="hotproperty" class="agent form <?php echo $hotproperty->getCfg('pageclass_sfx'); ?>">
<?php if ($hotproperty->getCfg('show_page_title', 1)) : ?>
	<?php echo MosetsHTML::_('content.header', 1, $this->escape($this->pageTitle)); ?>
<?php endif; ?>

	<form action="<?php echo JRoute::_($this->get('action')); ?>" method="post" name="adminForm" class="form-validate" enctype="multipart/form-data">
		<p class="buttons">
			<?php echo MosetsHTML::_('form.input', 'submit', 'save', JText::_('Save')); ?>
			<?php echo MosetsHTML::_('link', JRoute::_(base64_decode($this->get('referer'))), JText::_('Cancel')); ?>
		</p>
		
		<fieldset>				
			<legend><?php echo JText::_('General'); ?></legend>
			<ol class="attributes">
				<li class="name">
					<?php echo MosetsHTML::_('form.label', JText::_('Name'), 'Agent[' . $this->row->id . '][name]', true); ?>
					<?php echo MosetsHTML::_('form.input', 'text', 'Agent[' . $this->row->id . '][name]', $this->row->name, false, array('class' => 'required', 'maxlength' => '255')); ?>
				</li>
				<li class="email">
					<?php echo MosetsHTML::_('form.label', JText::_('Email'), 'Agent[' . $this->row->id . '][email]', true); ?>
					<?php echo MosetsHTML::_('form.input', 'text', 'Agent[' . $this->row->id . '][email]', $this->row->email, false, array('class' => 'required validate-email')); ?>
				</li>
				<li class="mobile">
					<?php echo MosetsHTML::_('form.label', JText::_('Mobile number'), 'Agent[' . $this->row->id . '][mobile]'); ?>
					<?php echo MosetsHTML::_('form.input', 'text', 'Agent[' . $this->row->id . '][mobile]', $this->row->mobile); ?>
				</li>
				<li class="photo">
					<fieldset class="file">
						<legend><span class="hasTip" title="<?php echo JText::_('Photo'); ?>::<?php echo JText::_('TOOLTIP IMAGE'); ?>"><?php echo JText::_('Image'); ?></span></legend>
						<ol>
							<li>
								<?php echo MosetsHTML::_('form.label', JText::_('File'), 'Agent[' . $this->row->id . '][photo]'); ?>
								<?php echo MosetsHTML::_('form.input', 'file', 'Agent[' . $this->row->id . '][photo]', $this->row->photo); ?>
							</li>
						<?php if ($this->row->photo) : ?>
							<li class="preview">
								<?php echo MosetsHTML::_('hotproperty.image.photo', 'agent', $this->row->photo); ?>
								<?php echo MosetsHTML::_('form.label', MosetsHTML::_('form.checkbox', 'Agent[' . $this->row->id . '][removephoto]') . JText::_('Remove Photo')); ?>
							</li>
						<?php endif; ?>
						</ol>
					</fieldset>
				</li>
				<li class="description">
					<?php echo MosetsHTML::_('form.label', JText::_('Description'), 'Agent[' . $this->row->id . '][desc]'); ?>
					<?php echo MosetsHTML::_('form.wysiwyg', 'Agent[' . $this->row->id . '][desc]', $this->row->desc, $width = '100%', $height = '400', $col = '70', $row = '15', $buttons = true, $params = array('pagebreak', 'readmore')); ?>
				</li>
			</ol>
		</fieldset>

		<input type="hidden" name="option" value="com_hotproperty" />
		<input type="hidden" name="controller" value="agents" />
		<input type="hidden" name="task" value="save" />
		<?php echo MosetsHTML::_('form.token'); ?>
	</form>
</div>
