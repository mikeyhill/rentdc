<?php
/**
 * @version		$Id$
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Agents layout
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

$hotproperty =& MosetsApplication::getInstance('hotproperty');
?>

<div id="hotproperty" class="agents <?php echo $hotproperty->getCfg('pageclass_sfx'); ?>">
<?php if ($hotproperty->getCfg('show_page_title', 1)) : ?>
	<?php echo MosetsHTML::_('content.header', 1, $this->escape($this->pageTitle)); ?>
<?php endif; ?>

<?php
$doWeNeedLimit = $this->pagination && $this->get('total') > 5 && $hotproperty->getCfg('use_displaynum');
if ($doWeNeedLimit) :
?>
	<div class="arrange">
	<?php if ($doWeNeedLimit) : ?>
		<div class="limit">
			<span class="label"><?php echo JText::_('Display #:'); ?></span>
			<?php echo $this->pagination->getLimitBox(); ?>
		</div>
	<?php endif; ?>
	</div>
<?php endif; ?>

<?php if ($this->rows) : ?>
	<ul class="results agents">
	<?php foreach ($this->rows as $agent) : ?>
		<li class="agent vcard">
			<?php
			$this->agent = $agent;
			$this->headerLevel = 2;
			?>
			<?php echo $this->loadTemplate('summary'); ?>
		</li>
	<?php endforeach; ?>
	</ul>
<?php else : ?>
	<p class="results agents empty"><?php echo JText::_('No result.'); ?></p>
<?php endif; ?>
	
<?php if ($this->pagination->{'pages.total'} > 1) : ?>
	<div class="paging">
		<?php echo $this->pagination->getPagesLinks(); ?>
		<span class="counter">
			<?php echo $this->pagination->getPagesCounter(); ?>
		</span>
	</div>
<?php endif; ?>
</div>