<?php
/**
 * @version		$Id: properties_info.php 894 2009-10-29 02:07:15Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Agent's info Layout
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */
?>

<p class="buttons">
	<?php echo MosetsHTML::_('hotproperty.icon.edit', 'agent', $this->agent); ?>
</p>
<?php require(JPath::clean(MosetsApplication::getPath('views', 'hotproperty') . '/agents/tmpl/_info.php')); ?>