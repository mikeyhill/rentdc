<?php
/**
 * @version		$Id: view.html.php 945 2010-02-01 10:39:52Z CY $
 * @package		Hotproperty
 * @subpackage	View
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Types View
 *
 * @package		Hotproperty
 * @subpackage	View
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyViewTypes extends HotpropertyView
{
	/**
	 * Constructor
	 * 
	 * @access	public
	 * @param	array	Associative configuration array
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Types',
			'name_singular' => 'Type'
		), $config));
	}
	
	/**
	 * Types listing
	 * 
	 * @return void
	 */
	function displayDefault()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		// Set view ordering		
		$this->setOrdering('Type.name', 'asc');
		// Set view limit
		$this->setLimit($hotproperty->getCfg('limit'));
		
		/**
		 * Retreive datas
		 */
		
		$rows = $this->get('data', null, array('all', array(
			'where' => array(
				'Type.published' => 1
			),
			'contain' => array(
				'Property' => array(
					'fields' => array('id')
				)
			),
			'order' => array($this->get('ordering') => $this->get('ordering_dir')),
			'limitstart' => $this->get('limitstart'),
			'limit' => $this->get('limit')
		)));
		$this->assignRef('rows', $rows);
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle(JText::_('Types'));
		
		/**
		 * Set pathway
		 */
		
		$menus		=& JSite::getMenu();
		$menu		= $menus->getActive();
		
		if ($menu && $menu->query['view'] != MosetsInflector::underscore($this->getName())) {
			$pathway =& $mainframe->getPathway();
			
			switch ($menu->query['view'])
			{
				default:
					$pathway->addItem(JText::_('Types'));
					break;
			}			
		}
	}
	
	/**
	 * Type's properties listing
	 * 
	 * @return void
	 */
	function displayProperties()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		// Set view ordering		
		$this->setOrdering($hotproperty->getCfg('ordering'), $hotproperty->getCfg('ordering_dir'));
		// Set view limit
		$this->setLimit($hotproperty->getCfg('limit'));
		
		/**
		 * Retreive datas
		 */
		
		$row = $this->get('data', null, array('first', array(
			'where' => array('Type.id' => JRequest::getInt('id'))
		)));
		$this->assignRef('row', $row);
		
		// Make sure the requested type exists
		if (empty($row)) {
			JError::raiseError(404, sprintf(JText::_('%s #%s not found.'), JText::_('Type'), $id));
			return false;
		}
		
		$db			=& MosetsFactory::getDBO();
		$nullDate	= $db->getNullDate(); 
		$now		= JFactory::getDate();
		
		$this->setModel($hotproperty->getModel('Property'));
		$properties = $this->get('data', 'Property', array('all', array(
			'where' => array(
				'Property.type' => $row->id,
				'Property.approved' => 1,
				'Property.published' => 1,
				array(
					'OR' => array(
						'Property.publish_up' => $nullDate,
						'Property.publish_up <=' => $now->toMySQL()
					)
				),
				array(
					'OR' => array(
						'Property.publish_down' => $nullDate,
						'Property.publish_down >=' => $now->toMySQL()
					)
				)
			),
			'contain' => array(
				'Type' => array(),
				'Agent' => array(
					'contain' => array(
						'Company' => array()
					)
				),
				'PropertyField' => array(),
				'Photo' => array(
					'limit' => 1,
					'order' => array('Photo.ordering' => 'ASC')
				)
			),
			'order' => array($this->get('ordering') => $this->get('ordering_dir')),
			'limitstart' => $this->get('limitstart'),
			'limit' => $this->get('limit')
		)));
		$this->assignRef('properties', $properties);
		$this->total = $this->get('total', 'Property');
		
		$this->setModel($hotproperty->getModel('Field'));
		$extrafields = $this->get('data', 'Field', array('all', array(
			'where' => array(
				'Field.name NOT IN' => array('name', 'full_text', !empty($id) ? 'type' : null), // Disable 'name', 'full_text' and 'type'
				'Field.published' => 1,
				'Field.hidden' => 0,
				'Field.listing' => 1
			),	
			'order' => array('Field.ordering' => 'ASC')
		)));
		$this->assignRef('extrafields', $extrafields);
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle($row->name);
		
		/**
		 * Set pathway
		 */
		
		$menus		=& JSite::getMenu();
		$menu		= $menus->getActive();
		
		if ($menu && $menu->query['view'] != MosetsInflector::underscore($this->getName())) {
			$pathway =& $mainframe->getPathway();
			
			switch ($menu->query['view'])
			{
				default:
					$pathway->addItem($this->escape($row->name));
					break;
			}			
		}
		
		/**
		 * Syndication Feeds
		 */
		
		$this->addFeed('rss');
		$this->addFeed('atom');
	}
}
?>