<?php
/**
 * @version		$Id: view.feed.php 945 2010-02-01 10:39:52Z CY $
 * @package		Hotproperty
 * @subpackage	View
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Types View Feed
 *
 * @package		Hotproperty
 * @subpackage	View
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyViewTypes extends HotpropertyView
{
	/**
	 * Type's properties listing
	 * 
	 * @return void
	 */
	function displayProperties()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		/**
		 * Retreive datas
		 */
		
		$row = $this->get('data', null, array('first', array(
			'where' => array('Type.id' => JRequest::getInt('id'))
		)));
		$this->assignRef('row', $row);
		
		// Make sure the requested type exists
		if (empty($row)) {
			JError::raiseError(404, sprintf(JText::_('%s #%s not found.'), JText::_('Type'), $id));
			return false;
		}
		
		$db			=& MosetsFactory::getDBO();
		$nullDate	= $db->getNullDate(); 
		$now		= JFactory::getDate();
		
		$this->setModel($hotproperty->getModel('Property'));
		$properties = $this->get('data', 'Property', array('all', array(
			'where' => array(
				'Property.type' => $row->id,
				'Property.approved' => 1,
				'Property.published' => 1,
				array(
					'OR' => array(
						'Property.publish_up' => $nullDate,
						'Property.publish_up <=' => $now->toMySQL()
					)
				),
				array(
					'OR' => array(
						'Property.publish_down' => $nullDate,
						'Property.publish_down >=' => $now->toMySQL()
					)
				)
			),
			'order' => array('Property.created' => 'desc'),
			'limit' => $mainframe->getCfg('feed_limit'),
			'contain' => array(
				'Agent' => array(),
				'Photo' => array(
					'limit' => 1,
					'order' => array('Photo.ordering' => 'ASC')
				)
			)
		)));
		$this->assignRef('properties', $properties);
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle($row->name);
		
		/**
		 * Output datas
		 */
		
		if ($row->desc) {
			$document->description = $row->desc;
		}
		
		foreach ($properties as $property)
		{
			$item = new JFeedItem();
			
			$item->author		= $property->Agent->name;
			$item->category		= $property->Type->name;
			$item->date			= ($property->created ? date('r', strtotime($property->created)) : null);
			$item->description	= (@$property->Photo[0]->thumb ? MosetsHTML::_('hotproperty.image.photo', 'thumbnail', $property->Photo[0]->thumb) . '<br />' : '') . $property->intro_text . $property->full_text;
			$item->guid			= $property->id;
			$item->link			= JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'properties', 'layout' => 'property', 'id' => $property->id)));
			$item->pubDate		= ($property->publish_up ? date('r', strtotime($property->publish_up)) : null);
			$item->title 		= $this->escape($property->name);

			// loads item info into rss array
			$document =& JFactory::getDocument();
			$document->addItem($item);
		}
	}
}
?>