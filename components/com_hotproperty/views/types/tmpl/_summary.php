<?php
/**
 * @version		$Id$
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * 
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

$hotproperty =& MosetsApplication::getInstance('hotproperty');
?>

<?php echo MosetsHTML::_('content.header', $this->headerLevel,
	MosetsHTML::_('link', JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'types', 'layout' => 'properties', 'id' => $this->type->id))), $this->escape($this->type->name)),
	$this->type->name
); ?>

<p class="description"><?php echo $this->type->desc; ?></p>

<dl class="attributes">
	<dt class="caption"><?php echo JText::_('Properties'); ?></dt>
	<dd class="value"><?php echo isset($this->type->Property) ? count($this->type->Property) : 0; ?></dd>
</dl>