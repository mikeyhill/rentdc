<?php
/**
 * @version		$Id: default.php 954 2010-02-26 09:07:08Z cy $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Search Layout
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */
$hotproperty =& MosetsApplication::getInstance('hotproperty');
?>

<div id="hotproperty" class="search <?php echo $hotproperty->getCfg('pageclass_sfx'); ?> form">
<?php if ($hotproperty->getCfg('show_page_title', 1)) : ?>
	<?php echo MosetsHTML::_('content.header', 1, $this->escape($this->pageTitle)); ?>
<?php endif; ?>

	<form action="<?php echo JRoute::_('index.php?option=com_hotproperty'); ?>" method="post" name="adminForm">
		<p class="buttons">
			<div id="tbutton" style="text-align:center;">
			<?php echo MosetsHTML::_('form.input', 'submit', null, JText::_('Search'), false); ?>
			<?php JHTML::_('behavior.mootools'); echo MosetsHTML::_('form.input', 'reset', JText::_('Reset')); ?>
			</div>
		</p>
		<fieldset>
			<ol class="attributes">
			<?php $k=1; ?>
			<?php foreach ($this->extrafields as $extrafield) : ?>
			<?php 
				$k++;
				if($k%2 ==0){$ro = 'row0';}else{$ro='row1';}
			?>
				<li class="<?php echo $extrafield->name; echo " $ro"; ?>" >
					<?php //echo MosetsHTML::_('form.label', JText::_($extrafield->caption), 'Field[' . $extrafield->name . ']'); ?>
					<?php echo MosetsHTML::_('hotproperty.form.extrafield_search', $extrafield, @$this->search_fields[$extrafield->name], true); ?>
				</li>
			<?php endforeach; ?>
			</ol>
		</fieldset>
		<p class="buttons">
			<div id="bbutton" style="text-align:center;">
			<?php echo MosetsHTML::_('form.input', 'submit', '', JText::_('Search'), false); ?>
			<?php echo MosetsHTML::_('form.input', 'reset', '', JText::_('Reset'), false); ?>
			</div>
			<?php //JHTML::_('behavior.mootools'); echo MosetsHTML::_('anchor', 'reset', JText::_('Reset'), array('onclick' => '$$(\'[name^=Field]\').setProperty(\'value\', \'\').removeProperty(\'checked\');')); ?>
		</p>
		
		<input type="hidden" name="option" value="com_hotproperty" />
		<input type="hidden" name="controller" value="searches" />
		<input type="hidden" name="task" value="search" />
		<?php echo MosetsHTML::_('form.token'); ?>
		<script type="text/javascript">
		window.addEvent('domready',function(){
			$$('.button').each(function(e){
			e.removeClass('button')
		})
		});
		</script>
	</form>
</div>