<?php
/**
 * @version		$Id: view.html.php 949 2010-02-11 11:46:27Z CY $
 * @package		Hotproperty
 * @subpackage	View
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Searches View
 *
 * @package		Hotproperty
 * @subpackage	View
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyViewSearches extends HotpropertyView
{
	/**
	 * Constructor
	 * 
	 * @access	public
	 * @param	array	Associative configuration array
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Searches',
			'name_singular' => 'Search'
		), $config));
	}
	
	/**
	 * Display default layout
	 * 
	 * @return void
	 */
	function displayDefault()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		/**
		 * Retreive datas
		 */
		
		$previous_search_fields = $mainframe->getUserState('search.fields');
		if (!empty($previous_search_fields)) {
			$search_fields = unserialize($mainframe->getUserState('search.fields'));
			$this->assignRef('search_fields', $search_fields);
		}
		
		$db =& MosetsFactory::getDBO();
		
		$this->setModel($hotproperty->getModel('Field'));
		$extrafields = $this->get('data', 'Field', array('all', array(
			'where' => array(
				'Field.name NOT IN' => array('full_text'), // Disable 'name', 'full_text' and 'type'
				'Field.published' => 1,
				'Field.hidden' => 0,
				'Field.search' => 1
			),	
			'order' => array('Field.ordering' => 'ASC')
		)));
		$this->assignRef('extrafields', $extrafields);
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle(JText::_('Search'));
		
		/**
		 * Set pathway
		 */
		
		$menus		=& JSite::getMenu();
		$menu		= $menus->getActive();
		
		if ($menu && $menu->query['view'] != MosetsInflector::underscore($this->getName())) {
			$pathway =& $mainframe->getPathway();
			
			switch ($menu->query['view'])
			{
				default:
					$pathway->addItem($this->escape(JText::_('Search')));
					break;
			}			
		}
	}
	
	/**
	 * Display search layout
	 * 
	 * @return void
	 */
	function displayResults()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		// Set view ordering		
		$this->setOrdering($hotproperty->getCfg('ordering'), $hotproperty->getCfg('ordering_dir'));
		// Set view limit
		$this->setLimit($hotproperty->getCfg('limit'));
		
		/**
		 * Retreive datas
		 */
		
		$search_fields = JRequest::getVar('Field', array(), 'get', 'array');
		
		if (empty($search_fields)) {
			$mainframe->redirect(JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'searches')), false), JText::_('Please complete the form before submitting.'));
		}
		
		$this->setModel($hotproperty->getModel('Property'));
		$model =& $this->getModel('Property');
		if ($results_ids = $model->search($search_fields)) {
			$db			=& MosetsFactory::getDBO();
			$nullDate	= $db->getNullDate(); 
			$now		= JFactory::getDate();

			$properties = $this->get('data', 'Property', array('all', array(
				'where' => array(
					'Property.id' => $results_ids,
					'Property.approved' => 1,
					'Property.published' => 1,
					array(
						'OR' => array(
							'Property.publish_up' => $nullDate,
							'Property.publish_up <=' => $now->toMySQL()
						)
					),
					array(
						'OR' => array(
							'Property.publish_down' => $nullDate,
							'Property.publish_down >=' => $now->toMySQL()
						)
					)
				),
				'contain' => array(
					'Type' => array(),
					'Agent' => array(
						'contain' => array(
							'Company' => array()
						)
					),
					'PropertyField' => array(),
					'Photo' => array(
						'limit' => 1,
						'order' => array('Photo.ordering' => 'ASC')
					)
				),
				'order' => array($this->get('ordering') => $this->get('ordering_dir')),
				'limitstart' => $this->get('limitstart'),
				'limit' => $this->get('limit')
			)));
			$this->assignRef('properties', $properties);
			$this->set('total', $this->get('total', 'Property'));

			$this->setModel($hotproperty->getModel('Field'));
			$extrafields = $this->get('data', 'Field', array('all', array(
				'where' => array(
					'Field.name NOT IN' => array('name', 'full_text'), // Disable 'name', 'full_text'
					'Field.published' => 1,
					'Field.hidden' => 0,
					'Field.listing' => 1
				),	
				'order' => array('Field.ordering' => 'ASC')
			)));
			$this->assignRef('extrafields', $extrafields);
		} else {
			$this->assign('properties', null);
			$this->set('total', 0);
		}
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle(JText::_('Search Results'));
		
		/**
		 * Set pathway
		 */
		
		$menus		=& JSite::getMenu();
		$menu		= $menus->getActive();
		
		if ($menu && $menu->query['view'] != MosetsInflector::underscore($this->getName())) {
			$pathway =& $mainframe->getPathway();
			
			switch ($menu->query['view'])
			{
				default:
					$pathway->addItem(JText::_('Search'), MosetsRoute::getLink('hotproperty', array('view' => 'searches')));
					$pathway->addItem(JText::_('Results'));
					break;
			}			
		}
		
		/**
		 * Syndication Feeds
		 */
		
		$this->addFeed('rss');
		$this->addFeed('atom');
	}
}
?>