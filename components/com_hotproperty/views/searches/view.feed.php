<?php
/**
 * @version		$Id: view.feed.php 945 2010-02-01 10:39:52Z CY $
 * @package		Hotproperty
 * @subpackage	View
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Searches View
 *
 * @package		Hotproperty
 * @subpackage	View
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyViewSearches extends HotpropertyView
{
	/**
	 * Constructor
	 * 
	 * @access	public
	 * @param	array	Associative configuration array
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Searches',
			'name_singular' => 'Search'
		), $config));
	}
	
	/**
	 * Display search layout
	 * 
	 * @return void
	 */
	function displayResults()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		/**
		 * Retreive datas
		 */
		
		$search_fields = JRequest::getVar('Field', array(), 'get', 'array');
		
		if (empty($search_fields)) {
			$mainframe->redirect(JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'searches')), false), JText::_('Please complete the form before submitting.'));
		}
		
		$this->setModel($hotproperty->getModel('Property'));
		$model =& $this->getModel('Property');
		if ($results_ids = $model->search($search_fields)) {
			$db			=& MosetsFactory::getDBO();
			$nullDate	= $db->getNullDate(); 
			$now		= JFactory::getDate();

			$properties = $this->get('data', 'Property', array('all', array(
				'where' => array(
					'Property.id' => $results_ids,
					'Property.approved' => 1,
					'Property.published' => 1,
					array(
						'OR' => array(
							'Property.publish_up' => $nullDate,
							'Property.publish_up <=' => $now->toMySQL()
						)
					),
					array(
						'OR' => array(
							'Property.publish_down' => $nullDate,
							'Property.publish_down >=' => $now->toMySQL()
						)
					)
				),
				'order' => array('Property.modified' => 'desc'),
				'limit' => $mainframe->getCfg('feed_limit'),
				'contain' => array(
					'Agent' => array(),
					'Photo' => array(
						'limit' => 1,
						'order' => array('Photo.ordering' => 'ASC')
					)
				)
			)));
			$this->assignRef('properties', $properties);
			$this->set('total', $this->get('total', 'Property'));
		} else {
			$this->assign('properties', null);
			$this->set('total', 0);
		}
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle(JText::_('Search Results'));
		
		/**
		 * Output datas
		 */
		
		$params		=& $mainframe->getParams();
		
		foreach ($properties as $property)
		{
			$item = new JFeedItem();
			
			$item->author		= $property->Agent->name;
			$item->category		= $property->Type->name;
			$item->date			= ($property->created ? date('r', strtotime($property->created)) : null);
			$item->description	= (@$property->Photo[0]->thumb ? MosetsHTML::_('hotproperty.image.photo', 'thumbnail', $property->Photo[0]->thumb) . '<br />' : '') . $property->intro_text . $property->full_text;
			$item->guid			= $property->id;
			$item->link			= JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'properties', 'layout' => 'property', 'id' => $property->id)));
			$item->pubDate		= ($property->publish_up ? date('r', strtotime($property->publish_up)) : null);
			$item->title 		= $this->escape($property->name);

			// loads item info into rss array
			$document	=& JFactory::getDocument();
			$document->addItem($item);
		}
	}
}
?>