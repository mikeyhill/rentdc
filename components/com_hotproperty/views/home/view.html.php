<?php
/**
 * @version		$Id: view.html.php 885 2009-10-09 01:51:43Z abernier $
 * @package		Hotproperty
 * @subpackage	View
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Home View
 *
 * @package		Hotproperty
 * @subpackage	View
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyViewHome extends HotpropertyView
{
	/**
	 * Constructor
	 * 
	 * @access	public
	 * @param	array	Associative configuration array
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Home',
			'name_singular' => 'Home'
		), $config));
	}
	
	/**
	 * Display default layout
	 * 
	 * @return void
	 */
	function displayDefault()
	{
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		/**
		 * Retreive datas
		 */
		
		$db			=& MosetsFactory::getDBO();
		$nullDate	= $db->getNullDate(); 
		$now		= JFactory::getDate();
		
		if ($hotproperty->getCfg('fp_show_featured')) {
			$this->setModel($hotproperty->getModel('Property'));
			$featured = $this->get('data', 'Property', array('all', array(
				'where' => array(
					'Property.featured' => 1,
					'Property.approved' => 1,
					'Property.published' => 1,
					array(
						'OR' => array(
							'Property.publish_up' => $nullDate,
							'Property.publish_up <=' => $now->toMySQL()
						)
					),
					array(
						'OR' => array(
							'Property.publish_down' => $nullDate,
							'Property.publish_down >='  => $now->toMySQL()
						)
					)
				),
				'contain' => array(
					'Featured' => array(),
					'Agent' => array(
						'contain' => array(
							'Company' => array()
						)
					),
					'Type' => array(),
					'PropertyField' => array(),
					'Photo' => array(
						'limit' => 1,
						'order' => array('Photo.ordering' => 'ASC')
					)
				),
				'order' => array('Featured.ordering' => 'ASC'),
				'limit' => $hotproperty->getCfg('fp_featured_count')
			)));
			$this->assignRef('featured', $featured);
			
			$this->setModel($hotproperty->getModel('Field'));
			$extrafields = $this->get('data', 'Field', array('all', array(
				'where' => array(
					'Field.name NOT IN'  => array('name', 'full_text'), // Disable 'name', 'full_text'
					'Field.published' => 1,
					'Field.hidden' => 0,
					'Field.featured' => 1
				),	
				'order' => array('Field.ordering' => 'ASC')
			)));
			$this->assignRef('extrafields', $extrafields);
		}
		
		$this->setModel($hotproperty->getModel('Type'));
		$types = $this->get('data', 'Type', array('all', array(
		    'where'		=> array('Type.published' => 1),
			'order'		=> array('Type.ordering' => 'ASC'),
			'contain'	=> array(
				'Property' => array(
				    'fields' => array('Property.id', 'Property.name', 'Property.featured'),
					'where' => array(
						'Property.approved' => 1,
						'Property.published' => 1,
						array(
							'OR' => array(
								'Property.publish_up' => $nullDate,
								'Property.publish_up <=' => $now->toMySQL()
							)
						),
						array(
							'OR' => array(
								'Property.publish_down' => $nullDate,
								'Property.publish_down >=' => $now->toMySQL()
							)
						)
					),
					'order' => array('Property.hits' => 'DESC')
				)
			)
		)));
		$this->assignRef('types', $types);
		
		/**
		 * Set Page title
		 */
		
		$this->setPageTitle(JText::_("Hot Property"));
		
		/**
		 * Syndication Feeds
		 */
		
		$this->addFeed('rss');
		$this->addFeed('atom');
	}
}
?>
