<?php
/**
 * @version		$Id: default.php 941 2009-12-19 20:16:24Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Home Layout
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

$hotproperty =& MosetsApplication::getInstance('hotproperty');
?>

<div id="hotproperty" class="home <?php echo $hotproperty->getCfg('pageclass_sfx'); ?>">
<?php if ($hotproperty->getCfg('show_page_title', 1)) : ?>
	<?php echo MosetsHTML::_('content.header', 1, $this->escape($this->pageTitle), false, null, array('class' => 'componentheading')); ?>
<?php endif; ?>

<?php if (count(@$this->featured)) : ?>
	<div class="featured">
		<?php echo MosetsHTML::_('content.header', 2, JText::_('Featured'), false, null, array('class' => 'sectiontableheader')); ?>
		<ul class="results featured">
		<?php foreach ($this->featured as $featured) : ?>
			<li class="property">
			<?php
			$this->property = $featured;
			$this->headerLevel = 3;
			?>
				<?php echo $this->loadTemplate('featured'); ?>
			</li>
		<?php endforeach; ?>
		</ul>
		<p class="more"><?php echo MosetsHTML::_('link', JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'featured'))), JText::_('More Featured Properties...'), array('class' => 'readon')); ?></p>
	</div>
<?php endif; ?>

<?php if ($hotproperty->getCfg('fp_show_search')) : ?>
	<div class="search">
		<?php echo MosetsHTML::_('content.header', 2, JText::_('Search'), false, null, array('class' => 'sectiontableheader')); ?>
		<form action="<?php echo JRoute::_($this->get('action')); ?>" method="post">
			<?php echo MosetsHTML::_('form.input', 'text', 'Field[name]', '', false, array('class' => 'required')); ?>
			<?php echo MosetsHTML::_('form.input', 'submit', 'search', JText::_('Search'), false, array('onclick' => "submitbutton('search');")); ?>
	<?php if ($hotproperty->getCfg('use_advsearch')) : ?>
			<p><?php echo MosetsHTML::_('link', JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'searches'))), JText::_('Advanced Search')); ?></p>
	<?php endif; ?>
		
			<input type="hidden" name="option" value="com_hotproperty" />
			<input type="hidden" name="controller" value="searches" />
			<input type="hidden" name="task" value="search" />
			<?php echo MosetsHTML::_('form.token'); ?>
		</form>
	</div>
<?php endif; ?>

<?php if (@$this->types) : ?>
	<div class="types">
		<?php echo MosetsHTML::_('content.header', 2, JText::_('Types'), false, null, array('class' => 'sectiontableheader')); ?>
		<ul class="types">
		<?php foreach ($this->types as $type) : ?>
			<?php if (@$type->Property) : ?>
				<li class="type">
					<?php echo MosetsHTML::_('content.header', 3, MosetsHTML::_('link', JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'types', 'layout' => 'properties', 'id' => $type->id))), $this->escape($type->name) . ($hotproperty->getCfg('fp_show_type_count') ? '<span class="counter">(' . count($type->Property) . ')</span>' : '')), $type->name); ?>
				<?php if ($type->desc) : ?>
					<p class="description"><?php echo $type->desc; ?></p>
				<?php endif; ?>
					<ul class="properties">
					<?php for ($i = 0; $i<3; $i++) : ?>
						<?php if (@$type->Property[$i]) : ?>
						<li class="property <?php echo $type->Property[$i]->featured ? 'featured' : ''; ?>">
							<?php echo MosetsHTML::_('link', JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'properties', 'layout' => 'property', 'id' => $type->Property[$i]->id))), $this->escape($type->Property[$i]->name)); ?>
						</li>
						<?php endif; ?>
					<?php endfor; ?>
					</ul>
					<?php if (@$type->Property[3]) : ?>
						<p class="more"><?php echo MosetsHTML::_('link', JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'types', 'layout' => 'properties', 'id' => $type->id))), JText::_('More...'), array('class' => 'readon')); ?></p>
					<?php endif; ?>
				</li>
			<?php endif; ?>
		<?php endforeach; ?>
		</ul>
		<p class="more"><?php echo MosetsHTML::_('link', JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'properties'))), JText::_('All Properties...'), array('class' => 'readon')); ?></p>
	</div>
<?php endif; ?>
</div>