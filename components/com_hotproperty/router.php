<?php
/**
 * @version		$Id$
 * @package		Hotproperty
 * @subpackage	Controller
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

define('PROPERTIES',	'properties');
define('FEATURED',		'featured');
define('TYPES',			'types');
define('COMPANIES',		'companies');
define('AGENTS',		'agents');
define('CONTACT',		'contact');
define('ADD',			'add');
define('EDIT',			'edit');
define('MANAGE',		'manage');
define('PROFILE',		'profile');
define('SEARCH',		'search');
define('APPEND_ID',		true);

// Import Mosets Framework
if (JPluginHelper::isEnabled('mosets', 'framework')) {
	JPluginHelper::importPlugin('mosets', 'framework');
} else {
	JError::raiseError(404, 'Mosets Framework plugin is required for this component. Please install and enable it.');
}

global $mainframe;
$mainframe->triggerEvent('onInitializeMosetsFramework');

function HotpropertyBuildRoute(&$query)
{
	$segments = array();

	switch (@$query['view'])
	{
		case 'properties':
			switch(@$query['layout'])
			{
				// Detail of a property: {type}/{property}
				case 'property':
					$hotproperty =& MosetsFactory::getApplication('hotproperty');
					$model =& $hotproperty->getModel('Property');
					$property = $model->getData('first', array(
						'where' => array('Property.id' => $query['id']),
						'contain' => array('Type' => array())
					));
					$segments[] = (APPEND_ID ? $property->Type->id . '_' . JFilterOutput::stringURLSafe($property->Type->name) : urlencode($property->Type->name));
					$segments[] = (APPEND_ID ? $property->id . '_' . JFilterOutput::stringURLSafe($property->name) : urlencode($property->name));
					break;
				// Edit or Add a property: /add or /{type}/{property}/edit
				case 'form':
					if (isset($query['id'])) {
						$hotproperty =& MosetsFactory::getApplication('hotproperty');
						$model =& $hotproperty->getModel('Property');
						$property = $model->getData('first', array(
							'where' => array('Property.id' => $query['id']),
							'contain' => array('Type' => array())
						));
						$segments[] = (APPEND_ID ? $property->Type->id . '_' . JFilterOutput::stringURLSafe($property->Type->name) : urlencode($property->Type->name));
						$segments[] = (APPEND_ID ? $property->id . '_' . JFilterOutput::stringURLSafe($property->name) : urlencode($property->name));
						$segments[] = urlencode(EDIT);
					} else {
						$segments[] = urlencode(ADD);
					}
					break;
				// Mangage properties for the logged agent: /manage
				case 'manage':
					$segments[] = urlencode(MANAGE);
					break;
				// List all properties: /properties
				default:
					$segments[] = urlencode(PROPERTIES);
					break;
			}
			break;
		// List all featured properties: /properties/featured
		case 'featured':
			$segments[] = urlencode(PROPERTIES);
			$segments[] = urlencode(FEATURED);
			break;
		case 'types':
			switch(@$query['layout'])
			{
				// List all properties of this type: /{type}
				case 'properties':
					$hotproperty =& MosetsFactory::getApplication('hotproperty');
					$model =& $hotproperty->getModel('Type');
					$type = $model->getData('first', array('where' => array('Type.id' => $query['id'])));
					$segments[] = (APPEND_ID ? $type->id . '_' . JFilterOutput::stringURLSafe($type->name) : urlencode($type->name));
					break;
				// List all types: /types
				default:
					$segments[] = urlencode(TYPES);
					break;
			}
			break;
		case 'companies':
			switch(@$query['layout'])
			{
				// List all agents of this company: /companies/{company}
				case 'agents':
					$hotproperty =& MosetsFactory::getApplication('hotproperty');
					$model =& $hotproperty->getModel('Company');
					$company = $model->getData('first', array('where' => array('Company.id' => $query['id'])));
					$segments[] = urlencode(COMPANIES);
					$segments[] = (APPEND_ID ? $company->id . '_' . JFilterOutput::stringURLSafe($company->name) : urlencode($company->name));
					break;
				// List all properties of this companies: /companies/{company}/properties
				case 'properties':
					$hotproperty =& MosetsFactory::getApplication('hotproperty');
					$model =& $hotproperty->getModel('Company');
					$company = $model->getData('first', array('where' => array('Company.id' => $query['id'])));
					$segments[] = urlencode(COMPANIES);
					$segments[] = (APPEND_ID ? $company->id . '_' . JFilterOutput::stringURLSafe($company->name) : urlencode($company->name));
					$segments[] = urlencode(PROPERTIES);
					break;
				// Contact this company: /companies/{company}/contact
				case 'contact':
					$hotproperty =& MosetsFactory::getApplication('hotproperty');
					$model =& $hotproperty->getModel('Company');
					$company = $model->getData('first', array('where' => array('Company.id' => $query['id'])));
					$segments[] = urlencode(COMPANIES);
					$segments[] = (APPEND_ID ? $company->id . '_' . JFilterOutput::stringURLSafe($company->name) : urlencode($company->name));
					$segments[] = urlencode(CONTACT);
					break;
				// List all companies: /companies
				default:
					$segments[] = urlencode(COMPANIES);
					break;
			}
			break;
		case 'agents':
			switch(@$query['layout'])
			{
				// List all properties of this agent: /companies/{company}/{agent}
				case 'properties':
					$hotproperty =& MosetsFactory::getApplication('hotproperty');
					$model =& $hotproperty->getModel('Agent');
					$agent = $model->getData('first', array(
						'where' => array('Agent.id' => $query['id']),
						'contain' => array('Company' => array())
					));
					$segments[] = urlencode(COMPANIES);
					$segments[] = (APPEND_ID ? $agent->Company->id . '_' . JFilterOutput::stringURLSafe($agent->Company->name) : urlencode($agent->Company->name));
					$segments[] = (APPEND_ID ? $agent->id . '_' . JFilterOutput::stringURLSafe($agent->name) : urlencode($agent->name));
					break;
				// Contact this agent: /companies/{company}/{agent}/contact
				case 'contact':
					$hotproperty =& MosetsFactory::getApplication('hotproperty');
					$model =& $hotproperty->getModel('Agent');
					$agent = $model->getData('first', array(
						'where' => array('Agent.id' => $query['id']),
						'contain' => array('Company' => array())
					));
					$segments[] = urlencode(COMPANIES);
					$segments[] = (APPEND_ID ? $agent->Company->id . '_' . JFilterOutput::stringURLSafe($agent->Company->name) : urlencode($agent->Company->name));
					$segments[] = (APPEND_ID ? $agent->id . '_' . JFilterOutput::stringURLSafe($agent->name) : urlencode($agent->name));
					$segments[] = urlencode(CONTACT);
					break;
				// Edit agent's profile: /profile
				case 'form':
					$segments[] = urlencode(PROFILE);
					break;
				// List all agents: /agents
				default:
					$segments[] = urlencode(AGENTS);
					break;
			}
			break;
		// Search form: /search
		case 'searches':
			$segments[] = urlencode(SEARCH);
			break;
		default:
			
			break;
	}
	
	unset($query['view']);
	unset($query['layout']);
	unset($query['id']);

	ksort($segments);
	return $segments;
}

function HotpropertyParseRoute($segments)
{
	global $mainframe;
	
	$vars = array();
	
	// Import Mosets Framework
	if (JPluginHelper::isEnabled('mosets', 'framework')) {
		JPluginHelper::importPlugin('mosets', 'framework');
	} else {
		JError::raiseError(404, 'Mosets Framework plugin is required for this component. Please install and enable it.');
	}
	$mainframe->triggerEvent('onInitializeMosetsFramework');
	$hotproperty =& MosetsFactory::getApplication('hotproperty');
	
	switch($segments[0])
	{
		case PROPERTIES:
			if (isset($segments[1]) && $segments[1] == FEATURED) {
				$vars['view'] = 'featured';
			} else {
				$vars['view'] = 'properties';
			}
			break;
		case TYPES:
			$vars['view'] = 'types';
			break;
		case COMPANIES:
			if (isset($segments[1])) {
				if (!isset($segments[2]) || in_array($segments[2], array('properties', 'contact'))) {
					$hotproperty =& MosetsFactory::getApplication('hotproperty');
					$model =& $hotproperty->getModel('Company');
					$company = $model->getData('first', array(
						'where' => (APPEND_ID ? array('Company.id' => substr($segments[1], 0, strpos($segments[1], '_'))) : array('Company.name' => urldecode($segments[1])))
					));
					$vars['view'] = 'companies';
					$vars['layout'] = isset($segments[2]) ? $segments[2] : 'agents';
					$vars['id'] = $company->id;
				} else {
					$hotproperty =& MosetsFactory::getApplication('hotproperty');
					$model =& $hotproperty->getModel('Agent');
					$agent = $model->getData('first', array(
						'where' => (APPEND_ID ? array('Agent.id' => substr($segments[2], 0, strpos($segments[2], '_'))) : array('Agent.name' => urldecode($segments[2])))
					));
					if (isset($segments[3]) && $segments[3] == CONTACT) {
						$vars['view'] = 'agents';
						$vars['layout'] = 'contact';
						$vars['id'] = $agent->id;
					} else {
						$vars['view'] = 'agents';
						$vars['layout'] = 'properties';
						$vars['id'] = $agent->id;
					}
				}
			} else {
				$vars['view'] = 'companies';
			}
			break;
		case AGENTS:
			$vars['view'] = 'agents';
			break;
		case PROFILE:
			$vars['view'] = 'agents';
			$vars['layout'] = 'form';
			break;
		case MANAGE:
			$vars['view'] = 'properties';
			$vars['layout'] = 'manage';
			break;
		case ADD:
			$vars['view'] = 'properties';
			$vars['layout'] = 'form';
			break;
		case SEARCH:
			$vars['view'] = 'searches';
			$fields = JRequest::getVar('Field', array(), 'default', 'array');
			if (!empty($fields)) {
				$vars['layout'] = 'results';
			}
			break;
		default:
			if (isset($segments[0])) {
				if (isset($segments[1])) {
					$hotproperty =& MosetsFactory::getApplication('hotproperty');
					$model =& $hotproperty->getModel('Property');
					$property = $model->getData('first', array(
						'where' => (APPEND_ID ? array('Property.id' => substr($segments[1], 0, strpos($segments[1], '_'))) : array('Property.name' => urldecode($segments[1])))
					));
					if (isset($segments[2]) && $segments[2] == EDIT) {
						$vars['view'] = 'properties';
						$vars['layout'] = 'form';
						$vars['id'] = $property->id;
					} else {
						$vars['view'] = 'properties';
						$vars['layout'] = 'property';
						$vars['id'] = $property->id;
					}
				} else {
					$hotproperty =& MosetsFactory::getApplication('hotproperty');
					$model =& $hotproperty->getModel('Type');
					$type = $model->getData('first', array(
						'where' => (APPEND_ID ? array('Type.id' => substr($segments[0], 0, strpos($segments[0], '_'))) : array('Type.name' => urldecode($segments[0])))
					));
					$vars['view'] = 'types';
					$vars['layout'] = 'properties';
					$vars['id'] = $type->id;
				}
			} else {
				$vars['view'] = 'home';
			}
			break;
	}
	
	return $vars;
}

function sanitize($str)
{
	return JFilterOutput::stringURLSafe($str);
}
?>