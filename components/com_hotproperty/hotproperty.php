<?php
/**
 * @version		$Id: hotproperty.php 945 2010-02-01 10:39:52Z CY $
 * @package		Hotproperty
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

global $mainframe;

jimport('joomla.filesystem.file');

// Import Mosets Framework
if (JPluginHelper::isEnabled('mosets', 'framework')) {
	JPluginHelper::importPlugin('mosets', 'framework');
} else {
	JError::raiseError(404, 'Mosets Framework plugin is required for this component. Please install and enable it.');
}
$mainframe->triggerEvent('onInitializeMosetsFramework');

$hotproperty =& MosetsFactory::getApplication('hotproperty');

$controller =& $hotproperty->getController();
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
?>
