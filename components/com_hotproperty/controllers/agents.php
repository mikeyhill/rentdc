<?php
/**
 * @version		$Id: agents.php 909 2009-11-06 03:16:35Z abernier $
 * @package		Hotproperty
 * @subpackage	Controller
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Agents Controller
 *
 * @package		Hotproperty
 * @subpackage	Controller
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyControllerAgents extends HotpropertyController
{
	/**
	 * Constructor
	 *
	 * @access	protected
	 * @param	An optional associative array of configuration settings.
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Agents',
			'name_singular' => 'Agent'
		), $config));
	}
	
	/**
	 * Save item(s)
	 * 
	 * Overload HotpropertyController::save() to make PHP validation
	 * 
	 * @access	public
	 * @return	boolean	Success
	 * @todo	Implement a global PHP validation into HotpropertyController and get rid of that overloaded function
	 */
	function save($apply = false)
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		$user	=& JFactory::getUser();
		$model	=& $this->getModel();
		$agent = $model->getData('first', array(
			'where'	=> array('Agent.user' => $user->get('id'))
		));
		$id = $agent->id;
		$datas	= JRequest::get('post');
		
		// Validate required fields
		$name		= $datas['Agent'][$id]['name'];
		$email		= $datas['Agent'][$id]['email'];
	
		jimport('joomla.mail.helper');
		if (!$name || JMailHelper::isEmailAddress($email) == false) {
			$this->setMessage(JText::_('Please complete the form before submitting.'), 'error');
			return false;
		}
		
		return parent::save($apply);
	}
	
	/**
	 * Contact an Agent
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function contact()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		$hotproperty =& MosetsFactory::getApplication('hotproperty');
		
		$id = JRequest::getInt('id', null);
		
		$db	=& MosetsFactory::getDBO();

		$model =& $this->getModel();
		$agent = $model->getData('first', array(
			'where' => array('id' => $id)
		));
		
		if (!empty($agent)) {
			jimport('joomla.mail.helper');
			
			$email			= JRequest::getVar('email',		'',	'post');
			$contactnumber	= JRequest::getVar('contactnumber','',	'post');
			$name			= JRequest::getVar('name',			'',	'post');
			$enquiry		= JRequest::getVar('enquiry', 		'',	'post');
			
			if (!$enquiry || !$name || empty($email) || JMailHelper::isEmailAddress($email) == false) {
				$this->setMessage(JText::_('Please complete the form before submitting.'), 'error');
				return false;
			} else {
				// Prevent form submission if one of the banned text is discovered in the email field
				require_once(JPath::clean(MosetsApplication::getPath('helpers', 'hotproperty') . '/spam.php'));
				if(!HotpropertyHelperSpam::isValid($name, $hotproperty->getCfg('banned_name')) || !HotpropertyHelperSpam::isValid($email, $hotproperty->getCfg('banned_email')) || !HotpropertyHelperSpam::isValid($enquiry, $hotproperty->getCfg('banned_enquiry'))) {
					$this->setMessage(JText::_('Sorry but your enquiry does not fulfil our spam policy.'), 'error');
					return false;
				}
				
				$mail_body		= sprintf(JText::_('CONTACT_ENQUIRY'), $name, $email, $contactnumber, $enquiry);
				$mail_recipient	= $agent->email;
				$mail_subject	= JText::_("Agent's Enquiry");

				$mail =& JFactory::getMailer();
				$mail->setSender(array($email, $name));
				$mail->addRecipient($mail_recipient);
				$mail->setSubject($mail_subject);
				$mail->setBody($mail_body);
				
				if ($mail->send()) {
					$this->setMessage(JText::_('Thank you for your enquiry. Our agent will contact you as soon as possible.'));
				} else {
					$this->setMessage(sprintf(JText::_('Error while %s: %s'), JText::_('sending mail'), $mail->getError()), 'error');
					return false;
				}
			}
		} else {
			JError::raiseError(404, sprintf(JText::_('Agent #%s not found'), $id));
			return false;
		}
		
		return true;
	}
}
?>