<?php
/**
 * @version		$Id: companies.php 772 2009-08-09 20:07:40Z abernier $
 * @package		Hotproperty
 * @subpackage	Controller
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Companies Controller
 *
 * @package		Hotproperty
 * @subpackage	Controller
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyControllerCompanies extends HotpropertyController
{
	/**
	 * Constructor
	 *
	 * @access	protected
	 * @param	An optional associative array of configuration settings.
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Companies',
			'name_singular' => 'Company'
		), $config));
	}
	
	/**
	 * Contact a Company
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function contact()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		$hotproperty =& MosetsFactory::getApplication('hotproperty');
		
		$id = JRequest::getInt('id', null);
		
		$db	=& MosetsFactory::getDBO();

		$model =& $this->getModel();
		$company = $model->getData('first', array(
			'where' => array('id' => $id)
		));
		
		if (!empty($company)) {
			jimport('joomla.mail.helper');
			
			$email			= JRequest::getVar('email',		'',	'post');
			$contactnumber	= JRequest::getVar('contactnumber','',	'post');
			$name			= JRequest::getVar('name',			'',	'post');
			$enquiry		= JRequest::getVar('enquiry', 		'',	'post');

			if (!$enquiry || !$name || empty($email) || JMailHelper::isEmailAddress($email) == false) {
				$this->setMessage(JText::_('Please complete the form before submitting.'), 'error');
				return false;
			} else {
				// Prevent form submission if one of the banned text is discovered in the email field
				require_once(JPath::clean(MosetsApplication::getPath('helpers', 'hotproperty') . '/spam.php'));
				if(!HotpropertyHelperSpam::isValid($name, $hotproperty->getCfg('banned_name')) || !HotpropertyHelperSpam::isValid($email, $hotproperty->getCfg('banned_email')) || !HotpropertyHelperSpam::isValid($enquiry, $hotproperty->getCfg('banned_enquiry'))) {
					$this->setMessage(JText::_('Sorry but your enquiry does not fulfil our spam policy.'), 'error');
					return false;
				}
				
				$mail_body		= sprintf(JText::_('CONTACT_ENQUIRY'), $name, $email, $contactnumber, $enquiry);
				$mail_recipient	= $company->email;
				$mail_subject	= JText::_("Company's Enquiry");

				$mail =& JFactory::getMailer();
				$mail->setSender(array($email, $name));
				$mail->addRecipient($mail_recipient);
				$mail->setSubject($mail_subject);
				$mail->setBody($mail_body);
				
				if ($mail->send()) {
					$this->setMessage(JText::_('Thank you for your enquiry. Our agent will contact you as soon as possible.'));
				} else {
					$this->setMessage(sprintf(JText::_('Error while %s: %s'), JText::_('sending mail'), $mail->getError()), 'error');
					return false;
				}
			}
		} else {
			JError::raiseError(404, sprintf(JText::_('Company #%s not found'), $id));
			return false;
		}
		
		return true;
	}
}
?>