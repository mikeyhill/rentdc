<?php
/**
 * @version		$Id: searches.php 944 2010-01-29 09:33:05Z CY $
 * @package		Hotproperty
 * @subpackage	Controller
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Properties Controller
 *
 * @package		Hotproperty
 * @subpackage	Controller
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyControllerSearches extends HotpropertyController
{
	/**
	 * Constructor
	 *
	 * @access	protected
	 * @param	An optional associative array of configuration settings.
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Searches',
			'name_singular' => 'Search'
		), $config));
	}
	
	/**
	 * Display the view
	 * 
	 * Overloads the parent::display in order to deal with 'use_advsearch' configuration's option
	 * 
	 * @access 	public
	 * @param	
	 * @return	void
	 */
	function display()
	{
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		if (!$hotproperty->getCfg('use_advsearch')) {
			JError::raiseError(404, 'Not found.');
		}
		
		parent::display();
	}
	
	/**
	 * Search for properties
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function search()
	{
		global $mainframe;
		
		$config =& JFactory::getConfig();
		$sef = $config->getValue('config.sef');
		
		$fields = JRequest::getVar('Field', array(), 'post', 'array');
		$fields = array_filter($fields, array($this, '_notEmptyField'));
		$mainframe->setUserState('search.fields', serialize($fields));
		
		if (empty($fields)) {
			$fields = unserialize($mainframe->getUserState('search.fields'));
		}
		
		$link = MosetsRoute::getLink('hotproperty', array('view' => 'searches', 'layout' => 'results'/*, 'Field' => $fields*/));
		$this->setRedirect(JRoute::_($link, false) . (($sef) ? '?' : '&') . http_build_query(array('Field' => $fields)));
	}
	
	/**
	 * 
	 * 
	 * @access	private
	 * @return	boolean	Success
	 */
	function _notEmptyField($field)
	{
		if (empty($field)) {
			return false;
		}
		
		if (is_array($field)) {
			$field = array_filter($field);
			
			if (empty($field)) {
				return false;
			}
			
			if (array_key_exists('range', $field) && empty($field[0])) {
				return false;
			}
			
			if (array_key_exists('from', $field) && empty($field['from']) || array_key_exists('to', $field) && empty($field['to'])) {
				return false;
			}
		}
		
		return true;
	}
}
?>