<?php
/**
 * @version		$Id: spam.php 768 2009-07-20 16:08:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Helpers
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * 
 *
 * @package		Hotproperty
 * @subpackage	Controller
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyHelperSpam extends JObject
{
	/**
	 * Constructor
	 *
	 * @static
	 * @access	protected
	 * @param	An optional associative array of configuration settings.
	 * @return	boolean	true if it's spam
	 */
	function isValid($text, $list)
	{
		if(empty($list) || empty($text)) return true;
		$array = explode(';', $list);
		foreach ($array as $value) {
			$value = trim($value);
			if(empty($value)) continue;
			if (JString::stristr($text, $value) !== false) {
				return false;
			}
		}
		return true;
	}
}