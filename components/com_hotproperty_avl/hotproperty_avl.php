<?php
/**
 * @version		$Id: hotproperty_avl.php 4 2009-11-04 07:58:45Z cy $
 * @package		Hotproperty Availability
 * @copyright	(C) 2004 - 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

// Import Mosets Framework
if (JPluginHelper::isEnabled('mosets', 'framework')) {
	JPluginHelper::importPlugin('mosets', 'framework');
} else {
	JError::raiseError(404, 'Mosets Framework plugin is required for this component. Please install and enable it.');
}
$mainframe->triggerEvent('onInitializeMosetsFramework');

mimport('mosets.utilities.route');

include_once ( JPATH_COMPONENT_ADMINISTRATOR.DS.'admin.hotproperty_avl.class.php' );
include_once ( JPATH_COMPONENT_SITE.DS.'hotproperty_avl.class.php' );
require_once( $mainframe->getPath( 'front_html' ) );

// Append CSS
$document =& JFactory::getDocument();
$document->addCustomTag("<link href=\"media/com_hotproperty_avl/css/default/styles.css\" rel=\"stylesheet\" type=\"text/css\"/>");

$id = JRequest::getInt('id');
$task = JRequest::getCmd( 'task' );

// Builds the breadcrumb
if( $id )
{
	global $hotproperty;
	$hotproperty =& MosetsFactory::getApplication('hotproperty');
	$propertyModel = $hotproperty->getModel('property');

	$property = $propertyModel->getData('first', array(
		'where' => array(
			'Property.id' => $id
		),
		'contain' => array(
			'Type' => array()
		)
	));

	$pathway =& $mainframe->getPathway();

	$pathway->addItem(htmlspecialchars($property->Type->name), MosetsRoute::getLink('hotproperty', array('view' => 'types', 'id' => $property->type)));
	$pathway->addItem(htmlspecialchars($property->name), MosetsRoute::getLink('hotproperty', array('view' => 'properties', 'id' => $property->id)));

	$pathway->addItem( JText::_( 'Availability' ) );	
}

switch( $task ) {
	# Extension Event - Editing
	case "ext_main":
		ext_viewyear( $option );
		break;
	case "ext_viewmonth":
		ext_main( $option );
		break;
	case "ext_viewyear":
		ext_viewyear( $option );
		break;
	case "ext_save":
		ext_save( $option );
		break;

	# Front end view
	case 'view':
		clear_prev_month();
		viewAvl( $id, $option );
		break;

	# Display Mambo default error msg when $task provided does not exists
	default:
		return JError::raiseError( 404, JText::_( 'ALERTNOTAUTH' ) );
		break;
}

function viewAvl( $property_id, $option ) {
	$database =& JFactory::getDBO();

	$today = getdate();
	$cal = new view_Calendar();
	$cal->edit = 0;
	$cal->property_id = $property_id;

	$year = JRequest::getInt( 'year', $today['year'] );

	# Get Availability Type
	$database->setQuery('SELECT * FROM #__hp_avl_types');
	$avl_types = $database->loadObjectList('avl_type_id');
	$cal->avl_types = $avl_types;

	# Retrieve assigned Availability
	$database->setQuery("SELECT avl_type_id, date FROM #__hp_avl WHERE property_id = '".$property_id."' AND YEAR(`date`) = '".$year."'");
	$cal->avl = $database->loadObjectList('date');

	# Get property name & type
	$database->setQuery("SELECT a.user AS user, p.id AS id, p.name AS name, t.id AS type_id, t.name AS type_name "
		.	"\nFROM #__hp_properties AS p, #__hp_prop_types AS t, #__hp_agents AS a "
		.	"\nWHERE p.type = t.id AND p.agent = a.id AND p.id = '".$property_id."'"
		.	"\nLIMIT 1");
	$property = $database->loadObject();

	$html = $cal->getYearHTML( $year );

	hp_avl_HTML::viewAvl( $html, $cal->avl_types, $property );
}

function ext_main( $option ) {
	global $Itemid, $mainframe;

	$database =& JFactory::getDBO();
	$my	=& JFactory::getUser();

	$today = getdate();
	$year = JRequest::getInt( 'year', 0 );
	$month = JRequest::getInt( 'month', 0 );
	$property_id = JRequest::getInt( 'property_id', 0 );
	
	$hotproperty =& MosetsFactory::getApplication('hotproperty');
	$agentModel = $hotproperty->getModel('agent');

	$agent = $agentModel->getData('first', array(
		'where' => array(
			'Agent.user' => $my->id
		)
	));

	# Redirect to HP main page if user is not a valid Agent
	if ( !($agent->user == $my->id && $agent->user > 0 && !empty($agent->id) && $my->id > 0) ) {
		$mainframe->redirect( "index.php?option=com_hotproperty&Itemid=".$Itemid );
	}

	# Property exists?
	$database->setQuery("SELECT agent FROM #__hp_properties WHERE id = '".$property_id."'");
	$tmp_agent = $database->loadResult();
	if($tmp_agent == '') {
		return JError::raiseError( 404, JText::_( 'ALERTNOTAUTH' ) );
	# Correct owner?
	} elseif($tmp_agent <> $agent->id) {
		return JError::raiseError( 404, JText::_( 'ALERTNOTAUTH' ) );
	}

	if ($property_id == 0) {
		hp_avl_HTML::ext_error( "<center><br /><h2>".JText::_( 'HOTPROPERTY_AVL_UNAVAILABLE' )."</h2>".JText::_( 'HOTPROPERTY_AVL_ERROR_01' )."</center>" );		
	}

	if ($year == 0 && $month == 0) {
		$year = JRequest::getInt( 'year', $today['year'] );
		$month = JRequest::getInt( 'month', $today['mon'] );
	}

	if ($property_id == 0)	 {
		$property_id = JRequest::getInt( 'property_id', 0 );
	}

	$cal = new frontend_Calendar();
	$cal->property_id = $property_id;

	# Get Availability Type
	$database->setQuery('SELECT * FROM #__hp_avl_types');
	$avl_types = $database->loadObjectList('avl_type_id');
	$cal->avl_types = $avl_types;

	# Retrieve assigned Availability
	$database->setQuery("SELECT avl_type_id, date FROM #__hp_avl WHERE property_id = '".$property_id."' AND MONTH(`date`) = '".$month."' AND YEAR(`date`) = '".$year."'");
	$cal->avl = $database->loadObjectList('date');

	$html['calendar'] = $cal->getMonthHTML( $month, $year );

	hp_avl_HTML::ext_main( $html, $avl_types, $month, $year, $property_id, $option );
}

function ext_save( $option ) {
	global $Itemid, $mainframe;

	$database =& JFactory::getDBO();
	$my	=& JFactory::getUser();

	$post		= JRequest::get('post');
	$year 		= JRequest::getInt( 'year', $today['year'] );
	$month 		= JRequest::getInt( 'month', $today['mon'] );
	$property_id = JRequest::getInt( 'property_id', 0 );
	
	$hotproperty =& MosetsFactory::getApplication('hotproperty');
	$agentModel = $hotproperty->getModel('agent');

	$agent = $agentModel->getData('first', array(
		'where' => array(
			'Agent.user' => $my->id
		)
	));

	# Redirect to HP main page if user is not a valid Agent
	if ( !($agent->user == $my->id && $agent->user > 0 && !empty($agent->id) && $my->id > 0) ) {
		$mainframe->redirect( "index.php?option=com_hotproperty&Itemid=".$Itemid );
	}

	# Property exists?
	$database->setQuery("SELECT agent FROM #__hp_properties WHERE id = '".$property_id."'");
	$tmp_agent = $database->loadResult();
	if($tmp_agent == '') {
		return JError::raiseError( 404, JText::_( 'ALERTNOTAUTH' ) );
	# Correct owner?
	} elseif($tmp_agent <> $agent->id) {
		return JError::raiseError( 404, JText::_( 'ALERTNOTAUTH' ) );
	}

	$row = new hp_avl($database);

	$row->clear_avl($property_id, $month, $year);

	foreach($post AS $k => $v) {
		if (substr($k,0,3) == "day" && $v > 0) {
			$date = date('Y-m-d', strtotime(substr($k,3,2).' '.date('F',mktime(0, 0, 0, $month, 1, 2004)).' '.$year));
			$row->add_avl($property_id, $date, $v);
		}
	}
	$mainframe->redirect( "index.php?option=$option&task=ext_viewmonth&month=$month&year=$year&property_id=$property_id&tmpl=component", JText::_( 'HOTPROPERTY_AVL_CALENDAR_HAS_BEEN_SAVED' )  );
  
}

function ext_viewyear( $option ) {
	global $Itemid, $mainframe, $hotproperty;

	$database =& JFactory::getDBO();
	$my	=& JFactory::getUser();

	$today = getdate();

	$year = JRequest::getInt( 'year', $today['year'] );
	$property_id = JRequest::getInt( 'property_id', 0 );

	if ($property_id == 0) {
		hp_avl_HTML::ext_error( JText::_( 'HOTPROPERTY_AVL_ERROR_02' ) );		
	} else {

		$hotproperty =& MosetsFactory::getApplication('hotproperty');
		$agentModel = $hotproperty->getModel('agent');

		$agent = $agentModel->getData('first', array(
			'where' => array(
				'Agent.user' => $my->id
			)
		));

		# Redirect to HP main page if user is not a valid Agent
		if ( !($agent->user == $my->id && $agent->user > 0 && !empty($agent->id) && $my->id > 0) ) {
			return JError::raiseError( 404, JText::_( 'ALERTNOTAUTH' ) );
		}

		# Property exists?
		$database->setQuery("SELECT agent FROM #__hp_properties WHERE id = '".$property_id."'");
		$tmp_agent = $database->loadResult();
		if($tmp_agent == '') {
			return JError::raiseError( 404, JText::_( 'ALERTNOTAUTH' ) );
		# Correct owner?
		} elseif($tmp_agent <> $agent->id) {
			return JError::raiseError( 404, JText::_( 'ALERTNOTAUTH' ) );
		}

		$cal = new frontend_Calendar();
		$cal->edit = 0;
		$cal->property_id = $property_id;

		# Get Availability Type
		$database->setQuery('SELECT * FROM #__hp_avl_types');
		$avl_types = $database->loadObjectList('avl_type_id');
		$cal->avl_types = $avl_types;

		# Retrieve assigned Availability
		$database->setQuery("SELECT `avl_type_id`, `date` FROM #__hp_avl WHERE property_id = '".$property_id."' AND YEAR(`date`) = '".$year."'");
		$cal->avl = $database->loadObjectList('date');

		$html = $cal->getYearHTML( $year );

		hp_avl_HTML::ext_viewyear( $html, $option );
	}
}

function clear_prev_month() {
	global $mainframe;
	
	$database =& JFactory::getDBO();
	$params = &$mainframe->getParams();

	if ($params->get('delete_previous_months_availability') == '1') {
		$database->setQuery("DELETE LOW_PRIORITY FROM #__hp_avl WHERE `date` < '".date('Y-m-01')."'");
		$database->query();
	}
}

?>