<?php
/**
 * @version		$Id: hotproperty_avl.class.php 4 2009-11-04 07:58:45Z cy $
 * @package		Hotproperty Availability
 * @copyright	(C) 2004 - 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

defined('_JEXEC') or die('Restricted access');

class frontend_Calendar extends Calendar {

	function getCalendarLink($month, $year)
    {
        return "index.php?option=com_hotproperty_avl&task=ext_viewmonth&month=$month&year=$year&property_id=$this->property_id&tmpl=component";
    }
    
    function getDateLink($month, $year)
    {
        return "";
    }

    function getMonthLink($month, $year)
    {
        return "index.php?option=com_hotproperty_avl&task=ext_viewmonth&month=$month&year=$year&property_id=$this->property_id&tmpl=component";
    }

	function getYearLink($month, $year)
	{
			return "index.php?option=com_hotproperty_avl&task=ext_viewyear&year=$year&property_id=$this->property_id&tmpl=component";
	}

}

class view_Calendar extends Calendar {

	function getCalendarLink($month, $year)
    {
		return '';
    }
    
    function getDateLink($month, $year)
    {
        return "";
    }

    function getMonthLink($month, $year)
    {
        return "";
    }

	function getYearLink($month, $year)
	{
			return "index.php?option=com_hotproperty_avl&task=view&year=$year&id=$this->property_id";
	}

}

?>