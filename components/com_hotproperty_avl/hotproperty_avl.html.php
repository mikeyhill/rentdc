<?php
/**
 * @version		$Id: hotproperty_avl.html.php 4 2009-11-04 07:58:45Z cy $
 * @package		Hotproperty Availability
 * @copyright	(C) 2004 - 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

defined('_JEXEC') or die('Restricted access');

class hp_avl_HTML {

	function viewAvl( $html, &$avl_types, $property ) {
		global $Itemid;
		$my =& JFactory::getUser();
	?>
	<div id="heading_Prop">
		<?php 
		# Show edit icon for authorized agent
		if ($property->user == $my->id && $property->user > 0 && $my->id > 0)
		{ 
		?><a href="<?php echo JRoute::_('index.php?option=com_hotproperty&view=properties&layout=form&id='.$property->id.'&Itemid='.$Itemid); ?>" title="<?php echo JText::_( 'EDIT' ); ?>">
		<img src="images/M_images/edit.png" width="18" height="18" align="middle" border="0" alt="<?php echo JText::_( 'EDIT' ); ?>" />
		</a>
		<?php 
		} 
		?>
	</div>

	<center>
	<?php echo $html; ?>
	<p class="return-to-property">
	<a href="<?php echo JRoute::_("index.php?option=com_hotproperty&view=properties&layout=property&id=".$property->id); ?>"><?php echo JText::_( 'HOTPROPERTY_AVL_RETURN_TO_PROPERTYS_DETAILS' ); ?></a>
	</p>
	</center>
	<p />
	<table cellpadding="5" cellspacing="0" border="0" class="avl-legend">
	<?php	foreach($avl_types AS $avl_type) {	?>
	<tr>
		<td style="background-color:<?php echo $avl_type->font_background; ?>; color:<?php echo $avl_type->font_color; ?>"><?php echo $avl_type->name; ?></td>
		<td><?php echo $avl_type->desc; ?></td>
	</tr>
	<?php	}	?>
	</table>

	<?php
	}

	function ext_main( &$html, &$avl_types, $month, $year, $property_id, $option ) {
	?>
	<body style="background-color:#f5f5f5;">
	<form action="index.php" method="POST" name="adminForm">
	<center>
	<a href="index.php?option=<?php echo $option; ?>&amp;task=ext_viewyear&amp;year=<?php echo $year; ?>&amp;returnmonth=<?php echo $month; ?>&amp;property_id=<?php echo $property_id; ?>&amp;tmpl=component"><?php echo $year; ?></a>
	<?php
		echo $html['calendar'];
	?>
	<p />
	<input type="hidden" name="option" value="<?php echo $option; ?>" />
	<input type="hidden" name="task" value="ext_save" />
	<input type="hidden" name="property_id" value="<?php echo $property_id; ?>" />
	<input type="hidden" name="month" value="<?php echo $month; ?>" />
	<input type="hidden" name="year" value="<?php echo $year; ?>" />
	<input type="submit" value="<?php echo JText::_( 'Save' ); ?>" class="button" />
	</form>
	</center>
	<p />
	<table cellpadding="5" cellspacing="0" border="0" class="avl-legend">
	<?php foreach($avl_types AS $avl_type) {	?>
	<tr>
		<td style="background-color:<?php echo $avl_type->font_background; ?>; color:<?php echo $avl_type->font_color; ?>"><?php echo $avl_type->name; ?></td>
		<td><?php echo $avl_type->desc; ?></td>
	</tr>
	<?php	}	?>
	</table>
	<?php
	}

	function ext_viewyear( $html, $option ) {
	?>
	<center>
	<?php
		echo $html;
	?>
	</center>
	<?php
	}

	function ext_error( $html ) {
		echo $html;
	}

}

?>