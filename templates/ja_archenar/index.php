<?php
/*------------------------------------------------------------------------
# JA Archenar for joomla 1.5 - Version 1.4 - Licence Owner JA976
# ------------------------------------------------------------------------
# Copyright (C) 2004-2008 J.O.O.M Solutions Co., Ltd. All Rights Reserved.
# @license - Copyrighted Commercial Software
# Author: J.O.O.M Solutions Co., Ltd
# Websites:  http://www.joomlart.com -  http://www.joomlancers.com
# This file may not be redistributed in whole or significant part.
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );
defined( 'DS') || define( 'DS', DIRECTORY_SEPARATOR );
include_once (dirname(__FILE__).DS.'/ja_vars.php');

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<jdoc:include type="head" />
<?php JHTML::_('behavior.mootools'); ?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link rel="stylesheet" href="<?php echo $tmpTools->baseurl(); ?>templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $tmpTools->baseurl(); ?>templates/system/css/general.css" type="text/css" />
<link href="<?php echo $tmpTools->templateurl();?>/css/template_css.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo $tmpTools->templateurl();?>/css/red.css" rel="alternate stylesheet" type="text/css" title="red" />
<link href="<?php echo $tmpTools->templateurl();?>/css/blue.css" rel="alternate stylesheet" type="text/css" title="blue" />
<link href="<?php echo $tmpTools->templateurl();?>/css/print.css" rel="stylesheet" type="text/css" media="print" />
<script language="javascript" type="text/javascript" src="<?php echo $tmpTools->templateurl();?>/ja_script.js"></script>
<script language="javascript" type="text/javascript">
preloadimages("<?php echo $tmpTools->templateurl();?>/images/arrow-on.gif","<?php echo $tmpTools->templateurl();?>/images/arrow-off.gif","<?php echo $tmpTools->templateurl();?>/images/topnav-bg-2.gif");
</script>
<?php //$jamenu->genMenuHead(); ?>
</head>

<body id="bd" onload="preloadimages()">

<div id="topnav">
	<jdoc:include type="modules" name="header" style="raw" />
	
</div>

<div id="container">
	<!-- BEGIN: HEADER -->
	<div id="header">
		<h1 class="header-logo"><a href="index.php"><img src="<?php echo $tmpTools->templateurl();?>/images/logo.gif" border="0" height="40" alt="<?php echo $tmpTools->sitename();?>" /></a></h1>
		<?php if ( $this->countModules( 'user4') ) { ?>
		<div id="search">
			<jdoc:include type="modules" name="user4" style="raw" />
		</div>
		<?php } ?>
		<div id="spotlight">
			<jdoc:include type="modules" name="top" style="xhtml" />
		</div>
		<div id="stripe">
				<div id="slideshow">
				<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
   codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0"
   width="740" height="100">
   <param name="movie" value="http://www.rentdoorcounty.com/images/banner.swf">
   <param name="quality" value="high">
   <embed src="http://www.rentdoorcounty.com/images/banner.swf" quality="high"
    pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"
    width="740" height="100">
   </embed>
 </object>
				</div>
			</div>
	</div>
	<!-- END: HEADER -->

	<!-- BEGIN: MAIN -->
	<div id="main" class="clearfix">

		<?php if ( $this->countModules( 'user1') || $this->countModules( 'user2') || $this->countModules( 'left') || $this->countModules( 'right') ) { ?>
		<!-- BEGIN: LEFT COLUMN -->
		<div id="leftcol" class="clearfix">
			<jdoc:include type="modules" name="user1" style="xhtml" />
			<jdoc:include type="modules" name="user2" style="xhtml" />
			<jdoc:include type="modules" name="left" style="xhtml" />
			<jdoc:include type="modules" name="right" style="xhtml" />
		</div>
		<!-- END: LEFT COLUMN -->
		<?php } ?>

		<!-- BEGIN: CONTENT -->
		<div id="<?php echo $divid ?>">
			<div id="pathway">
				<jdoc:include type="module" name="breadcrumb" />
			</div>
			<jdoc:include type="component" />
			
			<?php if ( $this->countModules( 'user5') || $this->countModules( 'user6') ) { ?>
			<div style="margin: 12px 0;" class="clearfix">
				<?php if ( $this->countModules( 'user5') ) { ?>
				<div id="spotlight-1">
					<jdoc:include type="modules" name="user5" style="xhtml" />
				</div>
				<?php } ?>
				<?php if ( $this->countModules( 'user6') ) { ?>
				<div id="spotlight-2">
					<jdoc:include type="modules" name="user6" style="xhtml" />
				</div>
				<?php } ?>
			</div>
			<?php } ?>
		</div>
		<!-- END: CONTENT -->
	</div>
	<!-- END: MAIN -->

	<!-- BEGIN: FOOTER -->
	<div id="footer">
		<jdoc:include type="modules" name="bottom" style="xhtml" />
		<div id="copyright">
			  <?php include (dirname(__FILE__).DS.'/footer.php');?>	
		</div>
	</div>
	<!-- END: FOOTER -->
</div>

<div id="botshad"></div>

<jdoc:include type="modules" name="debug" style="raw" />
</body>

</html>
