<?php
/*------------------------------------------------------------------------
# JA Archenar for joomla 1.5 - Version 1.4 - Licence Owner JA976
# ------------------------------------------------------------------------
# Copyright (C) 2004-2008 J.O.O.M Solutions Co., Ltd. All Rights Reserved.
# @license - Copyrighted Commercial Software
# Author: J.O.O.M Solutions Co., Ltd
# Websites:  http://www.joomlart.com -  http://www.joomlancers.com
# This file may not be redistributed in whole or significant part.
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

global $_VERSION;
require_once('libraries/joomla/utilities/date.php');
$date  = new JDate();
$config = new JConfig();
// NOTE - You may change this file to suit your site needs
?>

Copyright <?php echo mosCurrentDate( '%Y' ) . ' ' . $GLOBALS['mosConfig_sitename'];?>.&nbsp;
<style type="text/css">
<!--
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
}
.style2 {font-size: 10px}
-->
</style>
<script type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>

<body>
<span class="style2">design &amp; hosting by <a href="http://www.developmomentum.com" target="momentum" onclick="MM_openBrWindow('www.developmomentum.com','momentum','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=725,height=600')">Momentum</a></span>

