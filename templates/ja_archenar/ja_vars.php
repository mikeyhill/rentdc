<?php
/*------------------------------------------------------------------------
# JA Archenar for joomla 1.5 - Version 1.4 - Licence Owner JA976
# ------------------------------------------------------------------------
# Copyright (C) 2004-2008 J.O.O.M Solutions Co., Ltd. All Rights Reserved.
# @license - Copyrighted Commercial Software
# Author: J.O.O.M Solutions Co., Ltd
# Websites:  http://www.joomlart.com -  http://www.joomlancers.com
# This file may not be redistributed in whole or significant part.
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
include_once (dirname(__FILE__).DS.'/ja_templatetools.php');


$tmpTools = new JA_Tools($this);

if ( $this->countModules( 'left' ) || $this->countModules( 'right' ) || $this->countModules( 'user1' ) || $this->countModules( 'user2' )  ) {
	$divid = 'content';
} else {
	$divid = 'content-full';
}

?>
