<?php
/*------------------------------------------------------------------------
# JA Archenar for joomla 1.5 - Version 1.4 - Licence Owner JA976
# ------------------------------------------------------------------------
# Copyright (C) 2004-2008 J.O.O.M Solutions Co., Ltd. All Rights Reserved.
# @license - Copyrighted Commercial Software
# Author: J.O.O.M Solutions Co., Ltd
# Websites:  http://www.joomlart.com -  http://www.joomlancers.com
# This file may not be redistributed in whole or significant part.
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );
defined( 'DS') || define( 'DS', DIRECTORY_SEPARATOR );
include_once (dirname(__FILE__).DS.'/ja_vars.php');

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<?php mosShowHead(); ?>
<meta http-equiv="Content-Type" content="text/html; <?php echo _ISO; ?>" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link href="<?php echo $mosConfig_live_site;?>/templates/ja_archenar/css/template_css.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo $mosConfig_live_site;?>/templates/ja_archenar/css/print.css" rel="stylesheet" type="text/css" media="print" />
</head>
<body id="bd">
<div id="topnav">
	<?php mosLoadModules('header',-1); ?>
</div><!-- close topnav div -->
<div id="container">
					<div id="header">
						<h1 class="header-logo"><a href="http://www.rentdoorcounty.com"><img src="<?php echo $mosConfig_live_site;?>/templates/ja_archenarrev2/images/logo-blue.gif" border="0" alt="<?php echo $mosConfig_fromname?>" /></a></h1>
		<?php if ( mosCountModules( 'user4') ) { ?>
		<div id="search">
			<?php mosLoadModules ( 'user4', -1 ); ?>
		</div>
		<?php } ?>
		<div id="spotlight">
			<?php mosLoadModules('top',-2); ?>
</div>
					<div id="stripe">
				<div id="slideshow">				  <img src="<?php echo $mosConfig_live_site;?>/templates/ja_archenarrev2/images/banner.jpg" border="0" height="100" alt="<?php echo $mosConfig_fromname?>" />				</div>
			</div>
		<!-- BEGIN: MAIN -->
	<div id="main" class="clearfix">
		<!-- BEGIN: LEFT COLUMN -->
		<div id="leftcol" class="clearfix">
			<?php mosLoadModules('user1',-2); ?>
			<?php mosLoadModules('user2',-2); ?>
			<?php mosLoadModules('left',-2); ?>
			<?php mosLoadModules('right',-2); ?>
		</div><!-- close leftcol div -->
		<!-- END: LEFT COLUMN -->
		<!-- BEGIN: CONTENT -->
		<div id="content">
			<div id="pathway">
				<?php mosPathWay(); ?>
			</div><!-- close pathway div -->
			<?php mosMainBody(); ?>
		</div><!-- close content div -->
		<!-- END: CONTENT -->
	</div><!-- close main div --><!-- END: MAIN -->
	<!-- BEGIN: FOOTER -->
	<div id="footer">
		<?php mosLoadModules('bottom', -1); ?>
		<div id="copyright">
			<?php include_once( $GLOBALS['mosConfig_absolute_path'] . '/templates/ja_archenar/footer.php' ); ?>
		</div><!-- close copyright div -->
	</div><!-- close footer div -->
	<!-- END: FOOTER -->
</div><!-- close container div -->
<div id="botshad">
</div><!-- close botshad div -->

<?php mosLoadModules( 'debug', -1 );?>
</body>
</html>