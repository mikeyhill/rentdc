<?php
/**
* @author Peter Muusers-Meeuwsen (VAK18, www.vak18.com, peter@vak18.com)
* This plugin will replace your title tag with the alias instead of using the regular title.
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$mainframe->registerEvent( 'onAfterDisplayContent', 'aliasForPageTitle' );

/**
* Plugin that replaces the title tag with the article alias instead of using the regular article title
*/
function aliasForPageTitle( &$row, &$params, $page=0 ) 
{
    if(isset($row->alias) && strlen($row->alias) > 0)
    {
      // set initial alias to be used
      $targetAlias = $row->alias;
      // get the plugin and parameters
      $plugin =& JPluginHelper::getPlugin('content', 'aliasForPageTitle');
      $pluginParams 	= new JParameter( $plugin->params );
      
      // add the prefix
      if(strlen($pluginParams->get('prefix')) > 0){
        $targetAlias = str_replace('-', '---',$pluginParams->get('prefix')) . '-' . $targetAlias;
      }

      // add the suffix
      if(strlen($pluginParams->get('suffix')) > 0){
        $targetAlias = $targetAlias . '-' . str_replace('-', '---', $pluginParams->get('suffix'));
      }
      
      // convert the joomla-created dashes into spaces, but keep intentional dashes
      // using ïûï as a placeholder for intentional dashes, because this is usually not used in the alias field.
      $targetAlias = str_replace('---', '-ïûï-', $targetAlias);
      $targetAlias = str_replace('-', ' ', $targetAlias);
      $targetAlias = str_replace('ïûï', '-', $targetAlias);
      
      // convert into lowercase/uppercase according to plugin parameter settings
      switch($pluginParams->get('mode')){
        case '3':
          $targetAlias = strtoupper($targetAlias);
          break;
        case '2':
          $targetAlias = strtolower($targetAlias);
          break;
        case '1':
          $targetAlias = ucfirst($targetAlias);
          break;
        default:
          $targetAlias = ucwords($targetAlias);
      }
      // get the document
      $document= & JFactory::getDocument();
      // set the page title
      $document->setTitle($targetAlias);
	  }
}

?>