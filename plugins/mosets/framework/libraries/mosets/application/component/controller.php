<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	Application.Component
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Controller Class
 *
 * @package		Mosets
 * @subpackage	Application.Component
 * @author		Antoine Bernier
 */

mimport('joomla.application.component.controller');

class MosetsController extends JController
{
	/**
	 * The singular name of this controller
	 *
	 * @var		string
	 * @access	protected
	 */
	var $_nameSingular = null;
	
	/**
	 * The name of the component
	 *
	 * @var		string
	 * @access	protected
	 */
	var $_component = null;
	
	/**
	 * Constructor
	 *
	 * @access	protected
	 * @param	An optional associative array of configuration settings.
	 * 			Recognized key values include 'name_singular' (this list is not meant to be comprehensive).
	 * @return	void
	 */
	function __construct($config = array())
	{
		global $mainframe;
		
		// Set the component name
		if (empty($this->_component)) {
			if (array_key_exists('component', $config)) {
				$this->_component = $config['component'];
			} else {
				$this->_component = $this->getComponent();
			}
		}
		
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'base_path'		=> MosetsApplication::getPath('base', $this->getComponent()),
			'model_path'	=> MosetsApplication::getPath('models', $this->getComponent()),
			'view_path'		=> MosetsApplication::getPath('views', $this->getComponent())
		), $config));
		
		// Set the singular name
		if (array_key_exists('name_singular', $config)) {
			$this->_nameSingular = $config['name_singular'];
		}
		
		// Load Route utility
		if (!$mainframe->isAdmin()) {
			mimport('mosets.utilities.route');
		}
	}
	
	/**
	 * Returns a reference to the a Controller object, always creating it
	 *
	 * @static
	 * @access	public
	 * @param	string	The model type to instantiate
	 * @param	string	Prefix for the model class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	mixed	A model object, or false on failure
	*/
	function &getInstance($type, $component, $config = array())
	{
		$type				= preg_replace('/[^A-Z0-9_]/i', '', $type);
		$component			= preg_replace('/[^A-Z0-9_]/i', '', $component);
		$controllerClass	= ucfirst($component) . 'Controller' . MosetsInflector::camelize($type);
		$result				= false;

		if (!class_exists($controllerClass)) {
			jimport('joomla.filesystem.path');
			$path = JPath::find(
				MosetsController::addIncludePath(),
				MosetsController::_createFileName('controller', array('name' => $type))	// Create a controller filename according to our CamelCase classnames convention
			);
			if ($path) {
				require_once(MosetsApplication::getPath('controller', $component));
				require_once $path;

				if (!class_exists($controllerClass)) {
					JError::raiseWarning(0, 'Controller class ' . $controllerClass . ' not found in file.');
					return $result;
				}
			} else {
				return $result;
			}
		}

		$result = new $controllerClass(array_merge(array('component' => $component), $config));
		return $result;
	}
	
	/**
	 * Execute a task by triggering a method in the derived class.
	 * 
	 * Overload JoomlaController::execute() to automatically redirect
	 *
	 * @access	public
	 * @param	string		The task to perform. If no matching task is found, the '__default' task is executed, if defined.
	 * @return	mixed|false	The value returned by the called method, false in error case.
	 */
	function execute($task)
	{
		$return = parent::execute($task);
		
		// If the task has returned a value, we were executing a "not-displaying" task: we need to redirect to avoid a blank page
		if (isset($return)) {
			// The task has succeed
			if ($return) {
				// If no redirect has been set during the task execution
				if (empty($this->_redirect)) {
					$referer = JRequest::getString('referer', base64_encode(JRequest::getVar('HTTP_REFERER', null, 'server', 'string')));
					// We redirect to the referer if we have one set
					if (!empty($referer)) {
						$this->setRedirect(base64_decode($referer));
					// Otherwise redirect to home with a referer warning
					} else {
						$this->setRedirect('index.php?option=com_' . $this->getComponent());
						//JError::raiseNotice(404, get_class($this) . "::execute(): no 'referer' parameter was POSTed.");
					}
				}
			// The task returned false
			} else {
				// If no redirect has been set during the task execution
				if (empty($this->_redirect)) {
					// So we redirect to the current URL (which is the form's action one)
					$uri = JURI::getInstance();
					$this->setRedirect($uri->toString());
				}
			}
		// No return value, so we were executing a "displaying" task
		} else {
			// So no need to redirect
		}	
		
		return $return;
	}
	
	/*function validate($task)
	{
		global $mainframe;
		
		$valid = true;
		
		$datas = JRequest::get('post');
		
		if (!empty($datas)) {
			
			if (isset($this->_validation) && array_key_exists($task, $this->_validation)) {
				foreach ($this->_validation[$task] as $validationFieldName => $validationRule) {
					$validation =& HotpropertyClassValidation::getInstance($validationRule);
					$validationFieldName_parts = explode('.', $validationFieldName);
					if (count($validationFieldName_parts) > 1) {
						$modelName		= $validationFieldName_parts[0];
						$modelFieldName	= $validationFieldName_parts[1];
						foreach ($datas[$modelName] as $modelDatas)
						{
							foreach ($modelDatas as $modelId => $modelData)
							{
								if (!$validation->validate($modelFieldName, @$modelData[$modelFieldName])) {
									$valid = false;
									$mainframe->enqueueMessage($validation->getError(), 'warning');
								}
							}
						}
					} else {
						if (!$validation->validate($validationFieldName, @$datas[$validationFieldName])) {
							$valid = false;
							$mainframe->enqueueMessage($validation->getError(), 'error');
						}
					}
				}
			}
		}
		
		return $valid;
	}*/
	
	/**
	 * Method to get the singular name of the controller
	 *
	 * @access	public
	 * @return	string	The singular name of the controller
	 */
	function getNameSingular()
	{
		return $this->_nameSingular;
	}
	
	/**
	 * Get the component name
	 *
	 * @access	public
	 * @return	string	The component name
	 */
	function getComponent()
	{
		$component = $this->_component;

		if (empty($component)) {
			$r = null;
			if (!preg_match('/(.*)Contoller/i', get_class($this), $r)) {
				JError::raiseError (500, "MosetsController::getComponent() : Cannot get or parse class name.");
			}
			$component = strtolower($r[1]);
		}

		return $component;
	}
	
	/**
	 * Add a directory where MosetsController should search for controllers. 
	 * You may either pass a string or an array of directories.
	 *
	 * @access	public
	 * @param	string	A path to search.
	 * @return	array	An array with directory elements
	 */
	function addIncludePath($path = '')
	{
		static $paths;

		if (!isset($paths)) {
			$paths = array();
		}
		
		if (!empty($path) && !in_array($path, $paths)) {
			jimport('joomla.filesystem.path');
			array_unshift($paths, JPath::clean($path));
		}
		
		return $paths;
	}
	
	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @access	public
	 * @param	string	The model name. Optional.
	 * @param	string	The class prefix. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	object	The model.
	 */
	function &getModel($name = '', $config = array())
	{
		if (empty($name)) {
			$name = $this->getNameSingular();
		}
		
		$prefix = ucfirst($this->getComponent()) . 'Model';
		
		return parent::getModel($name, $prefix, $config);
	}
	
	/**
	 * Adds to the stack of model paths in LIFO order.
	 * 
	 * Overloads JoomlaController::addModelPath() to squeeze our MosetsModel in
	 *
	 * @static
	 * @param	string|array The directory (string), or list of directories
	 *                       (array) to add.
	 * @return	void
	 */
	function addModelPath($path)
	{
		mimport('mosets.application.component.model');
		MosetsModel::addIncludePath($path);
	}
	
	/**
	 * Method to get a reference to the current view and load it if necessary.
	 *
	 * @access	public
	 * @param	string	The view name. Optional, defaults to the controller name.
	 * @param	string	The view type. Optional.
	 * @param	string	The class prefix. Optional.
	 * @param	array	Configuration array for view. Optional.
	 * @return	object	Reference to the view or an error.
	 */
	/*function &getView($name = '', $type = '', $prefix = '', $config = array())
	{
		$prefix = ucfirst($this->getComponent()) . 'View';
		
		return parent::getView($name, $type, $prefix, $config);
	}*/
	
	/**
	 * Sets the internal message that is passed with a redirect
	 * 
	 * Overload JoomlaController::setMessage() to set the message type too
	 *
	 * @access	public
	 * @param	string	The message
	 * @return	string	Previous message
	 */
	function setMessage($message, $type = 'message')
	{
		$this->_messageType	= $type;
		
		return parent::setMessage($message);
	}
	
	/**
	 * Display the view
	 * 
	 * Overloads the JController's one to set view's default model to $view->getNameSingular()
	 * 
	 * @access 	public
	 * @param	
	 * @return	void
	 */
	function display($cachable = false)
	{
		$document	=& JFactory::getDocument();
		$viewType	= $document->getType();
		$viewName	= MosetsInflector::camelize(JRequest::getCmd('view'));

		$view =& $this->getView($viewName, $viewType, '', array('base_path' => $this->_basePath));

		// Get/Create the model
		if ($model =& $this->getModel($view->getNameSingular())) {	// Set the view default model to $view->getNameSingular()
			// Push the model into the view (as default)
			$view->setModel($model, true);
		}

		// Set the layout
		$view->setLayout(JRequest::getCmd('layout', $view->get('defaultLayout')));

		// Display the view
		if ($cachable && $viewType != 'feed') {
			global $option;
			$cache =& JFactory::getCache($option, 'view');
			$cache->get($view, 'display');
		} else {
			$view->display();
		}
	}
	
	/**
	 * Add an entity
	 * 
	 * @access 	public
	 * @return	void
	 */
	function add()
	{
		JRequest::setVar('layout', 'form');
			
		$this->display();
	}
	
	/**
	 * Edit item(s)
	 * 
	 * Make sure some items are selected and display the form layout
	 * 
	 * @access	public
	 * @return	void
	 */
	function edit()
	{
		JRequest::setVar('layout', 'form');
			
		$this->display();
	}
	
	/**
	 * Save item(s)
	 * 
	 * @access	public
	 * @param	boolean
	 * @return	boolean	Success
	 */
	function save($apply = false)
	{
		global $mainframe;
		
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		$datas = JRequest::get('post');
		/*echo "<pre>";
		echo "post=";
		print_r($datas);
		echo "</pre>";*/
		
		$files = array();
		$_files = JRequest::get('files');
		if (!empty($_files)) {
			foreach ($_files as $model => $files_keys)
				foreach ($files_keys as $files_key => $ids)
					foreach ($ids as $id => $fields)
						foreach ($fields as $field => $files_value)
						{
							if (!empty($_files[$model]['name'][$id][$field]))
								$files[$model][$id][$field][$files_key] = $files_value;
						}	
		}
		
		if (!empty($datas)) {
			$model = $this->getModel();
			
			if ($model->save($datas, $files)) {
				$this->setMessage(sprintf(JText::_('%s item(s) %s.'), count($datas[$model->getName()]), JText::_('saved')));
			} else {
				$this->setMessage(sprintf(JText::_('One or more of the selected item(s) can not be %s: %s'), JText::_('saved'), $model->getError()), 'error');
				return false;
			}
		} else {
			$this->setMessage(sprintf(JText::_('Nothing to %s.'), JText::_('save')), 'error');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Apply item(s)
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function apply()
	{
		$this->save(true);
		
		return false;
	}
	
	/**
	 * Cancel
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function cancel()
	{
		return true;
	}
	
	/**
	 * Remove item(s)
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function remove()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		// Retrieve checked items
		$ids = JRequest::getVar('id', array(), 'default', 'array');
		
		if (!empty($ids)) {
			// Get the model
			$model = $this->getModel();
		
			// Remove items and set the redirect message
			if ($model->remove($ids)) {
				$this->setMessage(sprintf(JText::_('%s item(s) %s.'), count($ids), JText::_('removed')));
			} else {
				$this->setMessage(sprintf(JText::_('One or more of the selected item(s) can not be %s: %s'), JText::_('removed'), $model->getError()), 'error');
				return false;
			}
		} else {
			$this->setMessage(sprintf(JText::_('Nothing to %s.'), JText::_('remove')), 'error');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Approve item(s)
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function approve()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		// Retrieve checked items
		$ids = JRequest::getVar('id', array(), 'default', 'array');
		
		if (!empty($ids)) {
			// Get the model
			$model = $this->getModel();
		
			// Approve items and set the redirect message
			if ($model->approve($ids)) {
				$this->setMessage(sprintf(JText::_('%s item(s) %s.'), count($ids), JText::_('approved')));
			} else {
				$this->setMessage(sprintf(JText::_('One or more of the selected item(s) can not be %s: %s'), JText::_('approved'), $model->getError()), 'error');
				return false;
			}
		} else {
			$this->setMessage(sprintf(JText::_('Nothing to %s.'), JText::_('approve')), 'error');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Publish item(s)
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function publish()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		// Retrieve checked items
		$ids = JRequest::getVar('id', array(), 'default', 'array');
		
		if (!empty($ids)) {
			// Get the model
			$model =& $this->getModel();
		
			// Publish items and set the redirect message
			if ($model->publish($ids)) {
				$this->setMessage(sprintf(JText::_('%s item(s) %s.'), count($ids), JText::_('published')));
			} else {
				$this->setMessage(sprintf(JText::_('One or more of the selected item(s) can not be %s: %s'), JText::_('published'), $model->getError()), 'error');
				return false;
			}
		} else {
			$this->setMessage(sprintf(JText::_('Nothing to %s.'), JText::_('publish')), 'error');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Unpublish item(s)
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function unpublish()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		// Retrieve checked items
		$ids = JRequest::getVar('id', array(), 'default', 'array');
		
		if (!empty($ids)) {
			// Get the model
			$model =& $this->getModel();
		
			// Unpublish items and set the redirect message
			if ($model->unpublish($ids)) {
				$this->setMessage(sprintf(JText::_('%s item(s) %s.'), count($ids), JText::_('unpublished')));
			} else {
				$this->setMessage(JText::_sprintf('One or more of the selected item(s) can not be %s: %s', JText::_('unpublished'), $model->getError()), 'error');
				return false;
			}
		} else {
			$this->setMessage(sprintf(JText::_('Nothing to %s.'), JText::_('unpublish')), 'error');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Archive item(s)
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function archive()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		// Retrieve checked items
		$ids = JRequest::getVar('id', array(), 'default', 'array');
		
		if (!empty($ids)) {
			// Get the model
			$model =& $this->getModel();
		
			// Archive items and set the redirect message
			if ($model->archive($ids)) {
				$this->setMessage(sprintf(JText::_('%s item(s) %s.'), count($ids), JText::_('archived')));
			} else {
				$this->setMessage(sprintf(JText::_('One or more of the selected item(s) can not be %s: %s'), JText::_('archived'), $model->getError()), 'error');
				return false;
			}
		} else {
			$this->setMessage(sprintf(JText::_('Nothing to %s.'), JText::_('archive')), 'error');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Unarchive item(s)
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function unarchive()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		// Retrieve checked items
		$ids = JRequest::getVar('id', array(), 'default', 'array');
		
		if (!empty($ids)) {
			// Get the model
			$model =& $this->getModel();
		
			// Unarchive items and set the redirect message
			if ($model->unarchive($ids)) {
				$this->setMessage(sprintf(JText::_('%s item(s) %s.'), count($ids), JText::_('unarchived')));
			} else {
				$this->setMessage(sprintf(JText::_('One or more of the selected item(s) can not be %s: %s'), JText::_('unarchived'), $model->getError()), 'error');
				return false;
			}
		} else {
			$this->setMessage(sprintf(JText::_('Nothing to %s.'), JText::_('unarchive')), 'error');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Feature item(s)
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function feature()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		// Retrieve checked items
		$ids = JRequest::getVar('id', array(), 'default', 'array');
		
		if (!empty($ids)) {
			// Get the model
			$model =& $this->getModel();
		
			// Feature items and set the redirect message
			if ($model->feature($ids)) {
				$this->setMessage(sprintf(JText::_('%s item(s) %s.'), count($ids), JText::_('featured')));
			} else {
				$this->setMessage(sprintf(JText::_('One or more of the selected item(s) can not be %s: %s'), JText::_('featured'), $model->getError()), 'error');
				return false;
			}
		} else {
			$this->setMessage(sprintf(JText::_('Nothing to %s.'), JText::_('feature')), 'error');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Unfeature item(s)
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function unfeature()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		// Retrieve checked items
		$ids = JRequest::getVar('id', array(), 'default', 'array');
		
		if (!empty($ids)) {
			// Get the model
			$model =& $this->getModel();
		
			// Unfeature items and set the redirect message
			if ($model->unfeature($ids)) {
				$this->setMessage(sprintf(JText::_('%s item(s) %s.'), count($ids), JText::_('unfeatured')));
			} else {
				$this->setMessage(sprintf(JText::_('One or more of the selected item(s) can not be %s: %s'), JText::_('unfeatured'), $model->getError()), 'error');
				return false;
			}
		} else {
			$this->setMessage(sprintf(JText::_('Nothing to %s.'), JText::_('unfeature')), 'error');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Order up an item(s)
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function orderup()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		// Retrieve checked items
		$ids = JRequest::getVar('id', array(), 'default', 'array');
		
		if (!empty($ids)) {
			$id = $ids[0];
		
			// Get the model
			$model =& $this->getModel();
		
			if ($model->orderup($id)) {
				$this->setMessage(JText::_('Item moved up.'));
			} else {
				$this->setMessage(sprintf(JText::_('Error while %s item: %s'), JText::_('ordering up'), $model->getError()), 'error');
				return false;
			}
		} else {
			$this->setMessage(sprintf(JText::_('Nothing to %s.'), JText::_('orderup')), 'error');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Order down an item(s)
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function orderdown()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		// Retrieve checked items
		$ids = JRequest::getVar('id', array(), 'default', 'array');
		
		if (!empty($ids)) {
			$id = $ids[0];
		
			// Get the model
			$model =& $this->getModel();
		
			if ($model->orderdown($id)) {
				$this->setMessage(JText::_('Item moved down.'));
			} else {
				$this->setMessage(sprintf(JText::_('Error while %s item: %s'), JText::_('ordering down'), $model->getError()), 'error');
				return false;
			}
		} else {
			$this->setMessage(sprintf(JText::_('Nothing to %s.'), JText::_('orderdown')), 'error');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Save order for item(s)
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function saveorder()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		// Retrieve checked items
		$ids	= JRequest::getVar('id', array(), 'default', 'array');
		
		if (!empty($ids)) {
			$order	= JRequest::getVar('order', array(), 'default', 'array');
		
			// Get the model
			$model =& $this->getModel();
		
			if ($model->saveorder($ids, $order)) {
				$this->setMessage(JText::_('Order saved.'));
			} else {
				$this->setMessage(sprintf(JText::_('Error while %s: %s'), JText::_('saving order'), $model->getError()), 'error');
				return false;
			}
		} else {
			$this->setMessage(sprintf(JText::_('Nothing to %s.'), JText::_('saveorder')), 'error');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Method to load and return a model object.
	 * 
	 * Overloads JoomlaController::_createModel() to squeeze our MosetsModel in
	 *
	 * @access	private
	 * @param	string  The name of the model.
	 * @param	string	Optional model prefix.
	 * @param	array	Configuration array for the model. Optional.
	 * @return	mixed	Model object on success; otherwise null on failure.
	 */
	function &_createModel($name, $prefix = '', $config = array())
	{
		$result = null;

		// Clean the model name
		$modelName	 = preg_replace('/[^A-Z0-9_]/i', '', $name);

		$result =& MosetsModel::getInstance($modelName, $this->getComponent(), $config);
		
		return $result;
	}
	
	/**
	 * Method to load and return a view object. This method first looks in the
	 * current template directory for a match, and failing that uses a default
	 * set path to load the view class file.
	 * 
	 * Overloads the JController's one to specify our own prefix
	 *
	 * Note the "name, prefix, type" order of parameters, which differs from the
	 * "name, type, prefix" order used in related public methods.
	 *
	 * @access	private
	 * @param	string	The name of the view.
	 * @param	string	Optional prefix for the view class name.
	 * @param	string	The type of view.
	 * @param	array	Configuration array for the view. Optional.
	 * @return	mixed	View object on success; null or error result on failure.
	 */
	function &_createView($name, $prefix = '', $type = '', $config = array())
	{
		require_once(MosetsApplication::getPath('view', $this->getComponent()));
		
		$prefix = ucfirst($this->getComponent()) . 'View';
		
		return parent::_createView($name, $prefix, $type, $config);
	}
	
	/**
	 * Create the filename for a resource.
	 * 
	 * Overloads JController::_createFileName() to underscorize Camelized class names
	 *
	 * @access	private
	 * @param	string	The resource type to create the filename for.
	 * @param	array	An associative array of filename information. Optional.
	 * @return	string	The filename.
	 */
	function _createFileName($type, $parts = array())
	{
		$filename = '';

		switch ($type)
		{
			case 'controller':
				$filename = MosetsInflector::underscore($parts['name']) . '.php';
				break;
			case 'view':
				if (!empty($parts['type'])) {
					$parts['type'] = '.' . $parts['type'];
				}

				$filename = MosetsInflector::underscore($parts['name']) . DS . 'view' . $parts['type'] . '.php';
				break;
		}
		
		return $filename;
	}
}
?>