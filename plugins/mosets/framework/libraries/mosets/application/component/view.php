<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	Application.Component
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * View Class
 *
 * @package		Mosets
 * @subpackage	Application.Component
 * @author		Antoine Bernier
 */

mimport('joomla.application.component.view');

class MosetsView extends JoomlaView
{
	/**
	 * The name of this view
	 *
	 * @var		string
	 * @access	protected
	 */
	var $_name = null;
	
	/**
	 * The singular name of this view
	 *
	 * @var		string
	 * @access	protected
	 */
	var $_nameSingular = null;
	
	/**
	 * The name of the component
	 *
	 * @var		string
	 * @access	protected
	 */
	var $_component = null;
	
	/**
	 * Filters
	 * 
	 * @var	array
	 */
	var $filters = array(
		'search' => array(),
		'list' => array()
	);

	/**
	 * Default layout
	 * 
	 * @var	array
	 */
	var $defaultLayout = 'default';
	
	/**
	 * Order
	 * 
	 * @var	array
	 */
	//var $order = array();
	
	/**
	 * The ordering column
	 * 
	 * @var	string
	 */
	var $ordering = null;
	
	/**
	 * The ordering direction
	 * 
	 * @var	string
	 */
	var $ordering_dir = null;
	
	/**
	 * Limitstart
	 * 
	 * @var	int
	 */
	var $limitstart = 0;
	
	/**
	 * Limit
	 * 
	 * @var	int
	 */
	var $limit = 0;
	
	/**
	 * Total
	 * 
	 * @var int
	 */
	var $total = null;
	
	/**
	 * Pagination
	 *
	 * @var JPagination
	 */
	var $pagination = null;
	
	/**
	 * Page title
	 */
	var $pageTitle = null;
	
	/**
	 * Constructor
	 * 
	 * @access	public
	 * @param	array	Associative configuration array
	 * @return	void
	 */
	function __construct($config = array())
	{
		global $mainframe, $option;
		
		// Set the singular name
		if (array_key_exists('name_singular', $config)) {
			$this->_nameSingular = $config['name_singular'];
		}
		
		// Set the component name
		if (empty($this->_component)) {
			if (array_key_exists('component', $config)) {
				$this->_component = $config['component'];
			} else {
				$this->_component = $this->getComponent();
			}
		}
		
		if (array_key_exists('default_layout', $config)) {
			$this->defaultLayout = $config['default_layout'];
		}
		
		// Set the default Component HTML helpers search path
		mimport('mosets.html.html');
		if (array_key_exists('helpers_html', $config)) {
			MosetsHTML::addIncludePath($config['helpers_html']);
		} else {
			MosetsHTML::addIncludePath(MosetsApplication::getPath('helpers_html', $this->getComponent()));
		}
		
		// Load our joomla.javascript.js if document is html
		$document	=& JFactory::getDocument();
		$user		=& JFactory::getUser();

		switch($document->getType())
		{
			case 'html':
				if ($user->get('id')) {
					$document->addScript(MOSETS_FRAMEWORK_URL . 'includes/js/joomla.javascript.js');
				}
				break;
		}
		
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'template_path' => JPath::clean(JPATH_BASE . '/components/com_' . $this->getComponent() . '/views/' . MosetsInflector::underscore(isset($config['name']) ? $config['name'] : $this->getName()) . '/tmpl')
		), $config));
		
		$this->setOrdering('', '');
		$this->setLimit($mainframe->getCfg('list_limit'));
	}
	
	/**
	 * Method to get the singular name of the view
	 *
	 * @access	public
	 * @return	string	The singular name of the view
	 */
	function getNameSingular()
	{
		return $this->_nameSingular;
	}
	
	/**
	 * Get the component name
	 *
	 * @access	public
	 * @return	string	The component name
	 */
	function getComponent()
	{
		$component = $this->_component;

		if (empty($component)) {
			$r = null;
			if (!preg_match('/(.*)View/i', get_class($this), $r)) {
				JError::raiseError (500, "MosetsView::getComponent() : Cannot get or parse class name.");
			}
			$component = strtolower($r[1]);
		}

		return $component;
	}
	
	/**
	 * Method to add a model to the view.
	 *
	 * @access	public
	 * @param	object	The model to add to the view.
	 * @param	boolean	Is this the default model?
	 * @return	object	The added model
	 */
	/*function &setModel($model_name, $default = false)
	{
		$model = MosetsModel::getInstance(strtolower($model_name));
		
		return parent::setModel($model, $default);
	}*/
	
	/**
	 * Set an array of filters
	 * 
	 * @access	public
	 * @param	array	Eg: array('search' => array('Model.field' => 'value', ...), 'list' => array('Model.field' => 'value', ...))
	 * @return	void
	 */
	function setFilters($filters)
	{
		global $mainframe, $option;
		
		$view_context = $option . '.' . MosetsInflector::underscore($this->getName()) . '.';
		
		$this->set('filters',	$mainframe->getUserStateFromRequest($view_context . 'filter', 'filter', $filters, 'array'));
		/*echo "<pre>";
		print_r($this->filters);
		echo "</pre>";*/
	}
	
	/**
	 * Set order
	 * 
	 * @access	public
	 * @param	array	Eg : array('name' => 'DESC', ...) but also with a path for compound datas : array('types-name' => 'ASC', ...)
	 * @return	void
	 */
	/*function setOrder($order)
	{
		foreach ($order as $order_field => $order_dir)
		{
			$this->order[$order_field] = $order_dir;
		}
	}*/
	
	/**
	 * Set ordering
	 * 
	 * @access	public
	 * @param	string	A field name but also a compound one, Eg : 'prop_types-name
	 * @return	void
	 */
	function setOrdering($ordering, $ordering_dir = 'ASC')
	{
		global $mainframe, $option;
		
		$view_context = $option . '.' . MosetsInflector::underscore($this->getName()) . '.';
		
		$this->set('ordering',		$mainframe->getUserStateFromRequest($view_context . 'ordering', 'ordering', (string) $ordering, 'cmd'));
		$this->set('ordering_dir',	$mainframe->getUserStateFromRequest($view_context . 'ordering_dir', 'ordering_dir', $ordering_dir, 'word'));
	}
	
	/**
	 * Set Limit
	 * 
	 * @access	public
	 * @param	string	A field name but also a compound one, Eg : 'prop_types-name
	 * @return	void
	 */
	function setLimit($limit, $limitstart = 0)
	{
		global $mainframe, $option;
		
		// The registry component context
		$component_context = $option . '.';
		// The registry view's context
		$view_context = $option . '.' . MosetsInflector::underscore($this->getName()) . '.';
		
		$this->set('limit', $mainframe->getUserStateFromRequest($component_context . 'limit', 'limit', $limit, 'int'));
		$this->set(
			'limitstart',
			$mainframe->isAdmin() ? $mainframe->getUserStateFromRequest($view_context . 'limitstart', 'limitstart', $limitstart, 'int') : JRequest::getInt('limitstart', $limitstart)
		);
		
		// In case limit has been changed, adjust limitstart accordingly
		$this->set('limitstart', $this->get('limit') != 0 ? (floor($this->get('limitstart') / $this->get('limit')) * $this->limit) : 0);
	}
	
	function setPageTitle($name)
	{
		$this->pageTitle = $name;
	}
	
	/**
	 * Generic layout display method
	 * 
	 * @return void
	 */
	function display($tpl = null)
	{
		global $mainframe;
		
		/**
		 * Executes display{Layout} method
		 */
		
		// Check it exists
		if (!method_exists($this, $method = 'display' . ucfirst($layout = $this->getLayout()))) {
			JError::raiseError(500, sprintf(get_class($this) . "::display() : Invalid call, cannot find the '%s' method in '%s' class.", $method, 'MosetsView' . ucfirst($this->getName())));
			return false;
		}
		
		// Add Mosets' styles
		$document =& JFactory::getDocument();
		$document->addStyleSheet(MOSETS_MEDIA_CSS_URL . 'styles.css');
		
		// Add some form's behaviors
		if ($layout == 'form') {
			// And some additional for form views
			MosetsHTML::_('behavior.keepalive');
			MosetsHTML::_('behavior.formvalidation');
		}
		
		// Executes it
		$this->{$method}();
		
		/**
		 * Set pagination
		 */
		
		mimport('mosets.html.pagination');
		if (is_null($this->total)) {
			if (!empty($this->_defaultModel)) {
				$this->set('total', $this->get('total', null));
			} else {
				$this->set('total', 0);
			}
		}
		$this->set('pagination', new MosetsPagination($this->get('total'), $this->get('limitstart'), $this->get('limit')));
		
		/**
		 * Set action & referer
		 */
		
		$action = JURI::getInstance();
		foreach (array('option' => 'string', 'task' => 'string', 'view' => 'string', 'layout' => 'string', 'id' => 'array', 'referer' => 'base64') as $var => $varType)
		{
			if (!array_key_exists($var, $action->getQuery(true))) {
				$value = JRequest::getVar($var, null, 'default', $varType);

				if (!empty($value)) {
					$action->setVar($var, $value);
				}
			}
		}
		$this->set('action', 'index.php' . $action->toString(array('query', 'fragment')));
		$this->set('referer', JRequest::getVar('referer', base64_encode($this->get('action')), 'default', 'base64'));
		
		/**
		 * Set document's title
		 */
		
		if (!$mainframe->isAdmin()) {
			$menus		=& JSite::getMenu();
			$menu		= $menus->getActive();
			
			$uri =& JURI::getInstance();
			// If the type we are displaying has an menu item, let's use its menu item 'page_title' param
			if (is_object($menu) && !array_diff_assoc($menu->query, $uri->getQuery(true))) {
				$menuParams	= new JParameter($menu->params);
				if ($pageTitle = $menuParams->get('page_title')) {
					$this->setPageTitle($pageTitle);
				}
			}
			
			$document->setTitle($this->pageTitle);
		}
		
		/*echo "<pre>";
		echo "view=";
		print_r($this);
		echo "</pre>";*/
		
		if (JRequest::getCmd('format') != 'pdf') {
			parent::display($tpl);
		}
	}
	
	/**
	 * Method to get data from a registered model or a property of the view
	 *
	 * @access	public
	 * @param	string	The name of the method to call on the model, or the property to get
	 * @param	string	The name of the model to reference, or the default value [optional]
	 * @param 	array	An array of arguments for the method
	 * @return	mixed	The return value of the method
	 */
	function &get($property, $default = null, $args = array())
	{
		if (func_num_args() > 1) {
			if (is_null($default)) {
				$model = $this->_defaultModel;
			} else {
				$model = $default;
			}
			
			// First check to make sure the model requested exists
			if (isset($this->_models[$model])) {			
				// Model exists, lets build the method name
				$method = 'get' . ucfirst($property);
				
				// Does the method exist?
				if (method_exists($this->_models[$model], $method)) {
					// The method exists, lets call it and return what we get
					$result = call_user_func_array(array(&$this->_models[$model], $method), $args);
					return $result;
				}
			} else {
				JError::raiseWarning(500, sprintf(get_class($this) . "::get() : The model '%s' is not registered to the current view.", $model));
			}
		}
		
		// degrade to JObject::get
		$result = JObject::get($property, $default);
		return $result;
		
	}
	
	/**
	 * Method to get the model object
	 *
	 * @access	public
	 * @param	string	$name	The name of the model (optional)
	 * @return	mixed			JModel object
	 */
	function &getModel($name = null)
	{
		if ($name === null) {
			$name = $this->_defaultModel;
		}
		return $this->_models[$name];
	}
	
	/**
	 * Method to add a model to the view.  We support a multiple model single
	 * view system by which models are referenced by classname.  A caveat to the
	 * classname referencing is that any classname prepended by JModel will be
	 * referenced by the name without JModel, eg. JModelCategory is just
	 * Category.
	 *
	 * @access	public
	 * @param	object	$model		The model to add to the view.
	 * @param	boolean	$default	Is this the default model?
	 * @return	object				The added model
	 */
	function &setModel(&$model, $default = false)
	{
		$name = $model->getName();
		$this->_models[$name] =& $model;
		
		if ($default) {
			$this->_defaultModel = $name;
		}
		return $model;
	}
	
	/**
	 * Get conditions from filters
	 * 
	 * @access	private
	 * @return	array	An indexed array, eg: array('Property.name LIKE' => '%foo%', 'Property.agent' => '2')
	 */
	function getFiltersConditions()
	{
		$filtersConditions = array();
		
		foreach ($this->filters as $type => $filters)
		{
			foreach ($filters as $field => $value)
			{
				if ($value != '') {
					switch ($type)
					{
						case 'search':
							$filtersConditions[$field . ' LIKE'] = '%' . $value . '%';
							break;
						case 'list':
							$filtersConditions[$field] = $value;
							break;
					}
				}
			}
		}
		
		/*echo "<pre>";
		echo "filters' conditions=";
		print_r($filtersConditions);
		echo "</pre>";*/
		return $filtersConditions;
	}
	
	/**
	 * Add a alternate Feed to the view
	 *
	 * @access	public
	 * @param	string	The type of the feed
	 * @param	array	An indexed array of options for the feed
	 * @return	void
	 */
	function addFeed($type = 'rss', $attribs = array())
	{
		$type = strtolower($type);
		
		$__mimeTypes = array(
			'rss' => 'application/rss+xml',
			'atom' => 'application/atom+xml'
		);
		
		$document =& JFactory::getDocument();
		$document->addHeadLink(JRoute::_('&format=feed&type=' . $type), 'alternate', 'rel', MosetsArrayHelper::array_merge_recursive_unique(array(
			'type' => $__mimeTypes[$type],
			'title' => strtoupper($type)
		), $attribs));
	}
	
	/**
	 * Create the filename for a resource
	 *
	 * @access private
	 * @param string 	$type  The resource type to create the filename for
	 * @param array 	$parts An associative array of filename information
	 * @return string The filename
	 * @since 1.5
	 */
	function _createFileName($type, $parts = array())
	{
		$filename = '';
		
		switch ($type)
		{
			case 'template' :
				$filename = MosetsInflector::underscore($parts['name']) . '.' . $this->_layoutExt;
				break;
				
			default :
				$filename = MosetsInflector::underscore($parts['name']) . '.php';
				break;
		}
		return $filename;
	}
}
?>