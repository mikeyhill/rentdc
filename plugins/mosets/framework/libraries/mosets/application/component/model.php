<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	Application.Component
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Model Class
 *
 * @abstract
 * @package		Mosets
 * @subpackage	Application.Component
 * @author		Antoine Bernier
 */

mimport('joomla.application.component.model');

class MosetsModel extends JModel
{
	/**
	 * The plural name of this model
	 *
	 * @var		string
	 * @access	protected
	 */
	var $_namePlural = null;
	
	/**
	 * The name of the component
	 *
	 * @var		string
	 * @access	protected
	 */
	var $_component = null;
	
	/**
	 * Associations
	 *
	 * @var		array
	 * @access	protected
	 */
	var $associations = array(
		'belongsTo' => array(),
		'hasOne' => array(),
		'hasMany' => array(),
		'hasAndBelongsToMany' => array()
	);
	
	/**
	 * Records offset
	 * 
	 * @var	int
	 */
	var $_limitstart = 0;
	
	/**
	 * The number of records
	 * 
	 * @var	int
	 */
	var $_limit = 0;
	
	/**
	 * 
	 * 
	 * @var	int
	 */
	var $_recursive = null;
	
	/**
	 * 
	 * 
	 * @var	int
	 */
	var $_contain = null;
	
	/**
	 * The queries generated, indexed by model
	 * 
	 * @var array
	 */
	var $_queries = array();
	
	/**
	 * Data array
	 *
	 * @var array
	 */
	var $_data = null;
	
	/**
	 * Total number of rows (without limit/limitstart)
	 *
	 * @var integer
	 */
	var $_total = null;	
	
	/**
	 * Constructor
	 *
	 * @access protected
	 * @param	An optional associative array of configuration settings.
	 * 			Recognized key values include 'name_plural', 'belongs_to', 'has_one', 'has_many' and 'has_and_belongs_to_many' (this list is not meant to be comprehensive).
	 * @return	void
	 */
	function __construct($config = array())
	{
		// Set the component name
		if (empty($this->_component)) {
			if (array_key_exists('component', $config)) {
				$this->_component = $config['component'];
			} else {
				$this->_component = $this->getComponent();
			}
		}
		
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'dbo' => MosetsFactory::getDBO(),
			'table_path' => MosetsApplication::getPath('tables', $this->getComponent())
		), $config));
		
		// Set the plural name
		if (array_key_exists('name_plural', $config)) {
			$this->_namePlural = $config['name_plural'];
		}
		
		// Set Associations
		if (array_key_exists('belongs_to', $config)) {
			$this->associations['belongsTo'] = $config['belongs_to'];
		}
		if (array_key_exists('has_one', $config)) {
			$this->associations['hasOne'] = $config['has_one'];
		}
		if (array_key_exists('has_many', $config)) {
			$this->associations['hasMany'] = $config['has_many'];
		}
		if (array_key_exists('has_and_belongs_to_many', $config)) {
			$this->associations['hasAndBelongsToMany'] = $config['has_and_belongs_to_many'];
		}
	}
	
	/**
	 * Returns a reference to the a Model object, always creating it
	 * 
	 * Overloads JoomlaModel::getInstance() to set our own model classname and to take into account our filename conventions
	 *
	 * @static
	 * @access	public
	 * @param	string	The model type to instantiate
	 * @param	string	Prefix for the model class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	mixed	A model object, or false on failure
	*/
	function &getInstance($type, $component, $config = array())
	{
		$type		= preg_replace('/[^A-Z0-9_\.-]/i', '', $type);
		$component	= preg_replace('/[^A-Z0-9_]/i', '', $component);
		$modelClass	= ucfirst($component) . 'Model' . MosetsInflector::camelize($type);
		$result		= false;

		if (!class_exists($modelClass)) {
			jimport('joomla.filesystem.path');
			$path = JPath::find(
				MosetsModel::addIncludePath(),
				MosetsModel::_createFileName('model', array('name' => $type))	// Create a model filename according to our CamelCase classnames convention
			);
			if ($path) {
				require_once(MosetsApplication::getPath('model', $component));
				require_once $path;

				if (!class_exists($modelClass)) {
					JError::raiseWarning(0, 'Model class ' . $modelClass . ' not found in file.');
					return $result;
				}
			} else {
				return $result;
			}
		}

		$result = new $modelClass(array_merge(array('component' => $component), $config));
		return $result;
	}
	
	/**
	 * Method to get the plural name of the model
	 *
	 * @access	public
	 * @return	string	The plural name of the model
	 */
	function getNamePlural()
	{
		return $this->_namePlural;
	}
	
	/**
	 * Get the component name
	 *
	 * @access	public
	 * @return	string	The component name
	 */
	function getComponent()
	{
		$component = $this->_component;

		if (empty($component)) {
			$r = null;
			if (!preg_match('/(.*)Model/i', get_class($this), $r)) {
				JError::raiseError (500, "MosetsModel::getComponent() : Cannot get or parse class name.");
			}
			$component = strtolower($r[1]);
		}

		return $component;
	}
	
	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @access	public
	 * @param	string		The table name. Optional.
	 * @param	string		The class prefix. Optional.
	 * @param	array		Configuration array for model. Optional.
	 * @return	MosetsTable
	 */
	function &getTable($name = '', $options = array())
	{
		if (empty($name)) {
			$name = $this->getNamePlural();
		}

		if ($table =& $this->_createTable($name, $options)) {
			return $table;
		}

		JError::raiseError(0, 'Table ' . $name . ' not supported. File not found.');
		$null = null;
		
        return $null;
	}
	
	/**
	 * Retrieves model's related table primary key
	 *
	 * @access	public
	 * @return	string	The primary key of the model's related table
	 */
	function getPrimaryKey()
	{
		$table =& $this->getTable();
				
		return $table->_tbl_key;
	}
	
	/**
	 * Retrieves model's related table fields
	 *
	 * @access	public
	 * @return	array	An array of fields
	 */
	function getTableFields()
	{
		$table = $this->getTable();
		$table_name = $table->getTableName();
		
		$table_fields = $this->_db->getTableFields($table_name);
		
		return $table_fields[$table_name];
	}
	
	function getAssociation($type)
	{
		return $this->associations['type'];
	}
	
	function getAssociations($types = array('belongsTo', 'hasOne', 'hasMany', 'hasAndBelongsToMany'))
	{
		$associations = array();
		
		foreach ($types as $type)
		{
			$associations[$type] = $this->getAssociation($type);
		}
	}
	
	/**
	 * Set SELECT fields
	 * 
	 * @access	public
	 * @param	array	An array of the fields to SELECT in the query
	 * 					Ex: array('id', 'name', 'Agent.name', ...)
	 * @return	void
	 */
	/*function setFields($fields, $recursion = array())
	{
		foreach ($fields as $field)
		{
			preg_match('/((\w+)\.)?(\w+)/i', $field, $matches);
			$model = $this->getInstance($modelName, $this->getComponent());
			foreach ($fields as $field)
			{
				if (array_key_exists($field, $model->getTableFields())) {
					$this->_fields[$modelName] = $field;
				}
			}
		
		}
		
		// some fields are required to map fetched datas
		if ((!is_null($this->_recursive) && (count($recursion) <= $this->_recursive || $this->_recursive < 0)) || !is_null($this->_contain)) {
			if (!empty($this->associations['hasOne']) || !empty($this->associations['hasMany'])) {
				if (!is_null($this->_contain) && count(array_intersect_key($this->_contain, array_merge($this->associations['hasOne'], $this->associations['hasMany']))) || is_null($this->_contain)) {
					$this->_fields[$this->getName()][] = $this->getPrimaryKey();
				}
			}

			foreach (array_merge($this->associations['belongsTo'], $this->associations['hasAndBelongsToMany']) as $associationName => $relation)
			{
				if (!empty($this->_contain) && !array_key_exists($associationName, $this->_contain)) {
					continue;
				}
				
				$this->_fields[$this->getName()][] = $relation['foreignKey'];
			}
		}
	}*/
	
	/**
	 * Set LIMIT
	 * 
	 * @access	public
	 * @param	int
	 * @return	void
	 */
	function setLimit($limit)
	{
		$this->_limit = (int) $limit;
	}
	
	/**
	 * Set LIMIT offset
	 * 
	 * @access	public
	 * @param	int
	 * @return	void
	 */
	function setLimitstart($limitstart)
	{
		$this->_limitstart = (int) $limitstart;
	}
	
	/**
	 * A value to control how deeply fetch datas from associated models
	 * 
	 * @access	public
	 * @param	int		Number of level(s) to fetch
	 * @return	void
	 */
	function setRecursive($recursive)
	{
		if ($recursive < 0)
			$recursive = -1;
			
		$this->_recursive = (int) $recursive;
	}
	
	/**
	 * Set contain
	 * 
	 * @access	public
	 * @param	array	the contain param
	 * @return	void
	 */
	function setContain($contain)
	{
		if (isset($contain['recursive'])) {
			unset($contain['recursive']);
		}
		
		$this->_contain = $contain;
	}
	
	/**
	 * Retrieves the data
	 * 
	 * @access	public
	 * @param	string	'all'|'count'|'first', default is 'first'
	 * @param	array 	An associative array with model's params, like : fields|where|group|order|limit
	 * @return	array	Array of objects containing the data from the database
	 */
	function getData($type = 'first', $params = array(), $recursion = array())
	{
		array_push($recursion, $this->getName());
		
		// Prevent from * RECURSION * : MosetsModel<model>::getData can be handled once per <model>
		$recursion_count = array_count_values($recursion);
		
		if ($recursion_count[$this->getName()] < 2) {
			// Set Model's params
			if (isset($params['limitstart']))							$this->setLimitstart($params['limitstart']);
			if (isset($params['limit']))								$this->setLimit($params['limit']);
			
			$recursive = ((isset($params['recursive']) && count($recursion) < 2) ? $params['recursive'] : null);
			$contain = (isset($params['contain']) ? $params['contain'] : null);
			
			$queryParams = array_intersect_key($params, array_fill_keys(array('fields', 'where', 'order', 'group'), null));
			$this->_queries[$this->getName()] = $this->_buildQuery($queryParams, $recursive, $contain);
			
			/*echo "<pre>";
			echo "queries=";
			print_r($this->_queries);
			echo "</pre>";*/
			
			/*echo "<pre>";
			echo "query=";
			print_r($this->_queries[$this->getName()]->render());
			echo "</pre>";*/
			switch ($type)
			{
				case 'all':
					$rows = $this->_getList($this->_queries[$this->getName()]->render());

					if (!empty($rows) && ((!is_null($recursive) && (count($recursion) <= $recursive || $recursive < 0)) || !is_null($contain))) {
						for ($i = 0 ; $i < count($rows) ; $i++)
						{
							$rows[$i] = $this->_associateData($rows[$i], $recursion, $recursive, $contain);
						}
					}
					break;
				case 'count':
					$rows = $this->_getCount($this->_queries[$this->getName()]->render());
					break;		
				case 'list':
					$rows = $this->_getList($this->_queries[$this->getName()]->render());
					break;
				case 'new':
					$row = $this->getTable();
					$row->load(0);
					
					$rows = array($row);
					break;
				case 'first':
				default:
					$rows = $this->_getFirst($this->_queries[$this->getName()]->render());

					if (!empty($rows) && ((!is_null($recursive) && (count($recursion) <= $recursive || $recursive < 0)) || !is_null($contain))) {
						$rows = $this->_associateData($rows, $recursion);
					}
					break;
			}

			$this->_data = $rows;
			/*echo "<pre>";
			echo "data=";
			print_r($this->_data);
			echo "</pre>";*/
			
			return $this->_data;
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * 
	 * @access	private
	 * @param	array	An indexed array of query params
	 * @param	array	
	 * @param	int		
	 * @param	array	
	 * @return	void
	 */
	function _buildQuery($params = array(), $recursive = null, $contain = null)
	{
		// Load MosetsQuery class
		mimport('mosets.database.query');
		
		$query = new MosetsQuery();
		
		// Select columns
		if (isset($params['fields'])) {
			$query->select($params['fields']);
		} else {
			$query->select($this->getName() . '.*');	// If no field specified, select 'Model.*'
		}
		
		// Select tables
		$table =& $this->getTable();
		$query->from(array($table->_tbl => $this->getName()));
		
		// Join one-to-one associations
		$this->_buildQueryJoinOneToOneAssociations($query, $recursive, $contain);
		
		// Set conditions
		if (isset($params['where'])){
			$query->where($params['where']);
		}
		
		// Set order
		if (isset($params['order'])){
			$query->orderBy($params['order']);
		}
		
		// Set group
		if (isset($params['group'])){
			$query->groupBy($params['group']);
		}
		
		return $query;
	}
	
	/**
	 * Deeply join the one-to-one associations to the query, passed as reference
	 * 
	 * @access	private
	 * @param	object	A reference to a MosetsQuery object
	 * @param	array	
	 * @param	int		
	 * @param	array	
	 * @return	void
	 */
	function _buildQueryJoinOneToOneAssociations(&$query, $recursive = null, $contain = null, $recursion = array())
	{
		$oneToOneAssociations = array_intersect_key($this->associations, array_fill_keys(array('belongsTo', 'hasOne'), null));
		
		foreach ($oneToOneAssociations as $associationType => $oneToOneAssociation)
		{
			foreach ($oneToOneAssociation as $associationModelName => $association)
			{
				array_push($recursion, $associationModelName);
				$recursion_count = array_count_values($recursion);
				
				if (
					// Prevent from infinite recursion
					$recursion_count[$associationModelName] < 2

					/**
					 * Check _recursive is ok OR _contain is set
					 */
					&&
					(
						// To be ok, _recursive should be set AND (should be >= $recursion OR can be negative)
						(
							!is_null($recursive)
							&&
							(
								count($recursion) <= $recursive
								||
								$recursive < 0
							)
						)
						// but in all cases, if _contain is set, it takes precedence over _recursive (even if it should stop)
						||
						!is_null($contain)
					)
				) {
					if (!empty($contain) && !array_key_exists($associationModelName, $contain)) {
						continue;
					}
					$associationModel =& $this->getInstance($associationModelName, $this->getComponent());
					$associationTable =& $associationModel->getTable();
				
					/**
					 * Build the join condition
					 */
				
					switch($associationType)
					{
						case 'belongsTo':
							$associationJoinCondition = $associationModelName . '.' . $associationModel->getPrimaryKey() . '=' . $this->getName() . '.' . $association['foreignKey'];
							break;
						case 'hasOne':
							$associationJoinCondition = $associationModelName . '.' . $association['foreignKey'] . '=' . $this->getName() . '.' . $this->getPrimaryKey();
							break;
					}
					$query->join(array(array(
						'type'		=> 'left',
						'table'		=> $associationTable->_tbl,
						'alias'		=> $associationModelName,
						'condition'	=> $associationJoinCondition
					)));
				
					/**
					 * Select assoctiations columns
					 */
				
					if (!empty($contain) && array_key_exists('fields', $contain[$associationModelName])) {
						$associationColumns = $contain[$associationModelName]['fields'];
					} else {
						$associationColumns = $associationModelName . '.*';
					}
					$query->select($associationColumns);
				
					/**
					 * Apply recursion
					 */
				
					$_contain = (!empty($contain) && array_key_exists('contain', $contain[$associationModelName]) ? $contain[$associationModelName]['contain'] : null);
				
					$associationModel->_buildQueryJoinOneToOneAssociations($query, $recursive, $_contain, $recursion);
				}
			}
		}
	}
	
	/**
	 * Fetch associated datas to 
	 * 
	 * @access	private
	 * @param	
	 * @return	object	
	 */
	function _associateData($row, $recursion = array(), $recursive = null, $contain = null)
	{
		// hasOne associations
		/*foreach ($this->associations['hasOne'] as $associationName => $relation)
		{
			// If 'contain' but without
			if (!empty($this->_contain) && !array_key_exists($associationName, $this->_contain)) {
				continue;
			}

			$associationModel = $this->getInstance($associationName, $this->getComponent());

			$associationParams = array(
				'where'		=> array($relation['foreignKey'] => $row->{$this->getPrimaryKey()}),
				'recursive' => $this->_recursive
			);
			if (!empty($this->_contain) && array_key_exists($associationName, $this->_contain)) {
				$associationParams = array_merge_recursive($associationParams, $this->_contain[$associationName]);
			}
			
			$associationData = $associationModel->getData('first', $associationParams, $recursion);

			if ($associationData) {
				$row->{$associationName} = $associationData;
			}
		}

		// belongsTo associations
		foreach ($this->associations['belongsTo'] as $associationName => $relation)
		{
			if (!empty($this->_contain) && !array_key_exists($associationName, $this->_contain)) {
				continue;
			}

			$associationModel = $this->getInstance($associationName, $this->getComponent());

			$associationParams = array(
				'where'		=> array($associationModel->getPrimaryKey() => $row->{$relation['foreignKey']}),
				'recursive' => $this->_recursive
			);
			if (!empty($this->_contain) && array_key_exists($associationName, $this->_contain)) {
				$associationParams = array_merge_recursive($associationParams, $this->_contain[$associationName]);
			}
			
			$associationData = $associationModel->getData('first', $associationParams, $recursion);

			if ($associationData) {
				$row->{$associationName} = $associationData;
			}
		}*/
		
		// hasMany associations
		foreach ($this->associations['hasMany'] as $associationName => $relation)
		{
			if (!empty($contain) && !array_key_exists($associationName, $contain)) {
				continue;
			}
			
			$associationModel = $this->getInstance($associationName, $this->getComponent());

			$associationParams = array(
				'where'		=> array($relation['foreignKey'] => $row->{$this->getPrimaryKey()}),
				'recursive' => $recursive
			);
			if (!empty($contain) && array_key_exists($associationName, $contain)) {
				$associationParams = array_merge_recursive($associationParams, $contain[$associationName]);
			}
			
			$associationData = $associationModel->getData('all', $associationParams, $recursion);
			$this->_queries[$associationName] = $associationModel->_queries[$associationName];
			
			if ($associationData) {
				$row->{$associationName} = $associationData;
			}
		}

		// hasAndBelongsToMany associations
		// todo
		
		return $row;
	}
	
	/**
	 * Sort _data from _order
	 * 
	 * @access	private
	 * @return	boolean
	 * @see		http://php.net/array_multisort -> example #3
	 */
	/*function _sortData()
	{
		if (is_array($this->_data) && !empty($this->_data) && !empty($this->_order)){
			// Obtain a list of columns
			$columns = array();
			foreach ($this->_data as $key => $row) {
			    foreach ($this->_order as $field_path => $direction)
			    {
					if (strpos($field_path, '.')) {
						$tmp = explode('.', $field_path);
						$model_name = MosetsInflector::camelize($tmp[0]);
						$field_name = $tmp[1];
						
						$associated_row = $this->array_key_search($model_name , $row);
						if ($associated_row && property_exists($associated_row, $field_name)) {
							$field_value = $associated_row->$field_name;
						} else {
							$this->setError(sprintf(get_class($this) . "::_sortData() : cannot order data by '%s', index not found!", $field_path));
							return false;
						}
					} else {
						if (property_exists($row, $field_path)) {
							$field_value = $row->$field_path;
						} else {
							$this->setError(sprintf(get_class($this) . "::_sortData() : cannot order data by '%s', index not found!", $field_path));
							return false;
						}						
					}
					
			        $columns[$field_path][$key] = $field_value;
			    }
			}

			// Creating a list of arguments for array_multisort
			$args = array();
			foreach ($this->_order as $field_path => $direction)
			{
			    $key = $columns[$field_path];
			
				// Because array_multisort is case sensible, don't forget to strtolower the array
				$key = array_map('strtolower', $key);
				
				$args[] = $key;

				switch ($direction)
				{
					case 'DESC':
					case 'desc':
						$sort_flag = SORT_DESC;
						break;
					default:
						$sort_flag = SORT_ASC;
						break;
				}
				$args[] = $sort_flag;
			}

			array_push($args, &$this->_data);

			call_user_func_array('array_multisort', $args);
			
			return true;
		}
	}*/
	
	/**
	 * Add a directory where MosetsModel should search for models.
	 * You may either pass a string or an array of directories.
	 * 
	 * Overloads JoomlaModel::addIncludePath() to add our component's models/ directory by default
	 *
	 * @access	public
	 * @param	string	A path to search.
	 * @return	array	An array with directory elements
	 */
	/*function addIncludePath($path = '')
	{
		$default_path = JPath::clean(JPATH_ADMINISTRATOR . '/components/com_' . $this->getComponent() . '/models');
		
		if (!in_array($default_path, parent::addIncludePath())) {
			parent::addIncludePath($default_path);
		}
		
		return parent::addIncludePath($path);
	}*/
	
	/**
	 * Adds to the stack of model table paths in LIFO order.
	 * 
	 * Overload Joomla::addTablePath() to squeeze our MosetsTable in
	 *
	 * @static
	 * @param	string|array The directory (-ies) to add.
	 * @return	void
	 */
	function addTablePath($path)
	{
		mimport('mosets.database.table');
		MosetsTable::addIncludePath($path);
	}
	
	/**
	 * Returns an object list of rows
	 *
	 * @access	private
	 * @param	string	The query
	 * @return	array
	 */
	function &_getList($query)
	{
		$this->_db->setQuery($query, $this->_limitstart, $this->_limit);
		$rows = $this->_db->fetch();
		
		for ($i = 0; $i < count($rows); $i++)
		{
			$row =& $rows[$i];
			
			$row = $this->_rearrangeFetchedRow($row);
		}
		
		return $rows;
	}
	
	/**
	 * Rearrange the flat row (indexed by model name) returned by MosetsDatabase::fecth into a mapped object
	 */
	function _rearrangeFetchedRow($row)
	{
		$result = array();
		
		if (array_key_exists($this->getName(), $row)) {
			$result = MosetsArrayHelper::cut_element($this->getName(), $row);
			
			// If the current model has one-to-one associations
			if (array_intersect_key($oneToOneAssociations = array_merge($this->associations['belongsTo'], $this->associations['hasOne']), $row)) {
				foreach ($oneToOneAssociations as $associationModelName => $association)
				{
					if (array_key_exists($associationModelName, $row)) {
						$associationModel =& $this->getInstance($associationModelName, $this->getComponent());
						$result[$associationModelName] = $associationModel->_rearrangeFetchedRow($row);
					}
				}
			}
		}
		
		return JArrayHelper::toObject($result);
	}
	
	/**
	 * Returns the number of rows
	 *
	 * @access	private
	 * @param	string The query
	 * @return	array
	 */
	function &_getCount($query)
	{
		$this->_db->setQuery($query, $this->_limitstart, $this->_limit);
		$result = $this->_db->loadResult();
		
		return $result;
	}
	
	/**
	 * Returns an object row
	 *
	 * @access	private
	 * @param	string	The query
	 * @return	array
	 */
	function &_getFirst($query)
	{
		$list = $this->_getList($query, 0, 1);
		
		return $list[0];
	}
	
	/**
	 * 
	 * 
	 * @access	public
	 * @return	int
	 */
	function getTotal()
	{
		if (empty($this->_total)) {
			$this->_total = $this->_getListCount($this->_queries[$this->getName()]->render());
		}
		
		return $this->_total;
	}
	
	/**
	 * Run a custom query
	 * 
	 * @access	public
	 * @param	string	The query to run
	 * @return	boolean	True if success
	 */
	function query($query)
	{
		$row =& $this->getTable();
		$row->_db->setQuery($query);
		
		if (!$row->_db->query()) {
			$this->setError($row->getError());
			return false;
		}
		
		return true;
	}
	
	/**
	 * Save
	 * 
	 * @access	public
	 * @param	indexed	An array of datas, indexed by primary key value
	 * @return	boolean	True if success
	 */
	function save($datas, $files = null)
	{
		/*echo "<pre>";
		echo "datas=";
		print_r($datas);
		echo "</pre>";

		echo "<pre>";
		echo "files=";
		print_r($files);
		echo "</pre>";*/

		if (!empty($datas)) {

			/**
			 * First, let's save current model's related datas
			 */

			$row = $this->getTable();
			$affected_rows_ids = array();
			foreach ($datas[$this->getName()] as $row_id => $row_data)
			{
				$row_files = null;
				if (!empty($files[$this->getName()][$row_id]))
					$row_files = $files[$this->getName()][$row_id];

				if (!($row->save($row_id, $row_data, $row_files))) {
					$this->setError($row->getError());
					return false;
				}
				$affected_rows_ids[$row_id] = $row->{$this->getPrimaryKey()};
			}
			
			if (!($row->reorder())) {
				$this->setError($row->getError());
				return false;
			}

			/*echo "<pre>";
			echo "affected_rows_ids=";
			print_r($affected_rows_ids);
			echo "</pre>";*/

			/**
			 * Then, let's save external models' datas
			 */

			foreach ($datas as $model_name => $model_datas)
			{
				if ($model_name != $this->getName() && $model = $this->getInstance($model_name, $this->getComponent())) {

					//Let's replace foreign keys values with the corresponding affected row id
					if (array_key_exists($model_name, $associations = array_merge($this->associations['hasOne'], $this->associations['hasMany']))) {

						$foreign_key = $associations[$model_name]['foreignKey'];

						foreach ($model_datas as $model_data_k => $model_data_v)
						{
							$model_datas[$model_data_k][$foreign_key] = $affected_rows_ids[ $model_data_v[$foreign_key] ];
						}
					}

					$model_files = !empty($files[$model_name]) ? $files[$model_name] : null;
					if (!$model->save(array($model_name => $model_datas), array($model_name => $model_files))) {
						$this->setError($model->getError());
						return false;
					}

				}
			}

		} else {
			$this->setError(get_class($this) . '::save() : Argument #1 cannot be empty !');
			return false;
		}

		return true;
	}
	
	/**
	 * Remove
	 * 
	 * @access	public
	 * @param	array	An array of ids
	 * @return	boolean	True if success
	 */
	function remove($ids)
	{
		if (!empty($ids)) {
			$row =& $this->getTable();
			
			foreach ($ids as $id)
			{
				//echo "Let's remove " . $this->getName() . "[" . $id . "]<br />";
		
				// First, recursively delete associated records (if there are any)
				$associations = array_merge($this->associations['hasOne'], $this->associations['hasMany']);
				/*echo "<pre>";
				echo "associations=";
				print_r($associations);
				echo "</pre>";*/
				if (!empty($associations)) {
					foreach ($associations as $associationName => $relation)
					{
						if (array_key_exists('dependent', $relation) && $relation['dependent']) {
							//echo "But first, let's remove the associated " . $associationName . "...<br />";
							
							$model = $this->getInstance($associationName, $this->getComponent());
						
							$associated_rows = $model->getData('all', array(
								'fields'	=> array($model->getPrimaryKey()),
								'where'		=> array($relation['foreignKey'] => $id),
								'recursive'	=> 0
							));
							/*echo "<pre>";
							echo "associated_rows=";
							print_r($associated_rows);
							echo "</pre>";*/
						
							if (!empty($associated_rows)) {
								//echo "I've found " . count($associated_rows) . " " . $associationName . " to be removed<br />";
								if ($relation['dependent'] === 'warn') {
									//echo "But you tell me not to delete them :(";
									$this->setError(sprintf(JText::_("Please remove all %s before proceeding."), strtolower(JText::_ ($model->getName()))));
									//echo get_class($this) . '::remove() : Please remove every ' . $model->getName() . ' dependencies before!';
									return false;
								} else {
									$associated_ids = array();
									foreach ($associated_rows as $associated_row)
									{
										$associated_ids[] = $associated_row->{$model->getPrimaryKey()};
									}

									if (!$model->remove($associated_ids)) {
										$this->setError($model->getError());
										return false;
									}
								}
							}/* else {
								echo "OK, they are no associated " . $associationName . "...<br />";
							}*/
						}
					}
				}
				
				// Delete given records
				if (!$row->remove($id)) {
					$this->setError($row->getError());
					return false;
				}
			}
		} else {
			$this->setError(get_class($this) . '::publish() : Argument #1 cannot be empty !');
			return false;
		}
		
		return true;
	}	
	
	/**
	 * Publish
	 * 
	 * @access	public
	 * @param	array	An array of ids
	 * @return	boolean	True if success
	 */
	function publish($ids)
	{	
		if (!empty($ids)) {
			$row =& $this->getTable();
		
			if (!$row->publish($ids)) {
				$this->setError($row->getError());
				return false;
			}
		} else {
			$this->setError(get_class($this) . '::publish() : Argument #1 cannot be empty !');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Unublish
	 * 
	 * @access	public
	 * @param	array	An array of ids
	 * @return	boolean	True if success
	 */
	function unpublish($ids)
	{	
		if (!empty($ids)) {
			$row =& $this->getTable();
		
			if (!$row->publish($ids, 0)) {
				$this->setError($row->getError());
				return false;
			}
		} else {
			$this->setError(get_class($this) . '::unpublish() : Argument #1 cannot be empty !');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Archive
	 * 
	 * @access	public
	 * @param	array	An array of ids
	 * @return	boolean	True if success
	 */
	function archive($ids)
	{	
		if (!empty($ids)) {
			$row =& $this->getTable();
		
			if (!$row->publish($ids, -1)) {
				$this->setError($row->getError());
				return false;
			}
		} else {
			$this->setError(get_class($this) . '::archive() : Argument #1 cannot be empty !');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Unarchive
	 * 
	 * @access	public
	 * @param	array	An array of ids
	 * @return	boolean	True if success
	 */
	function unarchive($ids)
	{		
		return $this->unpublish($ids);
	}
	
	/**
	 * Feature
	 * 
	 * @access	public
	 * @param	array	An array of ids
	 * @return	boolean	True if success
	 */
	function feature($ids)
	{
		if (!empty($ids)) {
			$row =& $this->getTable();
		
			foreach ($ids as $id)
			{
				if (!$row->feature($id, 1)) {
					$this->setError($row->getError());
					return false;
				}
			}
		} else {
			$this->setError(get_class($this) . '::feature() : Argument #1 cannot be empty !');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Unfeature
	 * 
	 * @access	public
	 * @param	array	An array of ids
	 * @return	boolean	True if success
	 */
	function unfeature($ids)
	{
		if (!empty($ids)) {
			$row =& $this->getTable();
		
			foreach ($ids as $id)
			{
				if (!$row->feature($id, 0)) {
					$this->setError($row->getError());
					return false;
				}
			}
		} else {
			$this->setError(get_class($this) . '::unfeature() : Argument #1 cannot be empty !');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Approve
	 * 
	 * @access	public
	 * @param	array	An array of ids
	 * @return	boolean	True if success
	 */
	function approve($ids)
	{
		
		if (!empty($ids)) {
			$row =& $this->getTable();
		
			foreach ($ids as $id)
			{
				if (!$row->approve($id)) {
					$this->setError($row->getError());
					return false;
				}
			}
		} else {
			$this->setError(get_class($this) . '::approve() : Argument #1 cannot be empty !');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Order Up
	 * 
	 * @access	public
	 * @param	array	An array of ids
	 * @return	boolean	True if success
	 */
	function orderup($id)
	{
		if (!empty($id)) {
			$row =& $this->getTable();
			
			if (!$row->orderup($id)) {
				$this->setError($row->getError());
				return false;
			}
		} else {
			$this->setError(get_class($this) . '::orderup() : Argument #1 cannot be empty !');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Order Down
	 * 
	 * @access	public
	 * @param	array	An array of ids
	 * @return	boolean	True if success
	 */
	function orderdown($id)
	{
		if (!empty($id)) {
			$row =& $this->getTable();
			
			if (!$row->orderdown($id)) {
				$this->setError($row->getError());
				return false;
			}
		} else {
			$this->setError(get_class($this) . '::orderdown() : Argument #1 cannot be empty !');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Order featured properties
	 * 
	 * @access	public
	 * @param	array	An array of ids	
	 * @return	boolean	True if success
	 */
	function saveorder($ids, $order)
	{
		if (!empty($ids)) {
			$row =& $this->getTable();
			
			foreach ($ids as $id_k => $id_v)
			{
				if (!$row->load($id_v)) {
					$this->setError($row->getError());
					return false;
				}
				
				// Set the new order
				$row->set('ordering', $order[$id_k]);
				
				// Update order in database
				if (!$row->store()) {
					$this->setError($row->getError());
					return false;
				}
			}
			
			// Reordering
			$row->reorder();
		} else {
			$this->setError(get_class($this) . '::saveorder() : Argument #1 cannot be empty !');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Increase hits +1
	 * 
	 * @access	public
	 * @param	int			
	 * @return	boolean	True if success
	 */
	function hit($id)
	{
		if (!empty($id)) {
			$row =& $this->getTable();
			
			if (!$row->hit($id)) {
				$this->setError($row->getError());
				return false;
			}
		} else {
			$this->setError(get_class($this) . '::hit() : Argument #1 cannot be empty !');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Method to load and return a model object.
	 * 
	 * Overloads JoomlaTable::_createTable() to squeeze our MosetsTable in
	 *
	 * @access	private
	 * @param	string	The name of the view
	 * @param	string	The class prefix. Optional.
	 * @return	mixed	Model object or boolean false if failed
	 */
	function &_createTable($name, $config = array())
	{
		$result = null;

		// Clean the model name
		$name	= preg_replace('/[^A-Z0-9_]/i', '', $name);

		//Make sure we are returning a DBO object
		if (!array_key_exists('dbo', $config))  {
			$config['dbo'] =& $this->getDBO();;
		}

		$instance =& MosetsTable::getInstance($name, $this->getComponent(), $config);
		
		return $instance;
	}
	
	/**
	 * Create the filename for a resource
	 * 
	 * Overloads JoomlaTable::_createFileName() to take into account our filename conventions
	 *
	 * @access	private
	 * @param	string 	$type  The resource type to create the filename for
	 * @param	array 	$parts An associative array of filename information
	 * @return	string The filename
	 * @since	1.5
	 */
	function _createFileName($type, $parts = array())
	{
		$filename = '';

		switch ($type)
		{
			case 'model':
				$filename = MosetsInflector::underscore($parts['name']) . ".php";
				break;
		}
		return $filename;
	}
}

/**
 * 
 * @abstract
 */
class MosetsAssociation extends JObject
{
	/**
	 * The association's source
	 *
	 * @var		string
	 * @access	protected
	 */
	var $_src = null;
	
	/**
	 * The association's destination
	 *
	 * @var		string
	 * @access	protected
	 */
	var $_dst = null;
	
	/**
	 * The association's type
	 *
	 * @var		string
	 * @access	protected
	 */
	var $_type = null;
	
	/**
	 * The association's foreign key
	 *
	 * @var		string
	 * @access	protected
	 */
	var $_foreignKey = null;
	
	/**
	 * The allowed association's types
	 *
	 * @var		string
	 * @access	private
	 */
	var $__validTypes = array('belongsTo', 'hasOne', 'hasMany', 'hasAndBelongsToMany');
	
	function __construct($src, $dst, $type, $config = array())
	{
		$this->_src = $src;
		$this->_dst = $dst;
		
		if (in_array($type, $this->__validTypes)) {
			$this->_type = $type;
		} else {
			$this->setError(get_class($this) . "::__construct(): invalid association's type.");
			return false;
		}
		
		if (array_key_exists('foreign_key', $config)) {
			$this->_foreignKey = $config['foreign_key'];
		} else {
			switch($this->_type)
			{
				case 'belongsTo':
					$foreignKey = sprintf('%s_id', $this->_dst);
					break;
				case 'hasOne':
				case 'hasMany':
					$foreignKey = sprintf('%s_id', $this->_src);
					break;
			}
			
			$this->_foreignKey = $foreignKey;
		}
	}
	
	function getSrc()
	{
		return $this->_src;
	}
	
	function getDst()
	{
		return $this->_dst;
	}
	
	function getType()
	{
		return $this->_type;
	}
	
	function getForeignKey()
	{
		return $this->_foreignKey;
	}
}

/**
 * 
 * @abstract
 */
class MosetsAssociationBelongsTo extends MosetsAssociation
{
	//var $_conditions = array();
	
	//var $_fields = array();
	
	//var $_orders = array();
	
	function __construct($src, $dst, $config)
	{
		parent::__construct($src, $dst, 'belongsTo', $config);
	}
}

/**
 * 
 * @abstract
 */
class MosetsAssociationHasOne extends MosetsAssociation
{
	var $_dependent = null;
	
	function __construct($src, $dst, $config)
	{
		parent::__construct($src, $dst, 'hasOne', $config);
		
		if (array_key_exists('dependent', $config)) {
			$this->_dependent = $config['dependent'];
		}
	}
}

/**
 * 
 * @abstract
 */
class MosetsAssociationHasMany extends MosetsAssociation
{
	var $_dependent = null;
	
	function __construct($src, $dst, $config)
	{
		parent::__construct($src, $dst, 'hasMany', $config);
		
		if (array_key_exists('dependent', $config)) {
			$this->_dependent = $config['dependent'];
		}
	}
}
?>