<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	Application
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Application Class
 *
 * @package		Mosets
 * @subpackage	Application
 * @author		Antoine Bernier
 */

class MosetsApplication extends JObject
{
	/**
	 * The name of the application, eg the component
	 *
	 * @var		array
	 * @access	protected
	 */
	var $_name = null;
	
	/**
	 * The application's configuration
	 * 
	 * @var	JParameters
	 * @access	protected
	 */
	var $_configuration = null;
	
	/**
	 * The application table prefix
	 * @var		string
	 * @access	protected
	 */
	var $_table_prefix = null;
	
	/**
	 * The application default views, for site and admin
	 * 
	 * @var		array
	 * @access	protected
	 */
	var $_default_views = array(
		'site'	=> null,
		'admin'	=> null
	);

	/**
	 * The scope of the application
	 *
	 * @var		string
	 * @access	public
	 */
	var $scope = null;
	
	var $_paths = array();
	
	/**
	 * Constructor
	 *
	 * @access	protected
	 * @param	An optional associative array of configuration settings.
	 * 			Recognized key values include 'name', 'table_prefix', 'default_views' (this list is not meant to be comprehensive).
	 * @return	void
	 */
	function __construct($config = array())
	{
		// Set the application's name
		if (empty($this->_name)) {
			if (array_key_exists('name', $config)) {
				$this->_name = $config['name'];
			} else {
				$this->_name = $this->getName();
			}
		}

		// Set the application's configuration
		$this->_configuration =& $this->getConfiguration();
		
		// Set the application's table prefix
		if (empty($this->_table_prefix)) {
			if (array_key_exists('table_prefix', $config)) {
				$this->_table_prefix = $config['table_prefix'];
			} else {
				$this->_table_prefix = $this->getName();
			}
		}
		
		// Set the application's default views
		if (empty($this->_default_views['site']) || empty($this->_default_views['admin'])) {
			if (array_key_exists('default_views', $config)) {
				if (array_key_exists('site', $config['default_views'])) {
					$this->_default_views['site'] = $config['default_views']['site'];
				}
				if (array_key_exists('admin', $config['default_views'])) {
					$this->_default_views['admin'] = $config['default_views']['admin'];	
				}
			} else {
				$this->_default_views['site'] = $this->_default_views['admin'] = MOSETS_DEFAULT_VIEW;
			}
		}
		
		// Set the application's scope
		// todo
		
		/**
		 * Set the application's default paths
		 */
		
		$this->_paths['base']					= JPath::clean(JPATH_BASE . '/components/com_' . $this->getName());
		$this->_paths['base_url']				= JURI::base() . 'components/com_' . $this->getName() . '/';
		
		$this->_paths['site']					= JPath::clean(JPATH_SITE . '/components/com_' . $this->getName());
		$this->_paths['site_url']				= JURI::root() . 'components/com_' . $this->getName() . '/';
		$this->_paths['administrator']			= JPath::clean(JPATH_ADMINISTRATOR . '/components/com_' . $this->getName());
		$this->_paths['administrator_url']		= JURI::root() . 'administrator/components/com_' . $this->getName() . '/';
		
		$this->_paths['classes']				= JPath::clean($this->_paths['administrator'] . '/classes');
		
		$this->_paths['includes']				= JPath::clean($this->_paths['administrator'] . '/includes');
		$this->_paths['includes_application']	= JPath::clean($this->_paths['administrator'] . '/includes/application.php');
		$this->_paths['includes_defines']		= JPath::clean($this->_paths['administrator'] . '/includes/defines.php');
		
		$this->_paths['controller']				= JPath::clean($this->_paths['administrator'] . '/controller.php');
		$this->_paths['controllers']			= JPath::clean($this->_paths['base'] . '/controllers');
		
		$this->_paths['model']					= JPath::clean($this->_paths['administrator'] . '/model.php');
		$this->_paths['models']					= JPath::clean($this->_paths['administrator'] . '/models');
		
		$this->_paths['table']					= JPath::clean($this->_paths['administrator'] . '/table.php');
		$this->_paths['tables']					= JPath::clean($this->_paths['administrator'] . '/tables');
		
		$this->_paths['view']					= JPath::clean($this->_paths['administrator'] . '/view.php');
		$this->_paths['views']					= JPath::clean($this->_paths['base'] . '/views');
		
		$this->_paths['helpers']					= JPath::clean($this->_paths['base'] . '/helpers');
		$this->_paths['helpers_html']			= JPath::clean($this->_paths['administrator'] . '/helpers/html');
		
		$this->_paths['assets']					= JPath::clean($this->_paths['administrator'] . '/assets');
		$this->_paths['assets_url']				= $this->_paths['administrator_url'] . 'assets/';
		$this->_paths['assets_css']				= JPath::clean($this->_paths['assets'] . '/css');
		$this->_paths['assets_css_url']			= $this->_paths['assets_url'] . 'css/';
		$this->_paths['assets_images']			= JPath::clean($this->_paths['assets'] . '/images');
		$this->_paths['assets_images_url']		= $this->_paths['assets_url'] . 'images/';
		$this->_paths['assets_js']				= JPath::clean($this->_paths['assets'] . '/js');
		$this->_paths['assets_js_url']			= $this->_paths['assets_url'] . 'js/';
		
		$this->_paths['media']					= JPath::clean(JPATH_ROOT . '/media/com_' . $this->getName());
		$this->_paths['media_url']				= JURI::root() . 'media/com_' . $this->getName() . '/';
		$this->_paths['media_css']				= JPath::clean($this->_paths['media'] . '/css');
		$this->_paths['media_css_url']			= $this->_paths['media_url'] . 'css/';
		$this->_paths['media_images']			= JPath::clean($this->_paths['media'] . '/images');
		$this->_paths['media_images_url']		= $this->_paths['media_url'] . 'images/';
		$this->_paths['media_js']				= JPath::clean($this->_paths['media'] . '/js');
		$this->_paths['media_js_url']			= $this->_paths['media_url'] . 'js/';
		
		// Set custom paths from config
		if (array_key_exists('paths', $config)) {
			$this->_paths = MosetsArrayHelper::array_merge_recursive_unique($this->_paths, $config['paths']);
		}
		
		// Import Application's plugins
		JPluginHelper::importPlugin($this->getName());
	}
	
	/**
	 * Returns a reference to the global MosetsApplication object, only creating it if it doesn't already exist.
	 *
	 * @access	public
	 * @param	mixed				A client name.
	 * @param	array				An optional associative array of configuration settings.
	 * @return	MosetsApplication	The application object.
	 */
	function &getInstance($client, $prefix = 'Mosets', $config = array())
	{
		static $instances;

		if (!isset($instances)) {
			$instances = array();
		}
		
		$client = strtolower($client);

		if (empty($instances[$client])) {
			jimport('joomla.filesystem.file');
			if (JFile::exists($path = JPath::clean(JPATH_ADMINISTRATOR . '/components/com_' . $client . '/includes/application.php'))) {
				require_once $path;

				$classname = ucfirst($prefix) . 'Application' . ucfirst($client);
				$instance = new $classname($config);
			} else {
				$error = JError::raiseError(500, 'Unable to load Mosets application: ' . $client);
				return $error;
			}

			$instances[$client] =& $instance;
		}

		return $instances[$client];
	}
	
	/**
	 * Method to get the application name
	 *
	 * The Application name by default parsed using the classname, or it can be set by passing a $config['name'] in the class constructor
	 *
	 * @access	public
	 * @return	string	The name of the Application
	 */
	function getName()
	{
		$name = $this->_name;

		if (empty($name))
		{
			$r = null;
			if (!preg_match('/Application(.*)/i', get_class($this ), $r)) {
				JError::raiseError(500, get_class($this) . "::getName() : Can\'t get or parse class name.");
			}
			$name = strtolower($r[1]);
		}

		return $name;
	}
		
	/**
	 * Get an application's constant, eg: <APPLICATION>_<NAME>
	 * 
	 * @static
	 * @access	public
	 * @param	string		The name of the constant
	 * @param	string		The application name.
	 * @return	constant	The value of the constant
	 */
	function getPath($name, $application)
	{		
		$application =& MosetsApplication::getInstance($application);
		
		return $application->_paths[$name];
	}
	
	/**
	 * Get the application's configuration
	 * 
	 * @access	public
	 * @return	JParameter	A JParameter Object
	 */
	function &getConfiguration()
	{
		global $mainframe;
		
		$configuration = $this->_configuration;
		
		if (!isset($configuration)) {
			$configuration =& JComponentHelper::getParams('com_' . $this->getName());
			if (!$mainframe->isAdmin()) {
				$configuration->merge($mainframe->getPageParameters());
			}
		}
		
		return $configuration;
	}
	
	function getCfg($name)
	{
		$configuration =& $this->getConfiguration();
		
		return $configuration->get($name);
	}
	
	function setCfg($name, $value)
	{
		$configuration =& $this->getConfiguration();
		
		return $configuration->set($name, $value);
	}
	
	/**
	 * Get an application's controller
	 * 
	 * @access	public
	 * @param	string					The controller type
	 * @return	<application>Controller
	 */
	function &getController($type = '')
	{
		global $mainframe;
		
		if (empty($type)) {
			$type = JRequest::getCmd('controller', JRequest::getCmd('view', $mainframe->isAdmin() ? $this->_default_views['admin'] : $this->_default_views['site']));
		}
		
		// Add application's controllers path
		mimport('mosets.application.component.controller');
		MosetsController::addIncludePath($this->_paths['controllers']);
		
		return MosetsController::getInstance($type, $this->getName());
	}
	
	/**
	 * Get an application's model
	 * 
	 * @access	public
	 * @param	string				The model type
	 * @return	<application>Model
	 */
	function &getModel($type)
	{
		// Add application's models path
		mimport('mosets.application.component.model');
		MosetsModel::addIncludePath($this->_paths['models']);
		
		return MosetsModel::getInstance($type, $this->getName());
	}
	
	/**
	 * Get an application's table
	 * 
	 * @access	public
	 * @param	string				The table type
	 * @return	<application>Table
	 */
	function &getTable($type)
	{
		// Add application's tables path
		mimport('mosets.database.table');
		MosetsTable::addIncludePath($this->_paths['tables']);
		
		return MosetsTable::getInstance($type, $this->getName());
	}
}
?>