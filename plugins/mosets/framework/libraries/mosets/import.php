<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Mosets Libraries import
 *
 * @package		Mosets
 * @author		Lee Cher Yeong <framework@mosets.com>
 * @author		Antoine Bernier
 */

jimport('joomla.filesystem.file');

// Load Mosets' loader
if (JFile::exists($path = JPath::clean(MOSETS_LIBRARIES . '/loader.php'))) {
	require_once($path);
} else {
	JError::raiseError(404, 'Cannot load Mosets\' loader, located at : ' . $path);
}

mimport('mosets.factory');
mimport('mosets.utilities.inflector');
mimport('mosets.utilities.arrayhelper');
?>