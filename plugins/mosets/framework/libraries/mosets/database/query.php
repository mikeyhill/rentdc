<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	Database
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Query Class
 *
 * @package		Mosets
 * @subpackage	Database
 * @author		Antoine Bernier
 */

class MosetsQuery extends JObject
{
	/**
	 * The columns to SELECT
	 *
	 * @var		array
	 * @access	protected
	 */
	var $_columns = array();
	
	/**
	 * The tables to select FROM
	 *
	 * @var		array
	 * @access	protected
	 */
	var $_tables = array();
	
	/**
	 * The JOIN statements
	 *
	 * @var		array
	 * @access	protected
	 */
	var $_joins = array();
	
	/**
	 * The WHERE conditions
	 * 
	 * @var		array
	 * @access	protected
	 */
	var $_conditions = array();
	
	/**
	 * The columns to GROUP BY
	 *
	 * @var		array
	 * @access	protected
	 */
	var $_groups = array();
	
	/**
	 * The columns to ORDER BY
	 *
	 * @var		array
	 * @access	protected
	 */
	var $_orders = array();
	
	/**
	 * DB object
	 *
	 * @var		JDatabase
	 * @access	protected
	 */
	var $_db = null;
	
	/**
	 * Constructor
	 *
	 * @access protected
	 * @param	array	An optional associative array of configuration settings.
	 * @return	void
	 */
	function __construct($config = array())
	{
		$this->_db =& MosetsFactory::getDBO();
		
		if (array_key_exists('select', $config))	$this->select($config['select']);
		if (array_key_exists('from', $config))		$this->from($config['from']);
		if (array_key_exists('join', $config))		$this->join($config['join']);
		if (array_key_exists('where', $config))		$this->where($config['where']);
		if (array_key_exists('group_by', $config))	$this->groupBy($config['group_by']);
		if (array_key_exists('order_by', $config))	$this->orderBy($config['order_by']);
	}
	
	/**
	 * Add a column
	 * 
	 * @access	public
	 * @param	array	An array of columns, eg: array('Property.*', 'Agent.name', ...)
	 * @return	string
	 */
	function select($columns)
	{
		$this->_columns = array_unique(array_merge($this->_columns, (array) $columns));
	}
	
	/**
	 * Add a table
	 * 
	 * @access	public
	 * @param	array	Eg: array('jos_hp_properties' => 'Property', ...)
	 * @return	string
	 */
	function from($tables)
	{
		$this->_tables = array_unique(array_merge($this->_tables, $tables));
	}
	
	/**
	 * Add a join clause
	 * 
	 * @access	public
	 * @param	array	Eg: array(array('type' => 'left|inner|outer', 'table' => 'jos_hp_agents', 'alias' => 'Agent', 'condition' => 'Agent.id=Property.agent'), ...)
	 * @return	string
	 */
	function join($joins)
	{
		$this->_joins = array_merge($this->_joins, $joins);
	}
	
	/**
	 * Add a where condition
	 * 
	 * @access	public
	 * @param	array	Eg: array(
	 * 							'Property.published' => 1,
	 * 							'Agent.id' => array(1, 2, 3),
	 * 							'Property.hits BETWEEN' => array(1, 2),	
	 * 							array(
	 * 								'OR' => array(
	 * 									'Property.publish_up' => '0000-00-00 00:00:00',
	 * 									'Property.publish_up <=' => '2009-07-28 18:20:00'
	 * 								)
	 * 							),
	 * 							array(
	 * 								'OR' => array(
	 * 									'Property.publish_down' => '0000-00-00 00:00:00',
	 * 									'Property.publish_down >=' => '2009-07-28 18:20:00'
	 * 								)
	 * 							),
	 * 							...
	 * 						)
	 * @return	string
	 */
	function where($conditions)
	{
		$this->_conditions = MosetsArrayHelper::array_merge_recursive_unique($this->_conditions, $conditions);
	}
	
	/**
	 * Add a group column
	 * 
	 * @access	public
	 * @param	array	Eg: array('Property.id', ...)
	 * @return	string
	 */
	function groupBy($groups)
	{
		$this->_groups = array_unique(array_merge($this->_groups, (array) $groups));
	}
	
	/**
	 * Add an order column
	 * 
	 * @access	public
	 * @param	array	Eg: array('Agent.id' => 'ASC|DESC', ...)
	 * @return	string
	 */
	function orderBy($orders)
	{
		$this->_orders = array_unique(array_merge($this->_orders, $orders));
	}
	
	/**
	 * Render the query
	 * 
	 * @access	public
	 * @return	string
	 */
	function render()
	{
		$query = "";
		
		$query .= $this->_renderSelect();
		$query .= $this->_renderFrom();
		
		if (!empty($this->_joins)) {
			$query .= $this->_renderJoin();
		}
		if (!empty($this->_conditions)) {
			$query .= $this->_renderWhere();
		}
		if (!empty($this->_groups)) {
			$query .= $this->_renderGroupBy();
		}
		if (!empty($this->_orders)) {
			$query .= $this->_renderOrderBy();
		}
		
		return $query;
	}
	
	/**
	 * Render the SELECT fragment
	 * 
	 * @access	private
	 * @return	string
	 */
	function _renderSelect()
	{
		$columns = array();
		if (!empty($this->_columns)) {
			foreach ($this->_columns as $column)
			{
				preg_match('/((\w+)\.)?(\w+|\*)/i', $column, $matches);
				$columns[] = (!empty($matches[1]) ? $this->_db->nameQuote($matches[2]) . '.' : '') . ($matches[3] != '*' ? $this->_db->nameQuote($matches[3]) : $matches[3]);
			}
		} else {
			$columns[] = '*';
		}
		
		return "SELECT " . implode(', ', $columns);
	}
	
	/**
	 * Render the FROM fragment
	 * 
	 * @access	private
	 * @return	string
	 */
	function _renderFrom()
	{
		$tables = array();
		foreach ($this->_tables as $table => $alias)
		{
			$tables[] = $this->_db->nameQuote($table) . ' AS ' . $this->_db->nameQuote($alias);
		}
		
		return " FROM " . implode(', ', $tables);
	}
	
	/**
	 * Render the JOIN fragment
	 * 
	 * @access	private
	 * @return	string
	 */
	function _renderJoin()
	{
		$joins = "";
		foreach ($this->_joins as $join)
		{
			preg_match('/((\w+)\.(\w+))\s*=\s*((\w+)\.(\w+))/i', $join['condition'], $matches);
			$joins .= "\n" . (array_key_exists('type', $join) ? strtoupper($join['type']) . " " : "") . "JOIN " . $this->_db->nameQuote($join['table']) . " AS " . $this->_db->nameQuote($join['alias']) . " ON " . $this->_db->nameQuote($matches[2]) . "." . $this->_db->nameQuote($matches[3]) . " = " . $this->_db->nameQuote($matches[5]) . "." . $this->_db->nameQuote($matches[6]);
		}
		
		return $joins;
	}
	
	/**
	 * Render the WHERE fragment
	 * 
	 * @access	private
	 * @return	string
	 */
	function _renderWhere()
	{
		$wheres = $this->_parseComplexConditions($this->_conditions);
		
		return (!empty($wheres) ? "\nWHERE " . implode(' AND ', $wheres) : "");
	}
	
	/**
	 * Render the GROUP BY fragment
	 * 
	 * @access	private
	 * @return	string
	 */
	function _renderGroupBy()
	{
		$groups = array();
		foreach ($this->_groups as $alias => $column)
		{
			preg_match('/((\w+)\.)?(\w+)/i', $column, $matches);
			if (isset($matches[3])) {
				$groups[] = (!empty($matches[1]) ? $this->_db->nameQuote($matches[2]) . '.' : '') . $this->_db->nameQuote($matches[3]);
			}
		}
		
		return (!empty($groups) ? "\nGROUP BY " . implode(', ', $groups) : "");
	}
	
	/**
	 * Render the ORDER BY fragment
	 * 
	 * @access	private
	 * @return	string
	 */
	function _renderOrderBy()
	{
		$orders = array();
		foreach ($this->_orders as $aliasdotcolumn => $direction)
		{
			if (is_numeric($aliasdotcolumn)) {
				$orders[] = $direction;
			} else {
				preg_match('/((\w+)\.)?(\w+)/i', $aliasdotcolumn, $matches);
				if (isset($matches[3])) {
					$orders[] = (!empty($matches[1]) ? $this->_db->nameQuote($matches[2]) . '.' : '') . $this->_db->nameQuote($matches[3]) . ' ' . strtoupper($direction);
				}
			}
		}
		
		return (!empty($orders) ? "\nORDER BY " . implode(', ', $orders) : "");
	}
	
	/**
	 * 
	 * 
	 * @access	private
	 * @param	array	
	 * @return	array	
	 */
	function _parseComplexConditions($conditions)
	{
		if (is_string($conditions)) {
			return array($conditions);
		} else {
			$ret = array();
			foreach ($conditions as $k => $v)
			{
				if (in_array($k, array('AND', 'OR'), true) || (is_numeric($k) && is_array($v) && array_keys_exist(array('AND', 'OR'), $v))) {
					$tmp = $this->_parseComplexConditions($v);
					if (count($tmp) > 1) {
						$ret[] = "(" . implode(" $k ", $tmp) . ")";
					} else {
						$ret[] = implode(" $k ", $tmp);
					}
				} else {
					if (is_numeric($k)) {

					} else {
						preg_match('/(\w+\.)?(\w+)(\s(.*))?/i', $k, $matches);	// Model.field operator
						$model		= isset($matches[1]) && !empty($matches[1]) ? substr($matches[1], 0, -1) : null;
						$field		= $matches[2];
						if (!isset($matches[4]) || empty($matches[4])) {
							if (is_array($v)) {
								$operator	= 'IN';
							} else {
								$operator	= '=';
							}
						} else {
							$operator = $matches[4];
						}
						if (is_array($v)) {
							// Quote array
							for ($i=0; $i<count($v); $i++)
							{
								$v[$i] = $this->_db->Quote($v[$i]);
							}
							$value		= '(' . implode(', ', $v) . ')';
						} else {
							$value		= $this->_db->Quote($v);
						}

						$ret[] = (!empty($model) ? $this->_db->nameQuote($model) . "." : "") . $this->_db->nameQuote($field) . " $operator " . $value;
					}
				}

			}

			return $ret;
		}
	}
}