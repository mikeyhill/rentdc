<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	Database.Database
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * MySQL Database Class
 *
 * @package		Mosets
 * @subpackage	Database.Database
 * @author		Antoine Bernier
 */

class MosetsDatabaseMySQL extends JDatabaseMySQL
{
	function fetch()
	{
		if (!($cur = $this->query())) {
			return null;
		}
		$array = array();
		$i = 0;
		while ($row = mysql_fetch_row( $cur ))
		{
			/*echo "<pre>";
			echo "row=";
			print_r($row);
			echo "</pre>";*/
			foreach($row as $index => $value)
			{
				$table = mysql_field_table($cur, $index);
				$column = mysql_field_name($cur, $index);
				$array[$i][$table][$column] = $row[$index];
			}
			$i++;
		}
		mysql_free_result($cur);
		
		/*for ($i = 0; $i < count($array); $i++)
		{
			$array[$i] = JArrayHelper::toObject($array[$i]);
		}*/
		
		return $array;
	}
}
?>