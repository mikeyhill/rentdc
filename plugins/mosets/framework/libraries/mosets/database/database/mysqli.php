<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	Database.Database
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * MySQL Database Class
 *
 * @package		Mosets
 * @subpackage	Database.Database
 * @author		Antoine Bernier
 */

class MosetsDatabaseMySQLi extends JDatabaseMySQLi
{
	function fetch()
	{
		if (!($cur = $this->query())) {
			return null;
		}
		$array = array();
		$i = 0;
		while ($row = mysqli_fetch_row( $cur ))
		{
			/*echo "<pre>";
			echo "row=";
			print_r($row);
			echo "</pre>";*/
			foreach($row as $index => $value)
			{
				$info = mysqli_fetch_fields($cur);
				$table = $info[$index]->table;
				$column = $info[$index]->name;
				$array[$i][$table][$column] = $row[$index];
			}
			$i++;
		}
		mysqli_free_result($cur);
		
		/*for ($i = 0; $i < count($array); $i++)
		{
			$array[$i] = JArrayHelper::toObject($array[$i]);
		}*/
		
		return $array;
	}
}
?>