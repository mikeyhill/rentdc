<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	Database
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Table Class
 *
 * @package		Mosets
 * @subpackage	Database
 * @author		Antoine Bernier
 */

mimport('joomla.database.table');

class MosetsTable extends JTable
{
	/**
	 * The name of this table
	 *
	 * @var		string
	 * @access	protected
	 */
	var $_name = null;
	
	/**
	 * The singular name of this table
	 *
	 * @var		string
	 * @access	protected
	 */
	var $_nameSingular = null;
	
	/**
	 * The name of the component
	 *
	 * @var		string
	 * @access	protected
	 */
	var $_component = null;
	
	/**
	 * 
	 */
	var $_useTable = true;
	
	/**
	 * The name of the table's prefix
	 * 
	 * @var string
	 */
	var $_tbl_prefix = null;
	
	/**
	 * The name of the table class (without the prefix)
	 * 
	 * @var string
	 */
	var $_tbl = null;
	
	/**
	 * A variable for assigning unique negative IDs to new rows entries
	 * 
	 * referenced to a static variable, decreased each time a new Table object is created (@see __construct) : so we can garanty this number is unique
	 * 
	 * static	int
	 */
	var $_id_new = null;
	
	/**
	 * Constructor
	 *
	 * @access protected
	 * @param	An optional associative array of configuration settings.
	 * 			Recognized key values include 'name', 'name_singular', 'tbl_prefix', 'tbl' and 'tbl_key' (this list is not meant to be comprehensive).
	 * @param	JDatabase object
	 * @return	void
	 */
	function __construct(&$db, $config = array())
	{
		// Set the class name
		if (empty($this->_name)) {
			if (array_key_exists('name', $config)) {
				$this->_name = $config['name'];
			} else {
				$this->_name = $this->getName();
			}
		}
		
		// Set the singular class name
		if (empty($this->_nameSingular)) {
			if (array_key_exists('name_singular', $config)) {
				$this->_nameSingular = $config['name_singular'];
			} else {
				$this->_nameSingular = $this->getNameSingular();
			}
		}
		
		// Set the component
		if (empty($this->_component)) {
			if (array_key_exists('component', $config)) {
				$this->_component = $config['component'];
			} else {
				$this->_component = $this->getComponent();
			}
		}
		
		// Set the singular class name
		if (array_key_exists('use_table', $config)) {
			$this->_useTable = $config['use_table'];
		}
		
		if ($this->_useTable) {
			// Set the table prefix
			if (empty($this->_tbl_prefix)) {
				if (array_key_exists('tbl_prefix', $config)) {
					$this->_tbl_prefix = $config['tbl_prefix'];
				} else {
					$application =& MosetsApplication::getInstance($this->getComponent());
					$this->_tbl_prefix = $application->_table_prefix;
				}
			}

			// Set the table name to use
			if (empty($this->_tbl)) {
				if (array_key_exists('tbl', $config)) {
					$this->_tbl = $config['tbl'];
				} else {
					$this->_tbl = $this->_createFileName('DBtable', array('name' => $this->getName()));
				}
			}

			// Set the primary key
			if (empty($this->_tbl_key)) {
				if (array_key_exists('tbl_key', $config)) {
					$this->_tbl_key = $config['tbl_key'];
				} else {
					$this->_tbl_key = 'id';
				}
			}

			parent::__construct('#__' . $this->_tbl_prefix . $this->_tbl, $this->_tbl_key, $db);
		}
		
		// Decrease the $id_new static variable and assign it to $this->_id_new
		static $id_new = 0;
		$id_new--;
		$this->_id_new =& $id_new;
	}
	
	/**
	 * Returns a reference to the a Table object, always creating it
	 * 
	 * Overloads JoomlaTable::getInstance() to set our own table classname and to take into account our filename conventions
	 *
	 * @access	public
	 * @param	type		The table type to instantiate
	 * @param	string		A prefix for the table class name. Optional.
	 * @param	array		Configuration array for model. Optional.
	 * @return	database	A database object
	*/
	function &getInstance($type, $component, $config = array())
	{
		$false = false;

		$type		= preg_replace('/[^A-Z0-9_\.-]/i', '', $type);
		$tableClass	= ucfirst($component) . 'Table' . MosetsInflector::camelize($type);

		if (!class_exists($tableClass)) {
			jimport('joomla.filesystem.path');
			$path = JPath::find(
				MosetsTable::addIncludePath(),
				MosetsTable::_createFileName('table', array('name' => $type))
			);
			if ($path) {
				require_once(MosetsApplication::getPath('table', $component));
				require_once $path;

				if (!class_exists($tableClass)) {
					JError::raiseWarning(0, 'Table class ' . $tableClass . ' not found in file.');
					return $false;
				}
			} else {
				JError::raiseWarning(0, 'Table ' . $type . ' not supported. File not found.');
				return $false;
			}
		}

		// Make sure we are returning a DBO object
		if (array_key_exists('dbo', $config)) {
			$db =& $config['dbo'];
		} else {
			$db =& MosetsFactory::getDBO();
		}

		$instance = new $tableClass($db, array_merge(array('component' => $component), $config));

		return $instance;
	}
	
	/**
	 * Method to get the table name
	 *
	 * @access	public
	 * @return	string The name of the table
	 */
	function getName()
	{
		$name = $this->_name;

		if (empty($name)) {
			$r = null;
			if (!preg_match('/Table((table)*(.*(table)?.*))$/i', get_class($this), $r)) {
				JError::raiseError(500, get_class($this) . "::getName() : Cannot get or parse class name.");
			}
			if (strpos($r[3], "table"))	{
				JError::raiseWarning('SOME_ERROR_CODE', get_class($this) . "::getName() : Your classname contains the substring 'table'. "
					. "This causes problems when extracting the classname from the name of your objects Mosets table. "
					. "Avoid Object names with the substring 'table'.");
			}
			$name = $r[3];
		}

		return $name;
	}
	
	/**
	 * Method to get the singular name of the table
	 *
	 * @access	public
	 * @return	string	The singular name of the table
	 */
	function getNameSingular()
	{
		return $this->_nameSingular;
	}
	
	/**
	 * Get the component name
	 *
	 * @access	public
	 * @return	string	The component name
	 */
	function getComponent()
	{
		$component = $this->_component;

		if (empty($component)) {
			$r = null;
			if (!preg_match('/(.*)Table/i', get_class($this), $r)) {
				JError::raiseError (500, "MosetsTable::getComponent() : Cannot get or parse class name.");
			}
			$component = strtolower($r[1]);
		}

		return $component;
	}
	
	/**
	 * Method to get the associated model
	 * 
	 * @access	public
	 * @return	MosetsModel object or false
	 */
	function &getModel()
	{
		if ($model = MosetsModel::getInstance($this->getNameSingular(), $this->getComponent())) {
			return $model;
		} else {
			JError::raiseError(500, get_class($this) . '::getModel() : Cannot get the corresponding model.');
			return false;
		}
	}
	
	/**
	 * Loads a row from the database and binds the fields to the object properties
	 * 
	 * Overload JoomlaTable::load() in order to take into account negative IDs
	 *
	 * @access	public
	 * @param	mixed	Optional primary key.  If not specifed, the value of current key is used
	 * @return	boolean	True if successful
	 */
	function load($oid = null)
	{
		$k = $this->_tbl_key;

		if ($oid !== null) {
			$this->$k = $oid;
		}

		$oid = $this->$k;

		if ($oid === null) {
			return false;
		}
		
		$this->reset();
		$this->defaultValues();

		$db =& $this->getDBO();

		$query = 'SELECT *'
		. ' FROM '.$this->_tbl
		. ' WHERE '.$this->_tbl_key.' = '.$db->Quote($oid);
		$db->setQuery($query);

		if ($result = $db->loadAssoc()) {
			return $this->bind($result);
		} elseif ($oid <= 0) {
			return $this;
		} else {
			$this->setError($db->getErrorMsg());
			return false;
		}
	}
	
	/**
	 * Resets the default properties
	 * 
	 * Overload JoomlaTable::reset()
	 * 
	 * @access	public
	 * @return	boolean	True if success
	 */
	function reset()
	{
		$k = $this->_tbl_key;
		foreach (get_class_vars(get_class($this)) as $name => $value)
		{
			if (array_key_exists($name, $this->getProperties()) && $name != $k) {
				$this->$name = $value;
			}
		}
		
		return true;
	}
	
	/**
	 * Initiate row fields to their default values
	 * 
	 * @access	public
	 * @return	boolean	True if success
	 */
	function defaultValues()
	{
		return true;
	}
	
	
	/**
	 * Generic method to call just before data be stored
	 *
	 * @access	public
	 * @return	boolean	True if success
	 */
	function beforeStore($data, $files = null)
	{
		return true;
	}
	
	/**
	 * Inserts a new row if id is negative or updates an existing row in the database table
	 *
	 * Overloads JoomlaTable::store to allow negative IDs (new rows) to be stored
	 *
	 * @access public
	 * @param	boolean		If false, null object variables are not updated
	 * @return	null|string	Null if successful otherwise returns and error message
	 */
	function store($updateNulls = false)
	{
		$k = $this->_tbl_key;

		if ($this->$k > 0) {	// Now takes into accounts negative IDs
			$ret = $this->_db->updateObject($this->_tbl, $this, $this->_tbl_key, $updateNulls);
		} else {
			// If negative, set the primary key to null, so it generates a new ID
			$this->{$this->_tbl_key} = null;
			
			$ret = $this->_db->insertObject($this->_tbl, $this, $this->_tbl_key);
		}
		if (!$ret) {
			$this->setError(get_class($this) . '::store failed - ' . $this->_db->getErrorMsg());
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * Generic method to call just after data be stored
	 *
	 * @access	public
	 * @return	boolean	True if success
	 */
	function afterStore($files = null)
	{
		return true;
	}
	
	/**
	 * Order up
	 * 
	 * @access	public
	 * @param	int		The primary key value
	 * @return	boolean	True if success
	 */
	function orderup($oid)
	{
		if (!$this->load($oid)) {
			return false;
		}
		
		if ($this->move(-1) === false) {	// JTable::move() does not return a boolean value for true
			return false;
		}
		
		return true;
	}
	
	/**
	 * Order down
	 * 
	 * @access	public
	 * @param	int		The primary key value
	 * @return	boolean	True if success
	 */
	function orderdown($oid)
	{
		if (!$this->load($oid)) {
			return false;
		}
		
		if ($this->move(1) === false) {	// JTable::move() does not return a boolean value for true
			return false;
		}
		
		return true;
	}
	
	/**
	 * Returns the ordering value to place a new item last in its group
	 *
	 * @access	public
	 * @param	string	query WHERE clause for selecting MAX(ordering).
	 * @return	boolean	True if success
	 */
	function getNextOrder()
	{
		if (in_array('ordering', array_keys($this->getProperties()))) {
			if (!($next_order = parent::getNextOrder($this->_getOrderingCondition()))) {
				$this->setError(get_class($this) . '::getNextOrder() : cannot get next record ordering, ' . parent::getError());
				return false;
			}
			
			//echo "next order : " . $next_order . "<br/>";
			
			$this->set('ordering', $next_order);
		}
		
		return true;	// If 'ordering' is not supported, just ignore and return true
	}
	
	/**
	 * Reorder
	 * 
	 * Overloads JoomlaTable::reorder to take 'belongsTo' associations into account
	 * 
	 * @access	public
	 * @return	boolean	True if successfuly reordered or not reordered (ignored) / False if failed
	 */
	function reorder()
	{
		if (in_array('ordering', array_keys($this->getProperties()))) {
			if (!parent::reorder($this->_getOrderingCondition())) {
				$this->setError(get_class($this) . '::reorder() : reordering failed, ' . parent::getError());
				return false;
			}
		}
		
		return true;	// If 'ordering' is not supported, just ignore and return true
	}
	
	/**
	 * Get the $where ordering condition for getNextOrder() and reorder()
	 * 
	 * Based on belongsTo associations (where ordering set to true)
	 * 
	 * @access	private
	 * @return	string	The where condition, eg : 'property=21'
	 */
	function _getOrderingCondition()
	{
		$model = $this->getModel();
		$belongsTo_associations = $model->associations['belongsTo'];
		
		if (!empty($belongsTo_associations)) {
			
			// Let's extract the ordering associations :
			$ordering_associations = array();
			foreach ($belongsTo_associations as $associationName => $relation)
			{
				if (array_key_exists('ordering', $relation) && $relation['ordering'] == true)
					$ordering_associations[] = $associationName;
			}
			
			if (!empty($ordering_associations)) {
				$ordering_where = array();
				foreach ($ordering_associations as $ordering_association)
				{
					$foreignKey = $belongsTo_associations[$ordering_association]['foreignKey'];
				
					if (!empty($this->{$foreignKey}))
						$ordering_where[] = $foreignKey . '=' . $this->_db->Quote($this->{$foreignKey});
				}
				
				$result = implode(' AND ', $ordering_where);
				//echo "ordering condition : '" . $result . "'<br/>";
				return $result;
			}
		}
		
		//echo 'no ordering condition <br />';
		return '';
	}
	
	/**
	 * Generic Save method
	 * 
	 * @access	public
	 * @param	int		Primary key value
	 * @param	array	Array of datas
	 * @return	boolean	The new primary key value or false if it fails
	 */
	function save($oid, $data, $files = null)
	{
		/*echo "let's save a " . $this->getName() . " [id=" . $oid . "] :";
		echo "<pre>";
		echo "data=";
		print_r($data);
		echo "</pre>";
		
		echo "<pre>";
		echo "files=";
		print_r($files);
		echo "</pre>";*/
		
		if (!$this->load($oid)) {
			return false;
		}
		
		if (($oid <= 0) && !$this->defaultValues($data)) {
			return false;
		}
		
		if (!$this->bind($data)) {
			return false;
		}
		
		if (($oid <= 0) && !$this->getNextOrder()) {
			return false;
		}

		if (!$this->beforeStore($data, $files)) {
			return false;
		}

		if (!$this->store()) {
			return false;
		}
		
		if (!$this->afterStore($files)) {
			return false;
		}

		/*echo "OK, saved :";
		echo "<pre>";
		echo "row=";
		print_r($this);
		echo "</pre>";*/
		
		return $this;
	}
	
	/**
	 * Generic method to call just before data be deleted
	 *
	 * @access	public
	 * @return	boolean	True if success
	 */
	function beforeDelete()
	{
		return true;
	}
	
	/**
	 * Generic method to call just after data be deleted
	 *
	 * @access	public
	 * @return	boolean	True if success
	 */
	function afterDelete()
	{
		return true;
	}
	
	/**
	 * Generic remove method
	 * 
	 * @access	public
	 * @param	int		Primary key value
	 * @return	boolean	True if success
	 */
	function remove($oid)
	{
		if (!$this->load($oid)) {
			return false;
		}
		
		if (!$this->beforeDelete()) {
			return false;
		}
		
		if (!$this->delete($oid)) {
			return false;
		}
		
		if (!$this->afterDelete()) {
			return false;
		}
		
		parent::reorder($this->_getOrderingCondition());	// Reorder the whole table to "fill the gaps"
		
		return true;
	}
	
	/**
	 * Generic Feature/Unfeature method
	 * 
	 * @access	public
	 * @param	int		Primary key value
	 * @param	0|1		Optional featured value
	 * @return	boolean	True if success
	 */
	function feature($oid, $featured = 1)
	{
		if (!in_array('featured', array_keys($this->getProperties()))) {
			$this->setError(get_class($this).' does not support featuring.');
			return false;
		}
		
		if (!$this->load($oid)) {
			return false;
		}
	
		$this->set('featured', $featured);

		if (!$this->store()) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Generic Approve/Unapprove method
	 * 
	 * @access	public
	 * @param	int		Primary key value
	 * @param	0|1		Optional approved value
	 * @return	boolean	True if success
	 */
	function approve($oid, $approved = 1)
	{
		if (!in_array('approved', array_keys($this->getProperties())) || !in_array('published', array_keys($this->getProperties())) || !in_array('featured', array_keys($this->getProperties()))) {
			$this->setError(get_class($this).' does not support approving.');
			return false;
		}
		
		if (!$this->load($oid)) {
			return false;
		}
	
		$this->set('approved', $approved);
		$this->set('published', 0);
		$this->set('featured', 0);

		if (!$this->store()) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Add a directory where MosetsModel should search for models.
	 * You may either pass a string or an array of directories.
	 * 
	 * Overloads JoomlaModel::addIncludePath() to add our component's models/ directory by default
	 *
	 * @access	public
	 * @param	string	A path to search.
	 * @return	array	An array with directory elements
	 */
	/*function addIncludePath($path = '')
	{
		$default_path = MosetsApplication::getPath('tables', $this->getComponent());
		
		if (!in_array($default_path, parent::addIncludePath())) {
			parent::addIncludePath($default_path);
		}
		
		return parent::addIncludePath($path);
	}*/
		
	/**
	 * Create the filename for a resource.
	 *
	 * @access	private
	 * @param	string	The resource type to create the filename for.
	 * @param	array	An associative array of filename information. Optional.
	 * @return	string	The filename.
	 */
	function _createFileName($type, $parts = array())
	{
		$filename = '';

		switch ($type)
		{
			case 'table':
				$filename = MosetsInflector::underscore($parts['name']) . ".php";
			break;
			case 'DBtable':
				$filename = MosetsInflector::underscore($parts['name']);
			break;
		}
		
		return $filename;
	}
}
?>