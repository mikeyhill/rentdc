<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	Utilities
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Image Class
 *
 * @package		Mosets
 * @subpackage	Utilities
 * @author		Antoine Bernier
 */
class MosetsImage extends JObject
{
	var $tmpFile=null;
	var $imageName=null;
	var $type=null;
	var $size=null;
	var $directory=null;
	var $method=null;
	var $quality=80;
	var $square=false;
	var $_imageData=null;

	/**
	 * Constructor
	 * 
	 * @access	public
	 * @param	array	
	 * @param	string	The destination image directory
	 * @return	void
	 */
	function __construct($file = null, $dest_dir = null)
	{
		if (!empty($file) && array_key_exists('tmp_name', $file)) {
			$this->set('tmpFile',		$file['tmp_name']);
			$this->set('imageName',	$file['name']);
			$this->set('type',			$file['type']);
			$this->set('size',			$file['size']);
		}
		if (!empty($dest_dir))
			$this->set('directory', 	$dest_dir);
	}
	
	/**
	 * Detect avalaible image libraries
	 * 
	 * @access	public
	 * @return	array	An indexed array
	 */
	function find_libraries()
	{
		$libraries = array();

		// Initialization - To allow Windows machine to do proper detection
		$shell_cmd = '';
		if (substr(PHP_OS, 0, 3) == 'WIN') {
			return array();
		}
		unset($output);

		// Detect Imagemagick
		@exec($shell_cmd . 'convert -version', $output, $status);
		if (!$status) {
			if (preg_match("/imagemagick[ \t]+([0-9\.]+)/i", $output[0], $matches))
				$libraries['imagemagick'] = $matches[0];
		}

		// Detect Netpbm
		unset($output);
		@exec($shell_cmd . 'jpegtopnm -version 2>&1', $output, $status);
		if (!$status) {
			if (preg_match("/netpbm[ \t]+([0-9\.]+)/i", $output[0], $matches))
				$libraries['netpbm'] = $matches[0];
		}

		// Detect GD1/GD2
		$GDfuncList = get_extension_funcs('gd');

		ob_start();
		@phpinfo(INFO_MODULES);
		$output = ob_get_contents();
		ob_end_clean();
		$matches[1] = '';
		if (preg_match("/GD Version[ \t]*(<[^>]+>[ \t]*)+([^<>]+)/s", $output, $matches)) {
			$gdversion = $matches[2];
		}

		if ($GDfuncList) {
			if (trim($gdversion) == '1.6.2 or higher') {
				$libraries['gd1'] = $gdversion;
			} else {
				$libraries['gd2'] = $gdversion;
			}
		}
		/*if ($GDfuncList) {
			if (in_array('imagegd2',$GDfuncList))
				$libraries['gd2'] = $gdversion;
			else
				$libraries['gd1'] = $gdversion;
		}*/

		return $libraries;
	}
	
	/**
	 * Method that generate a thumbnail of the image
	 */
	function resize()
	{
		// Get image infos
		if (substr(0,7,$this->tmpFile) == 'http://') {
			$imginfo = _getimagesize_remote($this->tmpFile);
		} else {
			$imginfo = getimagesize($this->tmpFile);
		}
		
		// Mapping array of IMAGETYPE_XXX corresponding values
		$imagetype_map = array(
			1 => 'GIF',
			2 => 'JPG',
			3 => 'PNG',
			4 => 'SWF',
			5 => 'PSD',
			6 => 'BMP',
			7 => 'TIFF',
			8 => 'TIFF',
			9 => 'JPC',
			10 => 'JP2',
			11 => 'JPX',
			12 => 'JB2',
			13 => 'SWC',
			14 => 'IFF'
		);
		
		// Get IMAGETYPE's string value
		$imagetype = $imagetype_map[$imginfo[2]];

		// GD can only handle GIF, JPG & PNG images
		if (in_array($this->method, array('gd1', 'gd2')) && !in_array($imagetype, array('JPG', 'PNG', 'GIF'))) {
			$this->setError(JText::_("GD can only handle JPG, GIF and PNG files."));
			return false;
		}

		// Get height/width
		$srcWidth = $imginfo[0];
		$srcHeight = $imginfo[1];

		// Generate new width/height
		$ratio = max($srcWidth, $srcHeight) / $this->size;
		$ratio = max($ratio, 1.0);
		$destWidth = (int)($srcWidth / $ratio);
		$destHeight = (int)($srcHeight / $ratio);

		$offWidth = 0;
		$offHeight = 0;
		if ($this->square && $srcWidth > $this->size && $srcHeight > $this->size) {
			if ($srcWidth > $srcHeight) {
				$offWidth = ($srcWidth - $srcHeight) / 2;
				$offHeight = 0;
				$srcWidth = $srcHeight;
				$destHeight = $destWidth;
			} elseif ($srcHeight > $srcWidth) {
				$offWidth = 0;
				$offHeight = ($srcHeight - $srcWidth) / 2;
				$srcHeight = $srcWidth;
				$destWidth = $destHeight;
			}
		}

		// Method for thumbnails creation
		switch ($this->method)
		{
			case "gd1" :
				if (!function_exists('imagecreatefromjpeg')) {
					$this->setError(JText::_("GD image library not installed."));
					return false;
				}
				if ($imagetype == 'JPG')
					$src_img = imagecreatefromjpeg($this->tmpFile);
				else
					$src_img = imagecreatefrompng($this->tmpFile);
				if (!$src_img){
					$this->setError(JText::_("Invalid image!"));
					return false;
				}
				$dst_img = imagecreate($destWidth, $destHeight);
				imagecopyresized($dst_img, $src_img, 0, 0, 0, 0, $destWidth, (int)$destHeight, $srcWidth, $srcHeight);
				ob_start();
				imagejpeg($dst_img, null, $this->quality);
				$this->_imageData = ob_get_contents();
				ob_end_clean();
				imagedestroy($src_img);
				imagedestroy($dst_img);
				break;

			case "gd2" :
				if (!function_exists('imagecreatefromjpeg')) {
					$this->setError(JText::_("GD image library not installed."));
					return false;
				}
				if (!function_exists('imagecreatetruecolor')) {
					$this->setError(JText::_("GD2 image library does not support truecolor thumbnailing."));
					return false;
				}

				switch ($imagetype)
				{
					case 'JPG':
						$src_img = imagecreatefromjpeg($this->tmpFile);
						break;

					case 'PNG':
						$src_img = imagecreatefrompng($this->tmpFile);
						break;
					
					case 'GIF':
						$src_img = imagecreatefromgif ($this->tmpFile);
						break;
				}

				if (!$src_img) {
					$this->setError(JText::_("Invalid image!"));
					return false;
				}
				$dst_img = imagecreatetruecolor($destWidth, $destHeight);

				if ($imagetype == 'GIF') {
					$colorTransparent = imagecolortransparent($src_img);
					imagepalettecopy($src_img, $dst_img);
					imagefill($dst_img, 0, 0, $colorTransparent);
					imagecolortransparent($dst_img, $colorTransparent);
					imagetruecolortopalette($dst_img, true, 256);
				} elseif ($imagetype == 'PNG') {
					imagecolortransparent($dst_img, ImageColorAllocate($dst_img, 0, 0, 0));
					imagealphablending($dst_img, false);
				}
				
				imagecopyresampled($dst_img, $src_img, 0, 0, $offWidth, $offHeight, $destWidth, (int)$destHeight, $srcWidth, $srcHeight);
				
				ob_start();
				switch ($imagetype)
				{
					case 'GIF':
						imagegif ($dst_img);
						break;
					case 'PNG':
						imagepng($dst_img);
						break;
					case 'JPG':
					default:
						imagejpeg($dst_img, null, $this->quality);
						break;
				}
				$this->_imageData = ob_get_contents();
				ob_end_clean();
				
				imagedestroy($src_img);
				imagedestroy($dst_img);
				break;
				
			/*case "netpbm":	// @todo	caution with hpconf->get('img_netpbmpath') and trailing slash : may be wrong!
				if ($hpconf->get('img_netpbmpath')) { 
					if (!is_dir($hpconf->get('img_netpbmpath')))	{
						$this->setError(JText::_("NetPbm path incorrect."));
						return false;
					} 
				} 
				
				if ($imagetype == 'PNG') { 
					$cmd = $hpconf->get('img_netpbmpath') . "pngtopnm $this->tmpFile | " . $hpconf->get('img_netpbmpath') . "pnmscale -xysize $destWidth ".(int)$destHeight." | " . $hpconf->get('img_netpbmpath') . "pnmtopng > " . $this->directory . DS . $this->imageName;
				} else if ($imagetype == 'JPG') { 
					$cmd = $hpconf->get('img_netpbmpath') . "jpegtopnm $this->tmpFile | " . $hpconf->get('img_netpbmpath') . "pnmscale -xysize $destWidth ".(int)$destHeight." | " . $hpconf->get('img_netpbmpath') . "pnmtojpeg -quality=$this->quality > " . $this->directory . DS . $this->imageName;
				} else if ($imagetype == 'GIF') { 
					$cmd = $hpconf->get('img_netpbmpath') . "giftopnm $this->tmpFile | " . $hpconf->get('img_netpbmpath') . "pnmscale -xysize $destWidth ".(int)$destHeight." | " . $hpconf->get('img_netpbmpath') . "ppmquant 256 | " . $hpconf->get('img_netpbmpath') . "ppmtogif > " . $this->directory . DS . $this->imageName; 
				}
				exec($cmd);
				break;
				
			case "imagemagick":
				$tmp_name = substr(strrchr($this->directory.$this->imageName, "/"), 1);
				copy($this->tmpFile, $this->directory . DS . $tmp_name);
				$uploadfile = $this->directory . DS . $tmp_name;
				$cmd = $hpconf->get('img_impath')."convert -resize ".$destWidth."x".(int)$destHeight." $uploadfile " . $this->directory . DS . $this->imageName;
				exec($cmd);
				unlink($uploadfile);
				break;*/
		}
		
		if ($this->method == 'netpbm' || $this->method == 'imagemagick') {
			$filename = $this->directory . DS . $this->imageName;
			$handle = fopen($filename, "r");
			$this->_imageData = fread($handle, filesize($filename));
			fclose($handle);
			unlink($this->directory . DS . $this->imageName);
		}

		// Set mode of uploaded picture
		/*if (file_exists($this->directory.DS.$this->imageName)) {
			chmod($this->directory.DS.$this->imageName, octdec('755'));
			// We check that the image is valid
			$imginfo = getimagesize($this->directory.DS.$this->imageName);
			if ($imginfo == null){
				$this->setError(JText::_("Invalid image!"));
				return false;
			}
		} else {
			$this->setError(JText::_("Resize failed!").$this->directory.DS.$this->imageName);
			return false;
		}*/
		
		return true;
	}

	/**
	 * Save the image to its directory
	 * 
	 * @access	public
	 * @return	boolean	True if success
	 */
	function saveToDirectory()
	{
		// Verify we can save the image
		if (!$this->_checkSave()) {
			return false;
		}
		
		if ($fp = fopen($this->directory . DS . $this->imageName,'w')) {
			if (fwrite($fp,$this->_imageData) === false) {
				$this->setError(sprintf(JText::_('Unable to write to file: %s.'), $this->directory . DS . $this->imageName));
				return false;
			}
		} else {
			$this->setError(sprintf(JText::_('Unable to open %s for writting.'), $this->directory . DS . $this->imageName));
			return false;
		}
		
		return true;
	}
	
	/**
	 * Remove the image from its directory
	 * 
	 * @access	public
	 * @return	boolean	True if success
	 */
	function removeFromDirectory()
	{
		// Verify we can remove the image
		if (!$this->_checkRemove()) {
			return false;
		}
		
		if (is_file($this->directory . DS . $this->imageName) && !unlink($this->directory . DS . $this->imageName)) {
			$this->setError(sprintf(JText::_('Error while deleting photo: %s'), $this->directory . DS . $this->imageName));
			return false;
		}
		
		return true;
	}
	
	/**
	 * Get the size of a remote image
	 * 
	 * @access	private	
	 * @param	string	$image_url	The remote image URL
	 * @return	array
	 */
	function _getimagesize_remote($image_url)
	{
		$handle = fopen ($image_url, "rb");
		$contents = '';
	    if ($handle) {
	    	do
			{
				$count += 1;
				$data = fread($handle, 8192);
				if (strlen($data) == 0) {
					break;
				}
				$contents .= $data;
			} while (true);
		} else {
			$this->setError(JText::_("Could not get the size of the remote image: unable to open."));
			return false;
		}
		fclose ($handle);

		$im = ImageCreateFromString($contents);
		if (!$im) { return false; }
		$gis = array();
		$gis[0] = ImageSX($im);
		$gis[1] = ImageSY($im);
		$gis[3] = "width={$gis[0]} height={$gis[1]}";
		ImageDestroy($im);
	
		return $gis;
	}
	
	/**
	 * Check the image can be saved
	 * 
	 * @access	private
	 * @return	boolean	True if success
	 */
	function _checkSave()
	{
		// Is there a specified source path?
		if ($this->tmpFile == '') {
			$this->setError(JText::_('Please specify an image before uploading.'));
			return false;
		}
		
		// Uploaded ?
		if (!is_uploaded_file($this->tmpFile)) {
			$this->setError(JText::_('Please specify a valid image before uploading.'));
			return false;
		}
		
		// Is destination directory writable?
		if (!is_writable($this->directory)) { 
			$this->setError(sprintf(JText::_('%s is not writable.'), $this->directory));
			return false;
		}
		
		// No duplicate: if the filename already exists, let's rename it, eg: mydog.png to mydog_2.png
		if (file_exists($imagePath = $this->directory . DS . $this->imageName)) {
			$i = 2;
			while (file_exists($imagePath . '_' . $i)) {
				$i++;
			}
			$filenameWithoutExtension = substr($this->imageName, 0, strrpos($this->imageName, '.'));
			$fileNameExtension = end(explode('.', $this->imageName));
			$this->imageName = $filenameWithoutExtension . '_' . $i . $fileNameExtension;
		}
		
		return true;
		
	}
	
	/**
	 * Check the image can be removed
	 * 
	 * @access	private
	 * @return	boolean	True if success
	 */
	function _checkRemove()
	{		
		// Is destination directory writable?
		if (!is_writable($this->directory)) {
			$this->setError(sprintf(JText::_('%s is not writable.'), $this->directory));
			return false;
		}
		
		return true;
		
	}
}
?>