<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	Utilities
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Route Class
 *
 * @package		Mosets
 * @subpackage	Utilities
 * @author		Antoine Bernier
 */
class MosetsRoute extends JObject
{
	/**
	 * Build link
	 * 
	 * @static
	 * @access	public
	 * @param	string	Name of the Mosets application
	 * @param	array	Indexed array of URL parameters
	 * @return	string
	 */
	function getLink($client, $parameters = array())
	{		
		/**
		 * Build the link from $view, $layout and URL $parameters
		 */
	
		$link = 'index.php?';
	
		/*foreach ($parameters as $key => $value)
		{
			$link .= '&' . $key . '=' . $value;
		}*/
		
		$link .= http_build_query(array_merge(array('option' => 'com_' . $client), $parameters));
		
		/**
		 * Search for an existing menu item matching the requested link
		 */
	
		$itemId = null;
	
		$menus				=& JApplication::getMenu('site', array());
		$component			=& JComponentHelper::getComponent('com_' . $client);
		$componentMenuItems	= $menus->getItems('componentid', $component->id);

		foreach ((array) $componentMenuItems as $index => $componentMenuItem)
		{
			$diff = array_diff_assoc($componentMenuItem->query, $parameters);
		
			unset($diff['option']);
			if (array_key_exists('layout', $diff) && array_key_exists('task', $parameters)) {
				if ($diff['layout'] == 'form' && in_array($parameters['task'], array('add', 'edit')) || $diff['layout'] == $parameters['task']) {
					unset($diff['layout']);
				}
			}
		
			if (empty($diff)) {
				$itemId = $componentMenuItem->id;
				break;
			}
		}
	
		// If it doesn't match, just let find Itemid matching the default view
		if (is_null($itemId)) {
			$application = MosetsApplication::getInstance($client);
			$items = $menus->getItems('link', 'index.php?option=com_' . $client . '&view=' . $application->_default_views['site']);
			
			if (!empty($items)) {
				$itemId = $items[0]->id;
			}
		}
	
		if (isset($itemId)) {
			$link .= '&Itemid=' . $itemId;
		};
		
		
		return $link;
	}
}