<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	Utilities.Compat
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * PHP <= 5.0 compatibility functions
 *
 * @package		Mosets
 * @subpackage	Utilities.Compat
 * @author		Lee Cher Yeong <framework@mosets.com>
 * @author		Antoine Bernier
 */

if (!function_exists('property_exists')) {
    function property_exists($class, $property)
	{
        if (is_object($class)) {
            $vars = get_object_vars($class);
        } else {
            $vars = get_class_vars($class);
        }
        return array_key_exists($property, $vars);
    }
}
if (!function_exists('array_intersect_key')) {
    function array_intersect_key()
    {
        $arrs = func_get_args();
        $result = array_shift($arrs);
        foreach ($arrs as $array) {
            foreach ($result as $key => $v) {
                if (!array_key_exists($key, $array)) {
                    unset($result[$key]);
                }
            }
        }
        return $result;
   }
}
if (!function_exists('http_build_query')) {
function http_build_query($data, $prefix='', $sep='', $key='')
{
	$ret = array();
	foreach ((array)$data as $k => $v)
	{
		if (is_int($k) && $prefix != null) {
			$k = urlencode($prefix . $k);
		}
		if ((!empty($key)) || ($key === 0))  $k = $key.'['.urlencode($k).']';
		if (is_array($v) || is_object($v)) {
			array_push($ret, http_build_query($v, '', $sep, $k));
		} else {
			array_push($ret, $k.'='.urlencode($v));
		}
	}
	if (empty($sep)) $sep = ini_get('arg_separator.output');
	return implode($sep, $ret);
	}
}
?>
