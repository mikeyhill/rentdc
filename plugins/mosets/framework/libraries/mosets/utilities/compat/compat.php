<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	Utilities.Compat
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * PHP Compatibility Functions
 *
 * @package		Mosets
 * @subpackage	Utilities.Compat
 * @author		Lee Cher Yeong <framework@mosets.com>
 * @author		Antoine Bernier
 */

if (version_compare(phpversion(), '5.0') < 0) {
	require_once(dirname(__FILE__) . DS . 'php50x.php');
}
if (version_compare(phpversion(), '5.2') < 0) {
	require_once(dirname(__FILE__) . DS . 'php52x.php');
}
if (version_compare(phpversion(), '5.3') < 0) {
	require_once(dirname(__FILE__) . DS . 'php53x.php');
}
if (!function_exists('array_keys_exist')) {
    function array_keys_exist($keys, $search) {                                                                                          
	    foreach ($keys as $key)
		{
			if (array_key_exists($key, $search))
				return true;
		}

		return false;
	}
}
?>