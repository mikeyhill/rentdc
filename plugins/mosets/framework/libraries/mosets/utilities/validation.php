<?php
/**
 * @version		$Id$
 * @package		Hotproperty
 * @subpackage	Classes
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Validation Class
 * 
 * Based on http://book.cakephp.org/view/134/Built-in-Validation-Rules
 *
 * @package		Hotproperty
 * @subpackage	Classes
 * @author		Antoine Bernier
 * @todo		Need to be implemented with HotpropertyController and HotpropertyModel
 */
class HotpropertyClassValidation extends JObject
{
	/**
	 * Validation rule
	 *
	 * @var		string|array	Eg: 'rulename' or array('rulename', ruleargs), ex: 'alphanumeric' or array('comparison', '>=', 5)
	 * @access	protected
	 */
	var $_rule = array();
	
	/**
	 * Required
	 *
	 * @var		boolean		If ‘required’ is true, the field should not be empty
	 * @access	protected
	 */
	var $_required = null;
	
	/**
	 * Message
	 * 
	 * @var		string		A custom validation error message
	 * @access	protected
	 */
	var $_message = null;
	
	function __construct($config = array())
	{
		$this->_rule = $config['rule'];
		
		if (isset($config['required']))		$this->_required = $config['required'];
		if (isset($config['allow_empty']))	$this->_allowEmpty = $config['allow_empty'];
		if (isset($config['message']))		$this->_message = $config['message'];
	}
	
	/**
	 * Returns a reference to the a Validation object, always creating it
	 * 
	 * @access	public
	 * @param	array	An array indexed by configuration options
	 * @return	mixed	A validation object, or false on failure
	*/
	function &getInstance($config = array())
	{
		$instance = new HotpropertyClassValidation($config);
		return $instance;
	}
	
	/**
	 * Checks whether a field is valid
	 * 
	 * @access	public
	 * @param	string	The name of the field
	 * @param	mixed	The value of the field
	 * @return	boolean	Success
	*/
	function validate($fieldName, $fieldValue = null)
	{
		$return = false;
		
		if (!empty($fieldValue)) {
			switch (gettype($this->_rule))
			{
				case 'string':
					$return = $this->{$this->_rule}($fieldValue);
					break;
				case 'array':
					$ruleArgs = $this->_rule;
					$ruleName = array_shift($ruleArgs);
					array_unshift($ruleArgs, $fieldValue);
					$return = call_user_func_array(array(&$this, $ruleName), $ruleArgs);
					break;
			}
			if (!$return) {
				if ($this->_message) {
					$this->setError($this->_message);
				} else {
					$this->setError(sprintf($this->getError(), $fieldName));
				}				
			}			
		} else {
			if ($this->_required) {
				$this->setError(sprintf(JText::_('%s is required.'), $fieldName));
				return false;
			} else {
				return true;
			}
		}
		
		return $return;
	}
	
	/**
	 * Checks whether a value is alphanumeric
	 * 
	 * @access	public
	 * @param	mixed	The value to test
	 * @return	boolean	Success
	*/
	function alphaNumeric($value)
	{
		$regex = '/^[\p{Ll}\p{Lm}\p{Lo}\p{Lt}\p{Lu}\p{Nd}]+$/mu';
		
		if ($this->_regex($regex, $value)) {
			return true;
		}
		
		$this->setError(sprintf(JText::_('%s is not a valid alpha numeric value.'), $value));
		return false;
	}
	
	/**
	 * Checks whether a value is a valid email
	 * 
	 * @access	public
	 * @param	mixed	The value to test
	 * @return	boolean	Success
	*/
	function email($value)
	{
		$regex = "/^[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[a-z]{2,4}|museum|travel)$/i";
		
		if ($this->_regex($regex, $value)) {
			return true;
		}
		
		$this->setError(sprintf(JText::_('%s is not a valid email value.'), $value));
		return false;
	}
	
	/**
	 * Checks whether a value is a valid URL
	 * 
	 * @access	public
	 * @param	mixed	The value to test
	 * @return	boolean	Success
	*/
	function url($value)
	{
		$regex = '/^(?:(?:https?|ftps?|file|news|gopher):\\/\\/)?(?:(?:(?:25[0-5]|2[0-4]\d|(?:(?:1\d)?|[1-9]?)\d)\.){3}(?:25[0-5]|2[0-4]\d|(?:(?:1\d)?|[1-9]?)\d)|(?:[0-9a-z]{1}[0-9a-z\\-]*\\.)*(?:[0-9a-z]{1}[0-9a-z\\-]{0,62})\\.(?:[a-z]{2,6}|[a-z]{2}\\.[a-z]{2,6})(?::[0-9]{1,4})?)(?:\\/?|\\/[\\w\\-\\.,\'@?^=%&:;\/~\\+#]*[\\w\\-\\@?^=%&\/~\\+#])$/i';
		
		if ($this->_regex($regex, $value)) {
			return true;
		}
		
		$this->setError(sprintf(JText::_('%s is not a valid URL value.'), $value));
		return false;
	}
	
	/**
	 * Checks whether a value is a valid IPv4 address
	 * 
	 * @access	public
	 * @param	mixed	The value to test
	 * @return	boolean	Success
	*/
	function ip($value)
	{
		$bytes = explode('.', $value);
		if (count($bytes) == 4) {
			foreach ($bytes as $byte) {
				if (!(is_numeric($byte) && $byte >= 0 && $byte <= 255)) {
					$this->setError(sprintf(JText::_('%s is not a valid ip value.'), $value));
					return false;
				}
			}
			return true;
		}
		
		$this->setError(sprintf(JText::_('%s is not a valid ip value.'), $value));
		return false;
	}
	
	/**
	 * Checks that a value is a valid decimal.
	 * 
	 * If $places is null, the $value is allowed to be a scientific float.
	 * If no decimal point is found a false will be returned. Both the sign and exponent are optional.
	 *
	 * @access	public
	 * @param	integer	The value the test for decimal
	 * @param	integer	If set $value value must have exactly $places after the decimal point
	 * @param	string	If a custom regular expression is used this is the only validation that will occur.
	 * @return	boolean	Success
	 */
	function decimal($value, $places = null, $regex = null)
	{
		if (is_null($regex)) {
			if (is_null($places)) {
				$regex = '/^[-+]?[0-9]*\\.{1}[0-9]+(?:[eE][-+]?[0-9]+)?$/';
				return $this->_regex($regex, $value);
			} else {
				$regex = '/^[-+]?[0-9]*\\.{1}[0-9]{' . $places . '}$/';
			}
		}
		
		if ($this->_regex($regex, $value)) {
			return true;
		}
		
		$this->setError(sprintf(JText::_('%s is not a valid decimal number.'), $value));
		return false;
	}
	
	/**
	 * Checks whether the length of a string is greater or equal to a minimal length.
	 *
	 * @access	public
	 * @param	string	The string to test
	 * @param	integer	The minimal string length
	 * @return	boolean	Success
	 */
	function minLength($value, $min)
	{
		$length = strlen($value);
		
		if ($length >= $min) {
			return true;
		}
		
		$this->setError(sprintf(JText::_('%s is less than %s character(s) long.'), $value, $min));
		return false;
	}
	
	/**
	 * Checks whether the length of a string is smaller or equal to a maximal length.
	 *
	 * @access	public
	 * @param	string	The string to test
	 * @param	integer	The maximal string length
	 * @return	boolean	Success
	 */
	function maxLength($value, $max)
	{
		$length = strlen($value);
		
		if ($length <= $max) {
			return true;
		}
		
		$this->setError(sprintf(JText::_('%s is more than %s character(s) long.'), $value, $max));
		return false;
	}
	
	/**
	 * Checks whether the length of a string is within a specified range.
	 * 
	 * Spaces are included in the character count.
	 *
	 * @access	public
	 * @param	string	Value to check for length
	 * @param	integer	Minimum value in range (inclusive)
	 * @param	integer	Maximum value in range (inclusive)
	 * @return	boolean	Success
	
	 */
	function between($value, $min, $max)
	{
		$length = strlen($value);

		if ($length >= $min && $length <= $max) {
			return true;
		}
		
		$this->setError(sprintf(JText::_('%s is not within %s and %s character(s) long.'), $value, $min, $max));
		return false;
	}
	
	/**
	 * Checks whether a value is multiple
	 * 
	 * @access	public
	 * @param	mixed	The value1 to test
	 * @param	string	The operator
	 * @param	mixed	The value2 to test
	 * @return	boolean	Success
	*/
	function multiple($value, $options = array())
	{
		$defaults = array('in' => null, 'max' => null, 'min' => null);
		$options = array_merge($defaults, $options);
		$value = array_filter($value);
		if (empty($value)) {
			return false;
		}
		if ($options['max'] && sizeof($value) > $options['max']) {
			$this->setError(sprintf(JText::_('%s contains more than %s values.'), $value, $options['max']));
			return false;
		}
		if ($options['min'] && sizeof($value) < $options['min']) {
			$this->setError(sprintf(JText::_('%s contains less than %s values.'), $value, $options['min']));
			return false;
		}
		if ($options['in'] && is_array($options['in'])) {
			foreach ($value as $val) {
				if (!in_array($val, $options['in'])) {
					$this->setError(sprintf(JText::_('%s can only contain values within : %s.'), $value, implode(',', $options['in'])));
					return false;
				}
			}
		}
		
		return true;
	}
	
	/**
	 * Compare 2 values with an operator
	 * 
	 * @access	public
	 * @param	mixed	The value1 to test
	 * @param	string	The operator
	 * @param	mixed	The value2 to test
	 * @return	boolean	Success
	*/
	function comparison($value1, $operator, $value2)
	{
		$return = false;
		
		switch ($operator) {
			case 'isgreater':
			case '>':
				if ($value1 > $value2) {
					$return = true;
				} else {
					$this->setError(sprintf(JText::_('%s is not greater than %s.'), $value1, $value2));
				}
			break;
			case 'isless':
			case '<':
				if ($value1 < $value2) {
					$return = true;
				} else {
					$this->setError(sprintf(JText::_('%s is not less than %s.'), $value1, $value2));
				}
			break;
			case 'greaterorequal':
			case '>=':
				if ($value1 >= $value2) {
					$return = true;
				} else {
					$this->setError(sprintf(JText::_('%s is not greater or equal to %s.'), $value1, $value2));
				}
			break;
			case 'lessorequal':
			case '<=':
				if ($value1 <= $value2) {
					$return = true;
				} else {
					$this->setError(sprintf(JText::_('%s is not less or equal to %s.'), $value1, $value2));
				}
			break;
			case 'equalto':
			case '==':
				if ($value1 == $value2) {
					$return = true;
				} else {
					$this->setError(sprintf(JText::_('%s is not equal to %s.'), $value1, $value2));
				}
			break;
			case 'notequal':
			case '!=':
				if ($value1 != $value2) {
					$return = true;
				} else {
					$this->setError(sprintf(JText::_('%s is not not-equal to %s.'), $value1, $value2));
				}
			break;
			default:
				$this->setError(sprintf(JText::_('Unsupported operator %s.'), $operator));
			break;
		}
		
		return $return;
	}
	
	/**
	 * Checks whether a value is in a specified range
	 * 
	 * @access	public
	 * @param	int		The value to test
	 * @param	int		The lower limit
	 * @param	int		The upper limit
	 * @return	boolean	Success
	*/
	function range($value, $lower = null, $upper = null)
	{
		if (is_numeric($value)) {
			if (isset($lower) && isset($upper)) {
				if ($lower <= $upper) {
					if ($value > $lower && $value < $upper) {
						return true;
					}
				}
			} elseif (is_finite($value)) {
				return true;
			}
		}
		
		$this->setError(sprintf(JText::_('%s is not in the specified range : ]%s-%s[.'), $value, $lower, $upper));
		return false;
	}
	
	/**
	 * Checks if a value is in a given list.
	 *
	 * @access	public
	 * @param	string	Value to check
	 * @param	array	List to check against
	 * @return	boolean	Succcess
	 */
	function inList($value, $list)
	{
		if (in_array($value, $list)) {
			return true;
		}
		
		$this->setError(sprintf(JText::_('%s is not in the given list : {%s}.'), $value, implode(',', $list)));
		return false;
	}
	
	/**
	 * Date validation, determines if the string passed is a valid date.
	 * keys that expect full month, day and year will validate leap years
	 *
	 * @access	public
	 * @param	string	A valid date string
	 * @param	mixed	Use a string or an array of the keys below. Arrays should be passed as array('dmy', 'mdy', etc)
	 * 					Keys:	dmy 27-12-2006 or 27-12-06 separators can be a space, period, dash, forward slash
	 * 							mdy 12-27-2006 or 12-27-06 separators can be a space, period, dash, forward slash
	 * 							ymd 2006-12-27 or 06-12-27 separators can be a space, period, dash, forward slash
	 * 							dMy 27 December 2006 or 27 Dec 2006
	 * 							Mdy December 27, 2006 or Dec 27, 2006 comma is optional
	 * 							My December 2006 or Dec 2006
	 * 							my 12/2006 separators can be a space, period, dash, forward slash
	 * @param	string	If a custom regular expression is used this is the only validation that will occur.
	 * @return	boolean	Success
	 */
	function date($value, $format = 'ymd', $regex = null)
	{
		if (is_null($regex)) {
			$search = array();
			if (is_array($format)) {
				foreach ($format as $key => $value) {
					$search[$value] = $value;
				}
			} else {
				$search[$format] = $format;
			}
		
			$regex['dmy']	= '%^(?:(?:31(\\/|-|\\.|\\x20)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.|\\x20)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.|\\x20)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.|\\x20)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$%';
			$regex['mdy']	= '%^(?:(?:(?:0?[13578]|1[02])(\\/|-|\\.|\\x20)31)\\1|(?:(?:0?[13-9]|1[0-2])(\\/|-|\\.|\\x20)(?:29|30)\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:0?2(\\/|-|\\.|\\x20)29\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9])|(?:1[0-2]))(\\/|-|\\.|\\x20)(?:0?[1-9]|1\\d|2[0-8])\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$%';
			$regex['ymd']	= '%^(?:(?:(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00)))(\\/|-|\\.|\\x20)(?:0?2\\1(?:29)))|(?:(?:(?:1[6-9]|[2-9]\\d)?\\d{2})(\\/|-|\\.|\\x20)(?:(?:(?:0?[13578]|1[02])\\2(?:31))|(?:(?:0?[1,3-9]|1[0-2])\\2(29|30))|(?:(?:0?[1-9])|(?:1[0-2]))\\2(?:0?[1-9]|1\\d|2[0-8]))))$%';
			$regex['dMy']	= '/^((31(?!\\ (Feb(ruary)?|Apr(il)?|June?|(Sep(?=\\b|t)t?|Nov)(ember)?)))|((30|29)(?!\\ Feb(ruary)?))|(29(?=\\ Feb(ruary)?\\ (((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)))))|(0?[1-9])|1\\d|2[0-8])\\ (Jan(uary)?|Feb(ruary)?|Ma(r(ch)?|y)|Apr(il)?|Ju((ly?)|(ne?))|Aug(ust)?|Oct(ober)?|(Sep(?=\\b|t)t?|Nov|Dec)(ember)?)\\ ((1[6-9]|[2-9]\\d)\\d{2})$/';
			$regex['Mdy']	= '/^(?:(((Jan(uary)?|Ma(r(ch)?|y)|Jul(y)?|Aug(ust)?|Oct(ober)?|Dec(ember)?)\\ 31)|((Jan(uary)?|Ma(r(ch)?|y)|Apr(il)?|Ju((ly?)|(ne?))|Aug(ust)?|Oct(ober)?|(Sept|Nov|Dec)(ember)?)\\ (0?[1-9]|([12]\\d)|30))|(Feb(ruary)?\\ (0?[1-9]|1\\d|2[0-8]|(29(?=,?\\ ((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)))))))\\,?\\ ((1[6-9]|[2-9]\\d)\\d{2}))$/';
			$regex['My']	= '%^(Jan(uary)?|Feb(ruary)?|Ma(r(ch)?|y)|Apr(il)?|Ju((ly?)|(ne?))|Aug(ust)?|Oct(ober)?|(Sep(?=\\b|t)t?|Nov|Dec)(ember)?)[ /]((1[6-9]|[2-9]\\d)\\d{2})$%';
			$regex['my']	= '%^(((0[123456789]|10|11|12)([- /.])(([1][9][0-9][0-9])|([2][0-9][0-9][0-9]))))$%';

			foreach ($search as $key) {
				if ($this->_regex($regex[$key], $value) === true) {
					return true;
				}
			}
		} else {
			if ($this->_regex($regex, $value)) {
				return true;
			}
		}
		
		$this->setError(sprintf(JText::_('%s is not a valid date.'), $value));
		return false;
	}
	
	/**
	 * Checks if a value is in a given list.
	 *
	 * @access	public
	 * @param	string	Value to check
	 * @param	array	List to check against
	 * @return	boolean	Succcess
	 */
	function custom($value, $regex)
	{
		if ($this->_regex($value, $regex)) {
			return true;
		}
		
		$this->setError(sprintf(JText::_('%s does not match the following regex: %s.'), $value, $regex));
		return false;
	}
	
	/**
	 * Checks whether an expression matches a regex
	 * 
	 * @access	private
	 * @param	string	The regex
	 * @param	mixed	The expression to test
	 * @return	boolean	True if it is, false otherwise
	*/
	function _regex($regex, $expression)
	{
		if (preg_match($regex, $expression)) {
			return true;
		}
		
		return false;
	}
}
?>