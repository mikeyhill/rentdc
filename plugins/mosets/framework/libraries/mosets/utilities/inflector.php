<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	Utilities
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Inflector Class
 *
 * @package		Mosets
 * @subpackage	Utilities
 * @author		Antoine Bernier
 */
class MosetsInflector extends JObject
{
	/**
	 * Returns given $lower_case_and_underscored_word as a CamelCased word.
	 *
	 * @param	string $lower_case_and_underscored_word Word to camelize
	 * @return	string Camelized word. LikeThis.
	 * @access	public
	 * @static
	 */
	function camelize($lowerCaseAndUnderscoredWord)
	{
		$replace = str_replace(" ", "", ucwords(str_replace("_", " ", $lowerCaseAndUnderscoredWord)));
		
		return $replace;
	}
	/**
	 * Returns an underscore-syntaxed ($like_this_dear_reader) version of the $camel_cased_word.
	 *
	 * @param string $camel_cased_word Camel-cased word to be "underscorized"
	 * @return string Underscore-syntaxed version of the $camel_cased_word
	 * @access public
	 * @static
	 */
	function underscore($camelCasedWord)
	{
		$replace = strtolower(preg_replace('/(?<=\\w)([A-Z])/', '_\\1', $camelCasedWord));
		
		return $replace;
	}
}
?>