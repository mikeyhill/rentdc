<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	Utilities
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Array Helper
 *
 * @package		Mosets
 * @subpackage	Utilities
 * @author		Antoine Bernier
 */

mimport('joomla.utilities.arrayhelper');

class MosetsArrayHelper extends JArrayHelper
{
	/**
	 * array_merge_recursive which override value with next value
	 * 
	 * In this version the values are overwritten only if they are not an array.  If the value is an array, its elements will be merged/overwritten
	 *
	 * @access	public
	 * @param	array	Base array
	 * @param	array	Next array
	 * @return	array	Merged array
	 */
	function array_merge_recursive_unique($array0, $array1)
	{
	    $arrays = func_get_args();
	    $remains = $arrays;

	    // We walk through each arrays and put value in the results (without
	    // considering previous value).
	    $result = array();

	    // loop available array
	    foreach ($arrays as $array) {

	        // The first remaining array is $array. We are processing it. So
	        // we remove it from remaing arrays.
	        array_shift($remains);

	        // We don't care non array param, like array_merge since PHP 5.0.
	        if (is_array($array)) {
	            // Loop values
	            foreach ($array as $key => $value) {
	                if (is_array($value)) {
	                    // we gather all remaining arrays that have such key available
	                    $args = array();
	                    foreach ($remains as $remain) {
	                        if (array_key_exists($key, $remain)) {
	                            array_push($args, $remain[$key]);
	                        }
	                    }

	                    if (count($args) > 2) {
	                        // put the recursion
	                        $result[$key] = call_user_func_array(__FUNCTION__, $args);
	                    } else {
	                        foreach ($value as $vkey => $vval) {
	                            $result[$key][$vkey] = $vval;
	                        }
	                    }
	                } else {
	                    // simply put the value
	                    $result[$key] = $value;
	                }
	            }
	        }
	    }
	    return $result;
	}
	
	/**
	 * Recursively search for a key into an array/object an return the corresponding item
	 * 
	 * @access	public
	 * @param	string				the key to look for
	 * @param	array|object		the array|object to look into
	 * @return	array|object|false
	 */
	function array_key_search($needle, $haystack)
	{
		if (array_key_exists($needle, $haystack))
	        return (is_array($haystack) ? $haystack[$needle] : $haystack->$needle);

	    foreach ($haystack as $v)
	    {
	        if ((is_array($v) || is_object($v)) && $this->array_key_search($needle, $v))
	            return (is_array($v) ? $v[$needle] : $v->$needle);
	    }

	    return false;
	}
	
	/**
	 * Cut (copy and remove) element from an array
	 * 
	 * @access	public
	 * @param	string				the key to look for
	 * @param	array|object		the array|object to look into
	 * @return	array|object|false
	 */
	function cut_element($needle, &$haystack)
	{
		if (!isset($haystack[$needle])) return false;
		
		$needleOffset = array_search($needle, array_keys($haystack));
		$result = array_values(array_splice($haystack, $needleOffset, 1));
		
		return $result[0];
		
	}
}
?>