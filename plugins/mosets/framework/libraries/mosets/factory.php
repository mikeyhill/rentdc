<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Factory Class
 *
 * @package		Mosets
 * @author		Antoine Bernier
 */

mimport('joomla.factory');

class MosetsFactory extends JFactory
{
	/**
	 * Get a application object
	 *
	 * Returns a reference to the global {@link MosetsApplication} object, only creating it if it doesn't already exist.
	 *
	 * @access public
	 * @param	mixed	The name of the application
	 * @param	string	The Application prefix. Optional
	 * @param	array	An optional associative array of configuration settings.
	 * @return	object	MosetsApplication
	 */
	function &getApplication($client = null, $prefix = 'Mosets', $config = array())
	{
		static $instance;

		if (!is_object($instance)) {
			mimport('mosets.application.application');

			if (!$client) {
				JError::raiseError(500, 'Application Instantiation Error');
			}

			$instance = MosetsApplication::getInstance($client, $prefix, $config);
		}

		return $instance;
	}
	
	/**
	 * Get a database object
	 *
	 * Returns a reference to the global {@link MosetsDatabase} object, only creating it
	 * if it doesn't already exist.
	 *
	 * @return object MosetsDatabase
	 */
	function &getDBO()
	{
		static $instance;

		if (!is_object($instance)) {
			//get the debug configuration setting
			$conf =& JFactory::getConfig();
			$debug = $conf->getValue('config.debug');

			$instance = MosetsFactory::_createDBO();
			$instance->debug($debug);
		}

		return $instance;
	}
	
	/**
	 * Create an database object
	 *
	 * @access private
	 * @return object JDatabase
	 * @since 1.5
	 */
	function &_createDBO()
	{
		mimport('mosets.database.database');
		mimport('mosets.database.table');

		$conf =& JFactory::getConfig();

		$host 		= $conf->getValue('config.host');
		$user 		= $conf->getValue('config.user');
		$password 	= $conf->getValue('config.password');
		$database	= $conf->getValue('config.db');
		$prefix 	= $conf->getValue('config.dbprefix');
		$driver 	= $conf->getValue('config.dbtype');
		$debug 		= $conf->getValue('config.debug');

		$options	= array('driver' => $driver, 'host' => $host, 'user' => $user, 'password' => $password, 'database' => $database, 'prefix' => $prefix);

		$db =& MosetsDatabase::getInstance($options);

		if ( JError::isError($db) ) {
			jexit('Database Error: ' . $db->toString());
		}

		if ($db->getErrorNum() > 0) {
			JError::raiseError(500 , 'MosetsDatabase::getInstance: Could not connect to database <br />' . 'joomla.library:'.$db->getErrorNum().' - '.$db->getErrorMsg() );
		}

		$db->debug($debug);
		
		return $db;
	}
}
?>