<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	Base.Tree
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Tree Class
 *
 * @package		Mosets
 * @subpackage	Base.Tree
 * @author		Antoine Bernier
 */
class MosetsTree extends JObject
{
	/**
	 * Root of the tree
	 * 
	 * @var		MosetsNode
	 * @access	private
	 */
	var $__root = null;
	
	/**
	 * Registry of Nodes
	 * 
	 * @var		array
	 * @access	private
	 */
	var $__nodesRegistry = array();
	
	/**
	 * Current Node index
	 * 
	 * @var		string
	 * @access	private
	 */
	var $__currentNodeIndex = null;
	
	/**
	 * Constructor
	 *
	 * @access	protected
	 * @return	void
	 */
	function __construct()
	{
		$this->__root =& new MosetsNode('__root__');
		$this->__registerNode($this->__root, true);
	}
	
	/**
	 * Register a node
	 * 
	 * @access	private
	 * @param	MosetsNode	A reference to a node
	 * @param	boolean		If true, set it as current node
	 * @return	void
	 */
	function __registerNode(&$node, $current = false)
	{
		$this->__nodesRegistry[$node->id] =& $node;
		
		if ($current)
			$this->__currentNodeIndex = $node->id;
	}
	
	/**
	 * Get a reference to a node
	 * 
	 * If no node index provided, return the current one
	 * 
	 * @access	public
	 * @param	string	The node's index
	 * @return	A reference to the node
	 */
	function &getNode($nodeIndex = null)
	{
		$return = false;
		
		if (is_null($nodeIndex))
			$nodeIndex = $this->__currentNodeIndex;
		
		if (!array_key_exists($nodeIndex, $this->__nodesRegistry)) {
			$this->setError(sprintf(get_class($this) . "::getNode() : cannot find node '%s', index not found!", $nodeIndex));
			return $return;
		}
		
		return $this->__nodesRegistry[$nodeIndex];
	}
	
	/**
	 * Add a node to the tree
	 * 
	 * @access	public
	 * @param	MosetsNode	A reference to the node to be added
	 * @param	boolean		Whether to set it current or not
	 * @param	string		The index of the node to add it
	 * @return
	 */
	function addNode(&$node, $current = false, $destinationNodeIndex = null)
	{
		if (!($destinationNode =& $this->getNode($destinationNodeIndex))) {
			$this->setError(sprintf(get_class($this) . "::addNode() : cannot find node '%s', index not found!", $destinationNodeIndex));
			return false;
		}
		
		$destinationNode->addChild($node);
		$this->__registerNode($node, $current);
		
		return $this;
	}
	
	/**
	 * Set a node as current
	 * 
	 * If no node index is provide, let's reset to the root node
	 * 
	 * @access	public
	 * @param	string	The index of the node
	 * @return
	 */
	function setCurrentNode($nodeIndex = null)
	{
		if (!is_null($nodeIndex)) {
			if (!array_key_exists($nodeIndex, $this->__nodesRegistry)) {
				$this->setError(sprintf(get_class($this) . "::setCurrentNode() : cannot find node '%s', index not found!", $nodeIndex));
				return false;
			}
		} else {
			$nodeIndex = '__root__';
		}
		
		return ($this->__currentNodeIndex = $nodeIndex);
	}
	
	/**
	 * Get children nodes
	 * 
	 * If no node index is provide, let's retrieve them from the current node
	 * 
	 * @access	public
	 * @param	string	The index of the node
	 * @return	array	An array of nodes references
	 */
	function getChildrenNodes($nodeIndex = null)
	{
		static $result = array();
		
		if (!($node =& $this->getNode($nodeIndex))) {
			$this->setError(sprintf(get_class($this) . "::getParentsNodes() : cannot find node '%s', index not found!", $nodeIndex));
			return false;
		}
		
		if ($children = $node->children) {
			foreach($children as $child)
			{
				array_push($result, $child);
				$this->getChildrenNodes($child->id);
			}
		}
		
		return $result;
	}
	
	/**
	 * Get children nodes
	 * 
	 * If no node index is provide, let's retrieve them from the current node
	 * 
	 * @access	public
	 * @param	string	The index of the node
	 * @return	array	An array of nodes references
	 */
	function getParentsNodes($nodeIndex = null)
	{
		static $result = array();
		
		if (!($node =& $this->getNode($nodeIndex))) {
			$this->setError(sprintf(get_class($this) . "::getParentsNodes() : cannot find node '%s', index not found!", $nodeIndex));
			return false;
		}
		
		if (($parent = $node->parent) != $this->__root) {
			array_push($result, $parent);
			$this->getParentsNodes($parent->id);
		}
		
		return $result;
	}
	
	/**
	 * Breadth-first search for a node (@see http://en.wikipedia.org/wiki/Breadth-first_search)
	 * 
	 * @access	public
	 * @param	callback	A compare function, for example: create_function('$node', 'return ($node->data->name == "foo")')
	 * @param	string		The node's index to begin the search from
	 * @return	mixed		A reference to the found node, false otherwise
	 */
	function &breadthFirstSearch($compare_func, $fromNodeIndex = null)
	{
		$return	= false;
		$fifo	= array();
		
		if (!($fromNode =& $this->getNode($fromNodeIndex))) {
			$this->setError(sprintf(get_class($this) . "::breadthFirstSearch() : cannot find node '%s', index not found!", $fromNodeIndex));
			return false;
		}
		
		array_push($fifo, $fromNode);
		
		while(!empty($fifo)) {
			$examinedNode =& array_pop($fifo);
			if (call_user_func($compare_func, $examinedNode)) {
				return $examinedNode;
			} else {
				foreach ($examinedNode->children as $child)
					array_push($fifo, $child);
				if (empty($fifo))
					return $return;
			}
		}
	}
}

/**
 * Node Class
 *
 * @package		Mosets
 * @subpackage	Base.Tree
 * @author		Antoine Bernier
 */
class MosetsNode extends JObject
{
	/**
	 * The identifier
	 * 
	 * @var		string
	 * @access	public
	 */
	var $id = null;
	
	/**
	 * Data
	 * 
	 * @var		string
	 * @access	public
	 */
	var $data = null;
	
	/**
	 * A reference to the parent node
	 * 
	 * @var		MosetsNode
	 * @access	public
	 */
	var $parent = null;
	
	/**
	 * List of children
	 * 
	 * @var		array	An array of MosetsNode nodes
	 * @access	public
	 */
	var $children = array();
	
	/**
	 * Constructor
	 *
	 * @access	protected
	 * @param	string	The identifier of the node to be created
	 * @param	mixed	Data
	 * @return	void
	 */
	function __construct($id, $data = null)
	{
		$this->id = $id;
		$this->data = $data;
	}
	
	/**
	 * Add a child
	 *
	 * @access	public
	 * @param	MosetsNode	
	 * @return	void
	 */
	function addChild(&$node)
	{
		$node->parent =& $this;
		array_push($this->children, $node);
	}
}
?>