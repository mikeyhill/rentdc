<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	HTML
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * HTML Class
 *
 * @package		Mosets
 * @subpackage	HTML
 * @author		Antoine Bernier
 */

mimport('joomla.html.html');

class MosetsHTML extends JoomlaHTML
{
	/**
	 * Class loader method
	 *
	 * Additional arguments may be supplied and are passed to the sub-class.
	 * Additional include paths are also able to be specified for third-party use
	 *
	 * @param	string	The name of helper method to load, (prefix).(class).function
	 *                  prefix and class are optional and can be used to load custom
	 *                  html helpers.
	 */
	function _($type)
	{
		//Initialise variables
		$prefix = 'MosetsHTML';
		$file   = '';
		$func   = $type;

		// Check to see if we need to load a helper file
		$parts = explode('.', $type);

		switch(count($parts))
		{
			case 3 :
				$prefix		= preg_replace( '#[^A-Z0-9_]#i', '', $parts[0] );
				$file		= preg_replace( '#[^A-Z0-9_]#i', '', $parts[1] );
				$func		= preg_replace( '#[^A-Z0-9_]#i', '', $parts[2] );
				break;
			case 2 :
				$file		= preg_replace( '#[^A-Z0-9_]#i', '', $parts[0] );
				$func		= preg_replace( '#[^A-Z0-9_]#i', '', $parts[1] );
				break;
		}

		$className	= $prefix . ucfirst($file);

		if (!class_exists($className)) {
			jimport('joomla.filesystem.path');
			if ($path = JPath::find(MosetsHTML::addIncludePath(), strtolower($file) . '.php')) {
				require_once $path;

				if (!class_exists($className)) {
					JError::raiseWarning(0, $className . '::' . $func . ' not found in file.');
					return false;
				}
			} else {
				$args = func_get_args();
				return call_user_func_array(array('JoomlaHTML', '_'), $args);	// Degrades to JoomlaHTML::_()
			}
		}

		if (is_callable(array($className, $func))) {
			$args = func_get_args();
			array_shift($args);
			mimport('mosets.html.helper');
			return call_user_func_array(array($className, $func), $args);
		} else {
			$args = func_get_args();
			return call_user_func_array(array('JoomlaHTML', '_'), $args);	// Degrades to JoomlaHTML::_()
		}
	}
	
	function header($level, $text, $attribs = null)
	{
		return '<h' . $level . ' ' . MosetsHTMLHelper::renderAttributes($attribs) . '>' . $text . '</h' . $level . '>';
	}
	
	function anchor($idtext, $text, $attribs = null)
	{
		return MosetsHTML::_('link', JRequest::getVar('REQUEST_URI', null, 'server', 'string') . '#' . $idtext, $text, $attribs);
	}
	
	/**
	 * Write a <script></script> element
	 * 
	 * Overload JoomlaHTML::script() to define our custom path for js files
	 *
	 * @access	public
	 * @param	string 	The name of the script file
	 * @param	string 	The relative or absolute path of the script file
	 * @param	boolean If true, the mootools library will be loaded
	 */
	function script($filename, $path = MOSETS_MEDIA_JS_URL, $mootools = true)
	{
		parent::script($filename, $path, $mootools);
	}
	
	/**
	 * Write a <link rel="stylesheet" style="text/css" /> element
	 * 
	 * Overload JoomlaHTML::stylesheet() to define our custom path for css files
	 *
	 * @access	public
	 * @param	string 	The relative URL to use for the href attribute
	 */
	function stylesheet($filename, $path = MOSETS_MEDIA_CSS_URL, $attribs = array())
	{
		parent::stylesheet($filename, $path, $attribs);
	}
	
	/**
	 * Displays a calendar control field
	 * 
	 * Overload JoomlaHTML::calendar() to define default date values, a new default date format and some attribs
	 *
	 * @param	string	The date value
	 * @param	string	The name of the text field
	 * @param	string	The id of the text field
	 * @param	string	The date format
	 * @param	array	Additional html attributes
	 */
	function calendar($value, $name, $idtag = false, $format = '%Y-%m-%d %H:%M:%S', $attribs = null)
	{
		$attribs = MosetsHTMLHelper::mergeAttributes($attribs, array(
			'class' => 'inputbox',
			'size' => 25,
			'maxlength' => 19
		));
		
		$db =& MosetsFactory::getDBO();
		
		if ($value == $db->getNullDate()) {
			$value = JText::_('Never');
		} else if (!is_null($value)) {
			$value = htmlspecialchars(MosetsHTML::_('date', $value, $format), ENT_COMPAT, 'UTF-8');
		} else {
			$value = '';
		}
		
		return parent::calendar($value, $name, $idtag, $format, $attribs);
	}
	
	/**
	 * Add a directory where MosetsHTML should search for helpers.
	 * 
	 * You may either pass a string or an array of directories.
	 *
	 * @access	public
	 * @param	string	A path to search.
	 * @return	array	An array with directory elements
	 */
	function addIncludePath($path = '')
	{
		static $paths;

		if (!isset($paths)) {
			$paths = array(JPath::clean(MOSETS_LIBRARIES . '/mosets/html/html'));
		}

		// force path to array
		settype($path, 'array');

		// loop through the path directories
		foreach ($path as $dir)
		{
			if (!empty($dir) && !in_array($dir, $paths)) {
				array_unshift($paths, JPath::clean($dir));
			}
		}

		return $paths;
	}
}
?>