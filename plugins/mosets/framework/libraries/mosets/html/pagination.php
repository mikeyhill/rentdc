<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	HTML
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Pagination Class
 *
 * @package		Mosets
 * @subpackage	HTML
 * @author		Antoine Bernier
 */

mimport('joomla.html.pagination');

class MosetsPagination extends JPagination
{
	var $_startTotal = null;
	var $_endTotal = null;
	var $_adjacents = null;
	
	/**
	 * Constructor
	 * 
	 * @access	public
	 * @param	array	Associative configuration array
	 * @return	void
	 */
	function __construct($total, $limitstart, $limit, $config = array())
	{
		// Set start total elements
		if (!isset($this->_startTotal)) {
			if (array_key_exists('start', $config)) {
				$this->_startTotal = $config['start'];
			} else {
				$this->_startTotal = 2;
			}
		}
		
		// Set end total elements
		if (!isset($this->_endTotal)) {
			if (array_key_exists('end', $config)) {
				$this->_endTotal = $config['end'];
			} else {
				$this->_endTotal = 2;
			}
		}
		
		// Set adjacent elements
		if (!isset($this->_adjacents)) {
			if (array_key_exists('adjacents', $config)) {
				$this->_adjacents = $config['adjacents'];
			} else {
				$this->_adjacents = 10;
			}
		}
		
		parent::__construct($total, $limitstart, $limit);
	}
	
	/**
	 * Creates a dropdown box for selecting how many records to show per page
	 *
	 * @access	public
	 * @return	string	The html for the limit # input box
	 */
	function getLimitBox()
	{
		global $mainframe;

		if ($mainframe->isAdmin()) {
			return parent::getLimitbox();
		} else {
			$limits = array();
			// Restrict front view limits
			foreach (array(5, 10, 25, 50, 100) as $limit_value)
			{
				$limits[] = JHTML::_('select.option', $limit_value);
				
				if ($this->total <= $limit_value) {
					break;
				}
			}
			
			$html = '<ul>';
			foreach ($limits as $limit)
			{
				$html .= '<li>' . MosetsHTML::_('link', JRoute::_('&limit=' . $limit->text), $limit->text, ($this->limit == $limit->text ? array('class' => 'active') : null)) . '</li>';
			}
			$html .= '</ul>';
		}
		
		return $html;
	}
	
	/**
	 * Create and return the pagination data object
	 *
	 * @access	public
	 * @return	object	Pagination data object
	 */
	function _buildDataObject()
	{
		$data = parent::_buildDataObject();
		
		$total = $this->get('pages.total');
		$current = $this->get('pages.current');
		$start = $this->_startTotal;
		$end = $this->_endTotal;
		$width = $this->_adjacents;
		
		$losange = $current - ($width - $width % 2 * 1) / 2;
		$carre = $current + floor(($width - (1 - $width % 2)) / 2);
		if ($losange <= $start + 1) {
			$carre = $width;
		}
		if ($carre >= $total - $end + 1) {
			$losange = $total - $width + 1;
		}

		for ($p = 1; $p <= $total; $p++) 
		{
			$p_old = $p;
			$jump = false;
			
			if ($p > $start && $p < $losange) {
				$p = $losange;
				$jump = true;
			}

			if ($p < $total - $end + 1 && $p > $carre) {
				$p = $total - $end + 1;
				$jump = true;
			}

			if ($jump) {
				$data->pages[$p-1] = new JPaginationObject('...');
			}
			
			$data->pages[$p] = new JPaginationObject($p);
			if ($p != $this->get('pages.current') || $this->_viewall) {
				$offset = ($p - 1) * $this->limit;
				$offset = $offset == 0 ? '' : $offset;  // set the empty for removal from route
				
				$data->pages[$p]->base	= $offset;
				$data->pages[$p]->link	= JRoute::_("&limitstart=" . $offset);
			}
		}
		ksort($data->pages);
		
		/*echo "<pre>";
		print_r($data);
		echo"</pre>";*/
		return $data;
	}

	function _list_render($list)
	{
		// Initialize variables
		$lang =& JFactory::getLanguage();
		
		$html = '<ul class="pagination">';

		$html .= $list['previous']['data'];

		foreach($list['pages'] as $page)
		{
			$html .= $page['data'];
		}

		$html .= $list['next']['data'];

		$html .= '</ul>';
		
		return $html;
	}

	function _item_active(&$item)
	{
		$class = null;
		switch ($item->text)
		{
			case JText::_('Prev'):
				$class = 'previous';
				break;
			case JText::_('Next'):
				$class = 'next';
				break;
			default:
				break;
		}
		
		$rel = null;
		switch ($item->text)
		{
			case JText::_('Prev'):
			case $this->get('pages.current') - 1:
				$rel = 'prev';
				break;
			case JText::_('Next'):
			case $this->get('pages.current') + 1:
				$rel = 'next';
				break;
			default:
				break;
		}
		
		return '<li' . (!empty($class) ? ' class="' . $class . '"' : '') . '><a' . (!empty($rel) ? ' rel="' . $rel . '"' : '') . ' href="' . $item->link . '" title="' . $item->text . '">' . $item->text . '</a></li>';
	}

	function _item_inactive(&$item) {
		$class = null;
		if (empty($item->base)) {
			switch($item->text)
			{
				case JText::_('Prev'):
					$class = 'previous';
					break;
				case JText::_('Next'):
					$class = 'next';
					break;
				default:
					if ($item->text != '...') {
						$class = 'active';
					}
					break;
			}
		}
		return '<li' . (!empty($class) ? ' class="' . $class . '"' : '') . '><span>' . $item->text . '</span></li>';
	}
}
?>