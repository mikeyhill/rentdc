<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	HTML.HTML
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Filesystem HTML Class
 *
 * @package		Mosets
 * @subpackage	HTML.HTML
 * @author		Antoine Bernier
 */

class MosetsHTMLFilesystem
{
	/**
	 * Check if a list of files/folders are writable and display a notice message for those which are not
	 * 
	 * @access	public
	 * @param	array	A list of files/folders
	 * @return	void
	 * @todo	Integrate that function with JFTP to try chmoding before displaying an error
	 */
	function iswritable($filenames)
	{
		$not_writable = array();
	
		foreach ($filenames as $filename)
		{
			jimport('joomla.filesystem.path');
			$filename = JPath::clean($filename);
			
			if (!is_writable($filename)) {
				array_push($not_writable, substr($filename, strlen(JPATH_ROOT) + 1));
			}
		}
	
		if (!empty($not_writable)) {
			$msg = sprintf(JText::_('FOLLOWING FILES/DIRECTORIES ARE NOT WRITABLE: %s.'), implode(', ', $not_writable));
			return MosetsHTML::_('interface.advice', $msg, 'notice');
		}
		
		return;
	}
}
?>