<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	HTML.HTML
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Content HTML Class
 *
 * @package		Mosets
 * @subpackage	HTML.HTML
 * @author		Antoine Bernier
 */

class MosetsHTMLContent
{
	/**
	 * Helper to print a header with an anchor
	 * 
	 * @param	int		The header level, eg i where h1, h2, ... , hi, ..., hn
	 * @param	string	The header text
	 * @param	string	The anchor text
	 */
	function header($level, $text, $idtag = false, $anchor = null, $attribs = null)
	{
		if (empty($anchor)) {
			$anchor = '¶';
		}
		
		if (!$idtag) {
			$idtag	= $text;
		}
		$idtag = JFilterInput::clean($idtag, 'cmd');
		
		$attribs = MosetsHTMLHelper::mergeAttributes($attribs, array(
			'id' => $idtag,
			'class' => 'heading'
		));
		
		$text .= MosetsHTML::_('anchor', $idtag, $anchor, array('class' => 'anchor'));
	
		return MosetsHTML::_('header', $level, $text, $attribs);
	}
	
	/**
	 * 
	 * 
	 * @param	string	The link title
	 * @param	string	The order field for the column
	 * @param	string	The current direction
	 * @param	string	The selected ordering
	 * @param	string	An optional task override
	 */
	function sort($component, $title, $ordering, $direction = 'asc', $selected = 0, $attribs = null)
	{
		global $mainframe;
		
		$previous_ordering	= $mainframe->getUserState('com_' . $component . '.' . MosetsInflector::underscore(JRequest::getString('view')) . '.ordering');

		$direction	= strtolower($direction);

		$url		= JRoute::_('&ordering=' . $ordering . ($ordering == $previous_ordering ? '&ordering_dir=' . ($direction == 'desc' ? 'asc' : 'desc') : ''));
		$text		= $title;
		$attribs	= MosetsHTMLHelper::mergeAttributes($attribs, array(
			'title' => JText::_('Click to sort this column')
		));

		if ($ordering == $selected) {
			$attribs	= MosetsHTMLHelper::mergeAttributes($attribs, array(
				'class' => 'active ' . $direction
			));
		}

		$html = MosetsHTML::_('link', $url, $text, $attribs);

		return $html;
	}
}
?>