<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	HTML.HTML
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Form HTML Class
 *
 * @package		Mosets
 * @subpackage	HTML.HTML
 * @author		Antoine Bernier
 */

class MosetsHTMLForm
{
	/**
	 * Label
	 * 
	 * @access	public
	 * @param
	 */
	function label($value, $name = '', $required = false, $tip = '', $attribs = null, $translate = false)
	{
		if (!empty($name)) {
			$attribs = MosetsHTMLHelper::mergeAttributes($attribs, array('for' => JFilterInput::clean($name, 'cmd')));
		}
		
		if (!empty($tip)) {
			MosetsHTML::_('behavior.tooltip');
			$attribs = MosetsHTMLHelper::mergeAttributes($attribs, array(
				'class' => 'hasTip',
				'title' => ($translate ? JText::_($value) : $value) . '::' . ($translate ? JText::_($tip) : $tip)
			));
		}
		
		$star	= ($required ? '<abrr title="Required field">' . ($translate ? JText::_('*') : '*') . '</abrr>' : '');
		
		$html = '<label' . ($attribs ? ' ' . MosetsHTMLHelper::renderAttributes($attribs) : '') . '>' . ($translate ? JText::_($value) : $value) . $star . '</label>';
		
		return $html;
	}
	
	/**
	 * Input
	 * 
	 * @access	public
	 * @param	string	The input type, among text|password|checkbox|radio|submit|reset|file|hidden|image|button	// @see http://www.w3.org/TR/html401/interact/forms.html#adef-type-INPUT
	 * @param	string	The input name
	 * @param	string	The input value
	 * @param	string	The id tag
	 * @param	array	Associative array of input attributes
	 */
	function input($type, $name = null, $value = null, $idtag = false, $attribs = null)
	{
		if ($type == 'text') {
			$attribs = MosetsHTMLHelper::mergeAttributes($attribs, array('class' => 'inputbox'));
		} elseif ($type == 'submit' || $type == 'button') {
			$attribs = MosetsHTMLHelper::mergeAttributes($attribs, array('class' => 'button'));
		}
		
		$id = null;
		if (!is_null($idtag)) {
			$id = $name;
			if ($idtag) {
				$id = $idtag;
			}
			$id = JFilterInput::clean($id, 'cmd');
		}
		
		$html = '<input type="' . $type . '"' . ($name ? ' name="' . $name . '"' : '') . ($id ? ' id="' . $id . '"' : '') . (!is_null($value) ? ' value="' . $value . '"' : '') . ($attribs ? ' ' . MosetsHTMLHelper::renderAttributes($attribs) : '') . ' />';
		 
		return $html;
	}
	
	/**
	 * Input
	 * 
	 * @access	public
	 * @param	string	The button name
	 * @param	string	The button value
	 * @param	string	The id tag
	 * @param	array	Associative array of input attributes
	 */
	function button($value, $name = null, $idtag = false, $attribs = null)
	{
		$id = null;
		if (!is_null($idtag)) {
			$id = $name;
			if ($idtag) {
				$id = $idtag;
			}
			$id = JFilterInput::clean($id, 'cmd');
		}
		
		$html = '<button ' . ($name ? ' name="' . $name . '"' : '') . ($id ? ' id="' . $id . '"' : '') . ($attribs ? ' ' . MosetsHTMLHelper::renderAttributes($attribs) : '') . '>' . $value . '</button>';
		 
		return $html;
	}
	
	/**
	 * Textarea
	 * 
	 * @access	public
	 * @param	string	The input name
	 * @param	string	The input value
	 * @param	string	The id tag
	 * @param	int		The rows value
	 * @param	int		The cols value
	 * @param	array	Associative array of input attributes
	 */
	function textarea($name = null, $value = '', $idtag = false, $cols = 30, $rows = 5, $attribs = null)
	{
		$attribs = MosetsHTMLHelper::mergeAttributes($attribs, array('class' => 'inputbox'));
		
		$id = null;
		if (!is_null($idtag)) {
			$id = $name;
			if ($idtag) {
				$id = $idtag;
			}
			$id = JFilterInput::clean($id, 'cmd');
		}
		
		$html = '';
		$html .= '<textarea rows="' . $rows . '" cols="' . $cols . '"' . ($name ? ' name="' . $name . '"' : '') . ($id ? ' id="' . $id . '"' : '') . ($attribs ? ' ' . MosetsHTMLHelper::renderAttributes($attribs) : '') . '>';
		$html .= 	$value;
		$html .= '</textarea>';
		 
		return $html;
	}
	
	/**
	 * WYSIWYG Editor
	 * 
	 * @param	string	The control name
	 * @param	string	The contents of the text area
	 * @param	string	The width of the text area (px or %)
	 * @param	string	The height of the text area (px or %)
	 * @param	int		The number of columns for the textarea
	 * @param	int		The number of rows for the textarea
	 * @param	boolean	True and the editor buttons will be displayed
	 * @param	array	Associative array of editor parameters
	 */
	function wysiwyg($name, $html, $width = '100%', $height = '400', $col = '70', $row = '15', $buttons = true, $params = array('pagebreak'))
	{
		$editor =& JFactory::getEditor();
		return $editor->display($name, $html, $width, $height, $col, $row, $params);
	}
	
	/**
	 * Radio
	 * 
	 * @access	public
	 * @param	string	The input name
	 * @param	string	The input value
	 * @param	string	The selected value
	 * @param	string	The id tag
	 * @param	array	Associative array of input attributes
	 */
	function radio($name = null, $value = '1', $selected = null, $idtag = false, $attribs = null)
	{
		$id = null;
		if (!is_null($idtag)) {
			$id = $name;
			if ($idtag) {
				$id = $idtag;
			}
			$id = JFilterInput::clean($id, 'cmd');
		}
		
		// Checked
		if ($selected == $value) {
			$attribs = MosetsHTMLHelper::mergeAttributes($attribs, array('checked' => 'checked'));
		}
		
		return MosetsHTML::_('form.input', 'radio', $name, $value, $id, $attribs);
	}
	
	/**
	* Generates an HTML radio list
	*
	* @param	array	An array of objects
	* @param	string	The value of the HTML name attribute
	* @param	string	Additional HTML attributes for the <select> tag
	* @param	mixed	The key that is selected
	* @param	string	The name of the object variable for the option value
	* @param	string	The name of the object variable for the option text
	* @return	string	HTML for the select list
	*/
	function radiolist($arr, $name = null, $attribs = null, $key = 'value', $text = 'text', $selected = null, $idtag = false, $translate = false)
	{
		reset($arr);
		$html = '';

		$id = null;
		if (!is_null($idtag)) {
			$id = $name;
			if ($idtag) {
				$id = $idtag;
			}
			$id = JFilterInput::clean($id, 'cmd');
		}
		
		$html .= '<ul>';
		for ($i=0, $n = count($arr); $i < $n; $i++)
		{
			$html .= '<li>' . MosetsHTML::_('form.label', MosetsHTML::_('form.radio', $name, $arr[$i]->$key, $selected, $id . $arr[$i]->$key, $attribs) . ($translate ? JText::_($arr[$i]->$text) : $arr[$i]->$text)) . '</li>';
		}
		$html .= "</ul>";
		
		return $html;
	}
	
	/**
	* Generates a yes/no radio list
	*
	* @param	string	The value of the HTML name attribute
	* @param	string	Additional HTML attributes for the <select> tag
	* @param	mixed	The key that is selected
	* @return	string	HTML for the radio list
	*/
	function booleanlist($name = null, $attribs = null, $selected = null, $yes = null, $no = null, $id = false)
	{
		if (empty($yes))	$yes = 'Yes';
		if (empty($no))		$no = 'No';
		
		$arr = array(
			MosetsHTML::_('select.option',  '0', $no),
			MosetsHTML::_('select.option',  '1', $yes)
		);
		
		return MosetsHTML::_('form.radiolist', $arr, $name, $attribs, 'value', 'text', (int) $selected, $id, true);
	}
	
	/**
	 * Checkbox
	 * 
	 * @access	public
	 * @param	string	The input name
	 * @param	string	The input value
	 * @param	string	The selected value
	 * @param	string	The id tag
	 * @param	array	Associative array of input attributes
	 */
	function checkbox($name = null, $value = '1', $selected = null, $idtag = false, $attribs = null)
	{
		$id = null;
		if (!is_null($idtag)) {
			$id = $name;
			if ($idtag) {
				$id = $idtag;
			}
			$id = JFilterInput::clean($id, 'cmd');
		}
		
		// Checked
		if ($selected == $value) {
			$attribs = MosetsHTMLHelper::mergeAttributes($attribs, array('checked' => 'checked'));
		}
		
		return MosetsHTML::_('form.input', 'checkbox', $name, $value, $id, $attribs);
	}
	
	/**
	 * Checkbox list
	 * 
	 * @access	public
	 * @param	string	The input name
	 * @param	string	The input value
	 * @param	string	The selected value
	 * @param	string	The id tag
	 * @param	array	Associative array of input attributes
	 */
	function checkboxlist($arr, $name, $attribs = null, $key = 'value', $text = 'text', $selected = null, $idtag = false, $translate = false)
	{
		reset($arr);
		$html = '';

		$id_text = null;
		if (!is_null($idtag)) {
			$id_text = $name;
			if ($idtag) {
				$id_text = $idtag;
			}
			$id_text = JFilterInput::clean($id_text, 'cmd');
		}

		$html .= '<ul>';
		for ($i=0, $n=count($arr); $i < $n; $i++)
		{
			$k	= $arr[$i]->$key;
			$t	= $translate ? JText::_($arr[$i]->$text) : $arr[$i]->$text;
			
			$extra	= array();
			
			if ($id	= (isset($arr[$i]->id) ? @$arr[$i]->id : null)) {
				$extra = MosetsHTMLHelper::mergeAttributes($extra, array('id' => $id));
			}
			
			if (is_array($selected)) {
				foreach ($selected as $val)
				{
					$k2 = is_object($val) ? $val->$key : $val;
					if ($k == $k2) {
						$extra = MosetsHTMLHelper::mergeAttributes($extra, array('checked' => 'checked'));
						break;
					}
				}
			} else {
				if ($k == $selected) {
					$extra = MosetsHTMLHelper::mergeAttributes($extra, array('checked' => 'checked'));
				}
			}
			$html .= '<li>' . MosetsHTML::_('form.label', MosetsHTML::_('form.checkbox', $name, $k, null, $id_text . $k, MosetsHTMLHelper::mergeAttributes($attribs, $extra)) . $t) . '</li>';
		}
		$html .= "</ul>";
		
		return $html;
	}
}
?>