<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	HTML.HTML
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Grid HTML Class
 *
 * @package		Mosets
 * @subpackage	HTML.HTML
 * @author		Antoine Bernier
 */

class MosetsHTMLGrid
{
	/**
	 * id
	 * 
	 * Overload parent::id to name id: 'id' (not 'cid')
	 * 
	 * @access	public
	 * @param	int		The row index
	 * @param	int		The record id
	 * @param	boolean	
	 * @param	string	The name of the form element
	 * @return	string
	 */
	function id($rowNum, $recId, $checkedOut = false, $name = 'id')
	{
		return JoomlaHTML::_('grid.id', $rowNum, $recId, $checkedOut, $name);
	}
	
	/**
	 * An image link to save rows order
	 * 
	 * Overloads parent::order and only displays the save icon when sorting by 'ordering'
	 * 
	 * @access	public
	 * @param	array	An array of rows db objects
	 * @param	string	The order state
	 * @param	string	The saveorder image
	 * @param	string	The task to call
	 * @return	string	A html image link
	 */
	function order($rows, $order, $image = 'filesave.png', $task = "saveorder")
	{		
		if (strpos($order, 'ordering')) {
			return JoomlaHTML::_('grid.order', $rows, $image, $task);
		} else {
			return '';
		}
	}
	
	/**
	 * A select list of states
	 * 
	 * Overloads parent::state and adds a 'Not Archived' state
	 * 
	 * @access	public
	 * @param	???
	 * @param	string	Published option
	 * @param	string	Unpublished option
	 * @param	string	Archived option
	 * @param	string	Not Archived option
	 * @param	string	Trashed option
	 * @return	string	An html select list of states
	 */
	function state($name, $selected, $published = '', $unpublished = '', $archived = null, $not_archived = null, $trashed = null)
	{
		if (empty($published))		$published = JText::_('Published');
		if (empty($unpublished))	$unpublished = JText::_('Unpublished');		
		
		$state[] = MosetsHTML::_('select.option',  '', '- '. sprintf(JText::_('Select %s'), JText::_('State')) .' -');
		$state[] = MosetsHTML::_('select.option',  '1', $published);
		$state[] = MosetsHTML::_('select.option',  '0', $unpublished);

		if ($archived) {
			$state[] = MosetsHTML::_('select.option',  '-1', ucfirst($archived));
		}
		
		/*if ($not_archived) {
			$state[] = MosetsHTML::_('select.option',  'NA', JText::_($not_archived));
		}*/

		/*if ($trashed) {
			$state[] = MosetsHTML::_('select.option',  'T', JText::_($trashed));
		}*/

		return MosetsHTML::_('select.genericlist', $state, $name, array('size' => 1, 'onchange' => 'javascript:submitform();'), 'value', 'text', $selected);
	}
	
	/**
	 * A image link to publish/unpublish a property in the properties list
	 * 
	 * @access	public
	 * @param	Object	A property db object
	 * @param	int		Index in the grid
	 * @param	string	The publish image
	 * @param	string	The unpublish image
	 * @param	string	The $task prefix
	 */
	function published($row, $i, $prefix='')
	{
		if (!(property_exists($row, 'publish_up') || property_exists($row, 'publish_down'))) {
			return JoomlaHTML::_('grid.published', $row, $i);
		}
		
		MosetsHTML::_('behavior.tooltip');
		
		$config	=& JFactory::getConfig();
		$now	=& JFactory::getDate();
		$db		=& MosetsFactory::getDBO();
		$nullDate = $db->getNullDate();
		
		$publish_up =& JFactory::getDate($row->publish_up);
		$publish_down =& JFactory::getDate($row->publish_down);
		$publish_up->setOffset($config->getValue('config.offset'));
		$publish_down->setOffset($config->getValue('config.offset'));
		
		if ($now->toUnix() <= $publish_up->toUnix() && $row->published == 1) {
			$img	= 'publish_y.png';
			$task	= 'unpublish';
			$alt	= JText::_('Pending');
			$action	= JText::_('Unpublish');
		} else if (($now->toUnix() <= $publish_down->toUnix() || $row->publish_down == $nullDate) && $row->published == 1) {
			$img	= 'publish_g.png';
			$task	= 'unpublish';
			$alt	= JText::_('Published');
			$action	= JText::_('Unpublish');
		} else if ($now->toUnix() > $publish_down->toUnix() && $row->published == 1) {
			$img	= 'publish_r.png';
			$task	= 'unpublish';
			$alt	= JText::_('Expired');
			$action	= 'Unpublish';
		} else if ($row->published == 0) {
			$img	= 'publish_x.png';
			$task	= 'publish';
			$alt	= JText::_('Unpublished');
			$action	= JText::_('Publish');
		} else if ($row->published == -1) {
			$img	= 'disabled.png';
			$task	= 'unpublish';
			$alt	= JText::_('Archived');
			$action	= JText::_('Unpublish');
		}
		
		$times = '';
		if (isset($row->publish_up)) {
			if ($row->publish_up == $nullDate) {
				$times .= JText::_('Start: Always');
			} else {
				$times .= JText::_('Start') .": ". $publish_up->toFormat();
			}
		}
		if (isset($row->publish_down)) {
			if ($row->publish_down == $nullDate) {
				$times .= "<br />". JText::_('Finish: No Expiry');
			} else {
				$times .= "<br />". JText::_('Finish') .": ". $publish_down->toFormat();
			}
		}
		
		$href = '
		<span class="hasTip" title="' . JText::_('Publish Information') . '::' . $times . '">
			<a href="#' . $task . '" onclick="return listItemTask(\'cb'. $i .'\',\''. $prefix.$task .'\')" title="'. $action .'">
				<img src="' . JURI::root(true) . '/administrator/images/'. $img .'" border="0" alt="'. $alt .'" />
			</a>
		</span>';

		return $href;
	}
	
	/**
	 * A image link to feature/unfeature a property
	 * 
	 * @access	public
	 * @param	Object	A property db object
	 * @param	int		Index in the grid
	 * @param	string	The publish image
	 * @param	string	The unpublish image
	 * @param	string	The $task prefix
	 */
	function featured($row, $i, $imgY = 'tick.png', $imgX = 'publish_x.png', $imgZ = 'disabled.png', $prefix='')
	{
		if ($row->featured) {
			$img = $imgY;
		} else {
			if ($row->published < 0) {
				$img = $imgZ;
			} else {
				$img = $imgX;
			}
		}
		$task 	= $row->featured ? 'unfeature' : 'feature';
		$alt 	= $row->featured ? JText::_('Featured') : JText::_('Unfeatured');
		$action = $row->featured ? JText::_('Unfeature') : JText::_('Feature');

		$href = '
		<a href="#' . $task . '" onclick="return listItemTask(\'cb'. $i .'\',\''. $prefix.$task .'\')" title="'. $action .'">
			<img src="' . JURI::root(true) . '/administrator/images/'. $img .'" border="0" alt="'. $alt .'" />
		</a>';

		return $href;
	}
}
?>