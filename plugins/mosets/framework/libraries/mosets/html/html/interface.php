<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	HTML.HTML
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Interface HTML Class
 *
 * @package		Mosets
 * @subpackage	HTML.HTML
 * @author		Antoine Bernier
 */

class MosetsHTMLInterface
{
	/**
	 * Advices
	 * 
	 * @access	public
	 * @return	void
	 */
	function advice($msg, $msg_type = 'info')
	{
		$html = '';

		switch ($msg_type)
		{
			case 'info':
				$html .= '<p class="info">';
				$html .= $msg;
				$html .= '</p>';
				break;
			case 'notice':
				JError::raiseNotice(500, $msg);
				break;
			case 'warning':
				JError::raiseWarning(500, $msg);
				break;
		}		

		return $html;
	}
	
	/**
	 * Displays the publishing state legend
	 * 
	 * @access	public
	 * @return	void
	 */
	function legend()
	{
		?>
		<p class="legend">
			<img src="<?php echo JURI::root(true) . '/administrator/images/'; ?>publish_y.png" border="0" alt="<?php echo JText::_('Pending'); ?>" />
			<?php echo JText::_('Published, but is'); ?> <u><?php echo JText::_('Pending'); ?></u>
			|
			<img src="<?php echo JURI::root(true) . '/administrator/images/'; ?>publish_g.png" border="0" alt="<?php echo JText::_('Visible'); ?>" />
			<?php echo JText::_('Published and is'); ?> <u><?php echo JText::_('Current'); ?></u>
			|
			<img src="<?php echo JURI::root(true) . '/administrator/images/'; ?>publish_r.png" border="0" alt="<?php echo JText::_('Finished'); ?>" />
			<?php echo JText::_('Published, but has'); ?> <u><?php echo JText::_('Expired'); ?></u>
			|
			<img src="<?php echo JURI::root(true) . '/administrator/images/'; ?>publish_x.png" border="0" alt="<?php echo JText::_('Finished'); ?>" />
			<?php echo JText::_('Not Published'); ?>
			|
			<img src="<?php echo JURI::root(true) . '/administrator/images/'; ?>disabled.png" border="0" alt="<?php echo JText::_('Archived'); ?>" />
			<?php echo JText::_('Archived'); ?><br />
			<?php echo JText::_('Click on icon to toggle state.'); ?>
		</p>
		<?php
	}
}
?>