<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	HTML.HTML
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Behavior HTML Class
 *
 * @package		Mosets
 * @subpackage	HTML.HTML
 * @author		Antoine Bernier
 */

class MosetsHTMLBehavior
{
	/**
	 * Load validation script
	 * 
	 * Overload parent::formvalidation() to add our own validatation script
	 */
	function formvalidation()
	{
		JoomlaHTML::_('behavior.formvalidation');
		MosetsHTML::_('script', 'validate.js');
	}
}
?>