<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	HTML.HTML
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Select HTML Class
 *
 * @package		Mosets
 * @subpackage	HTML.HTML
 * @author		Antoine Bernier
 */

class MosetsHTMLSelect
{
	/**
	 * Generates an HTML select list
	 * 
	 * Overload parent::genericlist to append class="inputbox"
	 *
	 * @param	array	An array of objects
	 * @param	string	The value of the HTML name attribute
	 * @param	string	Additional HTML attributes for the <select> tag
	 * @param	string	The name of the object variable for the option value
	 * @param	string	The name of the object variable for the option text
	 * @param	mixed	The key that is selected (accepts an array or a string)
	 * @return	string	HTML for the select list
	 */
	function genericlist($arr, $name, $attribs = null, $key = 'value', $text = 'text', $selected = null, $idtag = false, $translate = false)
	{
		$attribs = MosetsHTMLHelper::mergeAttributes($attribs, array('class' => 'inputbox'));
		
		return JoomlaHTML::_('select.genericlist', $arr, $name, $attribs, $key, $text, $selected, $idtag, $translate);
	}
	
	/**
	* Generates an HTML radio list
	* 
	* Relocate radiolist method to MosetsHTMLForm
	*
	* @param	array 	An array of objects
	* @param	string	The value of the HTML name attribute
	* @param	string	Additional HTML attributes for the <select> tag
	* @param	mixed	The key that is selected
	* @param	string	The name of the object variable for the option value
	* @param	string	The name of the object variable for the option text
	* @return	string	HTML for the select list
	*/
	function radiolist($arr, $name, $attribs = null, $key = 'value', $text = 'text', $selected = null, $idtag = false, $translate = false)
	{
		return MosetsHTML::_('form.radiolist', $arr, $name, $attribs, $key, $text, $selected, $idtag, $translate);
	}
	
	/**
	* Generates a yes/no radio list
	* 
	* Relocate booleanlist method to MosetsHTMLForm
	*
	* @param	string	The value of the HTML name attribute
	* @param	string	Additional HTML attributes for the <select> tag
	* @param	mixed	The key that is selected
	* @return	string	HTML for the radio list
	*/
	function booleanlist($name, $attribs = null, $selected = null, $yes = 'yes', $no = 'no', $id = false)
	{
		return MosetsHTML::_('form.booleanlist', $name, $attribs, $selected, $yes, $no, $id);
	}
}
?>