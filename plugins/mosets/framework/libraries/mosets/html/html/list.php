<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	HTML.HTML
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * List HTML Class
 *
 * @package		Mosets
 * @subpackage	HTML.HTML
 * @author		Antoine Bernier
 */

class MosetsHTMLList
{
	/**
	 * Select list of groups
	 * 
	 * @access	public
	 * @param	string	Name and id of the select list
	 * @param	array	Attributes associative array
	 * @param	int		The id of the selected company
	 * @param	string	The id tag
	 * @param	boolean
	 * @return	string
	 */
	function groups($name, $attribs = null, $selected = null, $idtag = false, $translate = false)
	{
		$db =& MosetsFactory::getDBO();

		$query = "SELECT id AS value, name AS text"
			. "\nFROM #__core_acl_aro_groups"
			. "\nWHERE name NOT IN ('ROOT', 'USERS', 'Public Frontend', 'Public Backend')"
			. "\nORDER BY lft ASC"
			;
		$db->setQuery($query);

		$groups = array(MosetsHTML::_('select.option', '', '- ' . sprintf(JText::_('Select %s'), JText::_('User Group')) . ' -'));
		$groups = array_merge($groups, $db->loadObjectList());

		$groups = MosetsHTML::_('select.genericlist', $groups, $name, $attribs, 'value', 'text', $selected, $idtag, $translate);

		return $groups;
	}
	
	/**
	 * Build the select list for ordering
	 * 
	 * @access	public
	 * @return	string
	 */
	function ordering($name, $component, $table_name, $row, $value = 'ordering', $text = 'name')
	{
		$table = MosetsTable::getInstance($table_name, $component);
		$table->load($row->id);
		
		$ordering_condition = $table->_getOrderingCondition();
		
		$db =& MosetsFactory::getDBO();

		if (!empty($row) && $row->id > 0) {
			$query = 'SELECT ' . $value . ' AS value, ' . $text . ' AS text'
				. ' FROM ' . $table->_tbl
				. (!empty($ordering_condition) ? ' WHERE ' . $ordering_condition : '')
				. ' ORDER BY ' . $value
				;
			$order = MosetsHTML::_('list.genericordering', $query);
			$ordering = MosetsHTML::_('select.genericlist', $order, $name, 'class="inputbox" size="1"', 'value', 'text', intval($row->ordering));
		} else {
			$ordering = MosetsHTML::_('form.input', 'hidden', $name, 9999) . JText::_('DESCNEWITEMSLAST');
		}
		
		return $ordering;
	}
	
	/**
	 * Build the select list of avalaible image libraries
	 * 
	 * @access	public
	 * @access	public
	 * @param	string	Name and id of the select list
	 * @param	array	Attributes associative array
	 * @param	int		The id of the selected type
	 * @param	string	The id tag
	 * @param	boolean
	 * @return	string
	 */
	function image_libraries($name, $attribs = null, $selected = null, $idtag = false, $translate = false)
	{
		if (is_array($attribs)) {
			$attribs['class'] = (isset($attribs['class']) ? $attribs['class'] : '') . ' inputbox';
			$attribs = JArrayHelper::toString($attribs);
		}
		
		// Require the image class utility
		mimport('mosets.utilities.image');
		$libraries = MosetsImage::find_libraries();
		
		//$image_libraries = array(MosetsHTML::_('select.option', '', '- ' . sprintf(JText::_('Select %s'), JText::_('Image Library')) . ' -'));
		if (!empty($libraries['gd1'])) {
			$image_libraries[] = MosetsHTML::_('select.option',  'gd1', JText::_('GD Library') . ' ' . $libraries['gd1']);
		}
		$image_libraries[] = MosetsHTML::_('select.option', 'gd2', JText::_('GD2 Library') . ' ' . $libraries['gd2']);
		$image_libraries[] = MosetsHTML::_('select.option', 'netpbm', (isset($libraries['netpbm'])) ? $libraries['netpbm'] : JText::_('Netpbm'));
		$image_libraries[] = MosetsHTML::_('select.option', 'imagemagick', (isset($libraries['imagemagick'])) ? $libraries['imagemagick'] : JText::_('Imagemagick'));
		
		$image_libraries = MosetsHTML::_('select.genericlist', $image_libraries, $name, $attribs, 'value', 'text', $selected, $idtag, $translate);
		
		return $image_libraries;
	}
}
?>