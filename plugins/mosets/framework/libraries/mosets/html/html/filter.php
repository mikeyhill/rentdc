<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	HTML.HTML
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Filter HTML Class
 *
 * @package		Mosets
 * @subpackage	HTML.HTML
 * @author		Antoine Bernier
 */

class MosetsHTMLFilter
{
	function search($field, $value)
	{
		//echo MosetsHTML::_('form.label', JText::_('Search'), 'filter[search][' . $field . ']');
		echo MosetsHTML::_('form.input', 'text', 'filter[search][' . $field . ']', $value, 'filtersearch' . $field);
		//echo MosetsHTML::_('form.input', 'submit', '', JText::_('Search'), null, array('class' => 'button'));
	}

	/*function lisst($field, $value, $component = 'hotproperty')
	{
		preg_match('/(\w+)\.(\w+)/i', $field, $matches);
		$modelName = $matches[1];
		$fieldName = $matches[2];
		
		$model = MosetsModel::getInstance($matches[1], $component);

		if (array_key_exists($fieldName, $model->getTableFields())) {
			$result = false;
			foreach ($model->associations['belongsTo'] as $assoc => $data)
			{
				if ($data['foreignKey'] == $fieldName) {
					$result = $assoc;
					break;
				}
			}
			if ($result) {
				$arr		= array(MosetsHTML::_('select.option', null, '- ' . sprintf(JText::_('Select %s'), JText::_(ucfirst($fieldName))) . ' -', 'id', 'name'));
				$associated_model = MosetsModel::getInstance($result, $component);
				$arr		= array_merge($arr, $associated_model->getData('list', array(
					'fields'	=> array('id', 'name'),
					'order'		=> array('name' => 'ASC')
				)));
				$name		= 'filter[list][' . $modelName . '.' . $fieldName . ']';
				$attribs	= array('onchange' => 'document.adminForm.submit();');
				$key		= 'id';
				$text		= 'name';
				$selected	= $value;
				$idtag		= false;
				$translate	= false;

				echo MosetsHTML::_('select.genericlist', $arr, $name, $attribs, $key, $text, $selected, $idtag, $translate);
			} else {
				JError::raiseWarning(500, sprintf(get_class($this) . "::lisst : could not find %s", $fieldName));
			}
		} else {
			JError::raiseWarning(500, get_class($this) . "::lisst : Filter can only handle on ");
		}
	}*/
}
?>