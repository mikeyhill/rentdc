<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	HTML.HTML
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Range HTML Class
 *
 * @package		Mosets
 * @subpackage	HTML.HTML
 * @author		Antoine Bernier
 */

class MosetsHTMLRange
{
	function date($name, $value1 = null, $value2 = null, $idtag1 = false, $idtag2 = false, $format1 = '%Y-%m-%d %H:%M:%S', $format2 = '%Y-%m-%d %H:%M:%S', $attribs = null)
	{
		$id1 = $name . 'from';
		if ($idtag1) {
			$id1 = $idtag1;
		}
		$id1	= str_replace('[','',$id1);
		$id1	= str_replace(']','',$id1);
		
		$id2 = $name . 'to';
		if ($idtag2) {
			$id2 = $idtag2;
		}
		$id2	= str_replace('[','',$id2);
		$id2	= str_replace(']','',$id2);

		$html = '<ul>';

		$html .= '<li>';
		$html .= MosetsHTML::_('form.label', 'From', $id1, false, '', null, true);
		$html .= MosetsHTML::_('calendar', $value1, $name . '[from]', $id1, $format1, $attribs);
		$html .= '</li>';
		
		$html .= '<li>';
		$html .= MosetsHTML::_('form.label', 'To', $id2, false, '', null, true);
		$html .= MosetsHTML::_('calendar', $value2, $name . '[to]', $id2, $format2, $attribs);
		$html .= '</li>';
		
		$html .= '</ul>';

		return $html;
	}

	function text($name, $value1 = null, $value2 = null, $idtag1 = false, $idtag2 = false, $attribs = null)
	{
		$id1 = $name . 'from';
		if ($idtag1) {
			$id1 = $idtag1;
		}
		$id1	= str_replace('[','',$id1);
		$id1	= str_replace(']','',$id1);
		
		$id2 = $name . 'to';
		if ($idtag2) {
			$id2 = $idtag2;
		}
		$id2	= str_replace('[','',$id2);
		$id2	= str_replace(']','',$id2);
		
		$html = '<ul>';

		$html .= '<li>';
		$html .= MosetsHTML::_('form.label', 'From', $id1, false, '', null, true);
		$html .= MosetsHTML::_('form.input', 'text', $name . '[from]', $value1, $id1, $attribs);
		$html .= '</li>';
		
		$html .= '<li>';
		$html .= MosetsHTML::_('form.label', 'To', $id2, false, '', null, true);
		$html .= MosetsHTML::_('form.input', 'text', $name . '[to]', $value2, $id2, $attribs);
		$html .= '</li>';
		
		$html .= '</ul>';
		
		return $html;
	}
}
?>