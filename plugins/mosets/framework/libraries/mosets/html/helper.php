<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	HTML
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * HTML helper Class
 *
 * @package		Mosets
 * @subpackage	HTML
 * @author		Antoine Bernier
 */

class MosetsHTMLHelper
{
	function mergeAttributes($attribs1, $attribs2)
	{
		if (is_array($attribs1)) {
			$attribs1 = JArrayHelper::toString($attribs1);
		}
		if (is_array($attribs2)) {
			$attribs2 = JArrayHelper::toString($attribs2);
		}
		
		jimport('domit.xml_saxy_shared');
		$attr1 = $attr2 = new SAXY_Parser_Base();
		$parsed1 = $attr1->parseAttributes($attribs1);
		$parsed2 = $attr2->parseAttributes($attribs2);
		
		foreach ($parsed1 as $name => $value)
		{
			switch($name)
			{
				case 'class':
					if (array_key_exists($name, $parsed2) && !strpos($parsed2[$name], $value)) {
						$parsed2[$name] .= ' ' . $value;
					}
					break;
				default:
					$parsed2[$name] = $value;
					break;
			}
		}
		
		return $parsed2;
	}
	
	function renderAttributes($attributes)
	{
		if (!is_array($attributes)) {
			$attributes = MosetsHTMLHelper::mergeAttributes(array(), $attributes);
		}
		
		return JArrayHelper::toString($attributes);
	}
}