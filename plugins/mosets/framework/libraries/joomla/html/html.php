<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	Joomla.HTML
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * HTML Class
 *
 * @package		Mosets
 * @subpackage	Joomla.HTML
 * @author		Antoine Bernier
 */

jimport('joomla.html.html');

class JoomlaHTML extends JHTML
{
	/**
	 * Class loader method
	 *
	 * Additional arguments may be supplied and are passed to the sub-class.
	 * Additional include paths are also able to be specified for third-party use
	 *
	 * @param	string	The name of helper method to load, (prefix).(class).function
	 *                  prefix and class are optional and can be used to load custom
	 *                  html helpers.
	 */
	function _($type)
	{
		//Initialise variables
		$prefix = 'JoomlaHTML';
		$file   = '';
		$func   = $type;

		// Check to see if we need to load a helper file
		$parts = explode('.', $type);

		switch(count($parts))
		{
			case 3 :
				$prefix		= preg_replace( '#[^A-Z0-9_]#i', '', $parts[0] );
				$file		= preg_replace( '#[^A-Z0-9_]#i', '', $parts[1] );
				$func		= preg_replace( '#[^A-Z0-9_]#i', '', $parts[2] );
				break;
			case 2 :
				$file		= preg_replace( '#[^A-Z0-9_]#i', '', $parts[0] );
				$func		= preg_replace( '#[^A-Z0-9_]#i', '', $parts[1] );
				break;
		}

		$className	= $prefix . ucfirst($file);

		if (!class_exists($className)) {
			jimport('joomla.filesystem.path');
			if ($path = JPath::find(JoomlaHTML::addIncludePath(), strtolower($file) . '.php')) {
				require_once $path;

				if (!class_exists($className)) {
					JError::raiseWarning(0, $className . '::' . $func . ' not found in file.');
					return false;
				}
			} else {
				$args = func_get_args();
				return call_user_func_array(array('JHTML', '_'), $args);	// Degrades to JHTML::_()
			}
		}

		if (is_callable(array($className, $func))) {
			$args = func_get_args();
			array_shift($args);
			return call_user_func_array(array($className, $func), $args);
		} else {
			$args = func_get_args();
			return call_user_func_array(array('JHTML', '_'), $args);	// Degrades to JHTML::_()
		}
	}
	
	/**
	 * Displays a calendar control field
	 * 
	 * Overload JHTML::calendar() just to clean $id of invalid characters and to make $idtag optionnal
	 *
	 * @param	string	The date value
	 * @param	string	The name of the text field
	 * @param	string	The id of the text field
	 * @param	string	The date format
	 * @param	array	Additional html attributes
	 */
	function calendar($value, $name, $idtag = false, $format = '%Y-%m-%d', $attribs = null)
	{
		JoomlaHTML::_('behavior.calendar'); //load the calendar behavior

		$id = $name;
		if ($idtag) {
			$id = $idtag;
		}
		
		$id		= str_replace('[','',$id);
		$id		= str_replace(']','',$id);

		if (is_array($attribs)) {
			$attribs = JArrayHelper::toString( $attribs );
		}
		$document =& JFactory::getDocument();
		$document->addScriptDeclaration('window.addEvent(\'domready\', function() {Calendar.setup({
        inputField     :    "'.$id.'",     // id of the input field
        ifFormat       :    "'.$format.'",      // format of the input field
        button         :    "'.$id.'_img",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });});');

		return '<input type="text" name="'.$name.'" id="'.$id.'" value="'.htmlspecialchars($value, ENT_COMPAT, 'UTF-8').'" '.$attribs.' />'.
				 '<img class="calendar" src="'.JURI::root(true).'/templates/system/images/calendar.png" alt="calendar" id="'.$id.'_img" />';
	}
	
	/**
	 * Add a directory where JoomlaHTML should search for helpers.
	 * 
	 * You may either pass a string or an array of directories.
	 *
	 * @access	public
	 * @param	string	A path to search.
	 * @return	array	An array with directory elements
	 */
	function addIncludePath($path = '')
	{
		static $paths;

		if (!isset($paths)) {
			$paths = array(JPath::clean(MOSETS_LIBRARIES . '/joomla/html/html'));
		}

		// force path to array
		settype($path, 'array');

		// loop through the path directories
		foreach ($path as $dir)
		{
			if (!empty($dir) && !in_array($dir, $paths)) {
				array_unshift($paths, JPath::clean($dir));
			}
		}

		return $paths;
	}
}
?>