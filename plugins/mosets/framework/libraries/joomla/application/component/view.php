<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	Joomla.Application.Component
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * View Class
 *
 * @package		Mosets
 * @subpackage	Joomla
 * @author		Antoine Bernier
 */

jimport('joomla.application.component.view');

class JoomlaView extends JView
{
	/**
	 * Escapes a value for output in a view script.
	 * 
	 * Overload parent::escape to allow defining the $quote_style
	 *
	 * If escaping mechanism is one of htmlspecialchars or htmlentities, uses
	 * {@link $_encoding} setting.
	 *
	 * @param	mixed $var The output to escape.
	 * @return	mixed The escaped value.
	 */
	function escape($var, $quote_style = ENT_COMPAT)
	{
		if (in_array($this->_escape, array('htmlspecialchars', 'htmlentities'))) {
			return call_user_func($this->_escape, $var, $quote_style, $this->_charset);
		}

		return call_user_func($this->_escape, $var);
	}
}
?>