<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Loader Class
 *
 * @package		Mosets
 * @author		Antoine Bernier
 */

class MosetsLoader extends JLoader
{
	 /**
	 * Loads a class from specified directories.
	 * 
	 * By default, it will import the file from component's 'libraries' directory. If the file is not found, it degrades to JLoader::import
	 *
	 * @param string $name	The class name to look for ( dot notation ).
	 * @param string $base	Search this directory for the class.
	 * @param string $key	String used as a prefix to denote the full path of the file ( dot notation ).
	 * @return void
	 */
	function import($filePath, $base = null, $key = 'libraries.')
	{
		static $paths;

		if (!isset($paths)) {
			$paths = array();
		}

		$keyPath = $key ? $key . $filePath : $filePath;

		if (!isset($paths[$keyPath])) {
			if (!$base) {
				$base = dirname(__FILE__);
			}

			$parts = explode('.', $filePath);

			$classname = array_pop($parts);
			switch($classname)
			{
				case 'helper' :
					$classname = ucfirst(array_pop($parts)) . ucfirst($classname);
					break;
				default :
					$classname = ucfirst($classname);
					break;
			}

			// If we are loading a Joomla or Mosets class, prepend the classname with the corresponding prefix: 'Joomla' or 'Mosets'
			switch (strtolower($parts[0]))
			{
				case 'joomla':
				case 'mosets':
					$classname = ucfirst($parts[0]) . $classname;
					break;
				default:
					break;
			}
			
			$path  = str_replace('.', DS, $filePath);
			if (JPath::find($base, $path . '.php')) {
				$classes	= parent::register($classname, $base . DS . $path . '.php');
				$rs			= isset($classes[strtolower($classname)]);
			} else {
				return parent::import($filePath, ($base == dirname(__FILE__) ? null : $base), $key);	// Degrades to JLoader::import if the file is not found
			}

			$paths[$keyPath] = $rs;
		}

		return $paths[$keyPath];
	}
}

/**
 * Intelligent file importer
 *
 * @access	public
 * @param	string	A dot syntax path
 */
function mimport($path)
{
	return MosetsLoader::import($path);
}
?>