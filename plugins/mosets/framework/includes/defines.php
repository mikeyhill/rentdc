<?php
/**
 * @version		$Id: defines.php 682 2009-05-05 14:59:06Z abernier $
 * @package		Mosets
 * @subpackage	Includes
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Define Mosets Framework's constants
 *
 * @package		Mosets
 * @subpackage	Includes
 * @author		Lee Cher Yeong <@mosets.com>
 * @author		Antoine Bernier
 */

/**
 * Mosets Framework Constant
 */

define('MOSETS_FRAMEWORK',						JPath::clean(JPATH_PLUGINS . '/mosets/framework'));
define('MOSETS_FRAMEWORK_URL',					JURI::root() . 'plugins/mosets/framework/');
define('MOSETS_LIBRARIES',						JPath::clean(MOSETS_FRAMEWORK . '/libraries'));
define('MOSETS_DEFAULT_VIEW',					'home');

define('MOSETS_MEDIA',							JPath::clean(JPATH_ROOT . '/media/plg_mosets_framework'));
define('MOSETS_MEDIA_URL',						JURI::root() . 'media/plg_mosets_framework/');
define('MOSETS_MEDIA_CSS',						JPath::clean(MOSETS_MEDIA . '/css'));
define('MOSETS_MEDIA_CSS_URL',					MOSETS_MEDIA_URL . 'css/');
define('MOSETS_MEDIA_IMAGES',					JPath::clean(MOSETS_MEDIA . '/images'));
define('MOSETS_MEDIA_IMAGES_URL',				MOSETS_MEDIA_URL . 'images/');
define('MOSETS_MEDIA_JS',						JPath::clean(MOSETS_MEDIA . '/js'));
define('MOSETS_MEDIA_JS_URL',					MOSETS_MEDIA_URL . 'js/');
?>
