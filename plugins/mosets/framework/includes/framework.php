<?php
/**
 * @version		$Id: framework.php 682 2009-05-05 14:59:06Z abernier $
 * @package		Mosets
 * @subpackage	Includes
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * 
 *
 * @package		Mosets
 * @subpackage	Includes
 * @author		Lee Cher Yeong <framework@mosets.com>
 * @author		Antoine Bernier
 */

global $mainframe;

jimport('joomla.filesystem.file');

// Core framework includes
if (JFile::exists($path = JPath::clean(MOSETS_LIBRARIES . '/mosets/import.php'))) {
	require_once($path);
} else {
	JError::raiseError(404, 'Cannot load Mosets framework, located at : ' . $path);
}

// Load PHP compatibility functions
if (JFile::exists($path = JPath::clean(MOSETS_LIBRARIES . '/mosets/utilities/compat/compat.php'))) {
	require_once($path);
} else {
	JError::raiseError(404, 'Cannot load PHP compatibility functions, located at : ' . $path);
}