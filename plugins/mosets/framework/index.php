<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	Framework
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Mosets Framework index file
 *
 * @package		Mosets
 * @subpackage	Framework
 * @author		Antoine Bernier
 */

require_once(JPath::clean(dirname(__FILE__) . '/includes/defines.php'));

require_once(JPath::clean(MOSETS_FRAMEWORK . '/includes/framework.php'));
?>