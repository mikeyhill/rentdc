<?php
/**
 * @version		$Id$
 * @package		Mosets
 * @subpackage	Framework
 * @copyright	(C) 2008 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Mosets Framework Plugin
 *
 * @package		Mosets
 * @subpackage	Framework
 * @author		Antoine Bernier
 */

jimport('joomla.plugin.plugin');

class plgMosetsFramework extends JPlugin
{
	/**
	 * Constructor
	 *
	 * For php4 compatability we must not use the __constructor as a constructor for plugins
	 * because func_get_args ( void ) returns a copy of all passed arguments NOT references.
	 * This causes problems with cross-referencing necessary for the observer design pattern.
	 *
	 * @param	object	The object to observe
	 * @param	array 	An optional associative array of configuration settings.
	 * 					Recognized key values include 'name', 'group', 'params' (this list is not meant to be comprehensive).
	 */
	function plgMosetsFramework(&$subject, $config = array())
	{
		parent::__construct($subject, $config);
	}
	
	/**
	 * Constructor
	 */
	function __construct(&$subject, $config = array())
	{
		parent::__construct($subject, array(
			'name'	=> 'framework',
			'type'	=> 'mosets'
		));
	}
	
	function onInitializeMosetsFramework() {
		$this->loadLanguage('', JPATH_ADMINISTRATOR);
		require_once(JPath::clean(dirname(__FILE__) . '/framework/index.php'));
	}
}
?>