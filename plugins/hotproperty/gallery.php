<?php
/**
 * @version		$Id: gallery.php 880 2009-09-28 17:28:59Z abernier $
 * @package		Hotproperty
 * @subpackage	Plugins
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Gallery Plugin
 *
 * @package		Hotproperty
 * @subpackage	Plugins
 * @author		Antoine Bernier <abernier@mosets.com>
 */

jimport('joomla.plugin.plugin');

class plgHotpropertyGallery extends JPlugin
{
	/**
	 * Constructor
	 *
	 * For php4 compatability we must not use the __constructor as a constructor for plugins
	 * because func_get_args ( void ) returns a copy of all passed arguments NOT references.
	 * This causes problems with cross-referencing necessary for the observer design pattern.
	 *
	 * @param	object	The object to observe
	 * @param	array 	An optional associative array of configuration settings.
	 * 					Recognized key values include 'name', 'group', 'params' (this list is not meant to be comprehensive).
	 */
	function plgHotpropertyGallery(&$subject, $config = array())
	{
		parent::__construct($subject, $config);
	}
	
	function onAfterDisplayTitle(&$property)
	{
		$hotproperty	=& MosetsApplication::getInstance('hotproperty');
		
		// Include CSS
		MosetsHTML::_('stylesheet', $this->params->get('css'), JURI::root() . 'media/plg_hotproperty_gallery/');
		
		// Include JS
		MosetsHTML::_('script', $this->params->get('js'), JURI::root() . 'media/plg_hotproperty_gallery/');
		
		ob_start();
		?>
		<?php if (@$property->Photo) : ?>
			<div class="gallery">
				<ul class="photos">
				<?php foreach ($property->Photo as $photo) : ?>
					<li class="photo">
					<?php if ($hotproperty->getCfg('img_saveoriginal') && $photo->original) : ?>
						<a href="<?php echo MosetsApplication::getPath('media_images_original_url', 'hotproperty') . $photo->original; ?>">
					<?php endif; ?>
							<?php echo MosetsHTML::_('hotproperty.image.photo', 'standard', $photo->standard, $photo->title, array('id' => 'photo-' . $photo->id)); ?>
					<?php if ($hotproperty->getCfg('img_saveoriginal') && $photo->original) : ?>
						</a>
					<?php endif; ?>
					<p class="caption">
						<span class="title"><?php echo $photo->title; ?></span>
					<?php if ($photo->desc) : ?>
						<?php echo $photo->desc; ?>
					<?php endif; ?>
					</p>
					</li>
				<?php endforeach; ?>
				</ul>
			<?php if (count($property->Photo) > 1) : ?>
				<div class="nav">
					<ul class="thumbnails">
					<?php for ($i=0; $i < count($property->Photo); $i++) : ?>
					<?php $photo =& $property->Photo[$i]; ?>
						<li class="thumbnail">
							<?php echo MosetsHTML::_('anchor', 'photo-' . $property->Photo[$i]->id, MosetsHTML::_('hotproperty.image.photo', 'thumbnail', $photo->thumb, $photo->title)); ?>
						</li>
					<?php endfor; ?>
					</ul>
				</div>
			<?php endif; ?>
			</div>
		<?php endif; ?>
		<?php
		$html = ob_get_contents();
		ob_end_clean();
		
		return $html;
	}
}
?>