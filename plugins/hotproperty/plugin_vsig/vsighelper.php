<?php
/*
// "Very Simple Image Gallery for Hot Property" Plugin for Joomla 1.5 - Version 1.0.0
// License: http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
// Author: Andreas Berger - http://www.bretteleben.de
// Copyright (c) 2010 Andreas Berger - andreas_berger@bretteleben.de
// Project page and Demo at http://www.bretteleben.de
// ***Last update: 2010-01-15***
*/

defined( '_JEXEC' ) or die( 'Restricted access' );

// Helper Class
class plgContentVsigHelper {

		// replace quotes
    function beKickQuotes($e) {

		/* parameters
    $e	string
    */

			$e = str_replace('"', '\\"', $e);
			$e = str_replace("'", "&#39;", $e);
			return $e;
		}

		// check for mb_strtolower and use it if available
    function beStrtolower($mystring) {

		/* parameters
    $mystring				the string to convert
    */
		$mystring=(plgContentVsigHelper::beIs_utf8($mystring))?($mystring):(utf8_encode($mystring));

		$mystring=(function_exists('mb_strtolower'))?(mb_strtolower($mystring)):(strtolower($mystring));
		return $mystring;
		}

		// Returns true if $string is valid UTF-8 and false otherwise.
		function beIs_utf8($string) {
    // From http://w3.org/International/questions/qa-forms-utf-8.html
    return preg_match('%^(?:
          [\x09\x0A\x0D\x20-\x7E]            # ASCII
        | [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
        |  \xE0[\xA0-\xBF][\x80-\xBF]        # excluding overlongs
        | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
        |  \xED[\x80-\x9F][\x80-\xBF]        # excluding surrogates
        |  \xF0[\x90-\xBF][\x80-\xBF]{2}     # planes 1-3
        | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
        |  \xF4[\x80-\x8F][\x80-\xBF]{2}     # plane 16
    )*$%xs', $string);
		}

}
?>