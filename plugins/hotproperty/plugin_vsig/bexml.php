<?php
/*
// "Very Simple Image Gallery for Hot Property" Plugin for Joomla 1.5 - Version 1.0.0
// License: http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
// Author: Andreas Berger - http://www.bretteleben.de
// Copyright (c) 2010 Andreas Berger - andreas_berger@bretteleben.de
// Project page and Demo at http://www.bretteleben.de
// ***Last update: 2010-01-15***
*/

defined( '_JEXEC' ) or die( 'Restricted access' );

class JElementbexml extends JElement{
	var	$_name = 'Very Simple Image Gallery for Hot Property';
	var $_version = '1.0.0';

	function fetchElement($name, $value, &$node, $control_name){
		$view =  $node->attributes('view');

		switch ($view){

		case 'intro':
            $html="<div style='background-color:#c3d2e5;margin:-4px;padding:2px;'>";
            $html.="<b>".$this->_name." <br />Version: ".$this->_version."</b><br />";
            $html.="for support and updates visit:&nbsp;";
            $html.="<a href='http://www.bretteleben.de' target='_blank'>www.bretteleben.de</a>";
            $html.="</div>";
		break;

		case 'gallery':
            $html="<div style='background-color:#c3d2e5;margin:-4px;padding:2px;'>";
            $html.="<b>Gallery</b><br />Settings regarding the gallery in general.";
            $html.="</div>";
		break;

		case 'thumbs':
            $html="<div style='background-color:#c3d2e5;margin:-4px;padding:2px;'>";
            $html.="<b>Thumbnails</b><br />Settings regarding the thumbnails (size, position, ...).";
            $html.="</div>";
		break;

		case 'captions':
            $html="<div style='background-color:#c3d2e5;margin:-4px;padding:2px;'>";
            $html.="<b>Captions</b><br />Show title and/or description for images. (see <a href='http://www.bretteleben.de/lang-en/joomla/vsig-for-hot-property/anleitung-plugin.html' target='_blank'>Howto</a>).";
            $html.="</div>";
		break;

		case 'links':
            $html="<div style='background-color:#c3d2e5;margin:-4px;padding:2px;'>";
            $html.="<b>Links</b><br />Link images to the original image (opens in a new window) (see <a href='http://www.bretteleben.de/lang-en/joomla/vsig-for-hot-property/anleitung-plugin.html' target='_blank'>Howto</a>).";
            $html.="</div>";
		break;

		case 'sets':
            $html="<div style='background-color:#c3d2e5;margin:-4px;padding:2px;'>";
            $html.="<b>Sets</b><br />This feature allows to split galleries to multiple sets and enables necessary navigation (see <a href='http://www.bretteleben.de/lang-en/joomla/vsig-for-hot-property/anleitung-plugin.html' target='_blank'>Howto</a>).";
            $html.="</div>";
		break;

		default:
            $html="<div style='background-color:#c3d2e5;margin:-4px;padding:2px;'>";
            $html.="<b>Other settings</b><br />Choose a custom stylesheet or script (see <a href='http://www.bretteleben.de/lang-en/joomla/vsig-for-hot-property/anleitung-plugin.html' target='_blank'>Howto</a>).";
            $html.="</div>";
		break;

		}
		return $html;
	}
}