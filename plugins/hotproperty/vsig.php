<?php
/*
// "Very Simple Image Gallery for Hot Property" Plugin for Joomla 1.5 - Version 1.0.0
// License: http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
// Author: Andreas Berger - http://www.bretteleben.de
// Copyright (c) 2010 Andreas Berger - andreas_berger@bretteleben.de
// Project page and Demo at http://www.bretteleben.de
// ***Last update: 2010-01-15***
*/
 
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');

class plgHotpropertyVsig extends JPlugin {
	function plgHotpropertyVsig(&$subject, $config = array()) {
		parent::__construct($subject, $config);
	}
	
	function onAfterDisplayTitle(&$property) {

		$hotproperty	=& MosetsApplication::getInstance('hotproperty');
		$html2="";
		
		if (@$property->Photo){

			$images=$property->Photo;
	
			// import helper
	    JLoader::import( 'vsighelper', dirname( __FILE__ ).'/plugin_vsig' );
					
			// j!1.5 paths
			$path_absolute = 	JPATH_SITE;
			$path_site = 			JURI :: base();
			if(substr($path_site, -1)=="/") $path_site = substr($path_site, 0, -1);
			$path_imgroot = 	'/media/com_hotproperty/images/'; // default image root folder
			$path_ctrls = 		'/media/plg_hotproperty_vsig/'; 			// button folder
			$path_plugin = 		'/media/plg_hotproperty_vsig/'; 			// path to plugin folder
	
			$_target=JRequest::getURI();
			$vsig_cssadd=""; //collect css for page head
			$vsig_script=""; //collect javascript for page head
	
			if(@count($property->Photo)>=1) {
				//read in parameters
				$_width_ 			= trim(	$this->params->get('th_width'		, 120		));	//thumbs
				$_space_ 			= trim(	$this->params->get('th_space'		, 5			));	//thumbs
				$_imwidth_ 		= trim(	$this->params->get('im_width'		, 400		));	//image
				$_im_align_ 	= 			$this->params->get('im_align'		, 1			);	//image
				$_th_right_ 	= 			$this->params->get('th_right'		, 2			);	//layout
				$_th_area_ 		= trim(	$this->params->get('th_area'		, 30		));	//layout
				$_cap_show_ 	= 			$this->params->get('cap_show'		, 1			);	//captions
				$_cap_pos_ 		= 			$this->params->get('cap_pos'		, 1			);	//captions
				$_link_use_ 	= 			$this->params->get('link_use'		, 1			);	//links
				$_sets_use_ 	= trim(	$this->params->get('sets_use'		, ''		));	//sets
				$_sets_txt_ 	= trim(	$this->params->get('sets_txt'		, 'Set'	));	//sets
				$_ctrl_fwd_ 	= trim(	$this->params->get('ctrl_fwd'		, ''		));	//buttons
				$_ctrl_back_ 	= trim(	$this->params->get('ctrl_back'	, ''		));	//buttons
				$_ctrl_height_= 24;
				//if there is only one image, we make sure that the main image gets the full width
				if(@count($property->Photo)=='1'&&$_th_right_==1){$_th_right_=2;}
				//check if images are used for controls and allow default buttons
				$_ctrl_fwd_type = $_ctrl_back_type = "noimg";
				if(is_file($path_absolute.$path_ctrls.$_ctrl_fwd_)&&((substr(strtolower($_ctrl_fwd_),-3)=='jpg')||(substr(strtolower($_ctrl_fwd_),-3)=='gif')||(substr(strtolower($_ctrl_fwd_),-3)=='png'))){$_ctrl_fwd_=$path_ctrls.$_ctrl_fwd_;$_ctrl_fwd_type="img";}
				elseif($_ctrl_fwd_==""){$_ctrl_fwd_=$path_plugin.'fwd.png';$_ctrl_fwd_type="img";}
				if(is_file($path_absolute.$path_ctrls.$_ctrl_back_)&&((substr(strtolower($_ctrl_back_),-3)=='jpg')||(substr(strtolower($_ctrl_back_),-3)=='gif')||(substr(strtolower($_ctrl_back_),-3)=='png'))){$_ctrl_back_=$path_ctrls.$_ctrl_back_;$_ctrl_back_type="img";}
				elseif($_ctrl_back_==""){$_ctrl_back_=$path_plugin.'bwd.png';$_ctrl_back_type="img";}
	
				//calculations
				$_tempwidth=$_imwidth_;
				$_im_area_=$_tempwidth;
				if($_th_right_=="1"){
					$_im_area_=intval($_tempwidth-$_tempwidth/100*$_th_area_);
					$_rulerspace_=intval(($_tempwidth-$_im_area_)/10*9)."px";
				}
	
				//calculate the number of sets
				$_sets_number_=($_sets_use_&&$_sets_use_<=(count($images)+1))?(ceil(count($images)/$_sets_use_)):1;
	
				//justify
				$therealimagearea=$_im_area_;
				if($_th_right_=="2"){
					$countthumbs=intval($_im_area_/($_width_+10+$_space_));
					$toremove=$_im_area_-($countthumbs*($_width_+10+$_space_));
					$newwidth=$_width_+intval($toremove/$countthumbs);
				  $_width_=$newwidth;
					$therealimagearea=$countthumbs*($_width_+10+$_space_)-10-$_space_;
				}
	
				//create a unique identifier for the current gallery
				$identifier=$property->id;
	
				//write the styles for the gallery
				$vsig_cssadd.="  .vsig_cont".$identifier." {width:".($_width_+10+$_space_)."px;}\n";
				if($_th_right_!="1"){$vsig_cssadd.="  .vsig_top".$identifier." {width:".$_im_area_."px;margin:5px 5px 5px -5px;}\n  .vsig_top".$identifier." img {width:".$therealimagearea."px;}\n";}
				else{$vsig_cssadd.="  .vsig_top".$identifier." {width:".$_im_area_."px;float:left;margin:-5px 15px 5px -5px;}\n  .vsig_top".$identifier." img {width:".$therealimagearea."px;}\n  .vsig_ruler".$identifier." {width:".$_rulerspace_.";}\n";}
				if($_im_align_==0){$vsig_cssadd.="  .vsig".$identifier." {margin:0 0 0 auto;padding:0;display:block;width:".$_imwidth_."px;}\n";}
				elseif($_im_align_==1){$vsig_cssadd.="  .vsig".$identifier." {margin:auto;padding:0;display:block;width:".$_imwidth_."px;}\n";}
				elseif($_im_align_==3){$vsig_cssadd.="  .vsig".$identifier." {margin:10px;float:left;width:".$_imwidth_."px;}\n";}
				elseif($_im_align_==4){$vsig_cssadd.="  .vsig".$identifier." {margin:10px;float:right;width:".$_imwidth_."px;}\n";}
				else{$vsig_cssadd.="  .vsig".$identifier." {width:".$_imwidth_."px;}\n";}
				$vsig_cssadd.="  .vsig_ctrls_".$identifier." {display:block;width:".$therealimagearea."px;height:".$_ctrl_height_."px;text-align:right;}\n";
				//new
				$vsig_cssadd.="  .vsig_cont".$identifier." img {width:".$_width_."px;}\n";
	
				//manipulate uri
				$aktimg=0;
				$target[$identifier]=htmlspecialchars($_target);
				$foundrequest=JRequest::getVar('vsig'.$identifier, 'FALSE');
				if($foundrequest!='FALSE'){
					$aktimg=(int)$foundrequest;
					$target[$identifier] = preg_replace('@[&|&amp;]?vsig'.$identifier.'=[0-9]+@', '', $target[$identifier] );
					$target[$identifier] = @preg_replace('@(\?&amp;)@', '?', $target[$identifier] );
				}
				$vsig_adqm = strpos($target[$identifier], '?');
				$target[$identifier].=($vsig_adqm===false)?("?"):("");
				$target[$identifier].=(substr($target[$identifier], -1)!="?")?("&amp;"):("");    
				$target[$identifier] = @preg_replace('@(&amp&amp;)@', '&amp;', $target[$identifier] );
	
				//current set according to main image else 1
				$_sets_current_=($_sets_use_)?(intval($aktimg/$_sets_use_)+1):1;
	
				//sort images
				function compare($h1,$h2) {
					if($h1->ordering>$h2->ordering){return 1;}
					if($h1->ordering<$h2->ordering){return -1;}
					return 0;
				}
				usort($images, 'compare'); 
	
				//start collecting general html
				$html2.="\n<a name='g_vsig".$identifier."'></a>\n<div class='vsig vsig".$identifier."'>";
	
				//start collecting html for thumbs-part
				$html3='';
	
				//create current javascript array
				$vsig_script.="  var vsig_".$identifier."= new Array();\n";
				$vsig_script.="  var vsig_".$identifier."_b=new Array('".$path_site.$path_imgroot."std/','".$path_site.$path_imgroot."thb/','".$identifier."','".$target[$identifier]."vsig".$identifier."=');";
				//set counter for thumbs in set to 1
				$thumbs_in_set=1;
				//traverse the found images#######################
				$a=0;
				foreach ($images as $photos){
					//prepare captions############################
					$cur_cap=array("","");
					$cur_cap_set=0; //set trigger for captions
					if($_cap_show_){ //are captions activated and are there captions?
						$cur_cap=array($photos->title,$photos->desc);
						if($photos->title!=""||$photos->desc!=""){
							$cur_cap_set=1;
						}
					}
					//encode captions
					$cur_cap[0] = str_replace(array("\r\n", "\r", "\n"), "<br />", plgContentVsigHelper::beKickQuotes($cur_cap[0]));
					$cur_cap[1] = str_replace(array("\r\n", "\r", "\n"), "<br />", plgContentVsigHelper::beKickQuotes($cur_cap[1]));
					//create values for js
					$cur_cap_js=$cur_cap;
	
					//prepare links###############################
					$cur_link=array("#g_vsig".$identifier,"","_self"); //reset links
					if($_link_use_&&$hotproperty->getCfg('img_saveoriginal')&&$photos->original){ //are links activated and is there an original?
						$cur_link=array(MosetsApplication::getPath('media_images_original_url', 'hotproperty').$photos->original,$photos->title,"_blank");
					}
					//create values for js
					$cur_link_js=$cur_link;
					//encode links
	
					//prepare alt and title#######################
					$cur_alt = plgContentVsigHelper::beKickQuotes(utf8_encode($photos->title)); //encode alt-title
					$cur_alt_js = $cur_alt;
	
					//top image###################################
					if($a==$aktimg) {
						$html2 .= "\n<div class='vsig_top vsig_top".$identifier."'>";
						if($_link_use_&&$hotproperty->getCfg('img_saveoriginal')){ //links are activated and set
							$html2 .= "\n<a href='".$cur_link[0]."' title='".$cur_link[1]."' target='".$cur_link[2]."'>";
						}
						$html2.="\n".MosetsHTML::_('hotproperty.image.photo', 'standard', $photos->standard, $cur_alt, array('id' => 'topimg' . $identifier, 'title' => $cur_alt));
						if($_cap_show_){ //we have to show captions
							$html2 .= "\n<div class='".(($_cap_pos_)?'inside':'outside')."' style='width:".$therealimagearea."px;'>";
							if($cur_cap_set){ //captions (specific or default) are set for this image
								$html2 .= "<span>".$cur_cap[0]."</span><span>".$cur_cap[1]."</span>";
							}
							$html2 .= "</div>".(($_cap_pos_)?"":"<br class='vsig_clr' />");
						}
						if($_link_use_&&$hotproperty->getCfg('img_saveoriginal')){ //links are activated and set
							$html2 .= "\n</a>";
						}
						$html2 .= "\n</div>\n";
					}
	
					//thumbnails##################################
	
					//check if sets are used and if the current thumb belongs to the current set
					if(!$_sets_use_||($a>=($_sets_use_*$_sets_current_-$_sets_use_)&&$a<=($_sets_use_*$_sets_current_-1)&&$_sets_use_>=2)){
						//write html for thumbs
						$html3 .= '<div id="thbvsig_'.$identifier.'_'.$thumbs_in_set.'" class="vsig_cont vsig_cont'.$identifier.'"><div class="vsig_thumb">';
						$html3 .= '<a href="'.$target[$identifier].'vsig'.$identifier.'='.$a.'" rel="nofollow"';
						$html3 .= ' onclick=\'switchimg(vsig_'.$identifier.'['.$a.'],vsig_'.$identifier.'_b);return false;\'';
						$html3 .= ' title="'.$cur_alt.'">';
						$html3 .= MosetsHTML::_('hotproperty.image.photo', 'thumbnail', $photos->thumb, $cur_alt, array());
						$html3 .= "</a></div></div>\n";
						//increment counter for thumbs in set
						$thumbs_in_set++;
					}
					//array for javascript; name: identifier+prefix
					$vsig_script.="\n  vsig_".$identifier."[".$a."]=new Array(";
					$vsig_script.="'".$photos->standard."',";	//0-image
					$vsig_script.="'".$cur_cap_js[0]."',";		//1-captitle
					$vsig_script.="'".$cur_cap_js[1]."',";		//2-captext
					$vsig_script.="'".$cur_link_js[0]."',";		//3-linkhref
					$vsig_script.="'".$cur_link_js[1]."',";		//4-linktext
					$vsig_script.="'".$cur_link_js[2]."',";		//5-linktarget
					$vsig_script.="'".$cur_alt_js."',";				//6-alt
					$vsig_script.="'".$photos->thumb."');";	//7-thumb
					$a++;
				}
	
				//if the last set contains less thumbs than a full set, add dummies
				if($_sets_use_&&count($images)>=($_sets_use_+1)&&$_sets_use_>=2){
					for($b=$thumbs_in_set;$b<=$_sets_use_;$b++){
						$html3 .= '<div id="thbvsig_'.$identifier.'_'.$thumbs_in_set.'" class="vsig_cont vsig_cont'.$identifier.'" style="visibility:hidden;"><div class="vsig_thumb">';
						$html3 .= '<a href="" rel="nofollow" onclick=\'return false;\' title=""><img src=""/></a></div></div>'."\n";
						$thumbs_in_set++; //increment counter for thumbs in set
					}
				}
				//add controls
				if($_sets_use_&&$_sets_number_>=2) {
					$html4="<div class='vsig_ctrls vsig_ctrls_".$identifier."'>";
					$html4.="<div class='vsig_ctrl_left'></div><div class='vsig_ctrl_right'>";
					//back
					if($_sets_current_>=2){
						$html4.= '<a href="'.$target[$identifier].'vsig'.$identifier.'='.(($_sets_current_-1)*$_sets_use_-$_sets_use_).'" rel="nofollow"';
						$html4.= ' id=\'bbackvsig_'.$identifier.'\'';
						$html4.= ' onclick=\'switchset("vsig_'.$identifier.'",'.(($_sets_current_-1)*$_sets_use_-$_sets_use_).','.$_sets_use_.');return false;\'';
						$html4.= '>';
					}
					else{
						$html4.= '<a href="#g_vsig'.$identifier.'" rel="nofollow" id=\'bbackvsig_'.$identifier.'\' onclick=\'return false;\'>';
					}
					if($_ctrl_back_type=="img"){$html4.="<img src='".$path_site.$_ctrl_back_."' alt='".substr($_ctrl_back_, (strrpos($_ctrl_back_, "/")+1), -4)."'/>";}
					else{$html4.=$_ctrl_back_;}
					$html4.="</a>";
					//counter
					$html4.="&nbsp;&nbsp;".$_sets_txt_."<span id='countervsig_".$identifier."' class='vsig_counter'>&nbsp;".$_sets_current_."/".$_sets_number_."</span>&nbsp;&nbsp;";
					//forward
					if($_sets_current_<=$_sets_number_-1){
						$html4.= '<a href="'.$target[$identifier].'vsig'.$identifier.'='.($_sets_current_*$_sets_use_).'" rel="nofollow"';
						$html4.= ' id=\'bfwdvsig_'.$identifier.'\'';
						$html4.= ' onclick=\'switchset("vsig_'.$identifier.'",'.($_sets_current_*$_sets_use_).','.$_sets_use_.');return false;\'';
						$html4.= '>';
					}
					else{
						$html4.= '<a href="#g_vsig'.$identifier.'" rel="nofollow" id=\'bbackvsig_'.$identifier.'\' onclick=\'return false;\'>';
					}
					if($_ctrl_fwd_type=="img"){$html4.="<img src='".$path_site.$_ctrl_fwd_."' alt='".substr($_ctrl_fwd_, (strrpos($_ctrl_fwd_, "/")+1), -4)."'/>";}
					else{$html4.=$_ctrl_fwd_;}
					$html4.="</a></div></div>\n";
				}
				//if there is only 1 image we don't show the thumb
				if(@count($property->Photo)==1){$html3="";}
				if($_sets_use_&&$_sets_number_>=2) { //combine top image, controls and thumbs
					if($_th_right_=="1"){ //if thumbs are right PLUS sets are used => topimage-ruler-thumbs-controls
						$html2 .="<div class='vsig_ruler".$identifier." vsig_ruler'>\n".$html3."</div>\n".$html4;
					}
					else{ //if thumbs are below PLUS sets are used => topimage-controls-thumbs
						$html2 .=$html4.$html3;
					}
				}
				else{
					if($_th_right_=="1"){ //if thumbs are right NO sets are used => topimage-ruler-thumbs
						$html2 .="<div class='vsig_ruler".$identifier." vsig_ruler'>\n".$html3."</div>\n";
					}
					else{ //if thumbs are below NO sets are used => topimage-thumbs
						$html2 .=$html3;
					}
				}
	
				// just startup
				global $mainframe;
				$document =& JFactory::getDocument();
				//set robots to noindex,nofollow for subpages
				if(preg_match('#vsig[0-9]+#s', $_SERVER['REQUEST_URI'])>=1){
					$document->setMetaData("robots", "noindex, nofollow");
				}
	
				$html2 .="<div class=\"vsig_clr\"></div>\n</div>\n";
				// Include JS
				MosetsHTML::_('script', $this->params->get('js'), JURI::root() . 'media/plg_hotproperty_vsig/');
	
				//add arrays with alt, caption and link
				$document->addScriptDeclaration($vsig_script);
				//write collected CSS setting to the head of the page
				if($vsig_cssadd!=""){
					$document->addCustomTag("<style type='text/css'>\n".$vsig_cssadd."  </style>\n");
					$document->addCustomTag('<link href="'.$path_site.$path_plugin.$this->params->get('css').'" rel="stylesheet" type="text/css" />' );
				}
			}
		}
		return $html2;
	}
}
?>