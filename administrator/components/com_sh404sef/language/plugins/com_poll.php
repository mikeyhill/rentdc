<?php
/**
 * SEF extension for Joomla!
 *
 * @author      $Author: shumisha $
 * @copyright   Yannick Gaultier - 2007-2010
 * @package     sh404SEF-15
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     $Id: com_poll.php 1192 2010-04-04 16:11:04Z silianacom-svn $
 */

if (!defined('_JEXEC')) die('Direct Access to this location is not allowed.');
  
 // français
  $sh_LANG['fr']['_COM_SEF_SH_POLL_VOTE'] = 'Voter';
  $sh_LANG['fr']['_COM_SEF_SH_POLL_RESULTS'] = 'Resultats sondage';
  // english
  $sh_LANG['en']['_COM_SEF_SH_POLL_VOTE'] = 'Vote';
  $sh_LANG['en']['_COM_SEF_SH_POLL_RESULTS'] = 'Poll results';
  // spanish
  $sh_LANG['es']['_COM_SEF_SH_POLL_VOTE'] = 'Vote';
  $sh_LANG['es']['_COM_SEF_SH_POLL_RESULTS'] = 'Resultados';
  // german
  $sh_LANG['de']['_COM_SEF_SH_POLL_VOTE'] = 'Vote';
  $sh_LANG['de']['_COM_SEF_SH_POLL_RESULTS'] = 'Results';
  // hungarian : translation by Jozsef Tamas Herczeg 2007-08-15
  $sh_LANG['hu']['_COM_SEF_SH_POLL_VOTE'] = 'Szavazás';
  $sh_LANG['hu']['_COM_SEF_SH_POLL_RESULTS'] = 'Eredményekh';
  // italiano
  $sh_LANG['it']['_COM_SEF_SH_POLL_VOTE'] = 'Vote';
  $sh_LANG['it']['_COM_SEF_SH_POLL_RESULTS'] = 'Results';
  // dutch
  $sh_LANG['nl']['_COM_SEF_SH_POLL_VOTE'] = 'Stem';
  $sh_LANG['nl']['_COM_SEF_SH_POLL_RESULTS'] = 'Enquete uitslagen';
