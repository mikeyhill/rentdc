<?php
/**
 * SEF extension for Joomla!
 *
 * @author      $Author: shumisha $
 * @copyright   Yannick Gaultier - 2007-2010
 * @package     sh404SEF-15
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     $Id: com_chronocontact.php 1204 2010-04-06 18:00:51Z silianacom-svn $
 */

if (!defined('_JEXEC')) die('Direct Access to this location is not allowed.');

// français
$sh_LANG['fr']['_SH404SEF_CHRONOCONTACT_SEND'] = 'Envoyer';
$sh_LANG['fr']['_SH404SEF_CHRONOCONTACT_EXTRA'] = 'Extra';
// english
$sh_LANG['en']['_SH404SEF_CHRONOCONTACT_SEND'] = 'Send';
$sh_LANG['en']['_SH404SEF_CHRONOCONTACT_EXTRA'] = 'Extra';
// spanish
$sh_LANG['es']['_SH404SEF_CHRONOCONTACT_SEND'] = 'Mandar';
$sh_LANG['es']['_SH404SEF_CHRONOCONTACT_EXTRA'] = 'Extra';
// german
$sh_LANG['de']['_SH404SEF_CHRONOCONTACT_SEND'] = 'Send';
$sh_LANG['de']['_SH404SEF_CHRONOCONTACT_EXTRA'] = 'Extra';

$sh_LANG['hu']['_SH404SEF_CHRONOCONTACT_SEND'] = 'Send';
$sh_LANG['hu']['_SH404SEF_CHRONOCONTACT_EXTRA'] = 'Extra';
// italiano
$sh_LANG['it']['_SH404SEF_CHRONOCONTACT_SEND'] = 'Send';
$sh_LANG['it']['_SH404SEF_CHRONOCONTACT_EXTRA'] = 'Extra';
// dutch
$sh_LANG['nl']['_SH404SEF_CHRONOCONTACT_SEND'] = 'Send';
$sh_LANG['nl']['_SH404SEF_CHRONOCONTACT_EXTRA'] = 'Extra';

