<?php
/**
 * SEF extension for Joomla!
 *
 * @author      $Author: shumisha $
 * @copyright   Yannick Gaultier - 2007-2010
 * @package     sh404SEF-15
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     $Id: com_rd_rss.php 1192 2010-04-04 16:11:04Z silianacom-svn $
 */

if (!defined('_JEXEC')) die('Direct Access to this location is not allowed.');
  
 // français
  $sh_LANG['fr']['_COM_SEF_SH_RD_RSS'] = 'abonnement rss';
  // english
  $sh_LANG['en']['_COM_SEF_SH_RD_RSS'] = 'rss feed';
  // spanish
  $sh_LANG['es']['_COM_SEF_SH_RD_RSS'] = 'Abonar al rss';
  // german
  $sh_LANG['de']['_COM_SEF_SH_RD_RSS'] = 'rss feed';
  // hungarian : translation by Jozsef Tamas Herczeg 2007-05-25
  $sh_LANG['hu']['_COM_SEF_SH_RD_RSS'] = 'RSS-csatorna';
  // italiano
  $sh_LANG['it']['_COM_SEF_SH_RD_RSS'] = 'Abbonati ai feed RSS';
  // dutch
  $sh_LANG['nl']['_COM_SEF_SH_RD_RSS'] = 'rss feed';
?>
