<?php
/**
 * SEF extension for Joomla!
 *
 * @author      $Author: shumisha $
 * @copyright   Yannick Gaultier - 2007-2010
 * @package     sh404SEF-15
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     $Id: com_rsgallery2.php 1192 2010-04-04 16:11:04Z silianacom-svn $
 */

if (!defined('_JEXEC')) die('Direct Access to this location is not allowed.');
  
  // français
  $sh_LANG['fr']['_COM_SEF_SH_CREATE_NEW'] = 'Creer nouvel article';
  
  // english
  $sh_LANG['en']['_COM_SEF_SH_CREATE_NEW'] = 'Create new article';
  
  // spanish
  $sh_LANG['es']['_COM_SEF_SH_CREATE_NEW'] = 'Nuevo articulo';
  
  // german
  $sh_LANG['de']['_COM_SEF_SH_CREATE_NEW'] = 'Neuer artikel';
  
  // hungarian : translation by Jozsef Tamas Herczeg 2007-08-15
  $sh_LANG['hu']['_COM_SEF_SH_CREATE_NEW'] = 'Új cikk létrehozása';
  
  // italiano
  $sh_LANG['it']['_COM_SEF_SH_CREATE_NEW'] = 'Crea nuovo articolo';
  
  // dutch
  $sh_LANG['nl']['_COM_SEF_SH_CREATE_NEW'] = 'Maak nieuw artikel';

?>
