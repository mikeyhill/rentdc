<?php
/**
 * SEF extension for Joomla!
 *
 * @author      $Author: shumisha $
 * @copyright   Yannick Gaultier - 2007-2010
 * @package     sh404SEF-15
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     $Id: com_content.php 1192 2010-04-04 16:11:04Z silianacom-svn $
 */

if (!defined('_JEXEC')) die('Direct Access to this location is not allowed.');
  
  // français
  $sh_LANG['fr']['_COM_SEF_SH_CREATE_NEW'] = 'Creer nouvel article';
  $sh_LANG['fr']['_COM_SEF_SH_ARCHIVE'] = 'Archive';
  // english
  $sh_LANG['en']['_COM_SEF_SH_CREATE_NEW'] = 'Create new article';
  $sh_LANG['en']['_COM_SEF_SH_ARCHIVE'] = 'Archive';
  // spanish
  $sh_LANG['es']['_COM_SEF_SH_CREATE_NEW'] = 'Nuevo articulo';
  $sh_LANG['es']['_COM_SEF_SH_ARCHIVE'] = 'Archive';
  // german
  $sh_LANG['de']['_COM_SEF_SH_CREATE_NEW'] = 'Neuer artikel';
  $sh_LANG['de']['_COM_SEF_SH_ARCHIVE'] = 'Archive';
  // hungarian : translation by Jozsef Tamas Herczeg 2007-04-09
  $sh_LANG['hu']['_COM_SEF_SH_CREATE_NEW'] = 'Új cikk készítése';
  $sh_LANG['hu']['_COM_SEF_SH_ARCHIVE'] = 'Archívum';
  // italiano
  $sh_LANG['it']['_COM_SEF_SH_CREATE_NEW'] = 'Crea nuovo articolo';
  $sh_LANG['it']['_COM_SEF_SH_ARCHIVE'] = 'Archive';
  // dutch
  $sh_LANG['nl']['_COM_SEF_SH_CREATE_NEW'] = 'Maak nieuw artikel';
  $sh_LANG['nl']['_COM_SEF_SH_ARCHIVE'] = 'Archiveer';
?>
