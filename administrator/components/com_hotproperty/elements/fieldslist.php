<?php
/**
 * @version		$Id: fieldslist.php 768 2009-07-20 16:08:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Element
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * 
 *
 * @package		Hotproperty
 * @subpackage	Element
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */
class JElementFieldslist extends JElement
{
	/**
	 * Element type
	 * 
	 * @access	protected
	 * @var		string
	 */
	var $_name = 'Fieldslist';
	
	/**
	 * Constructor
	 */
	function __construct()
	{
		global $mainframe;

		// Import Mosets Framework
		if (JPluginHelper::isEnabled('mosets', 'framework')) {
			JPluginHelper::importPlugin('mosets', 'framework');
		} else {
			JError::raiseError(404, 'Mosets Framework plugin is required for this component. Please install and enable it.');
		}
		$mainframe->triggerEvent('onInitializeMosetsFramework');
		
		MosetsFactory::getApplication('hotproperty');
		mimport('mosets.html.html');
		MosetsHTML::addIncludePath(MosetsApplication::getPath('helpers_html', 'hotproperty'));
	}

	/**
	 * 
	 * 
	 * @param	string				Name of the form element
	 * @param	string				Value
	 * @param	JSimpleXMLElement	XML node in which the element is defined
	 * @param	string				Control set name, normally params
	 */
	function fetchElement($name, $value, &$node, $control_name)
	{
		return MosetsHTML::_('hotproperty.list.fields', $control_name . '[' . $name . '][]', array('multiple' => 'multiple'), $value, false, false, true, true);
	}
}
?>