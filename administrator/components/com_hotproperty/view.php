<?php
/**
 * @version		$Id: view.php 898 2009-11-02 03:31:05Z abernier $
 * @package		Hotproperty
 * @subpackage	View
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Hotproperty View
 *
 * @package		Hotproperty
 * @subpackage	View
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

mimport('mosets.application.component.view');

class HotpropertyView extends MosetsView
{
	/**
	 * Generic layout display method
	 * 
	 * @return void
	 */
	function display($tpl = null)
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		// Load some common HTML helpers, eg : sidemenu, guide, tooltips...
		if ($mainframe->isAdmin() && JRequest::getVar('tmpl') != 'component') {
			MosetsHTML::_('hotproperty.interface.general');
		} else {
			$document =& JFactory::getDocument();
			
			if ($document->getType() == 'html') {
				MosetsHTML::_('hotproperty.content.loadTheme');
			}
		}
		
		parent::display($tpl);
	}
}
?>
