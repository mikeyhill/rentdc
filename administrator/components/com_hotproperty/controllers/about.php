<?php
/**
 * @version		$Id: about.php 768 2009-07-20 16:08:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Controller
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * About controller
 *
 * @package		Hotproperty
 * @subpackage	Controller
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyControllerAbout extends HotpropertyController
{
	/**
	 * Constructor
	 *
	 * @access	protected
	 * @param	An optional associative array of configuration settings.
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'About',
			'name_singular' => 'About'
		), $config));
	}
}
?>
