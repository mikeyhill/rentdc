<?php
/**
 * @version		$Id: report.php 768 2009-07-20 16:08:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Controller
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Report controller
 *
 * @package		Hotproperty
 * @subpackage	Controller
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyControllerReport extends HotpropertyController
{
	/**
	 * Constructor
	 *
	 * @access	protected
	 * @param	An optional associative array of configuration settings.
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Report',
			'name_singular' => 'Report'
		), $config));
	}
	
	function generateClassic()
	{
		$fields = JRequest::getVar('fields', array(), 'default', 'array');
		
		if (!empty($fields)) {
			JRequest::setVar('layout', 'classic');
			
			$this->display();
		} else {
			$this->setMessage(JText::_('Please select at least one field.'), 'error');
			return false;
		}
	}
	
	function generateExcel()
	{
		JRequest::setVar('layout', 'excel');
		
		$this->display();
	}
	
	function generatePrint()
	{
		JRequest::setVar('layout', 'print');
		
		$this->display();
	}
}
?>