<?php
/**
 * @version		$Id: featured.php 768 2009-07-20 16:08:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Controller
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Featured controller
 *
 * @package		Hotproperty
 * @subpackage	Controller
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyControllerFeatured extends HotpropertyController
{
	/**
	 * Constructor
	 *
	 * @access	protected
	 * @param	An optional associative array of configuration settings.
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Featured',
			'name_singular' => 'Featured'
		), $config));
	}
	
	/**
	 * Publish featured property(ies)
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function publish()
	{
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		// Retrieve checked items
		$ids = JRequest::getVar('id', array(), 'default', 'array');
		
		if (!empty($ids)) {
			// Get the model
			$model = $hotproperty->getModel('Property');	// Explicitely call the properties model 
		
			// Publish items and set the redirect message
			if ($model->publish($ids)) {
				$this->setMessage(sprintf(JText::_('%s item(s) %s.'), count($ids), JText::_('published')));
			} else {
				$this->setMessage(sprintf(JText::_('One or more of the selected item(s) can not be %s: %s'), JText::_('published'), $model->getError()), 'error');
				return false;
			}
		} else {
			$this->setMessage(sprintf(JText::_('Nothing to %s.'), JText::_('publish')), 'error');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Unpublish item(s)
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function unpublish()
	{
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		// Retrieve checked items
		$ids = JRequest::getVar('id', array(), 'default', 'array');
		
		if (!empty($ids)) {
			// Get the model
			$model = $hotproperty->getModel('Property');	// Explicitely call the properties model 
		
			// Unpublish items and set the redirect message
			if ($model->unpublish($ids)) {
				$this->setMessage(sprintf(JText::_('%s item(s) %s.'), count($ids), JText::_('unpublished')));
			} else {
				$this->setMessage(sprintf(JText::_('One or more of the selected item(s) can not be %s: %s'), JText::_('unpublished'), $model->getError()), 'error');
				return false;
			}
		} else {
			$this->setMessage(sprintf(JText::_('Nothing to %s.'), JText::_('unpublish')), 'error');
			return false;
		}
		
		return true;
	}
}
?>