<?php
/**
 * @version		$Id: fields.php 768 2009-07-20 16:08:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Controller
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Fields Controller
 *
 * @package		Hotproperty
 * @subpackage	Controller
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyControllerFields extends HotpropertyController
{
	/**
	 * Constructor
	 *
	 * @access	protected
	 * @param	An optional associative array of configuration settings.
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Fields',
			'name_singular' => 'Field'
		), $config));
	}
	
	/**
	 * Make extrafields appear in listing
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function listing()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		// Retrieve checked items
		$ids = JRequest::getVar('id', array(), 'default', 'array');
		
		if (!empty($ids)) {
			// Get the model
			$model =& $this->getModel();
		
			// Listing items and set the redirect message
			if ($model->listing($ids)) {
				$this->setMessage(sprintf(JText::_('%s item(s) %s.'), count($ids), JText::_('shown in listing')));
			} else {
				$this->setMessage(sprintf(JText::_('One or more of the selected item(s) can not be %s: %s'), JText::_('shown in listing'), $model->getError()), 'error');
				return false;
			}
		} else {
			$this->setMessage(sprintf(JText::_('Nothing to %s.'), JText::_('show in listing')), 'error');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Make extrafields not appear in listing
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function unlisting()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		// Retrieve checked items
		$ids = JRequest::getVar('id', array(), 'default', 'array');
		
		if (!empty($ids)) {
			// Get the model
			$model =& $this->getModel();
		
			// Listing items and set the redirect message
			if ($model->unlisting($ids)) {
				$this->setMessage(sprintf(JText::_('%s item(s) %s.'), count($ids), JText::_('removed from listing')));
			} else {
				$this->setMessage(sprintf(JText::_('One or more of the selected item(s) can not be %s: %s'), JText::_('removed from listing'), $model->getError()), 'error');
				return false;
			}
		} else {
			$this->setMessage(sprintf(JText::_('Nothing to %s.'), JText::_('remove from listing')), 'error');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Make extrafields be searchable
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function searchable()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		// Retrieve checked items
		$ids = JRequest::getVar('id', array(), 'default', 'array');
		
		if (!empty($ids)) {
			// Get the model
			$model =& $this->getModel();
		
			// Listing items and set the redirect message
			if ($model->searchable($ids)) {
				$this->setMessage(sprintf(JText::_('%s item(s) %s.'), count($ids), JText::_('set to searchable')));
			} else {
				$this->setMessage(sprintf(JText::_('One or more of the selected item(s) can not be %s: %s'), JText::_('set to searchable'), $model->getError()), 'error');
				return false;
			}
		} else {
			$this->setMessage(sprintf(JText::_('Nothing to %s.'), JText::_('set to searchable')), 'error');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Make extrafields be not searchable
	 * 
	 * @access	public
	 * @return	boolean	Success
	 */
	function unsearchable()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		// Retrieve checked items
		$ids = JRequest::getVar('id', array(), 'default', 'array');
		
		if (!empty($ids)) {
			// Get the model
			$model =& $this->getModel();
		
			// Listing items and set the redirect message
			if ($model->unsearchable($ids)) {
				$this->setMessage(sprintf(JText::_('%s item(s) %s.'), count($ids), JText::_('set to not searchable')));
			} else {
				$this->setMessage(sprintf(JText::_('One or more of the selected item(s) can not be %s: %s'), JText::_('set to not searchable'), $model->getError()), 'error');
				return false;
			}
		} else {
			$this->setMessage(sprintf(JText::_('Nothing to %s.'), JText::_('set to not searchable')), 'error');
			return false;
		}
		
		return true;
	}
}
?>