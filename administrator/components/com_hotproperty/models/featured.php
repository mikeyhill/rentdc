<?php
/**
 * @version		$Id: featured.php 768 2009-07-20 16:08:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Model
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Featured model
 *
 * @package		Hotproperty
 * @subpackage	Model
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyModelFeatured extends HotpropertyModel
{
	/**
	 * Constructor
	 *
	 * @access protected
	 * @param	An optional associative array of configuration settings.
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Featured',
			'name_plural' => 'Featured',
			'belongs_to' => array(
				'Property' => array(
					'foreignKey' => 'property'
				)
			)
		), $config));
	}
}
?>
