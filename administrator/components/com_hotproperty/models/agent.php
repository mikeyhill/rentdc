<?php
/**
 * @version		$Id: agent.php 768 2009-07-20 16:08:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Model
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Agents Model
 *
 * @package		Hotproperty
 * @subpackage	Model
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyModelAgent extends HotpropertyModel
{
	/**
	 * Constructor
	 *
	 * @access protected
	 * @param	An optional associative array of configuration settings.
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Agent',
			'name_plural' => 'Agents',
			'belongs_to' => array(
				'Company' => array(
					'foreignKey' => 'company'
				),
				'User' => array(
					'foreignKey' => 'user'
				)
			),
			'has_many' => array(
				'Property' => array(
					'foreignKey'	=> 'agent',
					'dependent'		=> 'warn'
				)
			)
		), $config));
	}
}
?>
