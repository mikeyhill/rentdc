<?php
/**
 * @version		$Id: type.php 781 2009-08-17 20:50:08Z abernier $
 * @package		Hotproperty
 * @subpackage	Model
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Types Model
 *
 * @package		Hotproperty
 * @subpackage	Model
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */


class HotpropertyModelType extends HotpropertyModel
{
	/**
	 * Constructor
	 *
	 * @access protected
	 * @param	An optional associative array of configuration settings.
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Type',
			'name_plural' => 'Types',
			'has_many' => array(
				'Property' => array(
					'foreignKey'	=> 'type',
					'dependent'		=> 'warn'
				)
			)
		), $config));
	}
}
?>
