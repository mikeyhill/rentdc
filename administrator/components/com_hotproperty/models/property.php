<?php
/**
 * @version		$Id: property.php 953 2010-02-22 13:02:38Z cy $
 * @package		Hotproperty
 * @subpackage	Model
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Properties model
 *
 * @package		Hotproperty
 * @subpackage	Model
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyModelProperty extends HotpropertyModel
{
	/**
	 * Constructor
	 *
	 * @access protected
	 * @param	An optional associative array of configuration settings.
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Property',
			'name_plural' => 'Properties',
			'has_one' => array(
				'Featured' => array(
					'foreignKey'	=> 'property',
					'dependent'		=> true
				)
			),
			'belongs_to' => array(
				'Agent' => array(
					'foreignKey' => 'agent'
				),
				'Type' => array(
					'foreignKey' => 'type'
				)
			),
			'has_many' => array(
				'Photo' => array(
					'foreignKey'	=> 'property',
					'dependent'		=> true
				),
				'PropertyField' => array(
					'foreignKey'	=> 'property',
					'dependent'		=> true
				)
			)
		), $config));
	}
	
	/**
	 * Method to search for properties
	 *
	 * @access	public
	 * @param	array	An indexed array of fields to search for, eg: array('<Field's ID>' => 'Value', '<Field's ID>' => array('Value1', 'Value2'), ...)
	 * @return	array	An array of properties' IDs
	 */
	function search($post_fields = array())
	{
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		$where = array();
		$join = "";
		
		if (!empty($post_fields)) {
			// Log search
			if (@$post_fields['name']) {
				$search_term = strtolower($post_fields['name']);
				
				$logSearchModel =& $hotproperty->getModel('LogSearch');
				$hits = $logSearchModel->getData('count', array(
					'where' => array(
						'LogSearch.search_term' => $search_term
					)
				));

				if ($hits) {
					$query =  "UPDATE #__hp_log_searches SET hits=(hits+1) WHERE LOWER(search_term) = '" . $post_fields['name'] . "'";
				} else {
					$query = "INSERT INTO #__hp_log_searches VALUES ('" . $post_fields['name'] . "', 1)";
				}
				$this->query($query);
			}
			
			$field_model =& $hotproperty->getModel('Field');
			$fields = $field_model->getData('all', array(
				'where' => array(
					'Field.name' => array_keys($post_fields),
					'Field.published' => 1,
					'Field.search' => 1
				)
			));
			
			if (empty($fields)) {
				return false;
			}
			
			/*echo "<pre>";
			echo "fields=";
			print_r($fields);
			echo "</pre>";*/

			$i = 0;
			foreach ($fields as $field)
			{
				// Core Fields
				if ($field->iscore) {
					if ($field->search_type) {
						switch ($field->search_type)
						{
							// Less/More than
							case 'range_1':
								$operator = ($post_fields[$field->name]['range'] == 0) ? '<=' : '>=';
								$where[] = $this->_db->nameQuote('Property') . '.' . $this->_db->nameQuote($field->name) . ' ' . $operator . ' ' . $this->_db->Quote($post_fields[$field->name][0]);
								break;
							// From X to Y
							case 'range_2':
								$condition = '';
								if ($post_fields[$field->name]['from']) {
									$condition = $this->_db->nameQuote('Property') . '.' . $this->_db->nameQuote($field->name) . ' >= ' . $this->_db->Quote($post_fields[$field->name]['from']);
								}
								if ($post_fields[$field->name]['to']) {
									if ($post_fields[$field->name]['from'])
										$condition .= ' AND ';
									$condition .= $this->_db->nameQuote('Property') . '.' . $this->_db->nameQuote($field->name) . ' <= ' . $this->_db->Quote($post_fields[$field->name]['to']);
								}
								$where[] = $condition;
								break;
						}
					} else {
						switch ($field->field_type)
						{
							case 'text':
							case 'link':
							case 'multitext':
								$where[] = $this->_db->nameQuote('Property') . '.' . $this->_db->nameQuote($field->name) . ' LIKE ' . $this->_db->Quote('%' . $post_fields[$field->name] . '%');
								break;
							case 'selectmultiple':
							case 'checkbox':
								// Quote array
								$fieldValues = $post_fields[$field->name];
								for($i = 0; $i < count($fieldValues); $i++)
								{
									$fieldValues[$i] = $this->_db->Quote($fieldValues[$i]);
								}
								$where[] = $this->_db->nameQuote('Property') . '.' . $this->_db->nameQuote($field->name) . ' IN (' . implode(', ', $fieldValues) . ')';
								break;
							default:
								if ($field->name == 'company') {
									$join .= "\nJOIN #__hp_agents AS " . $this->_db->nameQuote('Agent') . " ON " . $this->_db->nameQuote('Agent') . "." . $this->_db->nameQuote('id') . " = " . $this->_db->nameQuote('Property') . "." . $this->_db->nameQuote('agent');
									$where[] = $this->_db->nameQuote('Agent') . '.' . $this->_db->nameQuote($field->name) . ' = ' . $this->_db->Quote($post_fields[$field->name]);
								} else {
									$where[] = $this->_db->nameQuote('Property') . '.' . $this->_db->nameQuote($field->name) . ' = ' . $this->_db->Quote($post_fields[$field->name]);
								}
								break;
						}
					}
				// Not-Core Fields
				} else {
					if ($field->search_type) {
						switch ($field->search_type)
						{
							case 'range_1':
								$operator = ($post_fields[$field->name]['range'] == 0) ? '<=' : '>=';
								$join .= "\nJOIN #__hp_properties2 AS " . $this->_db->nameQuote('PropertyField_' . $i) . " ON " . $this->_db->nameQuote('PropertyField_' . $i) . "." . $this->_db->nameQuote('field') . " = " . $this->_db->Quote($field->id) . " AND " . $this->_db->nameQuote('PropertyField_' . $i) . "." . $this->_db->nameQuote('property') . " = " . $this->_db->nameQuote('Property') . "." . $this->_db->nameQuote('id') . " AND " . $this->_db->nameQuote('PropertyField_' . $i) . "." . $this->_db->nameQuote('value') . " $operator " . intval($post_fields[$field->name][0]) . "";
								break;
							case 'range_2':
								$join .= "\nJOIN #__hp_properties2 AS " . $this->_db->nameQuote('PropertyField_' . $i) . " ON " . $this->_db->nameQuote('PropertyField_' . $i) . "." . $this->_db->nameQuote('field') . " = " . $this->_db->Quote($field->id) . " AND " . $this->_db->nameQuote('PropertyField_' . $i) . "." . $this->_db->nameQuote('property') . " = " . $this->_db->nameQuote('Property') . "." . $this->_db->nameQuote('id') . " AND ";
								if ($post_fields[$field->name]['from']) {
									$join .= $this->_db->nameQuote('PropertyField_' . $i) . "." . $this->_db->nameQuote('value') . " >= " . intval($post_fields[$field->name]['from']);
								}
								if ($post_fields[$field->name]['to']) {
									if ($post_fields[$field->name]['from'])
										$join .= " AND ";
									$join .= $this->_db->nameQuote('PropertyField_' . $i) . "." . $this->_db->nameQuote('value') . " <= " . intval($post_fields[$field->name]['to']);
								}
								$join .= "";
								break;
						}
					} else {
						switch ($field->field_type)
						{
							case 'text':
							case 'link':
							case 'multitext':
								$join .= "\nJOIN #__hp_properties2 AS " . $this->_db->nameQuote('PropertyField_' . $i) . " ON " . $this->_db->nameQuote('PropertyField_' . $i) . "." . $this->_db->nameQuote('field') . " = " . $this->_db->Quote($field->id) . " AND " . $this->_db->nameQuote('PropertyField_' . $i) . "." . $this->_db->nameQuote('property') . " = " . $this->_db->nameQuote('Property') . "." . $this->_db->nameQuote('id') . " AND " . $this->_db->nameQuote('PropertyField_' . $i) . "." . $this->_db->nameQuote('value') . " LIKE " . $this->_db->Quote('%' . $post_fields[$field->name] . '%') . "";
								break;
							case 'selectmultiple':
							case 'checkbox':
								$join .= "\nJOIN #__hp_properties2 AS " . $this->_db->nameQuote('PropertyField_' . $i) . " ON " . $this->_db->nameQuote('PropertyField_' . $i) . "." . $this->_db->nameQuote('field') . " = " . $this->_db->Quote($field->id) . " AND " . $this->_db->nameQuote('PropertyField_' . $i) . "." . $this->_db->nameQuote('property') . " = " . $this->_db->nameQuote('Property') . "." . $this->_db->nameQuote('id') . " AND " . $this->_db->nameQuote('PropertyField_' . $i) . "." . $this->_db->nameQuote('value') . " = " . $this->_db->Quote(implode('|', $post_fields[$field->name])) . "";
								break;
							default:
								$join .= "\nJOIN #__hp_properties2 AS " . $this->_db->nameQuote('PropertyField_' . $i) . " ON " . $this->_db->nameQuote('PropertyField_' . $i) . "." . $this->_db->nameQuote('field') . " = " . $this->_db->Quote($field->id) . " AND " . $this->_db->nameQuote('PropertyField_' . $i) . "." . $this->_db->nameQuote('property') . " = " . $this->_db->nameQuote('Property') . "." . $this->_db->nameQuote('id') . " AND " . $this->_db->nameQuote('PropertyField_' . $i) . "." . $this->_db->nameQuote('value') . " = " . $this->_db->Quote($post_fields[$field->name]) . "";
								break;
						}
					}
				}
				$i++;
			}
			/*echo "<pre>";
			echo "where=";
			print_r($where);
			echo "</pre>";
			echo "<pre>";
			echo "join=";
			print_r($join);
			echo "</pre>";*/
		}
		
		if (empty($where) && empty($join)) {
			return false;
		}
		
		$nullDate	= $this->_db->getNullDate(); 
		$now		= JFactory::getDate();
		
		$query = "SELECT " . $this->_db->nameQuote('Property') . "." . $this->_db->nameQuote('id')
			. "\nFROM #__hp_properties AS " . $this->_db->nameQuote('Property')
			. $join
			. "\nWHERE " . ($where ? implode(" AND ", $where) : "1")
			;
		/*echo "<pre>";
		echo "query=";
		print_r($query);
		echo "</pre>";*/
		
		$this->_db->setQuery($query);
		
		$return = $this->_db->loadResultArray();
		/*echo "<pre>";
		echo "return=";
		print_r($return);
		echo "</pre>";*/
		
		return $return;
	}
}
?>
