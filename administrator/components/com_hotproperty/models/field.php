<?php
/**
 * @version		$Id: field.php 768 2009-07-20 16:08:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Model
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Field model
 *
 * @package		Hotproperty
 * @subpackage	Model
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyModelField extends HotpropertyModel
{
	/**
	 * Constructor
	 *
	 * @access protected
	 * @param	An optional associative array of configuration settings.
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Field',
			'name_plural' => 'Fields',
			'has_many' => array(
				'PropertyField' => array(
					'foreignKey'	=> 'field',
					'dependent'		=> true
				)
			)
		), $config));
	}
	
	/**
	 * Listing
	 * 
	 * @access	public
	 * @param	array	An array of id numbers
	 * @return	boolean
	 */
	function listing($ids)
	{
		if (!empty($ids)) {
			$row =& $this->getTable();
		
			foreach ($ids as $id)
			{
				if (!$row->load($id)) {
					$this->setError($row->getError());
					return false;
				}
			
				if (!$row->listing()) {
					$this->setError($row->getError());
					return false;
				}
        
				if (!$row->store()) {
					$this->setError($row->getError());
					return false;
				}
			}
		} else {
			$this->setError(get_class($this) . '::approve() : Argument #1 cannot be empty !');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Unlisting
	 * 
	 * @access	public
	 * @param	array	An array of id numbers
	 * @return	boolean
	 */
	function unlisting($ids)
	{
		if (!empty($ids)) {
			$row =& $this->getTable();
		
			foreach ($ids as $id)
			{
				if (!$row->load($id)) {
					$this->setError($row->getError());
					return false;
				}
			
				if (!$row->listing(0)) {
					$this->setError($row->getError());
					return false;
				}
        
				if (!$row->store()) {
					$this->setError($row->getError());
					return false;
				}
			}
		} else {
			$this->setError(get_class($this) . '::approve() : Argument #1 cannot be empty !');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Searchable
	 * 
	 * @access	public
	 * @param	array	An array of id numbers
	 * @return	boolean
	 */
	function searchable($ids)
	{
		if (!empty($ids)) {
			$row =& $this->getTable();
		
			foreach ($ids as $id)
			{
				if (!$row->load($id)) {
					$this->setError($row->getError());
					return false;
				}
			
				if (!$row->searchable()) {
					$this->setError($row->getError());
					return false;
				}
        
				if (!$row->store()) {
					$this->setError($row->getError());
					return false;
				}
			}
		} else {
			$this->setError(get_class($this) . '::approve() : Argument #1 cannot be empty !');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Unsearchable
	 * 
	 * @access	public
	 * @param	array	An array of id numbers
	 * @return	boolean
	 */
	function unsearchable($ids)
	{
		if (!empty($ids)) {
			$row =& $this->getTable();
		
			foreach ($ids as $id)
			{
				if (!$row->load($id)) {
					$this->setError($row->getError());
					return false;
				}
			
				if (!$row->searchable(0)) {
					$this->setError($row->getError());
					return false;
				}
        
				if (!$row->store()) {
					$this->setError($row->getError());
					return false;
				}
			}
		} else {
			$this->setError(get_class($this) . '::approve() : Argument #1 cannot be empty !');
			return false;
		}
		
		return true;
	}
}
?>
