<?php
/**
 * @version		$Id: list.php 956 2010-03-08 02:59:41Z cy $
 * @package		Hotproperty
 * @subpackage	Helper.HTML
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * List HTML Helper
 *
 * @package		Hotproperty
 * @subpackage	Helper.HTML
 * @author		Antoine Bernier <abernier@mosets.com>
 */

require_once(JPath::clean(MOSETS_LIBRARIES . '/mosets/html/html/list.php'));

class HotpropertyList extends MosetsHTMLList
{
	/**
	 * Build the select list for ordering
	 * 
	 * @access	public
	 * @return	string
	 */
	function ordering($name, $table_name, $row, $value = 'ordering', $text = 'name')
	{
		return MosetsHTML::_('list.ordering', $name, 'hotproperty', $table_name, $row, $value, $text);
	}
	
	/**
	 * Select list of companies
	 * 
	 * @access	public
	 * @param	string	Name and id of the select list
	 * @param	array	Attributes associative array
	 * @param	int		The id of the selected company
	 * @param	string	The id tag
	 * @param	boolean
	 * @return	string
	 */
	function companies($name, $attribs = null, $selected = null, $idtag = false, $translate = false)
	{
		$db =& MosetsFactory::getDBO();		
		$query = 'SELECT id AS value, name AS text'
			. ' FROM #__hp_companies'
			. ' ORDER BY name'
			;
		$db->setQuery($query);
		
		$companies = array(MosetsHTML::_('select.option', '', '- ' . sprintf(JText::_('Select %s'), JText::_('Company')) . ' -'));
		$companies = array_merge($companies, $db->loadObjectList());
		
		$companies = MosetsHTML::_('select.genericlist', $companies, $name, $attribs, 'value', 'text', $selected, $idtag, $translate);
		
		return $companies;
	}
	
	/**
	 * Select list of agents
	 * 
	 * @access	public
	 * @param	string	Name and id of the select list
	 * @param	array	Attributes associative array
	 * @param	int		The id of the selected company
	 * @param	string	The id tag
	 * @param	boolean
	 * @return	string
	 */
	function agents($name, $attribs = null, $selected = null, $idtag = false, $translate = false)
	{
		$db =& MosetsFactory::getDBO();		

		$query = 'SELECT id AS value, name AS text'
			. ' FROM #__hp_agents'
			. ' ORDER BY name'
			;
		$db->setQuery($query);
		
		$agents = array(MosetsHTML::_('select.option', '', '- ' . sprintf(JText::_('Select %s'), JText::_('Agent')) . ' -'));
		$agents = array_merge($agents, $db->loadObjectList());
		
		$agents = MosetsHTML::_('select.genericlist', $agents, $name, $attribs, 'value', 'text', $selected, $idtag, $translate);
		
		return $agents;
	}
	
	/**
	 * Select list of users
	 * 
	 * @access	public
	 * @param	string	Name and id of the select list
	 * @param	array	Attributes associative array
	 * @param	int		The id of the selected company
	 * @param	string	The id tag
	 * @param	boolean
	 * @return	string
	 */
	function users($name, $attribs = null, $selected = null, $idtag = false, $translate = false, $unused_agents = false)
	{
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		$db =& MosetsFactory::getDBO();

		$query = "SELECT u.id AS value, u.name AS text"
			. "\nFROM #__users AS u"
			. "\nLEFT JOIN #__hp_agents AS a ON a.user=u.id"
			. "\nWHERE u.block = 0"
			. "\nAND u.gid >= " . $hotproperty->getCfg('agent_groupid')
			. ($unused_agents ? "\nAND ISNULL(a.user)" : "")
			. ($selected ? "\nOR u.id=$selected" : "")
			. "\nORDER BY u.name"
			;
		$db->setQuery($query);
		
		$users = array(MosetsHTML::_('select.option', '', '- ' . sprintf(JText::_('Select %s'), JText::_('User')) . ' -'));
		$users = array_merge($users, $db->loadObjectList());

		$users = MosetsHTML::_('select.genericlist', $users, $name, $attribs, 'value', 'text', $selected, $idtag, $translate);

		return $users;
	}
	
	/**
	 * Select list of types
	 * 
	 * @access	public
	 * @param	string	Name and id of the select list
	 * @param	array	Attributes associative array
	 * @param	int		The id of the selected company
	 * @param	string	The id tag
	 * @param	boolean
	 * @return	string
	 */
	function types($name, $attribs = null, $selected = null, $idtag = false, $translate = false, $published = false)
	{
		$db =& MosetsFactory::getDBO();

		$query = "SELECT id AS value, name AS text"
			. "\nFROM #__hp_prop_types"
			. ($published ? "\nWHERE published=1" : "")
			. "\nORDER BY name"
			;
		$db->setQuery($query);
		
		$types = array(MosetsHTML::_('select.option',  '', '- ' . sprintf(JText::_('Select %s'), JText::_('Type')) . ' -'));
		$types = array_merge($types, $db->loadObjectList());
		
		$types = MosetsHTML::_('select.genericlist', $types, $name, $attribs, 'value', 'text', $selected, $idtag, $translate);
		
		return $types;
	}
	
	/**
	 * Select list of properties
	 * 
	 * @access	public
	 * @param	string	Name and id of the select list
	 * @param	boolean	True if we only want published properties
	 * @param	boolean	True if we only want approved properties
	 * @param	array	Attributes associative array
	 * @param	int		The id of the selected company
	 * @param	string	The id tag
	 * @param	boolean
	 * @return	string
	 */
	function properties($name, $published = true, $approved = true, $attribs = null, $selected = null, $idtag = false, $translate = false)
	{
		$db =& MosetsFactory::getDBO();
		$nullDate = $db->getNullDate(); 
		$now = JFactory::getDate();

		$query = "SELECT id AS value, name AS text"
			. "\nFROM #__hp_properties"
			. "\nWHERE 1=1"
			. ($published ? "\nAND " . $db->nameQuote('published') . "=" . $db->Quote(1) . " AND (" . $db->nameQuote('publish_up') . "=" . $db->Quote($nullDate) . " OR " . $db->nameQuote('publish_up') . "<=" . $db->Quote($now->toMySQL()) . ") AND (" . $db->nameQuote('publish_down') . "=" . $db->Quote($nullDate) . " OR " . $db->nameQuote('publish_down') . ">=" . $db->Quote($now->toMySQL()) . ")" : "")
			. ($approved ? "\nAND " . $db->nameQuote('approved') . "=" . $db->Quote(1) : "")
			;
		$db->setQuery($query);
		
		$properties = array(MosetsHTML::_('select.option',  '', '- ' . sprintf(JText::_('Select %s'), JText::_('Property')) . ' -'));
		$properties = array_merge($properties, $db->loadObjectList());
		
		$properties = MosetsHTML::_('select.genericlist', $properties, $name, $attribs, 'value', 'text', $selected, $idtag, $translate);
		
		return $properties;
	}
	
	/**
	 * Select list of extrafield types
	 * 
	 * @access	public
	 * @param	string	Name and id of the select list
	 * @param	array	Attributes associative array
	 * @param	int		The id of the selected type
	 * @param	string	'Core' option
	 * @param	string	'Not-core' option
	 * @param	string	The id tag
	 * @param	boolean
	 * @return	string
	 */
	function fields($name, $attribs, $selected = null, $idtag = false, $translate = false, $published = false, $searchable = false)
	{
		$db =& MosetsFactory::getDBO();

		$query = "SELECT name AS value, caption AS text"
			. "\nFROM #__hp_prop_ef"
			. "\nWHERE 1=1"
			. ($published ? "\nAND " . $db->nameQuote('published') . "=" . $db->Quote(1) : "")
			. ($searchable ? "\nAND " . $db->nameQuote('search') . "=" . $db->Quote(1) : "")
			. "\nORDER BY " . $db->nameQuote('ordering')
			;
		$db->setQuery($query);
		
		$fields = array(MosetsHTML::_('select.option',  '', '- ' . sprintf(JText::_('Select %s'), JText::_('Extrafield(s)')) . ' -'));
		$fields = array_merge($fields, $db->loadObjectList());
		
		$fields = MosetsHTML::_('select.genericlist', $fields, $name, $attribs, 'value', 'text', $selected, $idtag, $translate);
		
		return $fields;
	}
	
	/**
	 * Select list of extrafield types
	 * 
	 * @access	public
	 * @param	string	Name and id of the select list
	 * @param	array	Attributes associative array
	 * @param	int		The id of the selected type
	 * @param	string	'Core' option
	 * @param	string	'Not-core' option
	 * @param	string	The id tag
	 * @param	boolean
	 * @return	string
	 */
	function field_types($name, $attribs, $selected = null, $core = null, $notcore = null, $idtag = false, $translate = false)
	{
		$field_types	= array();
		$field_types[]	= MosetsHTML::_('select.option',  '', '- ' . sprintf(JText::_('Select %s'), JText::_('Field Type')) . ' -');
		// Core option enabled
		if ($core) {
			$field_types[]	= MosetsHTML::_('select.option', 'core', JText::_('Core'));
		}
		// Not-core option enabled
		if ($core) {
			$field_types[]	= MosetsHTML::_('select.option', 'notcore', JText::_('Not Core'));
		}
		// @todo	rename key option with standard name
		// @see		http://www.w3.org/TR/html401/interact/forms.html
		$field_types[]	= MosetsHTML::_('select.option',  'text', JText::_('Text'));
		$field_types[]	= MosetsHTML::_('select.option',  'multitext', JText::_('Textarea'));
		$field_types[]	= MosetsHTML::_('select.option',  'selectlist', JText::_('Select List'));
		$field_types[]	= MosetsHTML::_('select.option',  'selectmultiple', JText::_('Multiple Select List'));
		$field_types[]	= MosetsHTML::_('select.option',  'checkbox', JText::_('Checkbox'));
		$field_types[]	= MosetsHTML::_('select.option',  'radiobutton', JText::_('Radiobutton'));
		$field_types[]	= MosetsHTML::_('select.option',  'link', JText::_('Web Link'));
		
		$field_types = MosetsHTML::_('select.genericlist', $field_types, $name, $attribs, 'value', 'text', $selected, $idtag, $translate);
		
		return $field_types;
	}
	
	/**
	 * Select list of types' appearances ('Featured', 'Listing', 'Searchable')
	 * 
	 * @access	public
	 * @param	string	Name and id of the select list
	 * @param	array	Attributes associative array
	 * @param	int		The id of the selected type
	 * @param	string	The id tag
	 * @param	boolean
	 * @return	string
	 */
	function field_appearances($name, $attribs = null, $selected = null, $idtag = false, $translate = false)
	{
		$field_appearances	= array();
		$field_appearances[]	= MosetsHTML::_('select.option',  '', '- ' . sprintf(JText::_('Select %s'), JText::_('Field Appearance')) . ' -');
		$field_appearances[]	= MosetsHTML::_('select.option',  'F', JText::_('Featured'));
		$field_appearances[]	= MosetsHTML::_('select.option',  'L', JText::_('Listing'));
		$field_appearances[]	= MosetsHTML::_('select.option',  'S', JText::_('Searchable'));
		
		$field_appearances = MosetsHTML::_('select.genericlist', $field_appearances, $name, $attribs, 'value', 'text', $selected, $idtag, $translate);
		
		return $field_appearances;
	}
	
	/**
	 * Build the select list for search types
	 * 
	 * @access	public
	 * @param	string	Name and id of the select list
	 * @param	array	Attributes associative array
	 * @param	int		The id of the selected type
	 * @param	string	The id tag
	 * @param	boolean
	 * @return	string
	 */
	function search_types($name, $attribs = null, $selected = null, $idtag = false, $translate = false)
	{
		$search_types	= array();
		$search_types[]	= MosetsHTML::_('select.option',  '0', JText::_('Default'));
		$search_types[]	= MosetsHTML::_('select.option',  'range_1', JText::_('Less/More Than X'));
		$search_types[]	= MosetsHTML::_('select.option',  'range_2', JText::_('From X to Y'));
		
		$search_types = MosetsHTML::_('select.genericlist', $search_types, $name, $attribs, 'value', 'text', $selected, $idtag, $translate);
		
		return $search_types;
	}
	
	/**
	 * Build the select list of avalaible CSS files
	 * 
	 * @access	public
	 * @access	public
	 * @param	string	Name and id of the select list
	 * @param	array	Attributes associative array
	 * @param	int		The id of the selected type
	 * @param	string	The id tag
	 * @param	boolean
	 * @return	string
	 */
	function css_files($name, $attribs = null, $selected = null, $idtag = false, $translate = false)
	{
		jimport('joomla.filesystem.file');
		jimport('joomla.filesystem.folder');
		$path = JPath::clean(MosetsApplication::getPath('media_css', 'hotproperty'));
		$files = JFolder::files($path, '.css');
		
		$css_files = array(MosetsHTML::_('select.option', '', '- ' . sprintf(JText::_('Select %s'), JText::_('CSS File')) . ' -'));
		foreach ($files as $file)
		{
			$css_files[] = MosetsHTML::_('select.option', JFile::stripExt($file), $file);
		}
		
		$css_files = MosetsHTML::_('select.genericlist', $css_files, $name, $attribs, 'value', 'text', $selected, $idtag, $translate);
		
		return $css_files;
	}
}
?>
