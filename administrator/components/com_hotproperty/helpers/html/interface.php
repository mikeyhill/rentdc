<?php
/**
 * @version		$Id: interface.php 926 2009-11-18 01:50:34Z abernier $
 * @package		Hotproperty
 * @subpackage	Helper.HTML
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Interface HTML Helper
 *
 * @package		Hotproperty
 * @subpackage	Helper.HTML
 * @author		Antoine Bernier <abernier@mosets.com>
 */

require_once(JPath::clean(MOSETS_LIBRARIES . '/mosets/html/html/interface.php'));

class HotpropertyInterface extends MosetsHTMLInterface
{
	/**
	 * A method to play the __construct() role since MosetsHTML::_() doesn't instantiate the class
	 * 
	 * @access	public
	 * @return	void
	 */
	function general()
	{
		global $mainframe;

		$document =& JFactory::getDocument();
		if ($document->getType() == 'html') {
			$document->addCustomTag('<!--[if IE]><link href="' . MosetsApplication::getPath('assets_css_url', 'hotproperty') . 'ie.css" rel="stylesheet" type="text/css" /><![endif]-->');
			$document->addCustomTag('<!--[if lte IE 6]><link href="' . MosetsApplication::getPath('assets_css_url', 'hotproperty') . 'ie6.css" rel="stylesheet" type="text/css" /><![endif]-->');
			$document->addCustomTag('<!--[if IE 7]><link href="' . MosetsApplication::getPath('assets_css_url', 'hotproperty') . 'ie7.css" rel="stylesheet" type="text/css" /><![endif]-->');
		}
		$document->addStyleSheet(MosetsApplication::getPath('assets_css_url', 'hotproperty') . 'interface.css');

		if ($mainframe->isAdmin()) {
			MosetsHTML::_('hotproperty.interface.sidemenu');
		}
		MosetsHTML::_('hotproperty.interface.guide');
	}
	
	/**
	 * The Hot Property's Guide
	 * 
	 * @access	public
	 * @return	void
	 */
	function guide()
	{
		/**
		 * Get informations from DB
		 */
		
		$database =& MosetsFactory::getDBO();
		$my =& JFactory::getUser();

		// Get number of property types
		$database->setQuery("SELECT count(*) FROM #__hp_prop_types");
		$total["types"] = $database->loadResult();

		// Get number of companies
		$database->setQuery("SELECT count(*) FROM #__hp_companies");
		$total["companies"] = $database->loadResult();

		// Get number of agents
		$database->setQuery("SELECT count(*) FROM #__hp_agents");
		$total["agents"] = $database->loadResult();

		// Get number of properties
		$database->setQuery("SELECT count(*) FROM #__hp_properties");
		$total["properties"] = $database->loadResult();
		
		/**
		 * Guide steps
		 */
		
		$step1 = ($total["types"] <= 0 || $total["agents"] <= 0 || $total["companies"] <= 0);
		$step2 = ($total["properties"] <= 0);
		$step3 = ($total["properties"] == 1);
		
		$uri		=& JURI::getInstance();
		$referer	= base64_encode('index.php' . $uri->toString(array('query', 'fragment')));
		
		/**
		 * Display the guide
		 */
		
		if ($step1 || $step2 || $step3) {
			?>
			<div id="guide"><fieldset>
				<legend><?php echo JText::_("Hot Property's Guide"); ?></legend>
				
				<div class="content">
					<?php
					
					/**
					 * STEP 1 - Adding Types, Companies and Agents
					 */
					
					if ($step1) {
						?>
						<p><?php echo sprintf(JText::_('GUIDE INTRO'), $my->name); ?></p>
						<p>
							<span class="step"><?php echo JText::_('Step 1:'); ?></span>
							<?php echo JText::_('GUIDE STEP 1'); ?>
						</p>
						<ol>
							<?php if ($total["types"] <= 0) : ?>
								<li><?php echo MosetsHTML::_('link', JRoute::_('index.php?option=com_hotproperty&view=types&task=add&referer=' . $referer), JText::_('Create a property type')); ?></a></li>
							<?php endif; ?>
							<?php if ($total["companies"] <= 0) : ?>
								<li><?php echo MosetsHTML::_('link', JRoute::_('index.php?option=com_hotproperty&view=companies&task=add&referer=' . $referer), JText::_('Create a company')); ?></a></li>
							<?php endif; ?>
							<?php if ($total["agents"] <= 0) : ?>
								<li><?php echo MosetsHTML::_('link', JRoute::_('index.php?option=com_hotproperty&view=agents&task=add&referer=' . $referer), JText::_('Create an agent')); ?></a></li>
							<?php endif; ?>
						</ol>
						<?php
						
					/**
					 * STEP 2 - Setting up the configuration and adding the first property.
					 */
					
					} elseif ($step2) {
						?>
						<p>
							<span class="step"><?php echo JText::_('Step 2:'); ?></span>
							<?php echo JText::_('GUIDE STEP 2'); ?>
						</p>
						<?php
						
					/**
					 * STEP 3 - DONE. More Information
					 */
					
					} elseif ($step3) {
						?>
						<p>
							<span class="step"><?php echo JText::_('Step 3:'); ?></span>
							<?php echo JText::_('GUIDE STEP 3'); ?>
						</p>
						<p>
							<?php echo JText::_('GUIDE STEP 3B'); ?>
						</p>
						<p>
							<?php echo JText::_('Thank you for using Hot Property!'); ?>
						</p>
						<?php
					}
					?>
					</div>
				</fieldset></div>
			<?php
		}
	}
	
	/**
	 * The Side Menu
	 * 
	 * @access	public
	 * @return	void
	 */
	function sidemenu()
	{
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		$view = JRequest::getCmd('view');
		$task = JRequest::getCmd('task');
		
		$uri		=& JURI::getInstance();
		$referer	= base64_encode('index.php' . $uri->toString(array('query', 'fragment')));
		?>
		<div id="sidemenu">
			<div class="t">
				<div class="t">
					<div class="t"></div>
				</div>
			</div>
			<div class="m">
				<center><?php echo MosetsHTML::_('link', JRoute::_('index.php?option=com_hotproperty'), MosetsHTML::_('image', MosetsApplication::getPath('assets_images_url', 'hotproperty') . 'logo.png', JText::_('Hot Property'))); ?></center>
				<dl>
					<dt><?php echo JText::_('Properties'); ?></dt>
					<dd <?php echo ($view == 'properties' && empty($task)) ? 'class="active"' : ''; ?>>
						<?php echo MosetsHTML::_('link', JRoute::_('index.php?option=com_hotproperty&view=properties'),JText::_('View Properties')); ?>
					</dd>
					<dd class="zebra<?php echo ($view == 'types' && empty($task)) ? ' active' : ''; ?>">
						<?php echo MosetsHTML::_('link', JRoute::_('index.php?option=com_hotproperty&view=types'), JText::_('Manage Types')); ?>
					</dd>
					<dd <?php echo ($view == 'featured' && empty($task)) ? 'class="active"' : ''; ?>>
						<?php echo MosetsHTML::_('link', JRoute::_('index.php?option=com_hotproperty&view=featured'), JText::_('Manage Featured')); ?>						</a>
					</dd>
					<dd class="zebra<?php echo ($view == 'fields' && empty($task)) ? ' active' : ''; ?>">
						<?php echo MosetsHTML::_('link', JRoute::_('index.php?option=com_hotproperty&view=fields'), JText::_('Extra Fields')); ?>						</a>
					</dd>
					<dd class="last<?php echo ($view == 'properties' && $task == 'add') ? ' active' : ''; ?>">
						<?php echo MosetsHTML::_('link', JRoute::_('index.php?option=com_hotproperty&view=properties&task=add&referer=' . $referer), sprintf('+ %s', JText::_('Add Property'))); ?>						</a>
					</dd>
					<?php if ($hotproperty->getCfg('use_companyagent')) : ?>
						<dt><?php echo JText::_('Companies & Agents'); ?></dt>
						<dd <?php echo ($view == 'companies' && empty($task)) ? 'class="active"' : ''; ?>>
							<?php echo MosetsHTML::_('link', JRoute::_('index.php?option=com_hotproperty&view=companies'), JText::_('View Companies')); ?>							</a>
						</dd>
						<dd class="zebra <?php echo ($view == 'companies' && $task == 'add') ? ' active' : ''; ?>">
							<?php echo MosetsHTML::_('link', JRoute::_('index.php?option=com_hotproperty&view=companies&task=add&referer=' . $referer), sprintf('+ %s', JText::_('Add company'))); ?>							</a>
						</dd>
						<dd <?php echo ($view == 'agents' && empty($task)) ? 'class="active"' : ''; ?>>
							<?php echo MosetsHTML::_('link', JRoute::_('index.php?option=com_hotproperty&view=agents'), JText::_('View Agents')); ?>							</a>
						</dd>
						<dd class="zebra last<?php echo ($view == 'agents' && $task == 'add') ? ' active' : ''; ?>">
							<?php echo MosetsHTML::_('link', JRoute::_('index.php?option=com_hotproperty&view=agents&task=add&referer=' . $referer), sprintf('+ %s', JText::_('Add Agent'))); ?>							</a>
						</dd>
					<?php endif; ?>
					<dt><?php echo JText::_('More...'); ?></dt>
					<dd class="zebra<?php echo ($view == 'report') ? ' active' : ''; ?>">
						<?php echo MosetsHTML::_('link', JRoute::_('index.php?option=com_hotproperty&view=report'), JText::_('Generate Report')); ?>
						</a>
					</dd>
					<dd <?php echo ($view == 'log_searches') ? 'class="active"' : ''; ?>>
						<?php echo MosetsHTML::_('link', JRoute::_('index.php?option=com_hotproperty&view=log_searches'), JText::_('Log Searches')); ?>
						</a>
					</dd>
					<dd class="last<?php echo ($view == 'about') ? ' active' : ''; ?>">
						<?php echo MosetsHTML::_('link', JRoute::_('index.php?option=com_hotproperty&view=about'), JText::_('About Hot Property')); ?>
						</a>
					</dd>
				</dl>
				<?php echo MosetsHTML::_('link', 'http://www.mosets.com', JText::_('www.mosets.com'), array('target' => '_blank')); ?>
				<div class="clr"></div>
			</div>
			<div class="b">
				<div class="b">
					<div class="b"></div>
				</div>
			</div>
		</div>
		<?php
	}
}
?>
