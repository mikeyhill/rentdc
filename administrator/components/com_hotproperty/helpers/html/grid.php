<?php
/**
 * @version		$Id: grid.php 957 2010-03-08 03:03:17Z cy $
 * @package		Hotproperty
 * @subpackage	Helper.HTML
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * List HTML Helper
 *
 * @package		Hotproperty
 * @subpackage	Helper.HTML
 * @author		Antoine Bernier <abernier@mosets.com>
 */

require_once(JPath::clean(MOSETS_LIBRARIES . '/mosets/html/html/grid.php'));

class HotpropertyGrid extends MosetsHTMLGrid
{
	/**
	 * A image link to make appear/not appear in listing
	 * 
	 * @access	public
	 * @param	Object	A property db object
	 * @param	int		Index in the grid
	 * @param	string	The publish image
	 * @param	string	The unpublish image
	 * @param	string	The $task prefix
	 */
	function listing($row, $i, $imgY = 'tick.png', $imgX = 'publish_x.png', $imgZ = 'disabled.png', $prefix='')
	{
		if ($row->listing) {
			$img = $imgY;
		} else {
			if ($row->listing < 0) {
				$img = $imgZ;
			} else {
				$img = $imgX;
			}
		}
		$task 	= $row->listing ? 'unlisting' : 'listing';
		$alt 	= $row->listing ? JText::_('Showed in listing') : JText::_('Not showed in listing');
		$action = $row->listing ? JText::_("Don't show in listing") : JText::_('Show in listing');

		$href = '
		<a href="javascript:void(0);" onclick="return listItemTask(\'cb'. $i .'\',\''. $prefix.$task .'\')" title="'. $action .'">
		<img src="images/'. $img .'" border="0" alt="'. $alt .'" /></a>'
		;

		return $href;
	}
	
	/**
	 * A image link to make an item searchable/not searchable
	 * 
	 * @access	public
	 * @param	Object	A property db object
	 * @param	int		Index in the grid
	 * @param	string	The publish image
	 * @param	string	The unpublish image
	 * @param	string	The $task prefix
	 */
	function searchable($row, $i, $imgY = 'tick.png', $imgX = 'publish_x.png', $imgZ = 'disabled.png', $prefix='')
	{
		if ($row->search) {
			$img = $imgY;
		} else {
			if ($row->search < 0) {
				$img = $imgZ;
			} else {
				$img = $imgX;
			}
		}
		$task 	= $row->search ? 'unsearchable' : 'searchable';
		$alt 	= $row->search ? JText::_('Showed in search') : JText::_('Not showed in search');
		$action = $row->search ? JText::_("Don't show in search") : JText::_('Show in search');

		$href = '
		<a href="javascript:void(0);" onclick="return listItemTask(\'cb'. $i .'\',\''. $prefix.$task .'\')" title="'. $action .'">
		<img src="images/'. $img .'" border="0" alt="'. $alt .'" /></a>'
		;

		return $href;
	}
}
?>
