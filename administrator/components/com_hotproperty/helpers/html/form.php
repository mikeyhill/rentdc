<?php
/**
 * @version		$Id: form.php 914 2009-11-11 23:32:02Z abernier $
 * @package		Hotproperty
 * @subpackage	Helper.HTML
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Form HTML Helper
 *
 * @package		Hotproperty
 * @subpackage	Helper.HTML
 * @author		Antoine Bernier <abernier@mosets.com>
 */

require_once(JPath::clean(MOSETS_LIBRARIES . '/mosets/html/html/form.php'));

class HotpropertyForm extends MosetsHTMLForm
{
	/**
	 * Extrafield
	 * 
	 * @access	public
	 * @param	TableFields extrafield object
	 * @param	An object (either a TableProperties or a TableProperties2 one) containing the extrafields values
	 * 
	 * @todo	This should be unified in 1.1 (all extrafield should have a field_type)
	 */
	function extrafield($field, $property = null, $label = false)
	{
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		$html = '';
		
		/**
		 * Extrafield name, value, id
		 */
		
		$fieldId = false;
		
		if ($field->iscore) {
			if ($field->name == 'notes')	$field->name = 'note';	// @todo	#__hp_properties and #__hp_prop_ef 's note field should have the same name : 'notes' or 'note'
				
			if (empty($property)) {
				$property = $hotproperty->getTable('Properties');
				$property->load(0);
			}
			
			$fieldName		= 'Property[' . $property->id . '][' . $field->name . ']';
			$fieldValue		= $property->{$field->name};
			$fieldCaption	= JText::_($field->caption);
		} else {
			
			/**
			 * Search for the property's field
			 */
			
			$propertyField = null;
			
			// In the property's fields (if there already exist one)
			if (!empty($property->PropertyField)) {
				foreach ($property->PropertyField as $pf)
				{
					if ($pf->field == $field->id) {
						$propertyField = $pf;
						break;
					}
				}
			}
			
			// Or if not found create a new property's field
			if (empty($propertyField)) {
				$propertyField = $hotproperty->getTable('PropertiesFields');
				$propertyField->id = $propertyField->_id_new;
				$propertyField->property = $property->id;
			}
			
			$html .= MosetsHTML::_('form.input', 'hidden', 'PropertyField[' . $propertyField->id . '][id]', $propertyField->id);
			$html .= MosetsHTML::_('form.input', 'hidden', 'PropertyField[' . $propertyField->id . '][property]', $propertyField->property);
			$html .= MosetsHTML::_('form.input', 'hidden', 'PropertyField[' . $propertyField->id . '][field]', $field->id);
			
			$fieldName		= 'PropertyField[' . $propertyField->id . '][value]';
			$fieldValue		= $propertyField->value;
			$fieldCaption	= $field->caption;
		}
		
		$fieldValue			= (!is_null($fieldValue) ? htmlspecialchars($fieldValue, ENT_QUOTES, 'UTF-8') : htmlspecialchars($field->default_value, ENT_QUOTES, 'UTF-8'));
		$fieldCaption		= htmlspecialchars($fieldCaption, ENT_QUOTES, 'UTF-8');
		$fieldSize			= $field->size;
		$fieldPrefix		= htmlspecialchars($field->prefix_text, ENT_QUOTES, 'UTF-8');
		$fieldSuffix		= htmlspecialchars($field->append_text, ENT_QUOTES, 'UTF-8');
		$fieldElements		= htmlspecialchars($field->field_elements, ENT_QUOTES, 'UTF-8');
		$fieldType			= $field->field_type;

		/**
		 * Display the extrafield depending of its type
		 */
		
		switch ($fieldType)
		{
			case 'text':
			case 'link':	// What is exactly the purpose of a link compared to a 'text one ?
				$attribs = array();
				if (!empty($fieldSize))
					$attribs['size'] = $fieldSize;
				
				if ($label) {
					$html .= MosetsHTML::_('form.label', $fieldCaption, $fieldName);
				}
				$html .= '<span class="prefix">' . $fieldPrefix . '</span>' . MosetsHTML::_('form.input', 'text', $fieldName, $fieldValue, $fieldId, $attribs) . '<span class="suffix">' . $fieldSuffix . '</span>';
				
				if (preg_match("/com_hotproperty_([a-z]+)/i", $fieldValue, $matches)) {
					JPlugin::loadLanguage( 'com_hotproperty_'.$matches[1] );
					$html .= '<div class="button2-left"><div class="blank">';
					$html .= '<a rel="{handler: \'iframe\', size: {x: 620, y: 680}}" href="';
					$html .= 'index.php?option=com_hotproperty_'.$matches[1].'&amp;task=ext_main&amp;property_id='.$property->id.'&amp;tmpl=component';
					$html .= '" title="'.JText::_('HOTPROPERTY_'.strtoupper($matches[1]).'_NAME').'" class="modal-button" style="padding:0 6px">';
					$html .= JText::_('HOTPROPERTY_'.strtoupper($matches[1]).'_MANAGE_EXTENSION');
					$html .= '</a>';
					$html .= '</div></div>';
				}
				break;
			case 'multitext':
				switch ($field->name)
				{
					case 'intro_text':
					//case 'full_text':
						if (strlen(trim($property->full_text)))
							$fieldValue .= '<hr id="system-readmore" />' . $property->full_text;
						if ($label) {
							$html .= MosetsHTML::_('form.label', $fieldCaption, $fieldName);
						}
						$html .= MosetsHTML::_('form.wysiwyg', $fieldName, $fieldValue);
						break;
					default:						
						if ($label) {
							$html .= MosetsHTML::_('form.label', $fieldCaption, $fieldName);
						}
						$html .= '<span class="prefix">' . $fieldPrefix . '</span>' . MosetsHTML::_('form.textarea', $fieldName, $fieldValue, $fieldId) . '<span class="suffix">' . $fieldSuffix . '</span>';
						break;
				}
				break;
			case 'selectlist':
			case 'selectmultiple':
				$attribs = array();
				if (!empty($fieldSize))
					$attribs['size'] = $fieldSize;
					
				// Get the selectlist options list
				switch ($field->name)
				{
					case 'type':
						if ($label) {
							$html .= MosetsHTML::_('form.label', $fieldCaption, $fieldName);
						}
						$html .= MosetsHTML::_('hotproperty.list.types', $fieldName, $fieldValue, null, 1, $fieldId);
						break;
					case 'agent':
						if ($label) {
							$html .= MosetsHTML::_('form.label', $fieldCaption, $fieldName);
						}
						$html .= MosetsHTML::_('hotproperty.list.agents', $fieldName, $fieldValue, null, 1, $fieldId);
						break;
					case 'company':
						if ($label) {
							$html .= MosetsHTML::_('form.label', $fieldCaption, $fieldName);
						}
						$html .= MosetsHTML::_('hotproperty.list.companies', $fieldName, $fieldValue, null, 1, $fieldId);
						break;
					default:
						$arr = array(MosetsHTML::_('select.option',  '', '- ' . sprintf(JText::_('Select %s'), $fieldCaption) . ' -'));

						foreach (explode('|', $fieldElements) as $field_element)
						{
							$arr[] = MosetsHTML::_('select.option',  $field_element, $field_element);
						}

						// Multiple ?
						if ($fieldType == 'selectmultiple') {
							$attribs['multiple'] = 'multiple';
							$fieldName .= '[]';
						}

						if ($label) {
							$html .= MosetsHTML::_('form.label', $fieldCaption, $fieldName);
						}
						$html .= MosetsHTML::_('select.genericlist',  $arr, $fieldName, $attribs, $key = 'value', $text = 'text', explode('|', $fieldValue), $fieldId);

						break;
				}
				break;
			case 'checkbox':
				$arr = array();
				foreach (explode('|', $fieldElements) as $field_element)
				{
					$arr[] = MosetsHTML::_('select.option',  $field_element, $field_element);
				}
				
				$html .= '<fieldset class="checkbox">';
				if ($label) {
					$html .= '<legend><span>' . $fieldCaption . '</span></legend>';
				}
				$html .= MosetsHTML::_('form.checkboxlist', $arr, $fieldName.'[]', null, 'value', 'text', explode('|', $fieldValue), $fieldId);
				$html .= '</fieldset>';
				break;
			case 'radiobutton':
				$arr = array();
				foreach (explode('|', $fieldElements) as $field_element)
				{
					$arr[] = MosetsHTML::_('select.option',  $field_element, $field_element);
				}
				
				$html .= '<fieldset class="radio">';
				if ($label) {
					$html .= '<legend><span>' . $fieldCaption . '</span></legend>';
				}
				$html .= MosetsHTML::_('form.radiolist', $arr, $fieldName, null, 'value', 'text', $fieldValue, $fieldId);
				$html .= '</fieldset>';
				break;
			// Special cases
			default:
				switch ($field->name)
				{
					case 'price':
					case 'hits':
						if ($label) {
							$html .= MosetsHTML::_('form.label', $fieldCaption, $fieldName);
						}
						$html .= MosetsHTML::_('form.input', 'text', $fieldName, $fieldValue, $fieldId);
						$html .= ' ' . $hotproperty->getCfg('currency');
						break;
					case 'featured':
						if ($label) {
							$html .= MosetsHTML::_('form.label', $fieldCaption, $fieldName);
						}
						$html .= MosetsHTML::_('form.checkbox', $fieldName, '1', $fieldValue, $fieldId);
						break;
					case 'created':
					case 'modified':
						if ($label) {
							$html .= MosetsHTML::_('form.label', $fieldCaption, $fieldName);
						}
						$html .= MosetsHTML::_('calendar', $fieldValue, $fieldName, $fieldId, '%Y-%m-%d %H:%M:%S', array('size' => '25',  'maxlength' => '19'));
						break;
					default:
						break;
				}
				break;
		}
		
		return $html;
	}
	
	/**
	 * Extrafield
	 * 
	 * @access	public
	 * @param	TableFields extrafield object
	 * @param	An object (either a TableProperties or a TableProperties2 one) containing the extrafields values
	 * 
	 * @todo	This should be unified in 1.1 (all extrafield should have a field_type)
	 */
	function extrafield_search($field, $value = null, $label = false)
	{
		$html = '';
		
		/**
		 * Extrafield name, value, id
		 */
		
		if ($field->name == 'notes')	$field->name = 'note';
		
		$fieldId			= false;
		$fieldName			= 'Field[' . $field->name . ']';
		$fieldValue			= $value;
		$fieldCaption		= $field->iscore ? htmlspecialchars(JText::_($field->caption), ENT_QUOTES, 'UTF-8') : htmlspecialchars($field->caption, ENT_QUOTES, 'UTF-8');
		$fieldSize			= $field->size;
		$fieldPrefix		= htmlspecialchars($field->prefix_text, ENT_QUOTES, 'UTF-8');
		$fieldSuffix		= htmlspecialchars($field->append_text, ENT_QUOTES, 'UTF-8');
		$fieldElements		= htmlspecialchars($field->field_elements, ENT_QUOTES, 'UTF-8');
		$fieldType			= $field->field_type;

		/**
		 * Display the extrafield depending of its type
		 */
		
		switch ($fieldType)
		{
			case 'text':
			case 'link':
				$attribs = array();
				if (!empty($fieldSize))
					$attribs['size'] = $fieldSize;
				
				switch ($field->search_type)
				{
					// Less/More than X
					case 'range_1':
						$options = array();
						$options[] = MosetsHTML::_('select.option', 0, JText::_('Less Than'));
						$options[] = MosetsHTML::_('select.option', 1, JText::_('More Than'));
						
						$html .= '<fieldset class="range moreorless">';
						if ($label) {
							$html .= '<legend><span>' . $fieldCaption . '</span></legend>';
						}
						$html .= 	'<ul>';
						$html .= 		'<li>';
						$html .= 			MosetsHTML::_('form.label', 'Type', $fieldName . '[range]');
						$html .= 			MosetsHTML::_('select.genericlist', $options, $fieldName . '[range]', null, 'value', 'text', $fieldValue['range']);
						$html .= 		'</li>';
						$html .= 		'<li>';
						$html .= 			MosetsHTML::_('form.label', 'Value', $fieldName . '[]');
						$html .= 			MosetsHTML::_('form.input', 'text', $fieldName . '[]', $fieldValue[0], $fieldId, $attribs);
						$html .= 		'</li>';
						$html.= 	'</ul>';
						$html .= '</fieldset>';
						break;
					// From - To
					case 'range_2':
						$html .= '<fieldset class="range fromto">';
						if ($label) {
							$html .= '<legend><span>' . $fieldCaption . '</span></legend>';
						}
						$html .= MosetsHTML::_('range.text', $fieldName, $fieldValue['from'], $fieldValue['to']);
						$html .= '</fieldset>';
						break;
					default:
						if ($label) {
							$html .= MosetsHTML::_('form.label', $fieldCaption, $fieldName);
						}
						$html .= MosetsHTML::_('form.input', 'text', $fieldName, $fieldValue, $fieldId, $attribs);
						break;
				}
				break;
			case 'multitext':
				if ($label) {
					$html .= MosetsHTML::_('form.label', $fieldCaption, $fieldName);
				}
				$html .= MosetsHTML::_('form.textarea', $fieldName, $fieldValue, $fieldId);
				break;
			case 'selectlist':
			case 'selectmultiple':
				$attribs = array();
				if (!empty($fieldSize))
					$attribs['size'] = $fieldSize;
					
				// Get the selectlist options list
				switch ($field->name)
				{
					case 'type':
						if ($label) {
							$html .= MosetsHTML::_('form.label', $fieldCaption, $fieldName);
						}
						$html .= MosetsHTML::_('hotproperty.list.types', $fieldName, null, $fieldValue, false, false, true);
						break;
					case 'agent':
						if ($label) {
							$html .= MosetsHTML::_('form.label', $fieldCaption, $fieldName);
						}
						$html .= MosetsHTML::_('hotproperty.list.agents', $fieldName, null, $fieldValue, false, false, true);
						break;
					case 'company':
						if ($label) {
							$html .= MosetsHTML::_('form.label', $fieldCaption, $fieldName);
						}
						$html .= MosetsHTML::_('hotproperty.list.companies', $fieldName, null, $fieldValue, false, false, true);
						break;
					default:
						$arr = array(MosetsHTML::_('select.option',  '', '- ' . sprintf(JText::_('Select %s'), $fieldCaption) . ' -'));

						foreach (explode('|', $fieldElements) as $field_element)
						{
							$arr[] = MosetsHTML::_('select.option',  $field_element, $field_element);
						}

						// Multiple ?
						if ($fieldType == 'selectmultiple') {
							$attribs['multiple'] = 'multiple';
							$fieldName .= '[]';
						}
						$selected = $fieldValue;
						
						if ($label) {
							$html .= MosetsHTML::_('form.label', $fieldCaption, $fieldName);
						}
						$html .= MosetsHTML::_('select.genericlist',  $arr, $fieldName, $attribs, $key = 'value', $text = 'text', $selected, $fieldId);

						break;
				}
				break;
			case 'checkbox':
				$arr = array();
				foreach (explode('|', $fieldElements) as $field_element)
				{
					$arr[] = MosetsHTML::_('select.option',  $field_element, $field_element);
				}
				
				$selected = explode('|', $fieldValue);
				
				$html .= '<fieldset class="checkbox">';
				if ($label) {
					$html .= '<legend><span>' . $fieldCaption . '</span></legend>';
				}
				$html .= MosetsHTML::_('form.checkboxlist', $arr, $fieldName.'[]', null, 'value', 'text', $selected, $fieldId);
				$html .= '</fieldset>';
				break;
			case 'radiobutton':
				$arr = array();
				foreach (explode('|', $fieldElements) as $field_element)
				{
					$arr[] = MosetsHTML::_('select.option',  $field_element, $field_element);
				}
				
				$selected = $fieldValue;
				
				$html .= '<fieldset class="radio">';
				if ($label) {
					$html .= '<legend><span>' . $fieldCaption . '</span></legend>';
				}
				$html .= MosetsHTML::_('form.radiolist', $arr, $fieldName, null, 'value', 'text', $selected, $fieldId);
				$html .= '</fieldset>';
				break;
			// Special cases (no 'field_type' field defined!, @todo should be fixed)
			default:
				switch ($field->name)
				{
					case 'price':
					case 'hits':
						switch ($field->search_type)
						{
							// Less/More than X
							case 'range_1':
								$options = array();
								$options[] = MosetsHTML::_('select.option', 0, JText::_('Less Than'));
								$options[] = MosetsHTML::_('select.option', 1, JText::_('More Than'));

								$html .= '<fieldset class="range moreorless">';
								if ($label) {
									$html .= '<legend><span>' . $fieldCaption . '</span></legend>';
								}
								$html .= 	'<ul>';
								$html .= 		'<li>';
								$html .= 			MosetsHTML::_('form.label', 'Type', $fieldName . '[range]');
								$html .= 			MosetsHTML::_('select.genericlist', $options, $fieldName . '[range]', null, 'value', 'text', $fieldValue['range']);
								$html .= 		'</li>';
								$html .= 		'<li>';
								$html .= 			MosetsHTML::_('form.label', 'Value', $fieldName . '[]');
								$html .= 			MosetsHTML::_('form.input', 'text', $fieldName . '[]', $fieldValue[0], $fieldId);
								$html .= 		'</li>';
								$html.= 	'</ul>';
								$html .= '</fieldset>';
								break;
							// From - To
							case 'range_2':
								$html .= '<fieldset class="range fromto">';
								if ($label) {
									$html .= '<legend><span>' . $fieldCaption . '</span></legend>';
								}
								$html .= MosetsHTML::_('range.text', $fieldName, $fieldValue['from'], $fieldValue['to']);
								$html .= '</fieldset>';
								break;
							default:
								if ($label) {
									$html .= MosetsHTML::_('form.label', $fieldCaption, $fieldName);
								}
								$html .= MosetsHTML::_('form.input', 'text', $fieldName, $fieldValue, $fieldId);
								break;
						}
						break;
					case 'featured':
						if ($label) {
							$html .= MosetsHTML::_('form.label', $fieldCaption, $fieldName);
						}
						$html .= MosetsHTML::_('form.checkbox', $fieldName, '1', $fieldValue, $fieldId);
						break;
					case 'created':
					case 'modified':
						switch ($field->search_type)
						{
							// Less/More than X
							case 'range_1':
								$options = array();
								$options[] = MosetsHTML::_('select.option', 0, JText::_('Less Than'));
								$options[] = MosetsHTML::_('select.option', 1, JText::_('More Than'));

								$html .= '<fieldset class="range moreorless">';
								if ($label) {
									$html .= '<legend><span>' . $fieldCaption . '</span></legend>';
								}
								$html .= '<ul>';
								$html .= 	'<li>';
								$html .= 		MosetsHTML::_('form.label', 'Type', $fieldName . '[range]');
								$html .= 		MosetsHTML::_('select.genericlist', $options, $fieldName . '[range]', null, 'value', 'text', $fieldValue['range']);
								$html .= 	'</li>';
								$html .= 	'<li>';
								$html .= 		MosetsHTML::_('form.label', 'Value', $fieldName . '[]');
								$html .= 		MosetsHTML::_('calendar', $fieldValue[0], $fieldName . '[]', $fieldId, '%Y-%m-%d %H:%M:%S', array('size' => '25',  'maxlength' => '19'));
								$html .= 	'</li>';
								$html.= '</ul>';
								break;
							// From - To
							case 'range_2':
								$html .= '<fieldset class="range fromto">';
								if ($label) {
									$html .= '<legend><span>' . $fieldCaption . '</span></legend>';
								}
								$html .= MosetsHTML::_('range.date', $fieldName, $fieldValue['from'], $fieldValue['to']);
								$html .= '</fieldset>';
								break;
							default:
								if ($label) {
									$html .= MosetsHTML::_('form.label', $fieldCaption, $fieldName);
								}
								$html .= MosetsHTML::_('calendar', $fieldValue, $fieldName, $fieldId, '%Y-%m-%d %H:%M:%S', array('size' => '25',  'maxlength' => '19'));
								break;
						}
						break;
					default:
						break;
				}
				break;
		}
		
		return $html;
	}
}
?>
