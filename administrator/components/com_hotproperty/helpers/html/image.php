<?php
/**
 * @version		$Id: image.php 768 2009-07-20 16:08:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Helper.HTML
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Image HTML Helper
 *
 * @package		Hotproperty
 * @subpackage	Helper.HTML
 * @author		Antoine Bernier <abernier@mosets.com>
 */

require_once(JPath::clean(MOSETS_LIBRARIES . '/mosets/html/html/image.php'));

class HotpropertyImage extends MosetsHTMLImage
{
	/**
	 * Display a photo
	 * 
	 * @access	public
	 * @param	string	The image type : thumbnail or standard or original or agent or company
	 * @param	string	The image name
	 * @param	string	The alt attribute
	 * @param	array	Associative array of additional attributes
	 * 
	 * @todo	Add noimage for original + agent + company ?
	 */
	function photo($type, $image_name, $alt = null, $attribs = null)
	{
		if (empty($alt)) {
			$alt = $image_name;
		}
		
		$img_path = null;
		$noimage = null;
		switch ($type)
		{
			case 'thumb':
			case 'thumbnail':
				$img_path	= MosetsApplication::getPath('media_images_thumbnail', 'hotproperty');
				$img_url	= MosetsApplication::getPath('media_images_thumbnail_url', 'hotproperty');
				$noimage	= MosetsApplication::getPath('media_images_thumbnail_noimage_url', 'hotproperty');
				break;
			case 'std':
			case 'standard':
				$img_path	= MosetsApplication::getPath('media_images_standard', 'hotproperty');
				$img_url	= MosetsApplication::getPath('media_images_standard_url', 'hotproperty');
				$noimage	= MosetsApplication::getPath('media_images_standard_noimage_url', 'hotproperty');
				break;
			case 'ori':
			case 'original':
				$img_path	= MosetsApplication::getPath('media_images_original', 'hotproperty');
				$img_url	= MosetsApplication::getPath('media_images_original_url', 'hotproperty');
				$noimage	= MosetsApplication::getPath('media_images_original_noimage_url', 'hotproperty');
				break;
			case 'agent':
				$img_path	= MosetsApplication::getPath('media_images_agent', 'hotproperty');
				$img_url	= MosetsApplication::getPath('media_images_agent_url', 'hotproperty');
				$noimage	= MosetsApplication::getPath('media_images_agent_noimage_url', 'hotproperty');
				break;
			case 'company':
				$img_path	= MosetsApplication::getPath('media_images_company', 'hotproperty');
				$img_url	= MosetsApplication::getPath('media_images_company_url', 'hotproperty');
				$noimage	= MosetsApplication::getPath('media_images_company_noimage_url', 'hotproperty');
				break;
			default:
				break;
		}
		
		$image_url		= $img_url . $image_name;
		$noimage_url	= $noimage;
		
		jimport('joomla.filesystem.file');
		$image_url = (JFile::exists(JPath::clean($img_path . DS . $image_name)) ? $image_url : $noimage_url);
		
		return MosetsHTML::_('image', $image_url, $alt, $attribs);
	}
}
?>
