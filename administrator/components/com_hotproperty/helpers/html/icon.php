<?php
/**
 * @version		$Id: icon.php 945 2010-02-01 10:39:52Z CY $
 * @package		Hotproperty
 * @subpackage	Helper.HTML
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Icon HTML Helper
 *
 * @package		Hotproperty
 * @subpackage	Helper.HTML
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyIcon
{
	function edit($type, $row, $attribs = array())
	{
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
	
		$user =& JFactory::getUser();
		$uri =& JURI::getInstance();
	
		$html = '';
		$overlib = '';
		$icon = 'edit.png';

		switch ($type)
		{
			case 'property':
				$agent = $row->Agent->user;
				$url = MosetsRoute::getLink('hotproperty', array('view' => 'properties', 'layout' => 'form', 'id' => $row->id, 'referer' => base64_encode(JRequest::getVar('REQUEST_URI', null, 'server', 'string'))));
				if ($row->published == 0) {
					$overlib = JText::_('Unpublished');
					$icon = 'edit_unpublished.png';
				} else {
					$overlib = JText::_('Published');
				}
				$date = MosetsHTML::_('date', $row->created);
				$author = $row->Agent->name;

				$overlib .= '&lt;br /&gt;';
				$overlib .= $date;
				$overlib .= '&lt;br /&gt;';
				$overlib .= $author;
				break;
			case 'agent':
				$agent = $row->user;
				$url = MosetsRoute::getLink('hotproperty', array('view' => 'agents', 'layout' => 'form', 'id' => $row->id, 'referer' => base64_encode(JRequest::getVar('REQUEST_URI', null, 'server', 'string'))));
				break;
		}

		if ($agent == $user->get('id') && $user->get('gid') >= $hotproperty->getCfg('agent_groupid')) {
			MosetsHTML::_('behavior.tooltip');
			$text = MosetsHTML::_('image.site', $icon, '/images/M_images/', null, null, JText::_('Edit'));

			$button = MosetsHTML::_('link', JRoute::_($url), $text);

			$html = '<span class="hasTip" title="' . JText::_('Edit Item') . ' :: ' . $overlib . '">' . $button . '</span>';
		}

		return $html;
	}
	
	function pdf($property, $attribs = array())
	{
		$url = MosetsRoute::getLink('hotproperty', array('view' => 'properties', 'layout' => 'property', 'id' => $property->id, 'format' => 'pdf'));

		$text = MosetsHTML::_('image.site', 'pdf_button.png', '/images/M_images/', null, null, JText::_('PDF'));

		$attribs['title']	= JText::_('PDF');
		//$attribs['onclick'] = "window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;";
		//$attribs['rel']     = 'nofollow';
		$attribs['rel']		= 'alternate';

		return MosetsHTML::_('link', JRoute::_($url), $text, $attribs);
	}
}
?>