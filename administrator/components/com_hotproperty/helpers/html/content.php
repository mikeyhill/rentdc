<?php
/**
 * @version		$Id: content.php 945 2010-02-01 10:39:52Z CY $
 * @package		Hotproperty
 * @subpackage	Helper
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Content HTML Helper
 * 
 * To call a method from this helper : JTHML:_('hotproperty.content', $args)
 *
 * @package		Hotproperty
 * @subpackage	Helper
 * @author		Antoine Bernier <abernier@mosets.com>
 */

require_once(JPath::clean(MOSETS_LIBRARIES . '/mosets/html/html/content.php'));

class HotpropertyContent extends MosetsHTMLContent
{
	/**
	 * 
	 * 
	 * @param	string	The link title
	 * @param	string	The order field for the column
	 * @param	string	The current direction
	 * @param	string	The selected ordering
	 * @param	string	An optional task override
	 */
	function sort($title, $ordering, $direction = 'asc', $selected = 0)
	{
		return parent::sort('hotproperty', $title, $ordering, $direction, $selected);
	}
	
	/**
	 * Extrafield
	 * 
	 * @access	public
	 * @param	HotpropertyTableFields object
	 * @param	HotpropertyTableProperties object
	 * @param	HotpropertyTableProperties2 object
	 * 
	 * @todo	This should be unified in 1.1 (all extrafield should have a field_type)
	 */
	function extrafield($field, $property)
	{
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		$html = '';
		
		/**
		 * Extrafield's value
		 */
		
		$fieldValue = null;
		
		// Either from $property if iscore
		if ($field->iscore) {
			// Notes are private
			if ($field->name == 'notes') {
				return null;
			}
			
			switch ($field->name)
			{
				case 'company':
					$fieldValue = @$property->Agent->Company->name;
					break;
				case 'agent':
					$fieldValue = @$property->Agent->name;
					break;
				case 'type':
					$fieldValue = @$property->Type->name;
					break;
				default:
					$fieldValue = @$property->{$field->name};
					break;
			}
		// Or from $property->PropertyField otherwise
		} else {
			$propertyField = null;
			if (!empty($property->PropertyField)) {
				foreach ($property->PropertyField as $pf)
				{
					if ($pf->field == $field->id) {
						$propertyField = $pf;
						break;
					}
				}
			}
			
			$fieldValue = @$propertyField->value;
		}
		
		// Set default value if empty
		if (is_null($fieldValue)) {
			$fieldValue = @$field->default_value;
		}
		
		$fieldName		= $field->name;
		if ($fieldName != 'intro_text') {
			$fieldValue = htmlspecialchars($fieldValue, ENT_QUOTES, 'UTF-8');
		}
		$fieldType		= $field->field_type;
		$fieldPrefix	= htmlspecialchars($field->prefix_text, ENT_QUOTES, 'UTF-8');
		$fieldSuffix	= htmlspecialchars($field->append_text, ENT_QUOTES, 'UTF-8');
		
		
		/**
		 * Render the extrafield depending of its type
		 */
		
		if (!empty($fieldValue)) {
			switch ($fieldType)
			{
				case 'text':
				case 'multitext':
					switch ($fieldName)
					{
						case 'intro_text':
							if (strlen(trim($property->full_text)) && JRequest::getCmd('view') == 'properties')
								$fieldValue .= $property->full_text;
						default:
							break;
					}
					$html .= ($fieldPrefix ? '<span class="prefix">' . $fieldPrefix . '</span>' : '') . $fieldValue . ($fieldSuffix ? '<span class="suffix">' . $fieldSuffix . '</span>' : '');
					if ($fieldType == 'multitext') {
						$html = '<p>' . $html . '</p>';
					}
					break;
				case 'link':
					$parts		= explode('|', $fieldValue);
					if (count($parts) > 1) {
						$linkUrl = trim($parts[1]);
						$linkText = $parts[0];
					} else {
						$linkUrl = trim($parts[0]);
						$linkText = $linkUrl;
					}
					
					// Look for index.php
					if( substr($linkUrl,0,9) == 'index.php' ) {
						$menus				=& JApplication::getMenu('site', array());
						$component			=& JComponentHelper::getComponent('com_hotproperty');
						$componentMenuItems	= $menus->getItems('componentid', $component->id);
						
						$linkUrl = str_replace( '{property_id}', $property->id, $linkUrl );
						$linkUrl = str_replace( '{type_id}', $property->type, $linkUrl );
						$linkUrl = str_replace( '{agent_id}', $property->agent, $linkUrl );
						$linkUrl = str_replace( '{Itemid}', $componentMenuItems[0]->id, $linkUrl );
						$linkUrl = JRoute::_($linkUrl);
						$html .= ($fieldPrefix ? '<span class="prefix">' . $fieldPrefix . '</span>' : '') . '<a href="' . $linkUrl . '">' . $linkText . '</a>' . ($fieldSuffix ? '<span class="suffix">' . $fieldSuffix . '</span>' : '');
					}

					// Make sure the link begins with http(s)?://
					elseif (!preg_match('/^http(s)?:\/\//', $linkUrl)) {
						$linkUrl = 'http://' . $linkUrl;
						$html .= ($fieldPrefix ? '<span class="prefix">' . $fieldPrefix . '</span>' : '') . '<a href="' . $linkUrl . '" target="' . ($hotproperty->getCfg('link_open_newwin') ? '_blank' : '_self') . '">' . $linkText . '</a>' . ($fieldSuffix ? '<span class="suffix">' . $fieldSuffix . '</span>' : '');
					}
					
					else {
						$html .= ($fieldPrefix ? '<span class="prefix">' . $fieldPrefix . '</span>' : '') . '<a href="' . $linkUrl . '" target="' . ($hotproperty->getCfg('link_open_newwin') ? '_blank' : '_self') . '">' . $linkText . '</a>' . ($fieldSuffix ? '<span class="suffix">' . $fieldSuffix . '</span>' : '');
					}
					
					break;
				case 'selectlist':
				case 'radiobutton':
					switch ($fieldName)
					{
						// Core 'type' extrafield
						case 'type':
							$html .= '<a href="' . JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'types', 'layout' => 'properties', 'id' => $property->type))) . '">' . $fieldValue . '</a>';
							break;
						// Core 'agent' extrafield
						case 'agent':
							$html .= '<a href="' . JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'agents', 'layout' => 'properties', 'id' => $property->agent))) . '">' . $fieldValue . '</a>';
							break;
						// Core 'company' extrafield
						case 'company':
							$html .= '<a href="' . JRoute::_(MosetsRoute::getLink('hotproperty', array('view' => 'companies', 'layout' => 'agents', 'id' => $property->Agent->Company->id))) . '">' . $fieldValue . '</a>';
							break;
						// Otherwise
						default:
							$html .= $fieldValue;
							break;
					}
					break;
				case 'selectmultiple':
				case 'checkbox':
					$html .= '<ul>';
					foreach (explode('|', $fieldValue) as $value)
						$html .= '<li>' . $value . '</li>';
					$html .= '</ul>';
					break;
				// Special cases (no field_type)
				default:
					switch ($fieldName)
					{
						case 'price':
							$html .= ($fieldPrefix ? '<span class="prefix">' . $fieldPrefix . '</span>' : '') . '<span class="currency">' . $hotproperty->getCfg('currency') . '</span> ' . number_format($fieldValue, $hotproperty->getCfg('dec_point'), $hotproperty->getCfg('dec_string'), ($hotproperty->getCfg('thousand_sep')) ? $hotproperty->getCfg('thousand_string') : '') . ($fieldSuffix ? '<span class="suffix">' . $fieldSuffix . '</span>' : '');
							break;
						/*case 'featured':
							$html .= ;
							break;*/
						case 'created':
						case 'modified':
							$html .= MosetsHTML::_('date',  $fieldValue,  JText::_('DATE_FORMAT_LC2'));
							break;
						default:
							$html .= $fieldValue;
							break;
					}
					break;
			}
		}
		
		return $html;
	}
	
	function loadTheme()
	{
		global $mainframe;
		
		static $loaded = false;
		
		if (!$loaded) {
			$hotproperty	=& MosetsApplication::getInstance('hotproperty');
			$document		=& JFactory::getDocument();

			$theme = $hotproperty->getCfg('css');
			$template = $mainframe->getTemplate();

			if ($theme) {
				$themePath	= JPath::clean(MosetsApplication::getPath('media_css', 'hotproperty') . '/' . $theme);
				$themeUrl	= MosetsApplication::getPath('media_css_url', 'hotproperty') . $theme . '/';

				// Load theme stylesheet
				if (JFile::exists(JPath::clean($themePath . '/styles.css'))) {
					$document->addStyleSheet($themeUrl . 'styles.css');
				}

				/**
				 * Load theme conditional comments for IE
				 */

				if (JFolder::exists($themeIePath = JPath::clean($themePath . '/ie'))) {
					$themeIeUrl = $themeUrl . 'ie/';
					foreach(JFolder::files($themeIePath, $filter = '(all|[[:digit:]]+).css') as $themeIeStylesheet)
					{
						switch ($themeIeStylesheet)
						{
							case 'all.css':
								$document->addCustomTag('<!--[if IE]><link href="' . $themeIeUrl . $themeIeStylesheet . '" rel="stylesheet" type="text/css" /><![endif]-->');
								break;
							default:
								$ieVersion = substr($themeIeStylesheet, 0, -4);
								$document->addCustomTag('<!--[if lte IE ' . $ieVersion . ']><link href="' . $themeIeUrl . $themeIeStylesheet . '" rel="stylesheet" type="text/css" /><![endif]-->');
								break;
						}
					}
				}

				/**
				 * Load theme anti-templates stylesheet
				 */

				if (JFolder::exists($themeAntitemplatePath = JPath::clean($themePath . '/templates/' . $template))) {
					$themeAntitemplateUrl = $themeUrl . 'templates/' . $template . '/';
					// Load theme anti-template stylesheet
					if (JFile::exists(JPath::clean($themeAntitemplatePath . '/styles.css'))) {
						$document->addStyleSheet($themeAntitemplateUrl . 'styles.css');
					}

					/**
					 * Load theme anti-template conditional comments for IE
					 */

					if (JFolder::exists($themeAntitemplateIePath = JPath::clean($themeAntitemplatePath . '/ie'))) {
						$themeAntitemplateIeUrl = $themeAntitemplateUrl . 'ie/';
						foreach(JFolder::files($themeAntitemplateIePath, $filter = '(all|[[:digit:]]+).css') as $themeAntitemplateIeStylesheet)
						{
							switch ($themeAntitemplateIeStylesheet)
							{
								case 'all.css':
									$document->addCustomTag('<!--[if IE]><link href="' . $themeAntitemplateIeUrl . $themeAntitemplateIeStylesheet . '" rel="stylesheet" type="text/css" /><![endif]-->');
									break;
								default:
									$ieVersion = substr($themeAntitemplateIeStylesheet, 0, -4);
									$document->addCustomTag('<!--[if lte IE ' . $ieVersion . ']><link href="' . $themeAntitemplateIeUrl . $themeAntitemplateIeStylesheet . '" rel="stylesheet" type="text/css" /><![endif]-->');
									break;
							}
						}
					}
				}
				
				$loaded = true;
			}
		}
		
		return $loaded;
	}
}