<?php
/**
 * @version		$Id: application.php 768 2009-07-20 16:08:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Includes
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * 
 *
 * @package		Hotproperty
 * @subpackage	Includes
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */
class MosetsApplicationHotproperty extends MosetsApplication
{
	/**
	 * Constructor
	 *
	 * @access	protected
	 * @param	An optional associative array of configuration settings.
	 * 			Recognized key values include 'name', 'client_id' (this list is not meant to be comprehensive).
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'hotproperty',
			'table_prefix' => 'hp_',
			'default_views' => array(
				'site' => 'home',
				'admin' => 'properties'
			),
			'paths' => array(
				'media_images_connector'		   		=> '_',
				                                   
				'media_images_company'			   		=> JPath::clean(JPATH_ROOT . '/media/com_' . $this->getName() . '/images/company'),
				'media_images_company_url'		   		=> JURI::root() . 'media/com_' . $this->getName() . '/images/company/',
				'media_images_company_noimage'	   		=> JPath::clean(JPATH_ROOT . '/media/com_' . $this->getName() . '/images/noimage_company.png'),
				'media_images_company_noimage_url' 		=> JURI::root() . 'media/com_' . $this->getName() . '/images/noimage_company.png',
				                                   
				'media_images_agent'			   		=> JPath::clean(JPATH_ROOT . '/media/com_' . $this->getName() . '/images/agent'),
				'media_images_agent_url'		   		=> JURI::root() . 'media/com_' . $this->getName() . '/images/agent/',
				'media_images_agent_noimage'	   		=> JPath::clean(JPATH_ROOT . '/media/com_' . $this->getName() . '/images/noimage_agent.png'),
				'media_images_agent_noimage_url'   		=> JURI::root() . 'media/com_' . $this->getName() . '/images/noimage_agent.png',
				                                   
				'media_images_original'			   		=> JPath::clean(JPATH_ROOT . '/media/com_' . $this->getName() . '/images/ori'),
				'media_images_original_url'		   		=> JURI::root() . 'media/com_' . $this->getName() . '/images/ori/',
				                                   
				'media_images_standard'			   		=> JPath::clean(JPATH_ROOT . '/media/com_' . $this->getName() . '/images/std'),
				'media_images_standard_url'		   		=> JURI::root() . 'media/com_' . $this->getName() . '/images/std/',
				'media_images_standard_noimage'			=> JPath::clean(JPATH_ROOT . '/media/com_' . $this->getName() . '/images/noimage_std.png'),
				'media_images_standard_noimage_url'		=> JURI::root() . 'media/com_' . $this->getName() . '/images/noimage_std.png',
				
				'media_images_thumbnail'				=> JPath::clean(JPATH_ROOT . '/media/com_' . $this->getName() . '/images/thb'),
				'media_images_thumbnail_url'			=> JURI::root() . 'media/com_' . $this->getName() . '/images/thb/',
				'media_images_thumbnail_noimage'		=> JPath::clean(JPATH_ROOT . '/media/com_' . $this->getName() . '/images/noimage_thb.png'),
				'media_images_thumbnail_noimage_url'	=> JURI::root() . 'media/com_' . $this->getName() . '/images/noimage_thb.png'
			)
		), $config));
	}
}
?>