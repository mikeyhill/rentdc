<?php
/**
 * @version		$Id: admin.hotproperty.php 913 2009-11-10 22:14:48Z abernier $
 * @package		Hotproperty
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

global $mainframe;

jimport('joomla.filesystem.file');

// Import Mosets Framework
if (JPluginHelper::isEnabled('mosets', 'framework')) {
	JPluginHelper::importPlugin('mosets', 'framework');
} else {
	JError::raiseError(404, 'Mosets Framework plugin is required for this component. Please install and enable it.');
}
$mainframe->triggerEvent('onInitializeMosetsFramework');

/* ===TESTS=== */

/*// Testing MosetsHTML
mimport('joomla.html.html');
// Testing a not overloaded method
echo JoomlaHTML::_('email.cloak', 'antoine.bernier@gmail.com');
mimport('mosets.html.html');
echo MosetsHTML::_('email.cloak', 'antoine.bernier@gmail.com');*/

//$hotproperty =& MosetsFactory::getApplication('hotproperty');
/*echo "<pre>";
echo "application=";
print_r($hotproperty);
echo "</pre>";*/
//$controller = $hotproperty->getController('Properties');
/*echo "<pre>";
echo "controller=";
print_r($controller);
echo "</pre>";*/
//$controller_model = $controller->getModel();
/*echo "<pre>";
echo "controller_model=";
print_r($controller_model);
echo "</pre>";*/

/*$db			=& MosetsFactory::getDBO();
$nullDate	= $db->getNullDate(); 
$now		= JFactory::getDate();*/

/*mimport('mosets.database.query');
$query = new MosetsQuery(array(
	'select' => array('Property.*', 'Agent.name', 'Company.name'),
	'from' => array('jos_hp_properties' => 'Property'),
	'join' => array(
		array(
			'type' => 'left',
			'table' => 'jos_hp_agents',
			'alias' => 'Agent',
			'condition' => 'Agent.id=Property.agent'
		)
	),
	'where' => array(
		'Property.id' => array(1, 2, 3),
		'Property.hits BETWEEN' => array(1, 2),
		'Property.approved' => 1,
		'Property.published' => 1,
		array(
			'OR' => array(
				'Property.publish_up' => '0000-00-00 00:00:00',
				'Property.publish_up <=' => '2009-07-28 18:20:00'
			)
		),
		array(
			'OR' => array(
				'Property.publish_down' => '0000-00-00 00:00:00',
				'Property.publish_down >=' => '2009-07-28 18:20:00'
			)
		)
	),
	'group_by' => array(
		'Property.id'
	),
	'order_by' => array(
		'Agent.name' => 'ASC'
	)
));
$query->select(array('Property.name'));
$query->join(array(
	array(
		'type' => 'left',
		'table' => 'jos_hp_companies',
		'alias' => 'Company',
		'condition' => 'Company.id = Agent.company'
	)
));
echo "<pre>";
echo "query=";
print_r($query);
echo "</pre>";

echo "<pre>";
"query rendering=";
print_r($query->render());
echo "</pre>";*/

/*$data = $controller_model->getData('all', array(
	//'fields' => array('Property.id', 'Property.name', 'Agent.id', 'Agent.name'),
	'where' => array(
		'Property.approved' => 1,
		'Property.published' => 1,
		array(
			'OR' => array(
				'Property.publish_up' => $nullDate,
				'Property.publish_up <=' => $now->toMySQL()
			)
		),
		array(
			'OR' => array(
				'Property.publish_down' => $nullDate,
				'Property.publish_down >=' => $now->toMySQL()
			)
		)
	),
	'recursive' => 3,
	'contain' => array(
		'Agent' => array(
			'fields' => array('Agent.name'),
			'contain' => array(
				'User' => array(),
				'Company' => array(
					'fields' => array('Company.name')
				)
			)
		),
		'Featured' => array(),
		//'Photos' => array()
	),
	'order' => array('Agent.name' => 'ASC'),
	'limitstart' => 0,
	//'limit' => 2
));
echo "<pre>";
echo "data=";
print_r($data);
echo "</pre>";*/

/* ===END OF TESTS=== */

$hotproperty =& MosetsFactory::getApplication('hotproperty');

$controller =& $hotproperty->getController();
/*echo "<pre>";
echo "controller=";
print_r($controller);
echo "</pre>";*/
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
?>
