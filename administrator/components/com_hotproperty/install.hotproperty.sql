CREATE TABLE IF NOT EXISTS `#__hp_agents` (
  `id` int(11) NOT NULL auto_increment,
  `user` int(10) unsigned NOT NULL default '0',
  `name` varchar(100) NOT NULL default '',
  `mobile` varchar(60) NOT NULL default '',
  `email` varchar(255) NOT NULL default '',
  `photo` varchar(100) NOT NULL default '',
  `company` int(11) NOT NULL default '0',
  `desc` text NOT NULL,
  `need_approval` tinyint(3) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM ;

CREATE TABLE IF NOT EXISTS `#__hp_companies` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL default '',
  `address` varchar(200) NOT NULL default '',
  `suburb` varchar(100) NOT NULL default '',
  `state` varchar(50) NOT NULL default '',
  `country` varchar(50) NOT NULL default '',
  `postcode` varchar(50) NOT NULL default '',
  `telephone` varchar(50) default NULL,
  `fax` varchar(50) default NULL,
  `email` varchar(255) NOT NULL default '',
  `website` varchar(255) default NULL,
  `photo` varchar(50) default NULL,
  `desc` text,
  PRIMARY KEY  (`id`)
) TYPE=MyISAM ;

CREATE TABLE IF NOT EXISTS `#__hp_featured` (
  `property` int(11) NOT NULL default '0',
  `ordering` int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`property`)
) TYPE=MyISAM;

CREATE TABLE IF NOT EXISTS `#__hp_log_searches` (
  `search_term` varchar(128) NOT NULL default '',
  `hits` int(10) unsigned NOT NULL default '0'
) TYPE=MyISAM;


CREATE TABLE IF NOT EXISTS `#__hp_photos` (
  `id` int(11) NOT NULL auto_increment,
  `property` int(11) NOT NULL default '0',
  `original` varchar(100) default NULL,
  `standard` varchar(100) NOT NULL default '',
  `thumb` varchar(100) NOT NULL default '',
  `title` varchar(255) NOT NULL default '',
  `desc` text NOT NULL,
  `ordering` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM ;


CREATE TABLE IF NOT EXISTS `#__hp_properties` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(200) NOT NULL default '',
  `agent` int(11) NOT NULL default '0',
  `address` varchar(200) NOT NULL default '',
  `suburb` varchar(100) NOT NULL default '',
  `state` varchar(50) NOT NULL default '',
  `country` varchar(50) NOT NULL default '',
  `postcode` varchar(50) NOT NULL default '',
  `price` decimal(15,2) NOT NULL default '0.00',
  `note` text,
  `intro_text` text,
  `full_text` text,
  `type` int(11) NOT NULL default '0',
  `featured` tinyint(3) unsigned NOT NULL default '0',
  `published` tinyint(3) NOT NULL default '0',
  `publish_up` datetime NOT NULL default '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL default '0000-00-00 00:00:00',
  `created` datetime NOT NULL default '0000-00-00 00:00:00',
  `modified` datetime NOT NULL default '0000-00-00 00:00:00',
  `approved` tinyint(3) unsigned NOT NULL default '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `hits` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM ;


CREATE TABLE IF NOT EXISTS `#__hp_properties2` (
  `id` int(11) NOT NULL auto_increment,
  `property` int(11) NOT NULL default '0',
  `field` varchar(100) NOT NULL default '',
  `value` mediumtext NOT NULL,
  PRIMARY KEY  (`id`)
) TYPE=MyISAM ;


CREATE TABLE IF NOT EXISTS `#__hp_prop_ef` (
  `id` int(11) NOT NULL auto_increment,
  `field_type` varchar(50) NOT NULL default '',
  `name` varchar(100) NOT NULL default '',
  `caption` varchar(255) NOT NULL default '',
  `default_value` varchar(255) default NULL,
  `size` int(11) NOT NULL default '0',
  `field_elements` text,
  `prefix_text` varchar(255) NOT NULL default '',
  `append_text` varchar(255) NOT NULL default '',
  `ordering` int(11) NOT NULL default '0',
  `hidden` tinyint(3) unsigned NOT NULL default '0',
  `published` tinyint(4) NOT NULL default '0',
  `featured` tinyint(3) unsigned NOT NULL default '0',
  `listing` tinyint(3) unsigned NOT NULL default '0',
  `hideCaption` smallint(6) unsigned NOT NULL default '0',
  `iscore` tinyint(3) unsigned NOT NULL default '0',
  `search` tinyint(4) NOT NULL default '0',
  `search_caption` varchar(255) NOT NULL,
  `search_type` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) TYPE=MyISAM ;


CREATE TABLE IF NOT EXISTS `#__hp_prop_types` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL default '',
  `desc` text,
  `published` tinyint(3) unsigned NOT NULL default '0',
  `ordering` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM ;

REPLACE INTO `#__hp_prop_ef` VALUES (1, 'text', 'name', 'Name', '', 0, '', '', '', 1, 0, 1, 1, 1, 0, 1, 1, 'Name', '');
REPLACE INTO `#__hp_prop_ef` VALUES (2, 'selectlist', 'agent', 'Agent Name', '', 0, '', '', '', 4, 0, 1, 1, 1, 0, 1, 0, 'Agent', '');
REPLACE INTO `#__hp_prop_ef` VALUES (3, 'text', 'address', 'Address', '', 0, NULL, '', '', 5, 0, 1, 0, 0, 1, 1, 0, 'Address', '');
REPLACE INTO `#__hp_prop_ef` VALUES (4, 'text', 'suburb', 'Suburb', NULL, 0, NULL, '', '', 6, 0, 1, 0, 0, 0, 1, 0, 'Suburb', '');
REPLACE INTO `#__hp_prop_ef` VALUES (5, 'text', 'state', 'State', NULL, 0, NULL, '', '', 7, 0, 1, 1, 1, 0, 1, 0, 'State', '');
REPLACE INTO `#__hp_prop_ef` VALUES (6, 'text', 'country', 'Country', NULL, 0, NULL, '', '', 9, 0, 1, 0, 0, 0, 1, 0, 'Country', '');
REPLACE INTO `#__hp_prop_ef` VALUES (7, 'text', 'postcode', 'Postcode', NULL, 0, NULL, '', '', 8, 0, 1, 0, 0, 0, 1, 0, 'Postcode', '');
REPLACE INTO `#__hp_prop_ef` VALUES (8, '', 'price', 'Price', '0', 0, NULL, '', '', 10, 0, 1, 1, 1, 0, 1, 0, 'Price', 'range_2');
REPLACE INTO `#__hp_prop_ef` VALUES (9, 'multitext', 'intro_text', 'Description', '', 0, '', '', '', 12, 0, 1, 0, 0, 1, 1, 0, 'Description Text', '');
REPLACE INTO `#__hp_prop_ef` VALUES (10, 'multitext', 'full_text', 'Full Text', '', 0, NULL, '', '', 13, 0, 1, 0, 0, 1, 1, 0, 'Full Text', '');
REPLACE INTO `#__hp_prop_ef` VALUES (11, 'selectlist', 'type', 'Type', '', 0, '', '', '', 3, 0, 1, 0, 1, 0, 1, 0, 'Property Type', '');
REPLACE INTO `#__hp_prop_ef` VALUES (12, '', 'featured', 'Featured', '0', 0, NULL, '', '', 11, 0, 1, 0, 0, 0, 1, 0, 'Featured', '');
REPLACE INTO `#__hp_prop_ef` VALUES (13, '', 'created', 'Created', NULL, 0, NULL, '', '', 16, 0, 0, 0, 0, 0, 1, 0, 'Date Created', 'range_3');
REPLACE INTO `#__hp_prop_ef` VALUES (14, '', 'modified', 'Modified', NULL, 0, NULL, '', '', 14, 0, 0, 0, 0, 0, 1, 0, 'Date Modified', 'range_3');
REPLACE INTO `#__hp_prop_ef` VALUES (15, '', 'hits', 'Hits', '0', 0, NULL, '', '', 15, 0, 0, 0, 1, 0, 1, 0, 'Hits', 'range_1');
REPLACE INTO `#__hp_prop_ef` VALUES (16, 'selectlist', 'company', 'Company', '', 0, NULL, '', '', 18, 0, 1, 1, 1, 0, 1, 0, 'Company', '');
REPLACE INTO `#__hp_prop_ef` VALUES (17, 'multitext', 'notes', 'Notes', NULL, 0, NULL, '', '', 17, 1, 1, 0, 0, 0, 1, 0, 'Notes', '');
