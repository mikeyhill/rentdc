<?php
/**
 * @version		$Id: view.html.php 898 2009-11-02 03:31:05Z abernier $
 * @package		Hotproperty
 * @subpackage	View
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Properties View
 *
 * @package		Hotproperty
 * @subpackage	View
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyViewProperties extends HotpropertyView
{
	/**
	 * Constructor
	 * 
	 * @access	public
	 * @param	array	Associative configuration array
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Properties',
			'name_singular' => 'Property'
		), $config));
		
		echo MosetsHTML::_('filesystem.iswritable', array(MosetsApplication::getPath('media_images_original', 'hotproperty'), MosetsApplication::getPath('media_images_standard', 'hotproperty'), MosetsApplication::getPath('media_images_thumbnail', 'hotproperty')));
	}
	
	/**
	 * Display default layout
	 * 
	 * @return void
	 */
	function displayDefault()
	{
		$hotproperty =& MosetsApplication::getInstance('hotproperty');

		$this->setFilters(array(
			'search' => array(
				'Property.name' => ''
			),
			'list' => array(
				'Property.agent'		=> '',
				'Property.type'			=> '',
				'Property.published'	=> '',
				'Company.id'			=> ''
			)
		));
		$this->setOrdering('Property.name');
		
		$rows = $this->get('data', null, array('all', array(
			'where' => $this->getFiltersConditions(),
			'order' => array(
				'Property.approved' => 'ASC',
				$this->get('ordering') => $this->get('ordering_dir')
			),
			'limit' => $this->get('limit'),
			'limitstart' => $this->get('limitstart'),
			'contain' => array(
				'Type' => array(),
				'Agent' => array(
					'contain' => array(
						'Company' => array()
					)
				),
				'Photo' => array()
			)
		)));
		$this->assignRef('rows', $rows);
	}
	
	/**
	 * Display form layout
	 * 
	 * @return void
	 */
	function displayForm()
	{
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		$ids = JRequest::getVar('id', array(), 'default', 'array');
		if (empty($ids)) {
			$rows = $this->get('data', null, array('new'));
		} else {
			$rows = $this->get('data', null, array('all', array(
				'where'	=> array(
					'Property.' . $this->get('primaryKey', null) => $ids
				),
				'contain' => array(
					'PropertyField' => array(),
					'Photo' => array(
						'order' => array('Photo.ordering' => 'ASC')
					)
				)
			)));
		}
		$this->assignRef('rows', $rows);
		
		$this->setModel($hotproperty->getModel('Field'));
		$extrafields = $this->get('data', 'Field', array('all', array(
			'where'		=> array(
				'Field.name <>' => 'full_text'
			),	// Disable 'full_text'
			'order'		=> array('Field.ordering' => 'ASC'),
			'recursive'	=> 0
		)));
		$this->assignRef('extrafields', $extrafields);
		
		
	}
	
}
?>
