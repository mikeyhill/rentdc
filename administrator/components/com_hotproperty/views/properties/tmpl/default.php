<?php
/**
 * @version		$Id: default.php 914 2009-11-11 23:32:02Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Properties Template
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

// Toolbar
JToolBarHelper::title(JText::_('Property Manager'), 'addedit.png');
JToolBarHelper::addNewX('add', 'Add');
JToolBarHelper::editList('edit', 'Edit');
JToolBarHelper::publishList('publish');
JToolBarHelper::unpublishList('unpublish');
JToolBarHelper::customX('feature', 'apply.png', 'apply.png', 'Feature', true);
JToolBarHelper::customX('unfeature', 'cancel.png', 'cancel.png', 'Unfeature', true);
JToolBarHelper::archiveList('archive');
JToolBarHelper::unarchiveList('unarchive');
JToolBarHelper::deleteList('Are you sure you want to remove the selected item(s)?', 'remove', 'Remove');
JToolBarHelper::divider();
JToolBarHelper::preferences('com_hotproperty', '550', '570', 'Settings');

$hotproperty =& MosetsApplication::getInstance('hotproperty');
?>

<form action="<?php echo $this->get('action'); ?>" method="post" name="adminForm">
	
	<!--Filters-->
	<?php
	$filters = $this->get('filters');
	?>
	<table id="filters">
		<tr>
			<td width="100%">
				<?php
				if (array_key_exists('search', $filters) && count($filters['search'])) {
					foreach ($filters['search'] as $field => $value)
					{
						echo MosetsHTML::_('form.label', sprintf(JText::_('Filter by %s'), JText::_('name')), 'filter[search][' . $field . ']');
						echo MosetsHTML::_('filter.search', $field, $this->escape($value));
					}
					echo MosetsHTML::_('form.button', JText::_('Go'), null,  null, array('onclick' => 'this.form.submit();'));
					echo MosetsHTML::_('form.button', JText::_('Reset'), null,  null, array('onclick' => "$$('#filters input, #filters select').setProperty('value', ''); this.form.submit();"));
				}
				?>
			</td>
			<td nowrap="nowrap">
				<?php
				// Filter by state
				echo MosetsHTML::_('grid.state', 'filter[list][Property.published]', $filters['list']['Property.published'], '', '', JText::_('Archived'));
				// Filter by type
				echo MosetsHTML::_('hotproperty.list.types', 'filter[list][Property.type]', array('onchange' => 'document.adminForm.submit();'), $filters['list']['Property.type']);
				// Filter by agent
				if ($hotproperty->getCfg('use_companyagent') == '1') {
					echo MosetsHTML::_('hotproperty.list.agents', 'filter[list][Property.agent]', array('onchange' => 'document.adminForm.submit();'), $filters['list']['Property.agent']);
					echo MosetsHTML::_('hotproperty.list.companies', 'filter[list][Company.id]', array('onchange' => 'document.adminForm.submit();'), $filters['list']['Company.id']);
				}
				?>
			</td>
		</tr>
	</table>
	
	<div id="editcell">
		<table class="adminlist">
			<thead>
				<tr>
					<th width="1%">
						<?php echo JText::_('#'); ?>
					</th>
					<th width="1%">
						<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->rows); ?>);" />
					</th>			
					<th>
						<?php echo MosetsHTML::_('grid.sort', 'Name', 'Property.name', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th nowrap="nowrap" width="1%">
						<?php echo MosetsHTML::_('grid.sort', 'Published', 'Property.published', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th nowrap="nowrap" width="1%">
						<?php echo MosetsHTML::_('grid.sort', 'Featured', 'Property.featured', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th width="11%">
						<?php echo MosetsHTML::_('grid.sort', 'Type', 'Type.name', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
				<?php if ($hotproperty->getCfg('use_companyagent') == '1') : ?>
					<th width="11%">
						<?php echo MosetsHTML::_('grid.sort', 'Agent', 'Agent.name', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th width="11%">
						<?php echo MosetsHTML::_('grid.sort', 'Company', 'Company.name', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
				<?php endif; ?>
					<th width="11%">
						<?php echo MosetsHTML::_('grid.sort', 'Location', 'Property.suburb', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th nowrap="nowrap" width="10">
						<?php echo MosetsHTML::_('grid.sort', 'Date', 'Property.created', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th nowrap="nowrap" width="1%">
						<?php echo MosetsHTML::_('grid.sort', 'Hits', 'Property.hits', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th nowrap="nowrap" width="1%">
						<?php echo MosetsHTML::_('grid.sort', 'ID', 'Property.id', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
				</tr>			
			</thead>
			<tfoot>
				<tr>
					<td colspan="12">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
			<?php
			$k = 0;
			for ($i=0, $n = count($this->rows); $i < $n; $i++)
			{
				$row =& $this->rows[$i];
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td align="center">
						<?php echo $this->pagination->getRowOffset($i); ?>
					</td>
					<td>
						<?php echo MosetsHTML::_('grid.id', $i, $row->id); ?>
					</td>
					<td>
						<?php
						$href = '';
						// Has this property photo(s) ?
						if (isset($row->Photo)) {
							$href .= MosetsHTML::_('image', MosetsApplication::getPath('assets_images_url', 'hotproperty') . 'photo.png', JText::_('This property has an image defined.'), array('align' => 'absmiddle'));
						}
						// Normal property
						if ($row->published >= 0) {
							$href .= '
							<a href="#edit" onclick="return listItemTask(\'cb' . $i . '\',\'edit\')">' . $this->escape($row->name) . '</a>'
							;
						// Archived property
						} else {
							$href .= $this->escape($row->name) . ' [ ' . JText::_('Archived') . ' ]';
						}

						echo $href;
						?>
					</td>
				<?php if ($row->approved) : ?>
					<td align="center">
						<?php echo MosetsHTML::_('grid.published', $row, $i); ?>
					</td>
					<td align="center">
						<?php echo MosetsHTML::_('grid.featured', $row, $i); ?>
					</td>
				<?php else : ?>
					<td colspan="2" align="center">
						[ <a href="javascript:void(0);" onClick="return listItemTask('cb<?php echo $i;?>','approve')"><?php echo JText::_('Approve'); ?></a> ]
					</td>
				<?php endif; ?>
					<td>
						<a href="<?php echo JRoute::_('index.php?option=com_hotproperty&view=types&task=edit&id[]=' . $row->type . '&referer=' . $this->get('referer')); ?>"><?php echo $this->escape($row->Type->name); ?></a>
					</td>
				<?php if ($hotproperty->getCfg('use_companyagent') == '1') : ?>
					<td>
						<a href="<?php echo JRoute::_('index.php?option=com_hotproperty&view=agents&task=edit&id[]=' . $row->agent . '&referer=' . $this->get('referer')); ?>"><?php echo $this->escape($row->Agent->name); ?></a>
					</td>
					<td>
						<a href="<?php echo JRoute::_('index.php?option=com_hotproperty&view=companies&task=edit&id[]=' . $row->Agent->Company->id . '&referer=' . $this->get('referer')); ?>"><?php echo $this->escape($row->Agent->Company->name); ?></a>
					</td>
				<?php endif; ?>
					<td>
						<?php echo (!empty($row->suburb) && !empty($row->state)) ? $this->escape($row->suburb) . ", " . $this->escape($row->state) : $this->escape($row->suburb) . $this->escape($row->state); ?>
					</td>
					<td align="center">
						<?php echo MosetsHTML::_('date',  $row->created, JText::_('DATE_FORMAT_LC4')); ?>
					</td>
					<td align="center">
						<?php echo $row->hits; ?>
					</td>
					<td align="center">
						<?php echo $row->id; ?>
					</td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
		</table>
	</div>
	<?php MosetsHTML::_('interface.legend'); ?>

	<input type="hidden" name="option" value="com_hotproperty" />
	<input type="hidden" name="controller" value="properties" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="referer" value="<?php echo $this->get('referer'); ?>" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="ordering" value="<?php echo $this->get('ordering'); ?>" />
	<input type="hidden" name="ordering_dir" value="<?php echo $this->get('ordering_dir'); ?>" />
	<?php echo MosetsHTML::_('form.token'); ?>
</form>
