<?php
/**
 * @version		$Id: form_item.php 875 2009-09-25 09:30:12Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Properties Form Item Template
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

$hotproperty =& MosetsApplication::getInstance('hotproperty');
?>

<table width="100%">
	<tr>
		<td valign="top">
			
			<fieldset id="properties<?php echo $this->row->id; ?>">
				
				<legend><?php echo JText::_('General'); ?></legend>
				
				<table class="admintable" width="100%">
					<tr>
						<td class="key">
							<?php echo MosetsHTML::_('form.label', JText::_('Name'), 'Property[' . $this->row->id . '][name]', true, JText::_('TOOLTIP NAME')); ?>
						</td>
						<td>
							<?php echo MosetsHTML::_('form.input', 'text', 'Property[' . $this->row->id . '][name]', $this->escape($this->row->name), false, array('class' => 'required', 'maxlength' => '255', 'tabindex' => '1')); ?>
						</td>
						<td class="key">
							<?php echo MosetsHTML::_('form.label', JText::_('Approved'), 'Property[' . $this->row->id . '][approved]', false, JText::_('TOOLTIP APPROVED')); ?>
						</td>
						<td>
							<?php echo MosetsHTML::_('form.booleanlist', 'Property[' . $this->row->id . '][approved]', null, $this->row->approved); ?>
						</td>
					</tr>
					<tr>
						<td class="key">
							<?php echo MosetsHTML::_('form.label', JText::_('Type'), 'Property[' . $this->row->id . '][type]', true, JText::_('TOOLTIP TYPE')); ?>
						</td>
						<td>
							<?php echo MosetsHTML::_('hotproperty.list.types', 'Property[' . $this->row->id . '][type]', array('class' => 'required', 'tabindex' => '2'), $this->row->type); ?>
						</td>
						<td class="key">
							<?php echo MosetsHTML::_('form.label', JText::_('Published'), 'Property[' . $this->row->id . '][published]', false, JText::_('TOOLTIP PUBLISHED')); ?>
						</td>
						<td>
							<?php echo MosetsHTML::_('form.booleanlist', 'Property[' . $this->row->id . '][published]', null, $this->row->published); ?>
						</td>
					</tr>
					<tr>
						<?php if ($hotproperty->getCfg('use_companyagent')) : ?>
							<td class="key">
								<?php echo MosetsHTML::_('form.label', JText::_('Agent'), 'Property[' . $this->row->id . '][agent]', true, JText::_('TOOLTIP AGENT')); ?>
							</td>
							<td>
								<?php echo MosetsHTML::_('hotproperty.list.agents', 'Property[' . $this->row->id . '][agent]', array('class' => 'required', 'tabindex' => '3'), $this->row->agent); ?>
							</td>
						<?php else: ?>
							<td></td>
							<td>
								<?php echo MosetsHTML::_('form.input', 'hidden', 'Property[' . $this->row->id . '][agent]', $hotproperty->getCfg('default_agent'))?>
							</td>
						<?php endif; ?>
						<td class="key">
							<?php echo MosetsHTML::_('form.label', JText::_('Featured'), 'Property[' . $this->row->id . '][featured]', false, JText::_('TOOLTIP FEATURED')); ?>
						</td>
						<td>
							<?php
							$f = $this->row->featured;
							if (is_object($f))
								$f = 1;
							echo MosetsHTML::_('form.booleanlist', 'Property[' . $this->row->id . '][featured]', null, $f);
							?>
						</td>
					</tr>
				</table>
				
			</fieldset>
			
			<fieldset id="fields<?php echo $this->row->id; ?>">
				
				<legend><?php echo JText::_('Extrafields'); ?></legend>
				
				<?php echo MosetsHTML::_('interface.advice', JText::_('Only published fields are listed below.'), 'info'); ?>
				
				<table class="admintable" width="100%">
					<tbody>
						<?php if (!empty($this->extrafields)) : ?>
							<?php
							$exclude = array('company', 'name', 'agent', 'type', 'created', 'modified', 'published', 'featured', 'hits');
							foreach ($this->extrafields as $extrafield)
							{
								// Only display published extrafields
								if ($extrafield->published && !in_array($extrafield->name, $exclude)) {
									?>
									<tr>
										<td class="key">
											<?php echo MosetsHTML::_('form.label', $extrafield->iscore ? JText::_($extrafield->caption) : $this->escape($extrafield->caption)); ?>
										</td>
										<td>
											<?php echo MosetsHTML::_('hotproperty.form.extrafield', $extrafield, $this->row);
											?>
										</td>
									</tr>
									<?php
								}
							}
							?>
						<?php endif; ?>
					</tbody>
				</table>
			</fieldset>
			
			<?php //if ($this->row->id > 0) : ?>
				<fieldset id="photos<?php echo $this->row->id; ?>">
				
					<legend><?php echo JText::_('Photos'); ?></legend>
					
					<?php echo MosetsHTML::_('interface.advice', sprintf(JText::_('According to your configuration, new photos will be resized to %spx and to %spx for thumbnails.'), $hotproperty->getCfg('imgsize_standard'), $hotproperty->getCfg('imgsize_thumb')), 'info'); ?>
					
					<?php
					if (!empty($this->row->Photo)) {
						for ($i=0, $n = count($this->row->Photo); $i < $n; $i++)
						{
							$photo =& $this->row->Photo[$i];
							/*echo "<pre>";
							echo "photo=";
							print_r($photo);
							echo "</pre>";*/
							?>
							<fieldset>
								<legend>Photo #<?php echo ($i+1); ?></legend>

								<?php //echo MosetsHTML::_('form.input', 'hidden', 'photos[' . $photo->id . '][id]', $photo->id); ?>
								<?php echo MosetsHTML::_('form.input', 'hidden', 'Photo[' . $photo->id . '][property]', $this->row->id); ?>

								<table class="admintable">
									<tr>
										<td class="key">
											<?php echo MosetsHTML::_('form.label', JText::_('Photo'), 'Photo[' . $photo->id . '][original]'); ?>
										</td>
										<td>
											<?php echo MosetsHTML::_('hotproperty.image.photo', 'thumbnail', $photo->thumb, $photo->thumb); ?>
											<?php echo MosetsHTML::_('form.input', 'file', 'Photo[' . $photo->id . '][original]', false); ?>
										</td>
									</tr>
									<tr>
										<td class="key">
											<?php echo MosetsHTML::_('form.label', JText::_('Title'), 'Photo[' . $photo->id . '][title]', false, JText::_('Give a title for the photo. If none provided, the filename will be assumed.')); ?>
										</td>
										<td>
											<?php echo MosetsHTML::_('form.input', 'text', 'Photo[' . $photo->id . '][title]', $this->escape($photo->title)); ?>
										</td>
									</tr>
									<tr>
										<td class="key">
											<?php echo MosetsHTML::_('form.label', JText::_('Description'), 'Photo[' . $photo->id . '][desc]', false, JText::_('Give a description for that photo.')); ?>
										</td>
										<td>
											<?php echo MosetsHTML::_('form.textarea', 'Photo[' . $photo->id . '][desc]', $this->escape($photo->desc)); ?>
										</td>
									</tr>
									<tr>
										<td class="key">
											<?php echo MosetsHTML::_('form.label', JText::_('Ordering'), 'Photo[' . $photo->id . '][ordering]', false, JText::_('')); ?>
										</td>
										<td>
											<?php echo MosetsHTML::_('hotproperty.list.ordering', 'Photo[' . $photo->id . '][ordering]', 'photos', $photo, 'ordering', 'title'); ?>
										</td>
									</tr>
									<tr>
										<td class="key">
											<?php echo MosetsHTML::_('form.label', JText::_('Remove'), 'Photo[' . $photo->id . '][remove]'); ?>
										</td>
										<td>
											<?php echo MosetsHTML::_('form.checkbox', 'Photo[' . $photo->id . '][remove]'); ?>
										</td>
									</tr>
								</table>

							</fieldset>
						<?php
						}
					}
					?>
					
					<div id="newphoto<?php echo $this->row->id; ?>" class="button2-left">
						<div class="image">
							<a href="javascript:void(0);"><?php echo JText::_( 'Add photo' ); ?></a>
						</div>
					</div>
					
					<?php
					$document =& JFactory::getDocument();
					$document->addScriptDeclaration("
						window.addEvent('domready', function() {
							$('newphoto" . $this->row->id . "').addEvent('click', function(ev) {
								newPhotoId--;
								new Element('fieldset').setHTML(
									'<legend>New Photo</legend>' +
									'<input type=\"hidden\" value=\"' + newPhotoId + '\" id=\"Photo' + newPhotoId + 'id\" name=\"Photo[' + newPhotoId + '][id]\"/>' +
									'<input type=\"hidden\" value=\"" . $this->row->id . "\" id=\"Photo' + newPhotoId + 'property\" name=\"Photo[' + newPhotoId + '][property]\"/>' +
									'<table class=\"admintable\">' +
										'<tr>' +
											'<td class=\"key\">' +
												'<label for=\"Photo' + newPhotoId + 'original\">" . JText::_('New Photo', true) . "</label>' +
											'</td>' +
											'<td>' +
												'<input type=\"file\" value=\"\" id=\"Photo' + newPhotoId + 'original\" name=\"Photo[' + newPhotoId + '][original]\"/>' +
											'</td>' +
										'</tr>' +
										'<tr>' +
											'<td class=\"key\">' +
												'<label for=\"Photo' + newPhotoId + 'title\">" . JText::_('Title', true) . "</label>' +
											'</td>' +
											'<td>' +
												'<input type=\"text\" value=\"\" id=\"Photo' + newPhotoId + 'title\" name=\"Photo[' + newPhotoId + '][title]\"/>' +
											'</td>' +
										'</tr>' +
										'<tr>' +
											'<td class=\"key\">' +
												'<label for=\"Photo' + newPhotoId + 'desc\">" . JText::_('Description', true) . "</label>' +
											'</td>' +
											'<td>' +
												'<textarea id=\"Photo' + newPhotoId + 'desc\" name=\"Photo[' + newPhotoId + '][desc]\" cols=\"30\" rows=\"5\"></textarea>' +
											'</td>' +
										'</tr>' +
										'<tr>' +
											'<td class=\"key\">' +
												'<label>" . JText::_('Ordering', true) . "</label>' +
											'</td>' +
											'<td>' +
												'" . JText::_('DESCNEWITEMSLAST', true) . "' +
											'</td>' +
										'</tr>' +
									'</table>'
								).injectBefore(this);
							});
						});
					");
					?>
				
				</fieldset>
			<?php //endif; ?>
			
		</td>
		<td valign="top" width="350" style="padding: 7px 0 0 5px">
			<table width="100%" style="border: 1px dashed silver; padding: 5px; margin-bottom: 10px;">
				<tr>
					<td>
						<strong><?php echo JText::_('Property ID'); ?>:</strong>
					</td>
					<td>
						<?php //echo MosetsHTML::_('form.input', 'hidden', 'Property[' . $this->row->id . '][id]', $this->row->id); ?>
						<?php
						if ($this->row->id > 0)
							echo $this->row->id;
						else
							echo JText::_('Not assigned yet');
						?>
					</td>
				</tr>
				<tr>
					<td>
						<strong><?php echo JText::_('State'); ?></strong>
					</td>
					<td>
						<?php echo $this->row->published > 0 ? JText::_('Published') : ($this->row->published < 0 ? JText::_('Archived') : JText::_('Unpublished'));?>
					</td>
				</tr>
				<tr>
					<td>
						<strong><?php echo JText::_('Hits'); ?></strong>
					</td>
					<td>
						<span id="hits"><?php echo $this->row->hits; ?></span>
						&nbsp;
						<?php
						echo MosetsHTML::_('form.input', 'hidden', 'Property[' . $this->row->id . '][hits]', $this->row->hits);
						if ($this->row->hits > 0) {
							echo MosetsHTML::_('form.input', 'button', 'reset_hits', JText::_('Reset'), false, array('onclick' => "$('Property" . $this->row->id . "hits').value=0; $('hits').setHTML(0);"));
						}
						?>
					</td>
				</tr>
				<tr>
					<td>
						<strong><?php echo JText::_('Created'); ?></strong>
					</td>
					<td>
						<?php echo MosetsHTML::_('date',  $this->row->created,  JText::_('DATE_FORMAT_LC2')); ?>
					</td>
				</tr>
				<tr>
					<td>
						<strong><?php echo JText::_('Modified'); ?></strong>
					</td>
					<td>
						<?php echo MosetsHTML::_('date',  $this->row->modified, JText::_('DATE_FORMAT_LC2')); ?>
					</td>
				</tr>
			</table>
		<?php
		jimport('joomla.html.pane');
		$pane	=& JPane::getInstance('sliders', array('allowAllClose' => true));

		echo $pane->startPane("content-pane");
			echo $pane->startPanel(JText::_('Publishing'), "publishing");
			?>
			<table width="100%" cellspacing="1" class="paramlist admintable">
				<tr>
					<td width="40%" class="paramlist_key">
						<?php echo MosetsHTML::_('form.label', JText::_('Created Date'), 'Property[' . $this->row->id . '][created]', false, JText::_('Creation date of the property.')); ?>
					</td>
					<td class="paramlist_value">
						<?php echo MosetsHTML::_('calendar', $this->row->created, 'Property[' . $this->row->id . '][created]'); ?>
					</td>
				</tr>
				<tr>
					<td width="40%" class="paramlist_key">
						<?php echo MosetsHTML::_('form.label', JText::_('Start Publishing'), 'Property[' . $this->row->id . '][publish_up]', false, JText::_('Start publishing date/time.')); ?>
					</td>
					<td class="paramlist_value">
						<?php echo MosetsHTML::_('calendar', $this->row->publish_up, 'Property[' . $this->row->id . '][publish_up]'); ?>
					</td>
				</tr>
				<tr>
					<td width="40%" class="paramlist_key">
						<?php echo MosetsHTML::_('form.label', JText::_('End Publishing'), 'Property[' . $this->row->id . '][publish_down]', false, JText::_('End publishing date/time.')); ?>
					</td>
					<td class="paramlist_value">
						<?php echo MosetsHTML::_('calendar', $this->row->publish_down, 'Property[' . $this->row->id . '][publish_down]'); ?>
					</td>
				</tr>
			</table>
			<?php
			echo $pane->endPanel();
			
			echo $pane->startPanel(JText::_('Metadata Information'), "metadata");
			?>
			<table width="100%" cellspacing="1" class="paramlist admintable">
				<tr>
					<td width="40%" class="paramlist_key">
						<?php echo MosetsHTML::_('form.label', JText::_('Description'), 'Property[' . $this->row->id . '][metadesc]', false, JText::_('Meta description.')); ?>
					</td>
					<td class="paramlist_value">
						<?php echo MosetsHTML::_('form.textarea', 'Property[' . $this->row->id . '][metadesc]', $this->row->metadesc); ?>
					</td>
				</tr>
				<tr>
					<td width="40%" class="paramlist_key">
						<?php echo MosetsHTML::_('form.label', JText::_('Keywords'), 'Property[' . $this->row->id . '][metakey]', false, JText::_('Meta keywords.')); ?>
					</td>
					<td class="paramlist_value">
						<?php echo MosetsHTML::_('form.textarea', 'Property[' . $this->row->id . '][metakey]', $this->row->metakey); ?>
					</td>
				</tr>
			</table>
			<?php
			echo $pane->endPanel();
		echo $pane->endPane();
		?>
		</td>
	</tr>
</table>
