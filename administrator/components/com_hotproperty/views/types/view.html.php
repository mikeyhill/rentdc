<?php
/**
 * @version		$Id: view.html.php 898 2009-11-02 03:31:05Z abernier $
 * @package		Hotproperty
 * @subpackage	View
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Types View
 *
 * @package		Hotproperty
 * @subpackage	View
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyViewTypes extends HotpropertyView
{
	/**
	 * Constructor
	 * 
	 * @access	public
	 * @param	array	Associative configuration array
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Types',
			'name_singular' => 'Type'
		), $config));
	}
	
	/**
	 * Display default layout
	 * 
	 * @return void
	 */
	function displayDefault()
	{	
		$this->setFilters(array(
			'search' => array(
				'Type.name' => ''
			),
			'list' => array(
				'Type.published'	=> ''
			)
		));
		$this->setOrdering('Type.ordering');
		
		$rows = $this->get('data', null, array('all', array(
			'where' => $this->getFiltersConditions(),
			'order' => array($this->get('ordering') => $this->get('ordering_dir')),
			'limit' => $this->get('limit'),
			'limitstart' => $this->get('limitstart'),
			'contain' => array(
				'Property' => array()
			)
		)));
		$this->assignRef('rows', $rows);
	}
	
	/**
	 * Display form layout
	 * 
	 * @return void
	 */
	function displayForm()
	{
		$ids = JRequest::getVar('id', array(), 'default', 'array');
		if (empty($ids)) {
			$rows = $this->get('data', null, array('new'));
		} else {
			$rows = $this->get('data', null, array('all', array(
				'where'	=> array('Type.' . $this->get('primaryKey', null) . ' IN' => $ids),
				'order' => array($this->get('ordering') => $this->get('ordering_dir'))
			)));
		}
		$this->assignRef('rows', $rows);
	}
}
?>
