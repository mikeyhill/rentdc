<?php
/**
 * @version		$Id: form_item.php 781 2009-08-17 20:50:08Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Types Form Item Template
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */
?>

<fieldset id="prop_types<?php echo $this->row->id; ?>">
				
	<legend><?php echo JText::_('General'); ?></legend>
	
	<table class="admintable" width="100%">
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Name'), 'Type[' . $this->row->id . '][name]', true); ?>
			</td>
			<td>
				<?php echo MosetsHTML::_('form.input', 'text', 'Type[' . $this->row->id . '][name]', $this->escape($this->row->name), false, array('class' => 'required', 'maxlength' => '255')); ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Published'), 'Type[' . $this->row->id . '][published]'); ?>
			</td>
			<td>
				<?php echo MosetsHTML::_('form.booleanlist', 'Type[' . $this->row->id . '][published]', null, $this->row->published); ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Description'), 'Type[' . $this->row->id . '][desc]'); ?>
			</td>
			<td>
				<?php echo MosetsHTML::_('form.wysiwyg', 'Type[' . $this->row->id . '][desc]', $this->row->desc, $width = '100%', $height = '400', $col = '70', $row = '15', $buttons = true, $params = array('pagebreak', 'readmore')); ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Ordering'), 'Type[' . $this->row->ordering . '][ordering]'); ?>
			</td>
			<td>
				<?php echo MosetsHTML::_('hotproperty.list.ordering', 'Type[' . $this->row->id . '][ordering]', 'types', $this->row); ?>
			</td>
		</tr>
	</table>
	
</fieldset>
