<?php
/**
 * @version		$Id: default.php 914 2009-11-11 23:32:02Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Types Template
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

// Toolbar
JToolBarHelper::title(JText::_('Property Type Manager'), 'categories.png');
JToolBarHelper::addNewX('add', 'Add');
JToolBarHelper::editList('edit', 'Edit');
JToolBarHelper::publishList('publish');
JToolBarHelper::unpublishList('unpublish');
JToolBarHelper::deleteList('Are you sure you want to remove the selected item(s)?', 'remove', 'Remove');

// Allow ordering only when sorting by ordering
$allow_ordering = ($this->get('ordering') == 'Type.ordering');
?>

<form action="<?php echo $this->get('action'); ?>" method="post" name="adminForm">
	
	<!--Filters-->
	<?php
	$filters = $this->get('filters');
	?>
	<table id="filters">
		<tr>
			<td width="100%">
				<?php
				if (array_key_exists('search', $filters) && count($filters['search'])) {
					foreach ($filters['search'] as $field => $value)
					{
						echo MosetsHTML::_('form.label', sprintf(JText::_('Filter by %s'), JText::_('name')), 'filter[search][' . $field . ']');
						echo MosetsHTML::_('filter.search', $field, $this->escape($value));
					}
					echo MosetsHTML::_('form.button', JText::_('Go'), null,  null, array('onclick' => 'this.form.submit();'));
					echo MosetsHTML::_('form.button', JText::_('Reset'), null,  null, array('onclick' => "$$('#filters input, #filters select').setProperty('value', ''); this.form.submit();"));
				}
				?>
			</td>
			<td nowrap="nowrap">
				<?php
				// Filter by state
				echo MosetsHTML::_('grid.state', 'filter[list][Type.published]', $filters['list']['Type.published']);
				?>
			</td>
		</tr>
	</table>
	<div id="editcell">
		<table class="adminlist">
			<thead>
				<tr>
					<th width="1%">
						<?php echo JText::_('#'); ?>
					</th>
					<th width="1%">
						<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->rows); ?>);" />
					</th>
					<th>
						<?php echo MosetsHTML::_('grid.sort', 'Name', 'Type.name', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th nowrap="nowrap" width="1%">
						<?php echo MosetsHTML::_('grid.sort', 'Published', 'Type.published', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th width="100">
						<?php echo MosetsHTML::_('grid.sort', 'Order', 'Type.ordering', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
						<?php echo MosetsHTML::_('grid.order', $this->rows, $this->get('ordering')); ?>
					</th>
					<th nowrap="nowrap" width="1%">
						<?php echo '# ' . JText::_('Properties'); ?>
					</th>
					<th nowrap="nowrap" width="1%">
						<?php echo MosetsHTML::_('grid.sort', 'ID', 'Type.id', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
				</tr>			
			</thead>
			<tfoot>
				<tr>
					<td colspan="7">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
			<?php
			$k = 0;
			for ($i=0, $n = count($this->rows); $i < $n; $i++)
			{
				$row =& $this->rows[$i];
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td align="center">
						<?php echo $this->pagination->getRowOffset($i); ?>
					</td>
					<td>
						<?php echo MosetsHTML::_('grid.id', $i, $row->id); ?>
					</td>
					<td>
						<a href="#edit" onclick="return listItemTask('cb<?php echo $i; ?>','edit')"><?php echo $this->escape($row->name); ?></a>
					</td>
					<td align="center">
						<?php echo MosetsHTML::_('grid.published', $row, $i); ?>
					</td>
					<td class="order">
						<span><?php echo $this->pagination->orderUpIcon($i, true, 'orderup', 'Move Up', $allow_ordering); ?></span>
						<span><?php echo $this->pagination->orderDownIcon($i, $n, true, 'orderdown', 'Move Down', $allow_ordering); ?></span>
						<input type="text" name="order[]" size="5" value="<?php echo $row->ordering; ?>" <?php echo $allow_ordering ?  '' : 'disabled="disabled"'; ?> class="text_area" style="text-align: center" />
					</td>
					<td align="center">
						<?php
						$count_properties = 0;
						if (!empty($row->Property))
							$count_properties = count($row->Property);
						echo $count_properties;
						?>
					</td>
					<td align="center">
						<?php echo $row->id; ?>
					</td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
		</table>
	</div>

	<input type="hidden" name="option" value="com_hotproperty" />
	<input type="hidden" name="controller" value="types" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="referer" value="<?php echo $this->get('referer'); ?>" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="ordering" value="<?php echo $this->get('ordering'); ?>" />
	<input type="hidden" name="ordering_dir" value="<?php echo $this->get('ordering_dir'); ?>" />
	<?php echo MosetsHTML::_('form.token'); ?>
</form>
