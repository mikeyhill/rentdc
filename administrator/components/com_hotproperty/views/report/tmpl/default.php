<?php
/**
 * @version		$Id: default.php 914 2009-11-11 23:32:02Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Report Template
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

// Toolbar
JToolBarHelper::title(JText::_('Report'));
JToolBarHelper::custom('generateClassic', 'edit.png', 'edit_f2.png', 'Generate', false);
?>

<form action="<?php echo $this->get('action'); ?>" method="post" name="adminForm">
	
	<table class="noshow" cellpadding="0">
		<tr>
			<td width="50%">
				<fieldset>
					<legend><?php echo JText::_('Fields'); ?></legend>
					
					<?php
					foreach ($this->extrafields as $extrafield)
					{
						echo '<div style="float:left;width:50%;line-height:2em;height:2em;overflow:hidden;margin-bottom:0em">';
							echo MosetsHTML::_('form.label', MosetsHTML::_('form.checkbox', 'fields[]', $extrafield->name, null, $this->escape($extrafield->name)) . $this->escape(JText::_($extrafield->caption)));
						echo '</div>';
					}
					?>
				</fieldset>
			</td>
			<td>
				<fieldset>
					<legend><?php echo JText::_('Options'); ?></legend>
					
					<?php echo MosetsHTML::_('form.booleanlist', 'show_all', null, '1', 'Show All','Only published properties'); ?>
					<br />
					
					<?php
					echo MosetsHTML::_('form.label', MosetsHTML::_('form.checkbox', 'nowrap') . JText::_('No wrapping text'));
					?>
					<br />
					
					<?php
					echo MosetsHTML::_('form.label', MosetsHTML::_('form.checkbox', 'show_fulltext') . JText::_('Show full text'));
					?>
				</fieldset>
			</td>
		</tr>
	</table>

	<input type="hidden" name="option" value="com_hotproperty" />
	<input type="hidden" name="controller" value="report" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="referer" value="<?php echo $this->get('referer'); ?>" />
	<input type="hidden" name="boxchecked" value="0" />
	<?php echo MosetsHTML::_('form.token'); ?>
</form>