<?php
/**
 * @version		$Id: excel.php 768 2009-07-20 16:08:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Excel report Template
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

// Toolbar
JToolBarHelper::title(JText::_('Report') . " : <small><small>[" . JText::_('excel') . "]</small></small>", 'addedit.png');
JToolBarHelper::custom('display', 'new.png', 'new.png', 'New', false);
?>

<form action="<?php echo $this->get('action'); ?>" method="post" name="adminForm">
	
	<script language="Javascript">
	<!--
	/*
	Select and Copy form element script- By Dynamicdrive.com
	For full source, Terms of service, and 100s DTHML scripts
	Visit http://www.dynamicdrive.com
	*/

	//specify whether contents should be auto copied to clipboard (memory)
	//Applies only to IE 4+
	//0=no, 1=yes
	var copytoclip=1

	function HighlightAll(theField) {
		var tempval=eval("document."+theField)
		tempval.focus()
		tempval.select()
		if (document.all&&copytoclip==1){
			therange=tempval.createTextRange()
			therange.execCommand("Copy")
			window.status="Contents highlighted and copied to clipboard!"
			setTimeout("window.status=''",1800)
		}
	}
	//-->
	</script>
	
	<fieldset>
		<legend><?php echo JText::_('CSV Data for Ms Excel') ?></legend>
		
		<a href="javascript:HighlightAll('adminForm.csv_excel')"><?php echo JText::_('Select All') ?></a>
		<br />
		<textarea name="csv_excel" rows="30" cols="80"><?php echo $this->header; echo $this->data; ?></textarea>
		<br />
		<a href="javascript:HighlightAll('adminForm.csv_excel')"><?php echo JText::_('Select All') ?></a>
	</fieldset>
	
	<input type="hidden" name="option" value="com_hotproperty" />
	<input type="hidden" name="controller" value="report" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<?php echo MosetsHTML::_('form.token'); ?>
</form>