<?php
/**
 * @version		$Id: classic.php 768 2009-07-20 16:08:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Classic report Template
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

// Toolbar
JToolBarHelper::title(JText::_('Report') . " : <small><small>[" . JText::_('classic') . "]</small></small>", 'addedit.png');
JToolBarHelper::custom('display', 'new.png', 'new.png', 'New', false);
?>

<form action="<?php echo $this->get('action'); ?>" method="post" name="adminForm">

	<a href="index.php?option=com_hotproperty&amp;view=report&amp;controller=report&amp;task=generatePrint&amp;nowrap=<?php echo $this->report_options['nowrap']; ?>&show_fulltext=<?php echo $this->report_options['show_fulltext']; ?>&amp;tmpl=component" target="_blank"><?php echo JText::_('Print') ?></a> | <a href="index.php?option=com_hotproperty&amp;view=report&amp;controller=report&amp;task=generateExcel"><?php echo JText::_('Convert All to CSV for Ms Excel') ?></a>
	
	<div id="editcell">
		<table class="adminlist">
			<thead>
				<tr>
					<th width="1%">
						<?php echo JText::_('#'); ?>
					</th>
					<?php foreach ($this->fields2 as $field2) : ?>
						<th nowrap="nowrap">
							<?php echo JText::_($field2->caption); ?>
						</th>
					<?php endforeach; ?>
				</tr>			
			</thead>
			<tfoot>
				<tr>
					<td colspan="11">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
			<?php
			$k = 0;
			for ($i=0, $n = count($this->rows); $i < $n; $i++)
			{
				$row =& $this->rows[$i];
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td align="center">
						<?php echo $this->pagination->getRowOffset($i); ?>
					</td>
					<?php 
					$j = 0;
					foreach ($row AS $r)
					{ 
						if ($j > 0)	{
							if (strlen($r) > 50 && $this->report_options['show_fulltext'] <> '1') {
								$r = substr($r, 0, 50) . '...';
							}
							//echo '<td align="left" nowrap>';
							echo '<td align="left"'.(($this->report_options['nowrap'] == '1') ? 'nowrap ' : '').'>';
							echo (($r <> '') ? htmlspecialchars($r) : '&nbsp;');
							echo '</td>';
						}
						$j++;
					}
					?>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
		</table>
	</div>

	<?php
	foreach ($this->fields AS $f)
	{
		echo "<input type='hidden' name='fields[]' value='$f' /> \n";
	}
	?>
	<input type="hidden" name="show_fulltext" value="<?php echo $this->report_options['show_fulltext']; ?>">
	<input type="hidden" name="nowrap" value="<?php echo $this->report_options['nowrap']; ?>">
	
	<input type="hidden" name="option" value="com_hotproperty" />
	<input type="hidden" name="controller" value="report" />
	<input type="hidden" name="task" value="generateClassic" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="ordering" value="<?php echo $this->get('ordering'); ?>" />
	<input type="hidden" name="ordering_dir" value="<?php echo $this->get('ordering_dir'); ?>" />
	<?php echo MosetsHTML::_('form.token'); ?>
</form>
