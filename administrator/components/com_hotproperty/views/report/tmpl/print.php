<?php
/**
 * @version		$Id: print.php 768 2009-07-20 16:08:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Print report Template
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */
?>

<a href="javascript:void();" onclick="javascript:window.print(); return false" title="Print"><?php echo '[' . JText::_('Print') . ']'; ?></a>
&nbsp;
<a href="javascript:void();" onclick="javascript:window.close(); return false" title="Print"><?php echo '[' . JText::_('Close') . ']'; ?></a>

<p />
<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
	<tr>
		<th width="20">#</th>
		<?php foreach ($this->fields2 AS $field2) { ?>
			<th align="left" nowrap><?php echo JText::_($field2); ?></th>
		<?php } ?>
	</tr>
<?php
	for ($i=0, $n=count($this->rows); $i < $n; $i++) {
		$row =& $this->rows[$i];
?>
	<tr class="<?php echo "row1"; ?>">
		<td width="20" align="center"><?php echo $i+1;?></td>
		<?php 
		$j = 0;
		foreach ($row AS $r) { 
			if ($j > 0)	{
				if (strlen($r) > 50 && $this->report_options['show_fulltext'] <> 1) {
					$r = substr($r, 0, 50) . '...'.$this->report_options['show_fulltext'];
				}
				//echo '<td align="left" nowrap>';
				echo '<td align="left"'.(($this->report_options['nowrap'] == 1) ? 'nowrap ' : '').'>';
				echo (($r <> '') ? htmlspecialchars($r) : '&nbsp;');
				echo '</td>';
			}
			$j++;
		} ?>
	</tr>
<?php	}
?>
</table>