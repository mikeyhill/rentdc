<?php
/**
 * @version		$Id: view.html.php 772 2009-08-09 20:07:40Z abernier $
 * @package		Hotproperty
 * @subpackage	View
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Report View
 *
 * @package		Hotproperty
 * @subpackage	View
 * @author		Antoine Bernier <abernier@mosets.com>
 * @todo		Reports implementation sends me a shiver down the spine : please, do something for that !
 */

class HotpropertyViewReport extends HotpropertyView
{
	/**
	 * Constructor
	 * 
	 * @access	public
	 * @param	array	Associative configuration array
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Report',
			'name_singular' => 'Report'
		), $config));
	}
	
	/**
	 * Display default layout
	 * 
	 * @return	void
	 */
	function displayDefault()
	{
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		$this->setModel($hotproperty->getModel('Field'));
		
		$extrafields = $this->get('data', 'Field', array('all', array(
			'where'		=> array('name <>' => 'full_text'),	// Disable 'full_text'
			'recursive'	=> 1
		)));
		$this->assignRef('extrafields', $extrafields);
	}
	
	/**
	 * Display classic report
	 * 
	 * @return	void
	 */
	function displayClassic()
	{
		global $mainframe;
		$database =& MosetsFactory::getDBO();

		$fields								= JRequest::getVar('fields', null, 'default', 'array');
		$show_all							= JRequest::getInt('show_all', 1);
		$report_options['nowrap']			= JRequest::getInt('nowrap', 0);
		$report_options['show_fulltext']	= JRequest::getInt('show_fulltext', 0);

		# Gather the requested fields' attributes (iscore, name, caption)
		$sql = "SELECT name, caption, iscore "
			. "\nFROM #__hp_prop_ef"
			. "\nWHERE name='" . ((count($fields) > 1) ? implode("' OR name='", $fields) : $fields[0]) . "'"
			. "\nORDER BY iscore DESC, ordering ASC"
			;
		$database->setQuery($sql);
		$fields2 = $database->loadObjectList();
		# Drop temp2 table
		$database->setQuery("DROP TABLE IF EXISTS #__hp_temp2");
		$database->query();

		# Create Table
		$sql = "CREATE TABLE #__hp_temp2 ("
			."\nid INT(11) NOT NULL, ";

		foreach ($fields2 AS $field2)
		{
			$sql .= "\n".$field2->name." MEDIUMTEXT NOT NULL, ";
		}
		$sql .= "\nPRIMARY KEY  (id)"
			. "\n) TYPE=MyISAM ;"
			;

		$database->setQuery($sql);
		if (!$database->query()) {
			echo $database->getErrorMsg();
		}

		# Populating core field's data
		$sql2 = "INSERT INTO #__hp_temp2 (`id`";

		foreach ($fields2 AS $col) {
			if ($col->iscore) {
				$sql2 .= ", `".$col->name."`";
			}
		}
		$sql2 .= ") \n SELECT p.id";
		foreach ($fields2 AS $col) {
			if ($col->iscore) {
				if ($col->name == 'company') {
					$sql2 .= ", c.name AS company";
				} elseif ($col->name == 'agent') {
					$sql2 .= ", a.name AS agent";
				} elseif ($col->name == 'type') {
					$sql2 .= ", t.name AS type";
				} else {
					if ($col->name == 'notes') {
						$sql2 .= ", p.note";  // Inconsistency in Hidden Note name. Fix.
					} else {
						$sql2 .= ", p.".$col->name;
					}
				}
			}
		}

		$sql2 .= "\n FROM (#__hp_properties AS p, #__hp_companies AS c)"
			. "\n LEFT JOIN #__hp_prop_types AS t ON p.type = t.id"
			. "\n LEFT JOIN #__hp_agents AS a ON p.agent = a.id"
			.	"\n WHERE a.company=c.id";

		$now = JFactory::getDate();
		$nullDate = $database->getNullDate();
		
		if ($show_all == '0') {
			$sql2 .=  "\n AND p.published='1'"
				. "\n	AND (publish_up = " . $database->Quote($nullDate) . " OR publish_up <= " . $database->Quote($now->toMySQL()) . ")"
				. "\n	AND (publish_down = " . $database->Quote($nullDate) . " OR publish_down >= " . $database->Quote($now->toMySQL()) . ")";
		}

		$database->setQuery($sql2);
		if (!$database->query()) {
			echo $database->getErrorMsg();
		}

		# Populating non-core field's data
		$sql3 = "SELECT e.name, e.field_type, p.property, p.field, p.value FROM (#__hp_properties2 AS p, #__hp_prop_ef AS e)"
			.	"\nWHERE e.id=p.field";

		$database->setQuery($sql3);
		$rows3 = $database->loadObjectList();

		foreach ($rows3 AS $row3) {
			if (in_array($row3->name,$fields)) {
				$sql4 = "UPDATE #__hp_temp2 SET ".$row3->name."='".$row3->value."' WHERE id='".$row3->property."'";
				$database->setQuery($sql4);
				$database->query();
			}
		}

		$database->setQuery("SELECT count(*) FROM #__hp_temp2");
		$total = $database->loadResult();
		echo $database->getErrorMsg();

		# Get the meat
		$sql5 = "SELECT * FROM #__hp_temp2 ORDER BY " . ((in_array('name', $fields)) ? 'name' : 'id') . " LIMIT " . $this->get('limitstart') . ", " . $this->get('limit');
		$database->setQuery($sql5);
		$rows = $database->loadObjectList();
		
		$this->assignRef('fields', $fields);
		$this->assignRef('fields2', $fields2);
		$this->assignRef('rows', $rows);
		$this->assignRef('report_options', $report_options);
		$this->assignRef('total', $total);
	}
	
	/**
	 * Display excel report
	 * 
	 * @return	void
	 */
	function displayExcel()
	{
		$database =& MosetsFactory::getDBO();

		$data = '';
		$seperator = ',';
		# Retrieve the caption
		$sql = "DESCRIBE #__hp_temp2";
		$database->setQuery($sql);
		$fields = $database->loadObjectList('Field');

		foreach ($fields AS $field)
		{
			if ($field->Field <> 'id') {
				$sql = "SELECT caption FROM #__hp_prop_ef WHERE name = '".$field->Field."' LIMIT 1";
				$database->setQuery($sql);
				$fields2[$field->Field] = $database->loadResult();
			}
		}

		# Get the meat
		$sql = "SELECT * FROM #__hp_temp2 ORDER BY ".((array_search('Name', $fields2)) ? 'name' : 'id');
		$database->setQuery($sql);
		$rows = $database->loadObjectList();

		# Create the .CSV
		$header = '';
		foreach ($fields2 AS $field)
		{
			$header .= $field.$seperator;
		}
		$header .= "\n";

		foreach ($rows AS $row)
		{
			$line = '';
			$j = 0;
			foreach ($row as $value)
			{
				if ($j > 0) {
					if (!isset($value) || $value == "") {
						//$value = $seperator;
					} else {
						$value = str_replace('"', '""', $value);
						$value = '"' . $value . '"'; // . $seperator;
					}
					$line .= $value;
					$line .= $seperator;
				}
				$j++;
			}
			$data .= trim($line)."\n";
		}
		# this line is needed because returns embedded in the data have "\r"
		# and this looks like a "box character" in Excel
		$data = str_replace("\r", "", $data);
		
		$this->assignRef('header', $header);
		$this->assignRef('data', $data);
	}
	
	function displayPrint()
	{
		$database =& MosetsFactory::getDBO();

		$report_options['nowrap'] = JRequest::getInt('nowrap',0);
		$report_options['show_fulltext'] = JRequest::getInt('show_fulltext',0);

		# Retrieve the caption
		$sql = "DESCRIBE #__hp_temp2";
		$database->setQuery($sql);
		$fields = $database->loadObjectList('Field');

		foreach ($fields AS $field) {
			if ($field->Field <> 'id') {
				$sql = "SELECT caption FROM #__hp_prop_ef WHERE name = '".$field->Field."' LIMIT 1";
				$database->setQuery($sql);
				$fields2[$field->Field] = $database->loadResult();
			}
		}

		# Get the meat
		$sql = "SELECT * FROM #__hp_temp2 ORDER BY ".((array_search('Name', $fields2)) ? 'name' : 'id');
		$database->setQuery($sql);
		$rows = $database->loadObjectList();

		$this->assignRef('fields2', $fields2);
		$this->assignRef('rows', $rows);
		$this->assignRef('report_options', $report_options);
	}
}
?>
