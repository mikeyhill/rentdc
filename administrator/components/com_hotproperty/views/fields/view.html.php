<?php
/**
 * @version		$Id: view.html.php 898 2009-11-02 03:31:05Z abernier $
 * @package		Hotproperty
 * @subpackage	View
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Fields View
 *
 * @package		Hotproperty
 * @subpackage	View
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyViewFields extends HotpropertyView
{
	/**
	 * Constructor
	 * 
	 * @access	public
	 * @param	array	Associative configuration array
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Fields',
			'name_singular' => 'Field'
		), $config));
	}
	
	/**
	 * Display default layout
	 * 
	 * @return void
	 **/
	function displayDefault()
	{		
		$this->setFilters(array(
			'search' => array(
				'Field.caption' => ''
			),
			'list' => array(
				'Field.field_type'	=> '',
				'Field.published'	=> ''
			)
		));
		$this->setOrdering('Field.ordering');
		
		$rows = $this->get('data', null, array('all', array(
			'where'	=> array_merge(
				array('Field.name <> ' => 'full_text'),	// Disable 'full_text'
				$this->getFiltersConditions()
			),
			'order' => array($this->get('ordering') => $this->get('ordering_dir')),
			'limit' => $this->get('limit'),
			'limitstart' => $this->get('limitstart'),
		)));
		$this->assignRef('rows', $rows);
	}
	
	/**
	 * Display form layout
	 * 
	 * @return void
	 */
	function displayForm()
	{
		$ids = JRequest::getVar('id', array(), 'default', 'array');
		if (empty($ids)) {
			$rows = $this->get('data', null, array('new'));
		} else {
			$rows = $this->get('data', null, array('all', array(
				'where'	=> array('Field.' . $this->get('primaryKey', null) . ' IN' => $ids),
				'order' => array($this->get('ordering') => $this->get('ordering_dir'))
			)));
		}
		$this->assignRef('rows', $rows);
	}
}
?>
