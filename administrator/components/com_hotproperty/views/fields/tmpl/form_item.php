<?php
/**
 * @version		$Id: form_item.php 781 2009-08-17 20:50:08Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Fields Form Item Template
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */
?>

<?php
$document =& JFactory::getDocument();
$document->addScriptDeclaration("window.addEvent('domready', function(){
	var caption			= $('Field" . $this->row->id . "caption');
	var fieldName		= $('Field" . $this->row->id . "name');
	var fieldType		= $('Field" . $this->row->id . "field_type');
	var fieldElements	= $('Field" . $this->row->id . "field_elements');
	var defaultValue	= $('Field" . $this->row->id . "default_value');
	var prefixText		= $('Field" . $this->row->id . "prefix_text');
	var appendText		= $('Field" . $this->row->id . "append_text');
	var size			= $('Field" . $this->row->id . "size');
	var searchOff		= $('Field" . $this->row->id . "search0');
	var searchOn		= $('Field" . $this->row->id . "search1');
	var searchOnOff		= $$(searchOff, searchOn);
	var searchType		= $('Field" . $this->row->id . "search_type');
	var searchTypeLabel = $$('[for^=Field" . $this->row->id . "search_type]');
	var searchCaption	= $('Field" . $this->row->id . "search_caption');
	var searchAll		= $$('[for^=Field" . $this->row->id . "search]');
	
	caption.addEvent('change', function() {
		if (searchCaption.value.length <= 0){
			searchCaption.value = $(this).value;
		}
	});
	
	function toggleFieldType(){
		switch (fieldType.value){
			case '':
				$$(fieldElements, prefixText, appendText, size, searchType).each(function(el){
					el.setProperty('disabled', 'disabled');
					el.getParent().getParent().addClass('hidden');
				});
				break;
			case 'selectlist':
			case 'selectmultiple':
			case 'checkbox':
			case 'radiobutton':
				$$(fieldElements).each(function(el){
					el.removeProperty('disabled');
					el.getParent().getParent().removeClass('hidden');
				});
				if (['selectlist','selectmultiple'].contains(fieldType.value)){
					$$(prefixText, appendText, size).each(function(el){
						el.removeProperty('disabled');
						el.getParent().getParent().removeClass('hidden');
					});
				} else {
					$$(prefixText, appendText, size).each(function(el){
						el.setProperty('disabled', 'disabled');
						el.getParent().getParent().addClass('hidden');
					});
				}
				break;
			case 'text':
			case 'multitext':
			case 'link':
				$$(fieldElements).each(function(el){
					el.setProperty('disabled', 'disabled');
					el.getParent().getParent().addClass('hidden');
				});
				document.formvalidator.handleResponse(true, defaultValue);
				$$(prefixText, appendText).each(function(el){
					el.removeProperty('disabled');
					el.getParent().getParent().removeClass('hidden');
				});
				if (['text','link'].contains(fieldType.value)){
					$$(size).each(function(el){
						el.removeProperty('disabled');
						el.getParent().getParent().removeClass('hidden');
					});
				} else {
					$$(size).each(function(el){
						el.setProperty('disabled', 'disabled');
						el.getParent().getParent().addClass('hidden');
					});
				}
				break;
		}
		toggleSearch();
	}
	toggleFieldType();
	fieldType.addEvent('change', function() {
		toggleFieldType();
	});
	
	function toggleSearch(){
		// Yes
		if (searchOn.checked == 1) {
			// Activate 'Search Caption'
			$$(searchCaption).each(function(el){
				el.removeProperty('disabled');
				el.getParent().getParent().removeClass('hidden');
			});
			// Eventually fill in 'Search Caption' with field's caption value if empty
			if (searchCaption.value.length <= 0){
				searchCaption.value = caption.value;
			}

			// Activate the 'Search Type' only if it is a 'text' extrafield (or exeptionally for price)
			if (fieldType.value == 'text' && !['name','address', 'suburb', 'state', 'country'].contains(fieldName.value) || ['created','modified', 'hits', 'price'].contains(fieldName.value)){
				$$(searchType).each(function(el){
					el.removeProperty('disabled');
					el.getParent().getParent().removeClass('hidden');
				});
			} else {
				$$(searchType).each(function(el){
					el.setProperty('disabled', 'disabled');
					el.getParent().getParent().addClass('hidden');
				});
				document.formvalidator.handleResponse(true, searchType);
			}
		// No
		} else {
			$$(searchCaption, searchType).each(function(el){
				el.setProperty('disabled', 'disabled');
				el.getParent().getParent().addClass('hidden');
			});
			document.formvalidator.handleResponse(true, searchAll);
		}
	}
	toggleSearch();
	searchOnOff.addEvent('change', function() { 
		toggleSearch();
	});
	
	// A custom handler to validate 'Default Value' (check whether the default value appears in field_elements list)
	document.formvalidator.setHandler('defaultValue', function(value){
		if (!fieldElements.disabled && !fieldElements.value.split('|').contains(value)){
			return false;
		}
		return true;
	});
});");
?>

<table width="100%">
	<tr>
		<td valign="top">
			<fieldset id="Field<?php echo $this->row->id; ?>">
			
				<legend><?php echo JText::_('General'); ?></legend>
	
				<table class="admintable" width="100%">
				
					<tr>
						<td class="key">
							<?php echo MosetsHTML::_('form.label', JText::_('Caption'), 'Field[' . $this->row->id . '][caption]', true, JText::_('TOOLTIP CAPTION')); ?>
						</td>
						<td>
							<?php echo MosetsHTML::_('form.input', 'text', 'Field[' . $this->row->id . '][caption]', $this->escape($this->row->caption), false, array('class' => 'required', 'maxlength' => '255')); ?>
							<?php echo MosetsHTML::_('form.input', 'hidden', 'Field[' . $this->row->id . '][name]', $this->escape($this->row->name)); ?>
						</td>
					</tr>
					
				<?php if (!$this->row->iscore) : ?>
					<tr>
						<td class="key">
							<?php echo MosetsHTML::_('form.label', JText::_('Field type'), 'Field[' . $this->row->id . '][field_type]', true); ?>
						</td>
						<td>
							<?php echo MosetsHTML::_('hotproperty.list.field_types', 'Field[' . $this->row->id . '][field_type]', array('class' => 'required'), $this->row->field_type); ?>
						</td>
					</tr>
					<tr>
						<td class="key">
							<?php echo MosetsHTML::_('form.label', JText::_('Field elements'), 'Field[' . $this->row->id . '][field_elements]', true, JText::_('TOOLTIP ELEMENTS')); ?>
						</td>
						<td>
							<?php echo MosetsHTML::_('form.input', 'text', 'Field[' . $this->row->id . '][field_elements]', $this->escape($this->row->field_elements), false, array('class' => 'required validate-pipelist', 'size' => 30)); ?>
						</td>
					</tr>
					<tr>
						<td class="key">
							<?php echo MosetsHTML::_('form.label', JText::_('Default value'), 'Field[' . $this->row->id . '][default_value]', false, JText::_('TOOLTIP DEFAULT VALUE')); ?>
						</td>
						<td>
							<?php echo MosetsHTML::_('form.input', 'text', 'Field[' . $this->row->id . '][default_value]', $this->escape($this->row->default_value), false, array('class' => 'validate-defaultValue')); ?>
						</td>
					</tr>
				<?php else: ?>
					<tr class="hidden">
						<td></td>
						<td>
							<div>
							<?php echo MosetsHTML::_('form.input', 'hidden', 'Field[' . $this->row->id . '][field_type]', $this->escape($this->row->field_type)); ?>
							<?php echo MosetsHTML::_('form.input', 'hidden', 'Field[' . $this->row->id . '][field_elements]', $this->escape($this->row->field_elements)); ?>
							<?php echo MosetsHTML::_('form.input', 'hidden', 'Field[' . $this->row->id . '][default_value]', $this->escape($this->row->default_value)); ?>
							</div>
						</td>
					</tr>
				<?php endif; ?>
				
				<?php if (!$this->row->iscore || $this->row->name == 'price') : ?>
					<tr>
						<td class="key">
							<?php echo MosetsHTML::_('form.label', JText::_('Prefix text'), 'Field[' . $this->row->id . '][prefix_text]', false, JText::_('TOOLTIP PREFIX TEXT')); ?>
						</td>
						<td>
							<?php echo MosetsHTML::_('form.input', 'text', 'Field[' . $this->row->id . '][prefix_text]', $this->escape($this->row->prefix_text), false); ?>
						</td>
					</tr>
					<tr>
						<td class="key">
							<?php echo MosetsHTML::_('form.label', JText::_('Suffix text'), 'Field[' . $this->row->id . '][append_text]', false, JText::_('TOOLTIP SUFFIX TEXT')); ?>
						</td>
						<td>
							<?php echo MosetsHTML::_('form.input', 'text', 'Field[' . $this->row->id . '][append_text]', $this->escape($this->row->append_text), false); ?>
						</td>
					</tr>
				<?php else: ?>
					<tr class="hidden">
						<td></td>
						<td>
							<div>
							<?php echo MosetsHTML::_('form.input', 'hidden', 'Field[' . $this->row->id . '][prefix_text]', $this->escape($this->row->prefix_text)); ?>
							<?php echo MosetsHTML::_('form.input', 'hidden', 'Field[' . $this->row->id . '][append_text]', $this->escape($this->row->append_text)); ?>
							</div>
						</td>
					</tr>
				<?php endif; ?>
				
				<?php if (!$this->row->iscore) : ?>
					<tr>
						<td class="key">
							<?php echo MosetsHTML::_('form.label', JText::_('Size'), 'Field[' . $this->row->id . '][size]', false, JText::_('TOOLTIP SIZE')); ?>
						</td>
						<td>
							<?php echo MosetsHTML::_('form.input', 'text', 'Field[' . $this->row->id . '][size]', $this->row->size, false, array('size' => 2, 'maxlength' => 3)); ?>
						</td>
					</tr>
				<?php else : ?>
					<tr class="hidden">
						<td></td>
						<td>
							<div>
							<?php echo MosetsHTML::_('form.input', 'hidden', 'Field[' . $this->row->id . '][size]', $this->row->size); ?>
							</div>
						</td>
					</tr>
				<?php endif; ?>
					
					<tr>
						<td class="key">
							<?php echo MosetsHTML::_('form.label', JText::_('Ordering'), 'Field[' . $this->row->id . '][ordering]'); ?>
						</td>
						<td>
							<?php echo MosetsHTML::_('hotproperty.list.ordering', 'Field[' . $this->row->id . '][ordering]', 'fields', $this->row); ?>
						</td>
					</tr>
					
				</table>
			</fieldset>
		</td>
			
		<td valign="top" width="350" style="padding: 7px 0 0 5px">
		<?php
		jimport('joomla.html.pane');
		$pane	=& JPane::getInstance('sliders', array('allowAllClose' => true));
		
		if ($this->row->name != 'name') {
			?>
			<div class="pane-sliders">
				<div class="panel">
					<h3 id="options" class="title jpane-toggler-down">
						<span>Options</span>
					</h3>
					<div class="content">
						<table class="paramlist admintable">
							<tr>
								<td class="key">
									<?php echo MosetsHTML::_('form.label', JText::_('Show in Featured'), 'Field[' . $this->row->id . '][featured]', false, JText::_('TOOLTIP SHOW IN FEATURED')); ?>
								</td>
								<td>
									<?php echo MosetsHTML::_('form.booleanlist', 'Field[' . $this->row->id . '][featured]', null, $this->row->featured); ?>
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo MosetsHTML::_('form.label', JText::_('Show in Listing'), 'Field[' . $this->row->id . '][listing]', false, JText::_('TOOLTIP SHOW IN LISTING')); ?>
								</td>
								<td>
									<?php echo MosetsHTML::_('form.booleanlist', 'Field[' . $this->row->id . '][listing]', null, $this->row->listing); ?>
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo MosetsHTML::_('form.label', JText::_('Hide Caption'), 'Field[' . $this->row->id . '][hideCaption]'); ?>
								</td>
								<td>
									<?php echo MosetsHTML::_('form.booleanlist', 'Field[' . $this->row->id . '][hideCaption]', null, $this->row->hideCaption); ?>
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo MosetsHTML::_('form.label', JText::_('Hidden'), 'Field[' . $this->row->id . '][hidden]', false, JText::_('TOOLTIP HIDDEN FIELD')); ?>
								</td>
								<td>
									<?php echo MosetsHTML::_('form.booleanlist', 'Field[' . $this->row->id . '][hidden]', null, $this->row->hidden); ?>
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo MosetsHTML::_('form.label', JText::_('Published'), 'Field[' . $this->row->id . '][published]', false, JText::_('TOOLTIP PUBLISHED FIELD')); ?>
								</td>
								<td>
									<?php echo MosetsHTML::_('form.booleanlist', 'Field[' . $this->row->id . '][published]', null, $this->row->published); ?>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		<?php }	?>
			
			<div class="pane-sliders">
				<div class="panel">
					<h3 id="options" class="title jpane-toggler-down">
						<span>Search</span>
					</h3>
					<div class="content">
						<table class="paramlist admintable">
							<tr>
								<td class="key">
									<?php echo MosetsHTML::_('form.label', JText::_('Searchable field'), 'Field[' . $this->row->id . '][search]', false, JText::_('TOOLTIP SEARCHABLE FIELD')); ?>
								</td>
								<td>
									<?php echo MosetsHTML::_('form.booleanlist', 'Field[' . $this->row->id . '][search]', null, $this->row->search); ?>
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo MosetsHTML::_('form.label', JText::_('Search caption'), 'Field[' . $this->row->id . '][search_caption]', true); ?>
								</td>
								<td>
									<?php echo MosetsHTML::_('form.input', 'text', 'Field[' . $this->row->id . '][search_caption]', $this->escape($this->row->search_caption), false, array('class' => 'required')); ?>
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo MosetsHTML::_('form.label', JText::_('Search type'), 'Field[' . $this->row->id . '][search_type]', true, JText::_('TOOLTIP SEARCH TYPE')); ?>
								</td>
								<td>
									<?php echo MosetsHTML::_('hotproperty.list.search_types', 'Field[' . $this->row->id . '][search_type]', array('class' => 'required'), $this->row->search_type); ?>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</td>
		
	</tr>
</table>
	

