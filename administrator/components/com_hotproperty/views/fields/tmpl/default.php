<?php
/**
 * @version		$Id: default.php 914 2009-11-11 23:32:02Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Extrafields Template
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

// Toolbar
JToolBarHelper::title(JText::_('Extra Fields Manager'), 'module.png');
JToolBarHelper::addNewX('add', 'Add');
JToolBarHelper::editList('edit', 'Edit');
JToolBarHelper::publishList('publish');
JToolBarHelper::unpublishList('unpublish');
JToolBarHelper::deleteList('Are you sure you want to remove the selected item(s)?', 'remove', 'Remove');

// Allow ordering only when sorting by ordering
$allow_ordering = ($this->get('ordering') == 'Field.ordering');
?>

<form action="<?php echo $this->get('action'); ?>" method="post" name="adminForm">
	
	<!--Filters-->
	<?php
	$filters = $this->get('filters');
	?>
	<table id="filters">
		<tr>
			<!--Search filters-->
			<td width="100%">
				<?php
				if (array_key_exists('search', $filters) && count($filters['search'])) {
					foreach ($filters['search'] as $field => $value)
					{
						echo MosetsHTML::_('form.label', sprintf(JText::_('Filter by %s'), JText::_('caption')), 'filter[search][' . $field . ']');
						echo MosetsHTML::_('filter.search', $field, $this->escape($value));
					}
					echo MosetsHTML::_('form.button', JText::_('Go'), null,  null, array('onclick' => 'this.form.submit();'));
					echo MosetsHTML::_('form.button', JText::_('Reset'), null,  null, array('onclick' => "$$('#filters input, #filters select').setProperty('value', ''); this.form.submit();"));
				}
				?>
			</td>
			<!--List filters-->
			<td nowrap="nowrap">
				<?php echo MosetsHTML::_('grid.state', 'filter[list][Field.published]', $filters['list']['Field.published']); ?>
			</td>
		</tr>
	</table>
	
	<div id="editcell">
		<table class="adminlist">
			<thead>
				<tr>
					<th width="1%">
						<?php echo JText::_('#'); ?>
					</th>
					<th width="1%">
						<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->rows); ?>);" />
					</th>			
					<th>
						<?php echo MosetsHTML::_('grid.sort', 'Caption', 'Field.caption', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th nowrap="nowrap" width="1%">
						<?php echo MosetsHTML::_('grid.sort', 'Published', 'Field.published', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th nowrap="nowrap" width="1%">
						<?php echo MosetsHTML::_('grid.sort', 'Featured', 'Field.featured', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th nowrap="nowrap" width="1%">
						<?php echo MosetsHTML::_('grid.sort', 'Listing', 'Field.listing', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th nowrap="nowrap" width="1%">
						<?php echo MosetsHTML::_('grid.sort', 'Searchable', 'Field.search', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th width="160">
						<?php echo MosetsHTML::_('grid.sort', 'Order', 'Field.ordering', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
						<?php echo MosetsHTML::_('grid.order', $this->rows, $this->get('ordering')); ?>
					</th>
					<th width="11%">
						<?php echo MosetsHTML::_('grid.sort', 'Field Type', 'Field.field_type', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th nowrap="nowrap" width="1%">
						<?php echo MosetsHTML::_('grid.sort', 'ID', 'Field.id', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
				</tr>			
			</thead>
			<tfoot>
				<tr>
					<td colspan="11">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
			<?php
			$k = 0;
			for ($i=0, $n = count($this->rows); $i < $n; $i++)
			{
				$row =& $this->rows[$i];
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td align="center">
						<?php echo $this->pagination->getRowOffset($i); ?>
					</td>
					<td>
						<?php echo MosetsHTML::_('grid.id', $i, $row->id); ?>
					</td>
					<td>
						<a href="#edit" onclick="return listItemTask('cb<?php echo $i; ?>','edit')"><?php echo $this->escape($row->caption); ?></a>
					</td>
					<td align="center">
						<?php echo MosetsHTML::_('grid.published', $row, $i, 'publish_g.png', 'publish_r.png'); ?>
					</td>
					<?php if (!$row->hidden) : ?>
						<td align="center">
							<?php echo MosetsHTML::_('grid.featured', $row, $i); ?>
						</td>
						<td align="center">
							<?php echo MosetsHTML::_('hotproperty.grid.listing', $row, $i); ?>
						</td>
					<?php else : ?>
						<td colspan="2" align="center">
							[ <?php echo JText::_('Hidden'); ?> ]
						</td>
					<?php endif; ?>
					<td align="center">
						<?php echo MosetsHTML::_('hotproperty.grid.searchable', $row, $i); ?>
					</td>
					<td class="order">
						<span><?php echo $this->pagination->orderUpIcon($i, true, 'orderup', 'Move Up', $allow_ordering); ?></span>
						<span><?php echo $this->pagination->orderDownIcon($i, $n, true, 'orderdown', 'Move Down', $allow_ordering); ?></span>
						<input type="text" name="order[]" size="5" value="<?php echo $row->ordering; ?>" <?php echo $allow_ordering ?  '' : 'disabled="disabled"'; ?> class="text_area" style="text-align: center" />
					</td>
					<td>
						<?php if ($row->iscore) : ?>
							<?php echo JText::_('Core'); ?>
						<?php else : ?>
							<?php echo $row->field_type; ?>
						<?php endif; ?>
					</td>
					<td align="center">
						<?php echo $row->id; ?>
					</td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
		</table>
	</div>

	<input type="hidden" name="option" value="com_hotproperty" />
	<input type="hidden" name="controller" value="fields" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="referer" value="<?php echo $this->get('referer'); ?>" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="ordering" value="<?php echo $this->get('ordering'); ?>" />
	<input type="hidden" name="ordering_dir" value="<?php echo $this->get('ordering_dir'); ?>" />
	<?php echo MosetsHTML::_('form.token'); ?>
</form>
