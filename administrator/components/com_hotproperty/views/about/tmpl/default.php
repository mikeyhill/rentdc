<?php
/**
 * @version		$Id: default.php 810 2009-08-31 16:30:37Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * About Template
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

// Toolbar
JToolBarHelper::title(JText::_('About Hot Property'), 'systeminfo.png');

MosetsHTML::_('behavior.switcher');

$hotproperty =& MosetsApplication::getInstance('hotproperty');
?>

<div id="submenu-wrap">
	<div id="submenu-box">
		<div class="t">
			<div class="t">
				<div class="t"></div>
	 		</div>
		</div>
		<div class="m" style="padding:0;">
			<div class="submenu-box">
				<div class="submenu-pad">
					<ul id="submenu" class="information">
						<li>
							<a id="general" class="active"><?php echo JText::_('General'); ?></a>
						</li>
						<li>
							<a id="license"><?php echo JText::_('License'); ?></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="b">
			<div class="b">
	 			<div class="b"></div>
			</div>
		</div>
	</div>
</div>

<form action="<?php echo $this->get('action'); ?>" method="post" name="adminForm">
	<div id="config-document">
		<div id="page-general">
			<table class="adminlist">
				<thead>
					<tr>
						<th colspan="2">
							<?php echo JText::_('General Information'); ?>
						</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th colspan="2">&nbsp;</th>
					</tr>
				</tfoot>
				<tbody>
					<tr>
						<td width="250">
							<strong><?php echo JText::_('Version'); ?></strong>
						</td>
						<td>
							<?php echo $hotproperty->getCfg('version'); ?>
						</td>
					</tr>
					<tr>
						<td>
							<strong><?php echo JText::_('Website'); ?></strong>
						</td>
						<td>
							<a href="http://www.mosets.com">www.mosets.com</a>
						</td>
					</tr>
					<tr>
						<td>
							<strong><?php echo JText::_('Author'); ?></strong>
						</td>
						<td>
							C.Y. Lee
						</td>
					</tr>
					<tr>
						<td>
							<strong><?php echo JText::_('Email'); ?></strong>
						</td>
						<td>
							<a href="mailto:hotproperty@mosets.com">hotproperty@mosets.com</a>
						</td>
					</tr>
					<tr>
						<td>
							<strong><?php echo JText::_('Credits'); ?></strong>
						</td>
						<td>
							<a href="mailto:abernier@mosets.com">Antoine Bernier <abernier@mosets.com></a><br />
							Volunteers and developers of the <a href="http://www.joomla.org/">Joomla!</a> project.
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div id="page-license">
			<table class="adminlist">
				<thead>
					<tr>
						<th>
							<?php echo JText::_('License Agreement'); ?>
						</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>&nbsp;</th>
					</tr>
				</tfoot>
				<tbody>
					<tr>
						<td>
							<pre><?php include JPath::clean(MosetsApplication::getPath('administrator', 'hotproperty') . '/LICENSE'); ?></pre>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	
	<input type="hidden" name="option" value="com_hotproperty" />
	<input type="hidden" name="controller" value="about" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="ordering" value="<?php //echo $this->get('ordering'); ?>" />
	<input type="hidden" name="ordering_dir" value="<?php //echo $this->get('ordering_dir'); ?>" />
	<?php echo MosetsHTML::_('form.token'); ?>
</form>