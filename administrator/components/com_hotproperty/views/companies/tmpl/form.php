<?php
/**
 * @version		$Id: form.php 768 2009-07-20 16:08:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Companies Form Template
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

// Toolbar
$task = JRequest::getCmd('task');
JToolBarHelper::title(sprintf(JText::_('%s <small><small>[%s]</small></small>'), JText::_('Companies'), JText::_($task)), 'addedit.png');
//JToolBarHelper::preview('index.php?option=com_content&id='.$id.'&tmpl=component', true);
JToolBarHelper::save();
if ($task == 'edit')
	JToolBarHelper::apply();
if ($task=="edit") {
	// for existing properties the button is renamed `close`
	JToolBarHelper::cancel('cancel', 'Close');
} else {
	JToolBarHelper::cancel();
}
//JToolBarHelper::help('screen.content.edit');
?>

<form action="<?php echo $this->get('action'); ?>" method="post" name="adminForm" class="form-validate" enctype="multipart/form-data">
	<?php for ($i=0, $n = count($this->rows); $i < $n; $i++)
	{
		$row =& $this->rows[$i];
		
		$this->row = $row;
		if (count($this->rows) > 1) {
			echo '<fieldset>';
				echo '<legend>' . JText::_('Company') . ' : [ #' . $row->id . ' ]</legend>';
		}
		echo $this->loadTemplate('item');
		if (count($this->rows) > 1) {
			echo '</fieldset>';
		}
	}
	?>
	
	<input type="hidden" name="option" value="com_hotproperty" />
	<input type="hidden" name="controller" value="companies" />
	<input type="hidden" name="task" value="" />
	<?php echo MosetsHTML::_('form.token'); ?>
</form>
