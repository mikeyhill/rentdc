<?php
/**
 * @version		$Id: form_item.php 781 2009-08-17 20:50:08Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Companies Form Item Template
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

$hotproperty =& MosetsApplication::getInstance('hotproperty');
?>

<fieldset id="companies<?php echo $this->row->id; ?>">
				
	<legend><?php echo JText::_('General'); ?></legend>
	
	<?php echo MosetsHTML::_('interface.advice', sprintf(JText::_("According to your configuration, company's photo will be resized to %spx."), $hotproperty->getCfg('imgsize_company')), 'info'); ?>
	
	<table class="admintable" width="100%">
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Name'), 'Company[' . $this->row->id . '][name]', true); ?>
			</td>
			<td>
				<?php echo MosetsHTML::_('form.input', 'text', 'Company[' . $this->row->id . '][name]', $this->escape($this->row->name), false, array('class' => 'required', 'maxlength' => '255')); ?>
			</td>
			<td rowspan="8" valign="top" align="right" width="1%" nowrap="nowrap">
				<?php
				if ($this->row->photo) {
					?>
					<div class="photogrid">
						<?php
						echo MosetsHTML::_('hotproperty.image.photo', 'company', $this->row->photo) . '<br />';
						
						echo MosetsHTML::_('form.checkbox', 'Company[' . $this->row->id . '][removephoto]');
						echo MosetsHTML::_('form.label', JText::_('Remove Photo'), 'Company[' . $this->row->id . '][removephoto]');
						?>
					</div>
					<?php
				}	
				?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Address'), 'Company[' . $this->row->id . '][address]'); ?>
			</td>
			<td colspan="2">
				<?php echo MosetsHTML::_('form.input', 'text', 'Company[' . $this->row->id . '][address]', $this->escape($this->row->address)); ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Suburb'), 'Company[' . $this->row->id . '][suburb]'); ?>
			</td>
			<td colspan="2">
				<?php echo MosetsHTML::_('form.input', 'text', 'Company[' . $this->row->id . '][suburb]', $this->escape($this->row->suburb)); ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('State'), 'Company[' . $this->row->id . '][state]'); ?>
			</td>
			<td colspan="2">
				<?php echo MosetsHTML::_('form.input', 'text', 'Company[' . $this->row->id . '][state]', $this->escape($this->row->state)); ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Country'), 'Company[' . $this->row->id . '][country]'); ?>
			</td>
			<td colspan="2">
				<?php echo MosetsHTML::_('form.input', 'text', 'Company[' . $this->row->id . '][country]', $this->escape($this->row->country)); ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Postcode'), 'Company[' . $this->row->id . '][postcode]'); ?>
			</td>
			<td colspan="2">
				<?php echo MosetsHTML::_('form.input', 'text', 'Company[' . $this->row->id . '][postcode]', $this->escape($this->row->postcode)); ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Telephone'), 'Company[' . $this->row->id . '][telephone]'); ?>
			</td>
			<td colspan="2">
				<?php echo MosetsHTML::_('form.input', 'text', 'Company[' . $this->row->id . '][telephone]', $this->escape($this->row->telephone)); ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Fax'), 'Company[' . $this->row->id . '][fax]'); ?>
			</td>
			<td colspan="2">
				<?php echo MosetsHTML::_('form.input', 'text', 'Company[' . $this->row->id . '][fax]', $this->escape($this->row->fax)); ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Email'), 'Company[' . $this->row->id . '][email]'); ?>
			</td>
			<td colspan="2">
				<?php echo MosetsHTML::_('form.input', 'text', 'Company[' . $this->row->id . '][email]', $this->row->email, false, array('class' => 'validate-email')); ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Website'), 'Company[' . $this->row->id . '][website]'); ?>
			</td>
			<td colspan="2">
				<?php echo MosetsHTML::_('form.input', 'text', 'Company[' . $this->row->id . '][website]', $this->row->website); ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Photo'), 'Company[' . $this->row->id . '][photo]'); ?>
			</td>
			<td colspan="2">
				<?php echo MosetsHTML::_('form.input', 'file', 'Company[' . $this->row->id . '][photo]', $this->row->photo); ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Description'), 'Company[' . $this->row->id . '][desc]'); ?>
			</td>
			<td colspan="2">
				<?php echo MosetsHTML::_('form.wysiwyg', 'Company[' . $this->row->id . '][desc]', $this->row->desc, $width = '100%', $height = '400', $col = '70', $row = '15', $buttons = true, $params = array('pagebreak', 'readmore')); ?>
			</td>
		</tr>
	</table>
	
</fieldset>
