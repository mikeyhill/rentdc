<?php
/**
 * @version		$Id: view.html.php 810 2009-08-31 16:30:37Z abernier $
 * @package		Hotproperty
 * @subpackage	View
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Search Logs View
 *
 * @package		Hotproperty
 * @subpackage	View
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyViewLogSearches extends HotpropertyView
{
	/**
	 * Constructor
	 * 
	 * @access	public
	 * @param	array	Associative configuration array
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'LogSearches',
			'name_singular' => 'LogSearch'
		), $config));
	}
	
	/**
	 * Display default layout
	 * 
	 * @return void
	 */
	function displayDefault()
	{
		$this->setOrdering('LogSearch.hits', 'DESC');
		
		$rows = $this->get('data', null, array('all', array(
			'recursive'	=> 1,
			'order' => array($this->get('ordering') => $this->get('ordering_dir'))
		)));
		
		$this->assignRef('rows', $rows);
	}
}
