<?php
/**
 * @version		$Id: view.html.php 898 2009-11-02 03:31:05Z abernier $
 * @package		Hotproperty
 * @subpackage	View
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Agents View
 *
 * @package		Hotproperty
 * @subpackage	View
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyViewAgents extends HotpropertyView
{
	/**
	 * Constructor
	 * 
	 * @access	public
	 * @param	array	Associative configuration array
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Agents',
			'name_singular' => 'Agent'
		), $config));
		
		echo MosetsHTML::_('filesystem.iswritable', array(MosetsApplication::getPath('media_images_agent', 'hotproperty')));
	}
	
	/**
	 * Display default layout
	 * 
	 * @return void
	 **/
	function displayDefault()
	{
		$this->setFilters(array(
			'search' => array(
				'Agent.name' => ''
			),
			'list' => array(
				'Agent.company'	=> '',
				'Agent.user'	=> ''
			)
		));
		$this->setOrdering('Agent.name');
		
		$rows = $this->get('data', null, array('all', array(
			'where' => $this->getFiltersConditions(),
			'order' => array($this->get('ordering') => $this->get('ordering_dir')),
			'limit' => $this->get('limit'),
			'limitstart' => $this->get('limitstart'),
			'contain' => array(
				'Company' => array(),
				'User' => array(),
				'Property' => array()
			)
		)));
		$this->assignRef('rows', $rows);
	}
	
	/**
	 * Display form layout
	 * 
	 * @return void
	 */
	function displayForm()
	{
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		$ids = JRequest::getVar('id', array(), 'default', 'array');
		if (empty($ids)) {
			$rows = $this->get('data', null, array('new'));
		} else {
			$rows = $this->get('data', null, array('all', array(
				'where'	=> array('Agent.' . $this->get('primaryKey', null) . ' IN' => $ids),
				'order' => array($this->get('ordering') => $this->get('ordering_dir'))
			)));
		}
		
		$this->assignRef('rows', $rows);
	}
}
?>