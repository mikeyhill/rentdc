<?php
/**
 * @version		$Id: form_item.php 782 2009-08-18 10:23:23Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Agents Form Item Template
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

$hotproperty =& MosetsApplication::getInstance('hotproperty');
?>

<fieldset id="agents<?php echo $this->row->id; ?>">
				
	<legend><?php echo JText::_('General'); ?></legend>
	
	<?php echo MosetsHTML::_('interface.advice', sprintf(JText::_("According to your configuration, agent's photo will be resized to %spx."), $hotproperty->getCfg('imgsize_agent')), 'info'); ?>
	
	<table class="admintable" width="100%">
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Name'), 'Agent[' . $this->row->id . '][name]', true); ?>
			</td>
			<td>
				<?php echo MosetsHTML::_('form.input', 'text', 'Agent[' . $this->row->id . '][name]', $this->escape($this->row->name), false, array('class' => 'required', 'maxlength' => '255')); ?>
			</td>
			<td rowspan="8" valign="top" align="right" width="1%" nowrap="nowrap">
				<div class="photogrid">
					<?php
					if ($this->row->id) {
						echo MosetsHTML::_('hotproperty.image.photo', 'agent', $this->row->photo) . '<br />';
						if ($this->row->photo) {
							echo MosetsHTML::_('form.checkbox', 'Agent[' . $this->row->id . '][removephoto]');
							echo MosetsHTML::_('form.label', JText::_('Remove Photo'), 'Agent[' . $this->row->id . '][removephoto]');
						}
					}
					?>
				</div>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Company'), 'Agent[' . $this->row->id . '][company]', true); ?>
			</td>
			<td colspan="2">
				<?php echo MosetsHTML::_('hotproperty.list.companies', 'Agent[' . $this->row->id . '][company]', array('class' => 'required'), $this->row->company); ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('User'), 'Agent[' . $this->row->id . '][user]'); ?>
			</td>
			<td colspan="2">
				<?php echo MosetsHTML::_('hotproperty.list.users', 'Agent[' . $this->row->id . '][user]', false, $this->row->user, false, false, true); ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Need Approval'), 'Agent[' . $this->row->id . '][need_approval]'); ?>
			</td>
			<td colspan="2">
				<?php echo MosetsHTML::_('form.booleanlist', 'Agent[' . $this->row->id . '][need_approval]', null, $this->row->need_approval); ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Email'), 'Agent[' . $this->row->id . '][email]', true); ?>
			</td>
			<td colspan="2">
				<?php echo MosetsHTML::_('form.input', 'text', 'Agent[' . $this->row->id . '][email]', $this->escape($this->row->email), false, array('class' => 'required validate-email')); ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Mobile number'), 'Agent[' . $this->row->id . '][mobile]'); ?>
			</td>
			<td colspan="2">
				<?php echo MosetsHTML::_('form.input', 'text', 'Agent[' . $this->row->id . '][mobile]', $this->escape($this->row->mobile)); ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Photo'), 'Agent[' . $this->row->id . '][photo]'); ?>
			</td>
			<td colspan="2">
				<?php echo MosetsHTML::_('form.input', 'file', 'Agent[' . $this->row->id . '][photo]', $this->row->photo); ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo MosetsHTML::_('form.label', JText::_('Description'), 'Agent[' . $this->row->id . '][desc]'); ?>
			</td>
			<td colspan="2">
				<?php echo MosetsHTML::_('form.wysiwyg', 'Agent[' . $this->row->id . '][desc]', $this->row->desc, $width = '100%', $height = '400', $col = '70', $row = '15', $buttons = true, $params = array('pagebreak', 'readmore')); ?>
			</td>
		</tr>
	</table>
	
</fieldset>
