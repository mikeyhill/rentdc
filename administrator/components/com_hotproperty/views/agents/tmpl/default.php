<?php
/**
 * @version		$Id: default.php 914 2009-11-11 23:32:02Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Agents Template
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

// Toolbar
JToolBarHelper::title(JText::_('Agent Manager'), 'user.png');
JToolBarHelper::addNewX('add', JText::_('Add'));
JToolBarHelper::editList('edit', 'Edit');
JToolBarHelper::deleteList('Are you sure you want to remove the selected item(s)?', 'remove', 'Remove');
?>

<form action="<?php echo $this->get('action'); ?>" method="post" name="adminForm">
	
	<!--Filters-->
	<?php
	$filters = $this->get('filters');
	?>
	<table id="filters">
		<tr>
			<!--Search filters-->
			<td width="100%">
				<?php
				if (array_key_exists('search', $filters) && count($filters['search'])) {
					foreach ($filters['search'] as $field => $value)
					{
						echo MosetsHTML::_('form.label', sprintf(JText::_('Filter by %s'), JText::_('name')), 'filter[search][' . $field . ']');
						echo MosetsHTML::_('filter.search', $field, $this->escape($value));
					}
					echo MosetsHTML::_('form.button', JText::_('Go'), null,  null, array('onclick' => 'this.form.submit();'));
					echo MosetsHTML::_('form.button', JText::_('Reset'), null,  null, array('onclick' => "$$('#filters input, #filters select').setProperty('value', ''); this.form.submit();"));
				}
				?>
			</td>
			<!--List filters-->
			<td nowrap="nowrap">
				<?php
				echo MosetsHTML::_('hotproperty.list.companies', 'filter[list][Agent.company]', array('onchange' => 'document.adminForm.submit();'), $filters['list']['Agent.company']);
				echo MosetsHTML::_('hotproperty.list.users', 'filter[list][Agent.user]', array('onchange' => 'document.adminForm.submit();'), $filters['list']['Agent.user']);
				?>
			</td>
		</tr>
	</table>
	
	<div id="editcell">
		<table class="adminlist">
			<thead>
				<tr>
					<th width="1%">
						<?php echo JText::_('#'); ?>
					</th>
					<th width="1%">
						<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->rows); ?>);" />
					</th>
					<th>
						<?php echo MosetsHTML::_('grid.sort', 'Name', 'Agent.name', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th width="11%">
						<?php echo MosetsHTML::_('grid.sort', 'Company', 'Agent.company', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th width="11%">
						<?php echo MosetsHTML::_('grid.sort', 'User', 'User.name', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th nowrap="nowrap" width="1%">
						<?php echo '# ' . JText::_('Properties'); ?>
						<?php //echo MosetsHTML::_('grid.sort', '# Properties', 'properties', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th nowrap="nowrap" width="1%">
						<?php echo '# ' . JText::_('Hits'); ?>
						<?php //echo MosetsHTML::_('grid.sort', '# Hits', 'hits', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th nowrap="nowrap" width="1%">
						<?php echo MosetsHTML::_('grid.sort', 'ID', 'Agent..id', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
				</tr>			
			</thead>
			<tfoot>
				<tr>
					<td colspan="8">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
			<?php
			$k = 0;
			for ($i=0, $n = count($this->rows); $i < $n; $i++)
			{
				$row =& $this->rows[$i];
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td align="center">
						<?php echo $this->pagination->getRowOffset($i); ?>
					</td>
					<td>
						<?php echo MosetsHTML::_('grid.id', $i, $row->id); ?>
					</td>
					<td>
						<?php
						// Has this agent a photo ?
						if ($row->photo) {
							echo MosetsHTML::_('image', MosetsApplication::getPath('assets_images_url', 'hotproperty') . 'photo.png', JText::_('This agent has an image defined.'), array('align' => 'absmiddle'));
						}
						?>
						<a href="#edit" onclick="return listItemTask('cb<?php echo $i; ?>','edit')"><?php echo $this->escape($row->name); ?></a>
					</td>
					<td>
						<a href="<?php echo JRoute::_('index.php?option=com_hotproperty&view=companies&task=edit&id[]=' . $row->company . '&referer=' . $this->get('referer')); ?>"><?php echo $this->escape($row->Company->name); ?></a>
					</td>
					<td>
						<?php if (!empty($row->User->name)) : ?>
							<a href="<?php echo JRoute::_('index.php?option=com_users&view=user&task=edit&cid[]=' . $row->User->id . '&referer=' . $this->get('referer')); ?>"><?php echo $this->escape($row->User->name); ?></a>
						<?php endif; ?>
					</td>
					<td align="center">
						<?php
						$count_properties = 0;
						if (!empty($row->Property))
							$count_properties = count($row->Property);
						echo $count_properties;
						?>
					</td>
					<td align="center">
						<?php
						// @todo	implement 'count' with containable
						$count_hits = 0;
						if (!empty($row->Property)) {
							foreach ($row->Property as $property)
							{
								$count_hits += $property->hits;
							}
						}
						echo $count_hits;
						?>
					</td>
					<td align="center">
						<?php echo $row->id; ?>
					</td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
		</table>
	</div>

	<input type="hidden" name="option" value="com_hotproperty" />
	<input type="hidden" name="controller" value="agents" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="referer" value="<?php echo $this->get('referer'); ?>" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="ordering" value="<?php echo $this->get('ordering'); ?>" />
	<input type="hidden" name="ordering_dir" value="<?php echo $this->get('ordering_dir'); ?>" />
	<?php echo MosetsHTML::_('form.token'); ?>
</form>
