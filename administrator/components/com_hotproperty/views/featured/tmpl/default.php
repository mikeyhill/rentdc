<?php
/**
 * @version		$Id: default.php 913 2009-11-10 22:14:48Z abernier $
 * @package		Hotproperty
 * @subpackage	Template
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Featured Template
 *
 * @package		Hotproperty
 * @subpackage	Template
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

// Toolbar
JToolBarHelper::title(JText::_('Featured Properties'), 'frontpage.png');
JToolBarHelper::publishList('publish');
JToolBarHelper::unpublishList('unpublish');
JToolBarHelper::trash('remove', 'Unfeature');

// Allow ordering
$allow_ordering = ($this->get('ordering') == 'Featured.ordering');
?>

<form action="<?php echo $this->get('action'); ?>" method="post" name="adminForm">
	
	<!--Filters-->
	<?php
	$filters = $this->get('filters');
	?>
	<table id="filters">
		<tr>
			<!--Search filters-->
			<td width="100%">
				<?php
				if (array_key_exists('search', $filters) && count($filters['search'])) {
					foreach ($filters['search'] as $field => $value)
					{
						echo MosetsHTML::_('form.label', sprintf(JText::_('Filter by %s'), JText::_('name')), 'filter[search][' . $field . ']');
						echo MosetsHTML::_('filter.search', $field, $this->escape($value));
					}
					echo MosetsHTML::_('form.button', JText::_('Go'), null,  null, array('onclick' => 'this.form.submit();'));
					echo MosetsHTML::_('form.button', JText::_('Reset'), null,  null, array('onclick' => "$$('#filters input, #filters select').setProperty('value', ''); this.form.submit();"));
				}
				?>
			</td>
		</tr>
	</table>
	
	<div id="editcell">
		<table class="adminlist">
			<thead>
				<tr>
					<th width="1%">
						<?php echo JText::_('#'); ?>
					</th>
					<th width="1%">
						<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->rows); ?>);" />
					</th>			
					<th>
						<?php echo MosetsHTML::_('grid.sort', 'Name', 'Property.name', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th nowrap="nowrap" width="1%">
						<?php echo MosetsHTML::_('grid.sort', 'Published', 'Property.published', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th width="100">
						<?php echo MosetsHTML::_('grid.sort', 'Order', 'Featured.ordering', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
						<?php echo MosetsHTML::_('grid.order', $this->rows, $this->get('ordering')); ?>
					</th>
					<th width="11%">
						<?php echo MosetsHTML::_('grid.sort', 'Type', 'Type.name', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th width="11%">
						<?php echo MosetsHTML::_('grid.sort', 'Agent', 'Agent.name', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th width="11%">
						<?php echo MosetsHTML::_('grid.sort', 'Location', 'Property.suburb', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th nowrap="nowrap" width="10">
						<?php echo MosetsHTML::_('grid.sort', 'Date', 'Property.created', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th nowrap="nowrap" width="1%">
						<?php echo MosetsHTML::_('grid.sort', 'Hits', 'Property.hits', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
					<th nowrap="nowrap" width="1%">
						<?php echo MosetsHTML::_('grid.sort', 'ID', 'Property.id', @$this->get('ordering_dir'), @$this->get('ordering')); ?>
					</th>
				</tr>			
			</thead>
			<tfoot>
				<tr>
					<td colspan="11">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
			<?php
			$k = 0;
			for ($i=0, $n = count($this->rows); $i < $n; $i++)
			{
				$row =& $this->rows[$i];
				
				$link		= JRoute::_('index.php?option=com_hotproperty&controller=properties&task=edit&id[]='.$row->Property->id);

				?>
				<tr class="<?php echo "row$k"; ?>">
					<td align="center">
						<?php echo $this->pagination->getRowOffset($i); ?>
					</td>
					<td>
						<?php echo MosetsHTML::_('grid.id', $i, $row->Property->id); ?>
					</td>
					<td>
						<?php
						$href = '';
						// Has this property photo(s) ?
						if (property_exists($row->Property, 'photos')) {
							$href .= MosetsHTML::_('image', MosetsApplication::getPath('assets_images_url', 'hotproperty') . 'photo.png', JText::_('This property has an image defined.'), array('align' => 'absmiddle'));
						}
						// Normal property
						if ($row->Property->published >= 0) {
							$href .= '
							<a href="' . JRoute::_('index.php?option=com_hotproperty&view=properties&task=edit&id[]=' . $row->Property->id . '&referer=' . $this->get('referer')) . '">' . $this->escape($row->Property->name) . '</a>'
							;
						// Archived property
						} else {
							$href .= $this->escape($row->Property->name) . ' [ ' . JText::_('Archived') . ' ]';
						}

						echo $href;
						?>
					</td>
					<td align="center">
						<?php echo MosetsHTML::_('grid.published', $row->Property, $i); ?>
					</td>
					<td class="order">
						<span><?php echo $this->pagination->orderUpIcon($i, true, 'orderup', 'Move Up', $allow_ordering); ?></span>
						<span><?php echo $this->pagination->orderDownIcon($i, $n, true, 'orderdown', 'Move Down', $allow_ordering); ?></span>
						<input type="text" name="order[]" size="5" value="<?php echo $row->ordering; ?>" <?php echo $allow_ordering ?  '' : 'disabled="disabled"'; ?> class="text_area" style="text-align: center" />
					</td>
					<td>
						<a href="<?php echo JRoute::_('index.php?option=com_hotproperty&view=types&task=edit&id[]=' . $row->Property->Type->id . '&referer=' . $this->get('referer')); ?>"><?php echo $this->escape($row->Property->Type->name); ?></a>
					</td>
					<td>
						<a href="<?php echo JRoute::_('index.php?option=com_hotproperty&view=agents&task=edit&id[]=' . $row->Property->Agent->id . '&referer=' . $this->get('referer')); ?>"><?php echo $this->escape($row->Property->Agent->name); ?></a>
					</td>
					<td>
						<?php echo (!empty($row->Property->suburb) && !empty($row->Property->state)) ? $this->escape($row->Property->suburb) . ", " . $this->escape($row->Property->state) : $this->escape($row->Property->suburb) . $this->escape($row->Property->state); ?>
					</td>
					<td align="center">
						<?php echo MosetsHTML::_('date',  $row->Property->created, JText::_('DATE_FORMAT_LC4')); ?>
					</td>
					<td align="center">
						<?php echo $row->Property->hits; ?>
					</td>
					<td align="center">
						<?php echo $row->Property->id; ?>
					</td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
		</table>
	</div>
	<?php MosetsHTML::_('interface.legend'); ?>

	<input type="hidden" name="option" value="com_hotproperty" />
	<input type="hidden" name="controller" value="featured" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="ordering" value="<?php echo $this->get('ordering'); ?>" />
	<input type="hidden" name="ordering_dir" value="<?php echo $this->get('ordering_dir'); ?>" />
	<?php echo MosetsHTML::_('form.token'); ?>
</form>
