<?php
/**
 * @version		$Id: view.html.php 772 2009-08-09 20:07:40Z abernier $
 * @package		Hotproperty
 * @subpackage	View
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Featured View
 *
 * @package		Hotproperty
 * @subpackage	View
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyViewFeatured extends HotpropertyView
{
	/**
	 * Constructor
	 * 
	 * @access	public
	 * @param	array	Associative configuration array
	 * @return	void
	 */
	function __construct($config = array())
	{
		parent::__construct(MosetsArrayHelper::array_merge_recursive_unique(array(
			'name' => 'Featured',
			'name_singular' => 'Featured'
		), $config));
	}
	
	/**
	 * Display default layout
	 * 
	 * @return void
	 **/
	function displayDefault()
	{
		$this->setOrdering('Featured.ordering');
		
		$rows = $this->get('data', null, array('all', array(
			'where' => $this->getFiltersConditions(),
			'order' => array($this->get('ordering') => $this->get('ordering_dir')),
			'limit' => $this->get('limit'),
			'limitstart' => $this->get('limitstart'),
			'contain' => array(
				'Property' => array(
					'contain' => array(
						'Type' => array(),
						'Agent' => array()
					)
				)
			)
		)));
		$this->assignRef('rows', $rows);
	}
}
?>
