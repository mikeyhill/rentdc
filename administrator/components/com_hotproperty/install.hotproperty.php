<?php
/**
 * @version		$Id: install.hotproperty.php 817 2009-09-04 11:17:09Z abernier $
 * @package		Hotproperty
 * @copyright	(C) 2009 Mosets Consulting	
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

// Get the contents of the file
jimport('joomla.filesystem.file');
$configFile = JFile::read(JPath::clean(JPATH_ADMINISTRATOR . '/components/com_hotproperty/config.xml'));

$xml		=& JFactory::getXMLParser('Simple');
$configData	= array('params' => array());
if ($xml->loadString($configFile)) {
	if ($params =& $xml->document->_children[0]->_children) {
		foreach ($params as $param)
		{
			$key = $param->_attributes['name'];
			$value = (strpos($param->_attributes['default'], '|') ? explode('|', $param->_attributes['default']) : $param->_attributes['default']);
			
			$configData['params'][$key] = $value;
		}
	}
}

$table =& JTable::getInstance('component');
$table->loadByOption('com_hotproperty');
$table->bind($configData);
$table->store();
?>

<style type="text/css">
#element-box th {display:none;}
</style>
<img src="<?php echo JURI::root() . 'administrator/components/com_hotproperty/assets/images/logo.png'; ?>" alt="Hot Property" style="float:left;border-right:1px solid #ccc;padding-right:15px;" />
<div style="margin-left:184px;">
	<h2 style="margin-bottom:0;">Hot Property v<?php echo $configData['params']['version']; ?></h2>
	<strong>A Joomla! Listing Solutions (Real Estates, Auto, Books, Cds etc.)</strong>
	<br /><br />
	&copy; Copyright 2004-<?php echo date('Y'); ?> by Mosets Consulting. <a href="http://www.mosets.com/">www.mosets.com</a><br />
	<input type="button" value="Go to Hot Property now" onclick="location.href='index.php?option=com_hotproperty'" style="margin-top:7px;cursor:pointer;" />
</div>

<?php
return true;
?>
