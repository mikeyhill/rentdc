<?php
/**
 * @version		$Id: types.php 768 2009-07-20 16:08:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Table
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Types Table
 *
 * @package		Hotproperty
 * @subpackage	Table
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 * @todo		Rename the #__hp_prop_types table into #__hp_types
 */

class HotpropertyTableTypes extends HotpropertyTable
{
	var $id = null;
	var $name = null;
	var $desc = null;
	var $published = null;
	var $ordering = null;

	/**
	 * Constructor
	 *
	 * @access protected
	 * @param	An optional associative array of configuration settings.
	 * @param	JDatabase object
	 * @return	void
	 */
	function __construct(&$db, $config = array())
	{
		parent::__construct($db, MosetsArrayHelper::array_merge_recursive_unique(array(
			'name'			=> 'Types',
			'name_singular'	=> 'Type',
			'tbl'			=> 'prop_types',
			'use_table'		=> true
		), $config));
	}
	
	/**
	 * Initiate row fields to their default values
	 * 
	 * Overload HotpropertyTable::reset()
	 * 
	 * @access	public
	 * @return	boolean	True if success
	 */
	function defaultValues()
	{
		$this->set('published', 1);
		
		return true;
	}
}
?>
