<?php
/**
 * @version		$Id: featured.php 768 2009-07-20 16:08:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Table
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Featured Table
 *
 * @package		Hotproperty
 * @subpackage	Table
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyTableFeatured extends HotpropertyTable
{
	var $property = null;
	var $ordering = null;

	/**
	 * Constructor
	 *
	 * @access protected
	 * @param	An optional associative array of configuration settings.
	 * @param	JDatabase object
	 * @return	void
	 */
	function __construct(&$db, $config = array())
	{
		parent::__construct($db, MosetsArrayHelper::array_merge_recursive_unique(array(
			'name'			=> 'Featured',
			'name_singular'	=> 'Featured',
			'tbl_key'		=> 'property',
			'use_table'		=> true
		), $config));
	}
	
	/**
	 * Add a row featured property
	 * 
	 * @access	public
	 * @param	int		The ID property to add
	 * @return	boolean	True on success
	 */
	function add($property)
	{
		$query = "INSERT INTO #__hp_featured"
			. "\nVALUES ('$property','-9999')"
			;
		$this->_db->setQuery($query);
		
		if (!$this->_db->query()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		
		return true;
	}
	
	/**
	 * Generic method to call just before data be deleted
	 * 
	 * Overload HotpropertyTable::beforeDelete() to set the associated property to 'featured'=0
	 *
	 * @access	public
	 * @return	boolean	True if success
	 */
	function beforeDelete()
	{
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		$property = $hotproperty->getTable('Properties');
		$property->load($this->property);
		
		$property->set('featured', 0);
		
		$property->store();
		
		return true;
	}
}
?>
