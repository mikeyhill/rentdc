<?php
/**
 * @version		$Id: users.php 768 2009-07-20 16:08:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Table
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Users Table
 * 
 * Dummy table : just require the JTableUser
 *
 * @package		Hotproperty
 * @subpackage	Table
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

require_once(JPATH_LIBRARIES . DS . 'joomla' . DS . 'database' . DS . 'table' . DS . 'user.php');

class HotpropertyTableUsers extends JTableUser
{
	/**
	 * The name of this table
	 *
	 * @var		string
	 * @access	protected
	 */
	var $_name = 'Users';
	
	/**
	 * The singular name of this table
	 *
	 * @var		string
	 * @access	protected
	 */
	var $_nameSingular = 'User';
}
