<?php
/**
 * @version		$Id: fields.php 768 2009-07-20 16:08:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Table
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Extrafields Table
 *
 * @package		Hotproperty
 * @subpackage	Table
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 * @todo		Rename the #__hp_prop_ef table into #__hp_fields
 */

class HotpropertyTableFields extends HotpropertyTable
{
	var $id = null;
	var $field_type = null;
	var $name = null;
	var $caption = null;
	var $default_value = null;
	var $size = null;
	var $field_elements = null;
	var $prefix_text = null;
	var $append_text = null;
	var $hidden = 0;
	var $ordering = null;
	var $published = 0;
	var $featured = 0;
	var $listing = 0;
	var $hideCaption = 0;
	var $iscore = 0;
	var $search = 0;
	var $search_caption = null;
	var $search_type = null;
	
	/**
	 * Constructor
	 *
	 * @access protected
	 * @param	An optional associative array of configuration settings.
	 * @param	JDatabase object
	 * @return	void
	 */
	function __construct(&$db, $config = array())
	{
		parent::__construct($db, MosetsArrayHelper::array_merge_recursive_unique(array(
			'name'			=> 'Fields',
			'name_singular'	=> 'Field',
			'tbl'			=> 'prop_ef',
			'use_table'		=> true
		), $config));
	}
	
	/**
	 * Initiate row fields to their default values
	 * 
	 * Overload HotpropertyTable::reset()
	 * 
	 * @access	public
	 * @return	boolean	True if success
	 */
	function defaultValues()
	{		
		$this->set('published', 1);
		
		return true;
	}
	
	/**
	 * Method to call just before data be stored
	 * 
	 * Overload HotpropertyTable::beforeStore()
	 *
	 * @access	public
	 * @return	boolean	True if success
	 */
	function beforeStore($data, $files = null)
	{
		// Generate a name for new extrafields (only for new ones!)
		if ($this->id <= 0)
			$this->set('name', strtolower(JFilterInput::clean($this->caption, 'alnum')));
		
		return true;
	}
	
	/**
	 * Set an extrafield to appear in listing
	 * 
	 * @access	public
	 * @return	boolean
	 */
	function listing($listing = 1)
	{
		$this->listing = $listing;
		
		return true;
	}
	
	/**
	 * Set an extrafield be searchable
	 * 
	 * @access	public
	 * @return	boolean
	 */
	function searchable($searchable = 1)
	{
		$this->search = $searchable;
		
		return true;
	}
	
	/**
	 * Check if the extrafield can be deleted
	 * 
	 * @access	public
	 * @return	boolean
	 */
	function beforeDelete()
	{		
		// Check the extrafield isn't Core
		if ($this->iscore) {
			$this->setError(sprintf(JText::_('<em>%s</em> cannot be removed since it is part of Core.'), $this->name, count($properties)));
			return false;
		}
		
		return true;
	}
}
?>