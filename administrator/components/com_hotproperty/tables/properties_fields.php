<?php
/**
 * @version		$Id: properties_fields.php 787 2009-08-21 15:53:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Table
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Mapping Properties Table
 *
 * @package		Hotproperty
 * @subpackage	Table
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyTablePropertiesFields extends HotpropertyTable
{
	var $id = null;
	var $property = null;
	var $field = null;
	var $value = null;
	
	/**
	 * Constructor
	 *
	 * @access protected
	 * @param	An optional associative array of configuration settings.
	 * @param	JDatabase object
	 * @return	void
	 */
	function __construct(&$db, $config = array())
	{
		parent::__construct($db, MosetsArrayHelper::array_merge_recursive_unique(array(
			'name'			=> 'PropertiesFields',
			'name_singular'	=> 'PropertyField',
			'tbl'			=> 'properties2',
			'use_table'		=> true
		), $config));
	}
	
	/**
	 * Binds a named array/hash to this object
	 *
	 * Overload JTable::bind to implode multiple values with '|'
	 *
	 * @access	public
	 * @param	$from	mixed	An associative array or object
	 * @param	$ignore	mixed	An array or space separated list of fields not to bind
	 * @return	boolean
	 */
	function bind($data, $ignore=array())
	{
		if (array_key_exists('value', $data)) {
			if (is_array($data['value'])) {
				$data['value'] = implode('|', array_filter($data['value']));
			}
		} else {
			$data['value'] = '';	// If 'value' is not set (for example when no checkbox list option is checked), set it to '' so the empty value will be bind (@see JTable::bind line 208)
		}
			
		return parent::bind($data, $ignore);
	}
}
?>