<?php
/**
 * @version		$Id: photos.php 777 2009-08-14 14:15:20Z abernier $
 * @package		Hotproperty
 * @subpackage	Table
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Photos Table
 *
 * @package		Hotproperty
 * @subpackage	Table
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyTablePhotos extends HotpropertyTable
{
	var $id = null;
	var $property = null;
	var $original = null;
	var $standard = null;
	var $thumb = null;
	var $title = null;
	var $desc = null;
	var $ordering = null;

	/**
	 * Constructor
	 *
	 * @access protected
	 * @param	An optional associative array of configuration settings.
	 * @param	JDatabase object
	 * @return	void
	 */
	function __construct(&$db, $config = array())
	{
		mimport('mosets.utilities.image');
		
		parent::__construct($db, MosetsArrayHelper::array_merge_recursive_unique(array(
			'name'			=> 'Photos',
			'name_singular'	=> 'Photo',
			'use_table'		=> true
		), $config));
	}
	
	/**
	 * Save method
	 * 
	 * Overload HotpropertyTable::save in order to check if the photo should be deleted
	 * 
	 * @access	public
	 * @param	int		Primary key value
	 * @param	array	Array of datas
	 * @return	boolean	The new primary key value or false if it fails
	 */
	function save($oid, $data, $files = null)
	{
		if (array_key_exists('remove', $data) && $data['remove']) {
			return $this->remove($oid);
		}
		
		// Ignore New photo that haven't a file
		if ($oid <= 0 && empty($files)) {
			return true;
		}
		
		return parent::save($oid, $data, $files);
	}
	
	/**
	 * Generic method to call just before data be stored
	 * 
	 * Overload HotpropertyTable::beforeStore() to upload the photo and make sure there is a title
	 *
	 * @access	public
	 * @return	boolean	True if success
	 */
	function beforeStore($data, $files = null)
	{
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		/**
		 * Upload the photo
		 */
		
		$file = $files['original'];
		
		if (!empty($file)) {
			if (empty($this->standard)) {
				// Add original
				if ($hotproperty->getCfg('img_saveoriginal')) {
					if (!$this->_addPhoto($file, 'original')) {
						return false;
					}
				}
				// Add standard
				if (!$this->_addPhoto($file, 'standard')) {
					return false;
				}
				// Add thumbnail
				if (!$this->_addPhoto($file, 'thumb')) {
					return false;
				}
			} else {
				// Edit original
				if ($hotproperty->getCfg('img_saveoriginal')) {
					if (!$this->_editPhoto($file, 'original')) {
						return false;
					}
				}
				// Edit standard
				if (!$this->_editPhoto($file, 'standard')) {
					return false;
				}
				// Edit thumbnail
				if (!$this->_editPhoto($file, 'thumb')) {
					return false;
				}
			}
		}
		
		/**
		 * Set default title is none provided
		 */
		
		if (empty($this->title))
			$this->set('title', $files['original']['name']);
			
		return true;
	}
	
	/**
	 * Generic method to call just before data be deleted
	 * 
	 * Overload HotpropertyTable::beforeDelete() to physically remove photos before deleting DB's records
	 *
	 * @access	public
	 * @return	boolean	True if success
	 */
	function beforeDelete()
	{
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		// Remove original
		if ($hotproperty->getCfg('img_saveoriginal')) {
			if (!$this->_removePhoto('original')) {
				return false;
			}
		}
		// Remove standard
		if (!$this->_removePhoto('standard')) {
			return false;
		}
		// Remove thumbnail
		if (!$this->_removePhoto('thumb')) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Upload and set a photo
	 * 
	 * @access	private
	 * @param	array	A indexed file array, eg : array('name' => , 'type' => , 'tmp_name' => , 'error' => , 'size' =>)
	 * @param	string	The table field the photo refers to, eg : original|standard|thumb
	 * @return	boolean	True if success
	 */
	function _addPhoto($file, $field)
	{
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		if (!empty($file)) {
			if (in_array($field, array('original', 'standard', 'thumb'))) {
				if (!empty($this->property) && $this->property > 0) {
					switch ($field)
					{
						case 'original':
							$directory = MosetsApplication::getPath('media_images_original', 'hotproperty');
							break;
						case 'standard':
							$directory = MosetsApplication::getPath('media_images_standard', 'hotproperty');
							break;
						case 'thumb':
							$directory = MosetsApplication::getPath('media_images_thumbnail', 'hotproperty');
							break;
					}
					// Instantiate the image and set its properties
					$image = new MosetsImage($file, $directory);
					$image->setProperties(array(
						'imageName'	=> $this->property . MosetsApplication::getPath('media_images_connector', 'hotproperty') . $file['name'],
						'method'	=> $hotproperty->getCfg('img_method'),
						'size'		=> $hotproperty->getCfg('imgsize_' . $field),
						'quality'	=> $hotproperty->getCfg('quality_photo'),
						'square'	=> false
					));

					if (!$image->resize()) {
						$this->setError($image->getError());
						return false;
					}

					if (!$image->saveToDirectory()) {
						$this->setError($image->getError());
						return false;
					}

					$this->set($field, $image->imageName);	// Since MosetsImage could have rename the photo's name, let's use $image->imageName as photo's name
				} else {
					$this->setError(get_class($this) . "::_addPhoto() : Cannot add the photo until 'property' is not set correctly.");
					return false;
				}
			} else {
				$this->setError(get_class($this) . "::_addPhoto() : Photo refers to an unknown field.");
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Remove a photo
	 * 
	 * @access	private
	 * @param	string
	 * @return	boolean	True if success
	 */
	function _removePhoto($field)
	{
		if (in_array($field, array('original', 'standard', 'thumb'))) {
			switch ($field)
			{
				case 'original':
					$directory = MosetsApplication::getPath('media_images_original', 'hotproperty');
					break;
				case 'standard':
					$directory = MosetsApplication::getPath('media_images_standard', 'hotproperty');
					break;
				case 'thumb':
					$directory = MosetsApplication::getPath('media_images_thumbnail', 'hotproperty');
					break;
			}
			// Instantiate the image and set its properties
			$image = new MosetsImage();
			$image->setProperties(array(
				'imageName'	=> $this->{$field},
				'directory'	=> $directory
			));
			echo "<pre>";
			print_r($image);
			echo "</pre>";

			if (!$image->removeFromDirectory()) {
				$this->setError($image->getError());
				return false;
			}

			$this->set($field, '');
		} else {
			$this->setError(get_class($this) . "::_removePhoto() : Photo refers to an unknown field.");
			return false;
		}
		
		return true;
	}
	
	/**
	 * Edit a photo
	 * 
	 * @access	private
	 * @param	array	A indexed file array, eg : array('name' => , 'type' => , 'tmp_name' => , 'error' => , 'size' =>)
	 * @param	string	
	 * @return	boolean	True if success
	 */
	function _editPhoto($file, $field)
	{
		if (in_array($field, array('original', 'standard', 'thumb'))) {
			if (!$this->_removePhoto($field)) {
				return false;
			}
		
			if (!$this->_addPhoto($file, $field)) {
				return false;
			}
		} else {
			$this->setError(get_class($this) . "::_editPhoto() : Photo refers to an unknown field.");
			return false;
		}
		
		return true;
	}
}
?>