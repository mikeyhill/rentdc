<?php
/**
 * @version		$Id: properties.php 768 2009-07-20 16:08:57Z abernier $
 * @package		Hotproperty
 * @subpackage	Table
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Properties Table
 *
 * @package		Hotproperty
 * @subpackage	Table
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyTableProperties extends HotpropertyTable
{
	var $id = null;
	var $name = null;
	var $agent = null;
	var $type = null;
	var $address = null;
	var $suburb = null;
	var $state = null;
	var $country = null;
	var $postcode = null;
	var $price = null;
	var $note = null;
	var $intro_text = null;
	var $full_text = null;
	var $featured = null;
	var $published = null;
	var $publish_up = null;
	var $publish_down = null;
	var $created = null;
	var $modified = null;
	var $approved = null;
	var $metakey = null;
	var $metadesc = null;
	var $hits = null;
	
	/**
	 * Constructor
	 *
	 * @access protected
	 * @param	An optional associative array of configuration settings.
	 * @param	JDatabase object
	 * @return	void
	 */
	function __construct(&$db, $config = array())
	{
		mimport('mosets.utilities.image');	// @todo: verify if we can get rid of this!
		
		parent::__construct($db, MosetsArrayHelper::array_merge_recursive_unique(array(
			'name'			=> 'Properties',
			'name_singular'	=> 'Property',
			'use_table'		=> true
		), $config));
	}
	
	/**
	 * Initiate row fields to their default values
	 * 
	 * Overload HotpropertyTable::reset()
	 * 
	 * @access	public
	 * @return	boolean	True if success
	 */
	function defaultValues()
	{
		global $mainframe;
		
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		$now = JFactory::getDate();
		
		$this->set('price',		0);
		$this->set('approved',		$mainframe->isAdmin() ? 1 : $hotproperty->getCfg('auto_approve'));
		$this->set('featured', 	0);
		$this->set('published', 	1);
		$this->set('created',		$now->toMySQL());
		$this->set('modified',		$now->toMySQL());
		$this->set('publish_up',	$now->toMySQL());
		$this->set('publish_down',	$this->_db->getNullDate());
		
		return true;
	}
	
	/**
	 * Make the property featured/unfeatured
	 * 
	 * Overload HotpropertyTable::feature() to also add/delete the corresponding featured row
	 * 
	 * @access	public
	 * @return	boolean
	 */
	function feature($oid, $featured = 1)
	{
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		if (!$this->load($oid)) {
			return false;
		}
	
		$this->set('featured', $featured);

		if (!$this->store()) {
			return false;
		}
		
		$row = $hotproperty->getTable('Featured');
		
		switch ($featured)
		{
			/**
			 * Add the property in the 'featured' table
			 */
			case 1:
				// Only if it doesn't already exists
				if (!$row->load($this->id)) {
					// Add the featured
					if (!$row->add($this->id)) {
						$this->setError($row->getError());
						return false;
					}

					// Reordering featured
					$row->reorder();
				}
				break;
			/**
			 * Delete the property in the 'featured' table
			 */
			case 0:
				// Only if exists
				if ($row->load($this->id)) {
					// Delete featured
					if (!$row->delete($this->id)) {
						$this->setError($row->getError());
						return false;
					}

					// Reordering featured
					$row->reorder();
				}
				break;
		}
		
		return true;
	}
	
	/**
	 * Method to call just before data be stored
	 * 
	 * Overload HotpropertyTable::beforeStore()
	 *
	 * @access	public
	 * @return	boolean	True if success
	 */
	function beforeStore($data, $files = null)
	{
		/**
		 * Search for the {readmore} tag and split the text up accordingly.
		 */
		
		$text = $this->intro_text;
		
		$tagPos	= JString::strpos($text, '<hr id="system-readmore" />');
		if ($tagPos === false) {
			$this->intro_text	= $text;
			$this->full_text	= '';
		} else {
			$this->intro_text	= JString::substr($text, 0, $tagPos);
			$this->full_text	= JString::substr($text, $tagPos + 27);
		}
		
		/**
		 * Apply the tzoffset to the form's dates
		 */
		
		// Grab time offset from global configuration
		$config = JFactory::getConfig();
		$tzoffset = $config->getValue('config.offset');
		
		// Created
		$created = JFactory::getDate($this->created, $tzoffset);
		$this->created = $created->toMySQL();
		
		// Publish up
		$publish_up = JFactory::getDate($this->publish_up, $tzoffset);
		$this->publish_up = $publish_up->toMySQL();
		
		// Publish down
		if (trim($this->publish_down) == JText::_('Never') || trim($this->publish_down) == '') {
			$this->publish_down = $this->_db->getNullDate();
		} else {
			$publish_down = JFactory::getDate($this->publish_down, $tzoffset);
			$this->publish_down = $publish_down->toMySQL();
		}
		
		return true;
	}
	
	/**
	 * Method to call just after data be stored
	 * 
	 * Overload HotpropertyTable::afterStore() to add/delete the corresponding featured row
	 *
	 * @access	public
	 * @return	boolean	True if success
	 */
	function afterStore($files = null)
	{
		// Don't forget to add/remove the featured record in #__hp_featured
		if ($this->id > 0 && !$this->feature($this->id, $this->featured)) {
			return false;
		}
		
		return true;
	}
	
}
?>
