<?php
/**
 * @version		$Id: agents.php 777 2009-08-14 14:15:20Z abernier $
 * @package		Hotproperty
 * @subpackage	Table
 * @copyright	(C) 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Agents Table
 *
 * @package		Hotproperty
 * @subpackage	Table
 * @author		Lee Cher Yeong <cy@mosets.com>
 * @author		Antoine Bernier <abernier@mosets.com>
 */

class HotpropertyTableAgents extends HotpropertyTable
{
	var $id = null;
	var $user = null;
	var $name = null;
	var $mobile = null;
	var $email = null;
	var $photo = null;
	var $company = null;
	var $desc = null;
	var $need_approval = null;

	/**
	 * Constructor
	 *
	 * @access protected
	 * @param	An optional associative array of configuration settings.
	 * @param	JDatabase object
	 * @return	void
	 */
	function __construct(&$db, $config = array())
	{
		mimport('mosets.utilities.image');

		parent::__construct($db, MosetsArrayHelper::array_merge_recursive_unique(array(
			'name'			=> 'Agents',
			'name_singular'	=> 'Agent',
			'use_table'		=> true
		), $config));
	}
	
	/**
	 * Method to call just before data be stored
	 * 
	 * Overload HotpropertyTable::beforeStore to check if the photo should be removed
	 *
	 * @access	public
	 * @return	boolean	True if success
	 */
	function beforeStore($data, $files = null)
	{
		if (array_key_exists('removephoto', $data) && $data['removephoto']) {
			if (!$this->_removePhoto()) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Method to call just after data be stored
	 * 
	 * Overload HotpropertyTable::afterStore() to upload the agent's photo
	 *
	 * @access	public
	 * @return	boolean	True if success
	 */
	function afterStore($files = null)
	{
		if (array_key_exists('photo', $files) && !empty($files['photo'])) {
			if (empty($this->photo)) {
				if (!$this->_addPhoto($files['photo'])) {
					return false;
				}
			} else {
				if (!$this->_editPhoto($files['photo'])) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	/**
	 * Generic method to call just before data be deleted
	 * 
	 * Overload HotpropertyTable::beforeDelete() to physically remove photos before deleting DB's records
	 *
	 * @access	public
	 * @return	boolean	True if success
	 */
	function beforeDelete()
	{
		if (!$this->_removePhoto()) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Upload and set the agent's photo
	 * 
	 * @access	private
	 * @param	array	A indexed file array, eg : array('name' => , 'type' => , 'tmp_name' => , 'error' => , 'size' => )
	 * @return	boolean	True if success
	 */
	function _addPhoto($file)
	{
		$hotproperty =& MosetsApplication::getInstance('hotproperty');
		
		if (!empty($file)) {
			// Instantiate the image and set its properties
			$image = new MosetsImage($file, MosetsApplication::getPath('media_images_agent', 'hotproperty'));
			$image->setProperties(array(
				'imageName'	=> $this->id . MosetsApplication::getPath('media_images_connector', 'hotproperty') . $file['name'],
				'method'	=> $hotproperty->getCfg('img_method'),
				'size'		=> $hotproperty->getCfg('imgsize_agent'),
				'quality'	=> $hotproperty->getCfg('quality_agent'),
				'square'	=> false
			));

			if (!$image->resize()) {
				$this->setError($image->getError());
				return false;
			}

			if (!$image->saveToDirectory()) {
				$this->setError($image->getError());
				return false;
			}

			$this->set('photo', $image->imageName);	// Since MosetsImage could have rename the photo's name, let's use $image->imageName as photo's name
			$this->store();
		}
		
		return true;
	}
	
	/**
	 * Remove the agent's photo
	 * 
	 * @access	private
	 * @return	boolean	True if success
	 */
	function _removePhoto()
	{
		if (!empty($this->photo)) {
			// Instantiate the image and set its properties
			$image = new MosetsImage();
			$image->setProperties(array(
				'imageName'	=> $this->photo,
				'directory'	=> MosetsApplication::getPath('media_images_agent', 'hotproperty')
			));

			if (!$image->removeFromDirectory()) {
				$this->setError($image->getError());
				return false;
			}

			$this->set('photo', '');
			$this->store();
		}
		
		return true;
	}
	
	/**
	 * Edit the agent's photo
	 * 
	 * @access	private
	 * @param	array	A indexed file array, eg : array('name' => , 'type' => , 'tmp_name' => , 'error' => , 'size' => )
	 * @return	boolean	True if success
	 */
	function _editPhoto($file)
	{
		if (!$this->_removePhoto()) {
			return false;
		}
		
		if (!$this->_addPhoto($file)) {
			return false;
		}
		
		return true;
	}
}
?>