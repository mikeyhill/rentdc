<?php
/**
 * @version		$Id: toolbar.hotproperty_avl.html.php 4 2009-11-04 07:58:45Z cy $
 * @package		Hotproperty Availability
 * @copyright	(C) 2004 - 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

defined('_JEXEC') or die('Restricted access');

class menu_hp_avl {

	# Availability Type menus
	
	function EDIT_TYPE() {
		$id = JRequest::getInt('id');
		if ( $id ) {
			JToolBarHelper::title( JText::_( 'HOTPROPERTY_AVL_EDIT_TYPE' ), 'addedit' );
		} else {
			JToolBarHelper::title( JText::_( 'HOTPROPERTY_AVL_ADD_TYPE' ), 'addedit' );
		}
		JToolBarHelper::save('saveType');
		JToolBarHelper::cancel('cancelType');
	}
	
	function VIEW_TYPES() {
		JToolBarHelper::title( JText::_( 'HOTPROPERTY_AVL_AVAILABILITY_TYPES' ), 'addedit' );
		JToolBarHelper::addNew('newType');
		JToolBarHelper::editList('editType');
		JToolBarHelper::deleteList('', 'removeType');
		JToolBarHelper::preferences('com_hotproperty_avl', '550', '570', 'Settings');
		
		$bar = & JToolBar::getInstance('toolbar');
		$bar->appendButton( 'Popup', 'help', "About", 'index.php?option=com_hotproperty_avl&task=about&tmpl=component', 420, 420 );
	}
}
?>