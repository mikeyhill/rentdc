<?php
/**
 * @version		$Id: install.hotproperty_avl.php 5 2009-11-04 14:11:14Z cy $
 * @package		Hotproperty Availability
 * @copyright	(C) 2004 - 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

defined('_JEXEC') or die('Restricted access');

require_once( 'components/com_hotproperty_avl/admin.hotproperty_avl.class.php' );
?>

<style type="text/css">
#element-box th {display:none;}
</style>
<img src="<?php echo JURI::root() . 'administrator/components/com_hotproperty/assets/images/logo.png'; ?>" alt="Hot Property Availability" style="float:left;border-right:1px solid #ccc;padding-right:15px;" />
<div style="margin-left:184px;">
	<h2 style="margin-bottom:0;">Hot Property Availability extension v<?php echo hp_avl_version; ?></h2>
	<strong></strong>
	<br /><br />
	&copy; Copyright 2004-<?php echo date('Y'); ?> by Mosets Consulting. <a href="http://www.mosets.com/">www.mosets.com</a><br />
	<input type="button" value="Set up Availability types now" onclick="location.href='index.php?option=com_hotproperty_avl'" style="margin-top:7px;cursor:pointer;" />
</div>