CREATE TABLE `#__hp_avl` (
  `avl_id` int(11) NOT NULL auto_increment,
  `avl_type_id` int(11) NOT NULL default '0',
  `property_id` int(11) NOT NULL default '0',
  `date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`avl_id`)
) TYPE=MyISAM;

CREATE TABLE `#__hp_avl_types` (
  `avl_type_id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `desc` mediumtext NOT NULL,
  `font_color` varchar(255) NOT NULL default '',
  `font_background` varchar(255) NOT NULL default '',
  `ordering` int(11) NOT NULL default '0',
  PRIMARY KEY  (`avl_type_id`)
) TYPE=MyISAM;    

INSERT INTO `#__hp_prop_ef` (`field_type`, `name`, `caption`, `default_value`, `size`, `field_elements`, `prefix_text`, `append_text`, `ordering`, `hidden`, `published`, `featured`, `listing`, `hideCaption`, `iscore`, `search`, `search_caption`, `search_type`) VALUES ('link', 'hp_avl', 'Availability', 'Check Availability | index.php?option=com_hotproperty_avl&task=view&id={property_id}&Itemid={Itemid}', 30, '', '', '', 1, 0, 1, 0, 0, 0, 0, 0, '', '');