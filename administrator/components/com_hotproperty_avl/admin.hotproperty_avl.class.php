<?php
/**
 * @version		$Id: admin.hotproperty_avl.class.php 4 2009-11-04 07:58:45Z cy $
 * @package		Hotproperty Availability
 * @copyright	(C) 2004 - 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

defined('_JEXEC') or die('Restricted access');

define ('hp_avl_version',"1.2");

/**
* Availability Types table class
*/
class hp_avl_types extends JTable {

	var $avl_type_id=null;
	var $name=null;
	var $desc=null;
	var $font_color=null;
	var $font_background=null;
	var $ordering=null;

	function __construct( &$db ) {
		parent::__construct( '#__hp_avl_types', 'avl_type_id', $db );
	}
}

/**
* Availability Main table class
*/
class hp_avl extends JTable {

	var $avl_id=null;
	var $avl_type_id=null;
	var $property_id=null;
	var $date=null;

	function __construct( &$db ) {
		parent::__construct( '#__hp_avl', 'avl_id', $db );
	}

	function clear_avl($property_id, $month, $year) {
		$database =& JFactory::getDBO();

		$database->setQuery("DELETE FROM #__hp_avl WHERE property_id = '".$property_id."' AND MONTH(`date`) = '".$month."' AND YEAR(`date`) = '".$year."'");
		if (!$database->query()) {
			echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
			exit();
		}
	}

	function add_avl($property_id, $date, $avl_type_id) {
		$database =& JFactory::getDBO();

		$database->setQuery("INSERT INTO #__hp_avl "
			. 	"\n (avl_type_id, property_id, date) "
			.	"\n VALUES ('".$avl_type_id."', '".$property_id."', '".$date."')");
		if (!$database->query()) {
			echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
			exit();
		}
	}

}

function html2rgb($color)
{
    if ($color[0] == '#')
        $color = substr($color, 1);

    if (strlen($color) == 6)
        list($r, $g, $b) = array($color[0].$color[1],
                                 $color[2].$color[3],
                                 $color[4].$color[5]);
    elseif (strlen($color) == 3)
        list($r, $g, $b) = array($color[0].$color[0], $color[1].$color[1], $color[2].$color[2]);
    else
        return false;

    $r = hexdec($r); $g = hexdec($g); $b = hexdec($b);

    return array($r, $g, $b);
}

class backend_Calendar extends Calendar {

    function getCalendarLink($month, $year)
    {
        return "index.php?option=com_hotproperty_avl&task=ext_viewmonth&month=$month&year=$year&property_id=$this->property_id&tmpl=component";
    }

    function getMonthLink($month, $year)
    {
        return "index.php?option=com_hotproperty_avl&task=ext_viewmonth&month=$month&year=$year&property_id=$this->property_id&tmpl=component";
    }

		function getYearLink($month, $year)
		{
				return "index.php?option=com_hotproperty_avl&task=ext_viewyear&year=$year&returnmonth=$month&property_id=$this->property_id&tmpl=component";
		}

}

// PHP Calendar Class Version 1.4 (5th March 2001)
//  
// Copyright David Wilkinson 2000 - 2001. All Rights reserved.
// 
// This software may be used, modified and distributed freely
// providing this copyright notice remains intact at the head 
// of the file.
//
// This software is freeware. The author accepts no liability for
// any loss or damages whatsoever incurred directly or indirectly 
// from the use of this script. The author of this software makes 
// no claims as to its fitness for any purpose whatsoever. If you 
// wish to use this software you should first satisfy yourself that 
// it meets your requirements.
//
// URL:   http://www.cascade.org.uk/software/php/calendar/
// Email: davidw@cascade.org.uk


class Calendar
{
    
	/*
			Availability Types
	*/
	var $avl_types;
	var $avl;
	var $property_id;
	var $edit; // If 1, getYearHTML will output select box
	
    /*
        Constructor for the Calendar class
    */
    function Calendar()
    {
			$this->edit = 1;
    }

    /*
        Get the array of strings used to label the days of the week. This array contains seven 
        elements, one for each day of the week. The first entry in this array represents Sunday. 
    */
    function getDayNames()
    {
        return $this->dayNames;
    }
    

    /*
        Set the array of strings used to label the days of the week. This array must contain seven 
        elements, one for each day of the week. The first entry in this array represents Sunday. 
    */
    function setDayNames($names)
    {
        $this->dayNames = $names;
    }

	/*
		Get the full name of a month
	*/
    function getMonthName($month)
    {
		$month++;
		switch($month)
		{
			// default:
			case 1:
				return JText::_('JANUARY');
				break;
			case 2:
				return JText::_('FEBRUARY');
				break;
			case 3:
				return JText::_('MARCH');
				break;
			case 4:
				return JText::_('APRIL');
				break;
			case 5:
				return JText::_('MAY');
				break;
			case 6:
				return JText::_('JUNE');
				break;
			case 7:
				return JText::_('JULY');
				break;
			case 8:
				return JText::_('AUGUST');
				break;
			case 9:
				return JText::_('SEPTEMBER');
				break;
			case 10:
				return JText::_('OCTOBER');
				break;
			case 11:
				return JText::_('NOVEMBER');
				break;
			case 12:
				return JText::_('DECEMBER');
				break;
		}
    }

    /* 
        Gets the start day of the week. This is the day that appears in the first column
        of the calendar. Sunday = 0.
    */
      function getStartDay()
    {
        return $this->startDay;
    }
    
    /* 
        Sets the start day of the week. This is the day that appears in the first column
        of the calendar. Sunday = 0.
    */
    function setStartDay($day)
    {
        $this->startDay = $day;
    }
    
    
    /* 
        Gets the start month of the year. This is the month that appears first in the year
        view. January = 1.
    */
    function getStartMonth()
    {
        return $this->startMonth;
    }
    
    /* 
        Sets the start month of the year. This is the month that appears first in the year
        view. January = 1.
    */
    function setStartMonth($month)
    {
        $this->startMonth = $month;
    }
    
    /*
        Return the URL to link to in order to display a calendar for a given month/year.
        You must override this method if you want to activate the "forward" and "back" 
        feature of the calendar.
        
        Note: If you return an empty string from this function, no navigation link will
        be displayed. This is the default behaviour.
        
        If the calendar is being displayed in "year" view, $month will be set to zero.
    */
    function getCalendarLink($month, $year)
    {
        return "";
    }
    
    /*
        Return the URL to link to  for a given date.
        You must override this method if you want to activate the date linking
        feature of the calendar.
        
        Note: If you return an empty string from this function, no navigation link will
        be displayed. This is the default behaviour.
    */
    function getDateLink($month, $year)
    {
        return "";
    }

		function getMonthLink($month, $year)
    {
        return "";
    }

		function getYearLink($month, $year)
		{
				return "";
		}
    /*
        Return the HTML for the current month
    */
    function getCurrentMonthView()
    {
        $d = getdate(time());
        return $this->getMonthView($d["mon"], $d["year"]);
    }
    

    /*
        Return the HTML for the current year
    */
    function getCurrentYearView()
    {
        $d = getdate(time());
        return $this->getYearView($d["year"]);
    }
    
    
    /*
        Return the HTML for a specified month
    */
    function getMonthView($month, $year)
    {
        return $this->getMonthHTML($month, $year);
    }
    

    /*
        Return the HTML for a specified year
    */
    function getYearView($year)
    {
        return $this->getYearHTML($year);
    }
    
    
    
    /********************************************************************************
    
        The rest are private methods. No user-servicable parts inside.
        
        You shouldn't need to call any of these functions directly.
        
    *********************************************************************************/


    /*
        Calculate the number of days in a month, taking into account leap years.
    */
    function getDaysInMonth($month, $year)
    {
        if ($month < 1 || $month > 12)
        {
            return 0;
        }
   
        $d = $this->daysInMonth[$month - 1];
   
        if ($month == 2)
        {
            // Check for leap year
            // Forget the 4000 rule, I doubt I'll be around then...
        
            if ($year%4 == 0)
            {
                if ($year%100 == 0)
                {
                    if ($year%400 == 0)
                    {
                        $d = 29;
                    }
                }
                else
                {
                    $d = 29;
                }
            }
        }
    
        return $d;
    }


    /*
        Generate the HTML for a given month
    */
    function getMonthHTML($m, $y, $showYear = 1)
    {
      $s = "";
        
      $a = $this->adjustDate($m, $y);
      $month = $a[0];
      $year = $a[1];
        
    	$daysInMonth = $this->getDaysInMonth($month, $year);
    	$date = getdate(mktime(12, 0, 0, $month, 1, $year));
    	
    	$first = $date["wday"];
		$monthName = $this->getMonthName($month - 1);
    	
    	$prev = $this->adjustDate($month - 1, $year);
    	$next = $this->adjustDate($month + 1, $year);
    	
    	if ($showYear == 1)
    	{
    	    $prevMonth = $this->getCalendarLink($prev[0], $prev[1]);
    	    $nextMonth = $this->getCalendarLink($next[0], $next[1]);
    	}
    	else
    	{
    	    $prevMonth = "";
    	    $nextMonth = "";
    	}
    	
    	$header = $monthName . (($showYear > 0) ? " " . $year : "");
		
    	$s .= "<table class=\"calendar\" style=\"overflow:hidden\">\n";
    	$s .= "<tr>\n";

    	// $s .= "<td align=\"center\" valign=\"top\">" . (($prevMonth == "") ? "&nbsp;" : "<a href=\"$prevMonth\">&lt;&lt;</a>")  . "</td>\n";

    	$s .= "<td align=\"right\" valign=\"top\" align=\"right\">";
		$s .= (($prevMonth == "") ? "&nbsp;" : "<div class=\"button2-right\"><div class=\"prev\"><a href=\"$prevMonth\"></a></div></div>");
		$s .= "</td>\n";


    	$s .= "<td align=\"center\" valign=\"top\" class=\"calendarHeader-month\" colspan=\"5\">";
			$link = $this->getMonthLink($month, $year);
			if ($link <> '') {
				$s .= "<a href=\"".$link."\">".$header."</a>";	
			} else {
				$s .= $header;
			}
			$s .= "</td>\n"; 

    	// $s .= "<td align=\"center\" valign=\"top\">" . (($nextMonth == "") ? "&nbsp;" : "<a href=\"$nextMonth\">&gt;&gt;</a>")  . "</td>\n";

    	$s .= "<td align=\"center\" valign=\"top\" align=\"right\">";
 		$s .= (($nextMonth == "") ? "&nbsp;" : "<div class=\"button2-left\" style=\"float:right\"><div class=\"next\"><a href=\"$nextMonth\"></a></div></div>");
		$s .= "</td>\n";

    	$s .= "</tr>\n";
    	
    	$s .= "<tr class=\"calendarHeader\">\n";
    	$s .= "<td align=\"center\" valign=\"top\" class=\"calendarHeader\">" . $this->dayNames[($this->startDay)%7] . "</td>\n";
    	$s .= "<td align=\"center\" valign=\"top\" class=\"calendarHeader\">" . $this->dayNames[($this->startDay+1)%7] . "</td>\n";
    	$s .= "<td align=\"center\" valign=\"top\" class=\"calendarHeader\">" . $this->dayNames[($this->startDay+2)%7] . "</td>\n";
    	$s .= "<td align=\"center\" valign=\"top\" class=\"calendarHeader\">" . $this->dayNames[($this->startDay+3)%7] . "</td>\n";
    	$s .= "<td align=\"center\" valign=\"top\" class=\"calendarHeader\">" . $this->dayNames[($this->startDay+4)%7] . "</td>\n";
    	$s .= "<td align=\"center\" valign=\"top\" class=\"calendarHeader\">" . $this->dayNames[($this->startDay+5)%7] . "</td>\n";
    	$s .= "<td align=\"center\" valign=\"top\" class=\"calendarHeader\">" . $this->dayNames[($this->startDay+6)%7] . "</td>\n";
    	$s .= "</tr>\n";
    	
    	// We need to work out what date to start at so that the first appears in the correct column
    	$d = $this->startDay + 1 - $first;
    	while ($d > 1)
    	{
    	    $d -= 7;
    	}

        // Make sure we know when today is, so that we can use a different CSS style
        $today = getdate(time());
    	
    	while ($d <= $daysInMonth)
    	{
    	    $s .= "<tr>\n";       
    	    
    	    for ($i = 0; $i < 7; $i++)
    	    {
        	    $class = ($year == $today["year"] && $month == $today["mon"] && $d == $today["mday"]) ? "calendarToday" : "calendarDate";

				// Display different colors for Availability types in ViewYear -> edit = 0
				if ($this->edit == 0 && $d > 0 && $d <= $daysInMonth) {
					$s .= "<td ";
					foreach($this->avl_types AS $avl_type) {
						$ddate = date('Y-m-d', strtotime($d.' '.date('F',mktime(0, 0, 0, $m, 1, 2004)).' '.$y) );
						// Assign class
						if (isset($this->avl[$ddate]) && $this->avl[$ddate]->avl_type_id == $avl_type->avl_type_id) {
							$s .= "style=\"background-color:".$avl_type->font_background."; color:".$avl_type->font_color."\" ";
						} else {
							$s .= "class=\"$class\" ";
						}
					}
					$s .= "align=\"right\" valign=\"top\">";
				} else {
  	        		$s .= "<td ";
					if ($d > 0 && $d <= $daysInMonth) {
						//TODO: Assign color codes. Use javascript to change value in real time
					} else {

					}
					$s .= "class=\"$class blank\" align=\"right\" valign=\"top\">";
				}

    	        if ($d > 0 && $d <= $daysInMonth)
    	        {
    	            $link = $this->getDateLink($d, $month, $year);
    	            $s .= (($link == "") ? $d : "<a href=\"$link\">$d</a>");
									if ($this->edit <> 0) {
										$s .= '<br />';
										$s .= '<select name="day'.$d.'" class="inputbox2" size="1">';
										$s .= '<option value="0"> </option>';
										foreach($this->avl_types AS $avl_type) {
											$s .= '<option value="'.$avl_type->avl_type_id.'" ';
											$ddate = date('Y-m-d', strtotime($d.' '.date('F',mktime(0, 0, 0, $m, 1, 2004)).' '.$y) );
											if (isset($this->avl[$ddate]) && $this->avl[$ddate]->avl_type_id == $avl_type->avl_type_id) {
												$s .= 'selected ';
											}
											$s .= 'style="background-color:'. $avl_type->font_background.';">';
											$s .= $avl_type->name.'</option>';
										}
										$s .= '</select>';
									}
    	        }
    	        else
    	        {
    	            $s .= "&nbsp;";
    	        }
      	        $s .= "</td>\n";       
        	    $d++;
    	    }
    	    $s .= "</tr>\n";    
    	}
    	
    	$s .= "</table>\n";

    	return $s;  	
    }
    
    
    /*
        Generate the HTML for a given year
    */
    function getYearHTML($year)
    {
		global $mainframe;
      	$s = "";
    	$prev = $this->getYearLink(1, $year - 1);
    	$next = $this->getYearLink(1, $year + 1);
        
        $s .= "<table class=\"calendar-year\" border=\"0\">\n";
        $s .= "<tr>";

		if( $mainframe->isAdmin() )
		{
	    	$s .= "<td align=\"right\" valign=\"top\" align=\"right\">";
			$s .= "<h2><div class=\"button2-right\"><div class=\"prev\">";
			$s .= (($prev == "") ? "&nbsp;" : "<a href=\"$prev\">←".($year-1)."</a>");
			$s .= "</div></div></h2></td>\n";
		}
		else
		{
			$s .= (($prev == "") ? "&nbsp;" : "<td><h3 class=\"previous-year\"><a href=\"$prev\">←".($year-1)."</a></h3></td>");
		}

        $s .= "<td class=\"calendarHeader\" valign=\"top\" align=\"center\"><h2 class=\"current-year\">" . (($this->startMonth > 1) ? $year . " - " . ($year + 1) : $year) ."</h2></td>\n";

		if( $mainframe->isAdmin() )
		{
	    	$s .= "<td align=\"center\" valign=\"top\" align=\"right\">";
			$s .= "<h2><div class=\"button2-left\" style=\"float:right\"><div class=\"next\">";
	 		$s .= (($next == "") ? "&nbsp;" : "<a href=\"$next\">".($year+1)."→</a>");
			$s .= "</div></div></h2></td>\n";
		}
		else
		{
	 		$s .= (($next == "") ? "&nbsp;" : "<td><h3 class=\"next-year\"><a href=\"$next\">".($year+1)."→</a></h3></td>");
		}

        $s .= "</tr>\n";
        $s .= "<tr>";
        $s .= "<td class=\"calendar-month\" valign=\"top\">" . $this->getMonthHTML(0 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "<td class=\"calendar-month\" valign=\"top\">" . $this->getMonthHTML(1 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "<td class=\"calendar-month\" valign=\"top\">" . $this->getMonthHTML(2 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "</tr>\n";
        $s .= "<tr>\n";
        $s .= "<td class=\"calendar-month\" valign=\"top\">" . $this->getMonthHTML(3 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "<td class=\"calendar-month\" valign=\"top\">" . $this->getMonthHTML(4 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "<td class=\"calendar-month\" valign=\"top\">" . $this->getMonthHTML(5 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "</tr>\n";
        $s .= "<tr>\n";
        $s .= "<td class=\"calendar-month\" valign=\"top\">" . $this->getMonthHTML(6 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "<td class=\"calendar-month\" valign=\"top\">" . $this->getMonthHTML(7 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "<td class=\"calendar-month\" valign=\"top\">" . $this->getMonthHTML(8 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "</tr>\n";
        $s .= "<tr>\n";
        $s .= "<td class=\"calendar-month\" valign=\"top\">" . $this->getMonthHTML(9 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "<td class=\"calendar-month\" valign=\"top\">" . $this->getMonthHTML(10 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "<td class=\"calendar-month\" valign=\"top\">" . $this->getMonthHTML(11 + $this->startMonth, $year, 0) ."</td>\n";
        $s .= "</tr>\n";
        $s .= "</table>\n";
        
        return $s;
    }

    /*
        Adjust dates to allow months > 12 and < 0. Just adjust the years appropriately.
        e.g. Month 14 of the year 2001 is actually month 2 of year 2002.
    */
    function adjustDate($month, $year)
    {
        $a = array();  
        $a[0] = $month;
        $a[1] = $year;
        
        while ($a[0] > 12)
        {
            $a[0] -= 12;
            $a[1]++;
        }
        
        while ($a[0] <= 0)
        {
            $a[0] += 12;
            $a[1]--;
        }
        
        return $a;
    }

    /* 
        The start day of the week. This is the day that appears in the first column
        of the calendar. Sunday = 0.
    */
    var $startDay = 0;

    /* 
        The start month of the year. This is the month that appears in the first slot
        of the calendar in the year view. January = 1.
    */
    var $startMonth = 1;

    /*
        The labels to display for the days of the week. The first entry in this array
        represents Sunday.
    */
    var $dayNames = array("S", "M", "T", "W", "T", "F", "S");

    /*
        The number of days in each month. You're unlikely to want to change this...
        The first entry in this array represents January.
    */
    var $daysInMonth = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

}

?>