<?php
/**
 * @version		$Id: toolbar.hotproperty_avl.php 4 2009-11-04 07:58:45Z cy $
 * @package		Hotproperty Availability
 * @copyright	(C) 2004 - 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

defined('_JEXEC') or die('Restricted access');

require_once( $mainframe->getPath( 'toolbar_html' ) );
require_once( $mainframe->getPath( 'toolbar_default' ) );

switch ($task) {

	# Availability Type Events
	case "newType":
	case "editType":
		menu_hp_avl::EDIT_TYPE();
		break;
	case "viewTypes":
	default:
		menu_hp_avl::VIEW_TYPES();
		break;

}
?>