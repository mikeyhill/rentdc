<?php
/**
 * @version		$Id: admin.hotproperty_avl.html.php 4 2009-11-04 07:58:45Z cy $
 * @package		Hotproperty Availability
 * @copyright	(C) 2004 - 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

defined('_JEXEC') or die('Restricted access');

class HTML_hp_avl {

	/***
	* Availability
	*/
	function ext_printcss() {
	?>
	<style type="text/css">
	table.calendar td {
		font-size: 16px;
		width: 80px;
		height: 50px;
		text-align: center;
	}
	table.calendar td.calendarToday {
		background-color: #ffae00;
	}
	select.inputbox2 {
		width: 80px;
	}
	#system-message dt.message {
		display:none;
	}
	#system-message dd {
		font-weight:bold;
		margin:0;
		text-indent:30px;
	}
	#system-message dd ul {
		border-bottom:3px solid #84A7DB;
		border-top:3px solid #84A7DB;
		color:#0055BB;
		list-style-image:none;
		list-style-position:outside;
		list-style-type:none;
		margin-bottom:10px;
		padding:10px;
	}
	</style>
	<?php
	}
	function ext_main( &$html, &$avl_types, $month, $year, $property_id, $option ) {
		HTML_hp_avl::ext_printcss();
	?>
	<form action="index3.php" method="POST" name="adminForm">
	<center>
	<a href="index3.php?option=<?php echo $option; ?>&amp;task=ext_viewyear&amp;year=<?php echo $year; ?>&amp;returnmonth=<?php echo $month; ?>&amp;property_id=<?php echo $property_id; ?>"><?php echo $year; ?></a>
	<p />
	<?php
		echo $html['calendar'];
	?>
	<p />
	<input type="hidden" name="option" value="<?php echo $option; ?>" />
	<input type="hidden" name="task" value="ext_save" />
	<input type="hidden" name="property_id" value="<?php echo $property_id; ?>" />
	<input type="hidden" name="month" value="<?php echo $month; ?>" />
	<input type="hidden" name="year" value="<?php echo $year; ?>" />
	<input type="submit" value="<?php echo JText::_( 'Save' ); ?>" class="button" />
	</form>
	</center>
	<p />
	<table cellpadding="5" cellspacing="0" border="0" style="border: 1px solid #C0C0C0">
	<?php	foreach($avl_types AS $avl_type) {	?>
	<tr>
		<td style="background-color:<?php echo $avl_type->font_background; ?>; color:<?php echo $avl_type->font_color; ?>"><?php echo $avl_type->name; ?></td>
		<td><?php echo $avl_type->desc; ?></td>
	</tr>
	<?php	}	?>
	</table>
	<?php
	}

	function ext_viewyear( $html, $option ) {
	?>
	<style type="text/css">
	table.calendar-year, table.calendar-month {}
	table.calendar-year td, table.calendar-month td {
		text-align: center;
	}
	table.calendar td.calendarHeader-month {
		font-size: 1.3em;
		color: red;
	}
	td.calendar-month {}
	table.calendar td.calendarDate {}
	table.calendar-year td.calendarDate.blank {
		border: 0px;
	}
	table.calendar td.calendarToday {
		background-color: #ffae00;
	}
	select.inputbox2 {
		width: 80px;
	}
	</style>
	<center><?php echo $html; ?></center>
	<?php
	}

	function ext_error( $html ) {
		echo $html;
	}

	/***
	* Managing Availability Types
	*/
	function viewTypes( $rows, $pageNav, $option ) {
	?>
	<form action="index2.php" method="POST" name="adminForm">
	<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
		<thead>
		<tr>
			<th width="20"><?php echo JText::_( '#' ); ?></th>
			<th width="20"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
			<th align="left" nowrap><?php echo JText::_( 'HOTPROPERTY_AVL_AVAILABILITY_TYPE_NAME' ); ?></th>
			<th align="left" nowrap><?php echo JText::_( 'HOTPROPERTY_AVL_PREVIEW' ); ?></th>
			<th align="left" nowrap><?php echo JText::_( 'HOTPROPERTY_AVL_DESCRIPTION' ); ?></th>
			<th nowrap colspan="2"><?php echo JText::_( 'HOTPROPERTY_AVL_REORDER' ); ?></th>
		</tr>
		</thead>
<?php
		$k = 0;
		for ($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];
?>
		<tr class="<?php echo "row$k"; ?>">
			<td width="20" align="center"><?php echo $i+$pageNav->limitstart+1;?></td>
			<td width="20">
				<input type="checkbox" id="cb<?php echo $i;?>" name="id[]" value="<?php echo $row->avl_type_id; ?>" onClick="isChecked(this.checked);" />
			</td>
			<td width="15%" align="left"><a href="#edit" onclick="return listItemTask('cb<?php echo $i; ?>','editType')"><?php echo $row->name; ?></a></td>
			<td width="15%" align="left"><span style="padding:0 6px;color:<?php echo $row->font_color; ?>;background-color:<?php echo $row->font_background; ?>;"><?php echo $row->name; ?></span></td>
			<td align="left" width="80%"><?php echo $row->desc; ?></td>
      <td>
			<?php		if ($i > 0 || ($i+$pageNav->limitstart > 0)) { ?>
        <a href="#reorder" onClick="return listItemTask('cb<?php echo $i;?>','orderup_type')">
        <img src="images/uparrow.png" width="12" height="12" border="0" alt="Move Up">
        </a>
        <?php		} else { echo "&nbsp;"; } ?>
      </td>
      <td>
			<?php		if ($i < $n-1 || $i+$pageNav->limitstart < $pageNav->total-1) { ?>
        <a href="#reorder" onClick="return listItemTask('cb<?php echo $i;?>','orderdown_type')">
        <img src="images/downarrow.png" width="12" height="12" border="0" alt="Move Down">
        </a>
        <?php		} else { echo "&nbsp;"; } ?>
      </td>

			<?php		$k = 1 - $k; ?>
		</tr>
<?php	}
?>
		<tfoot><tr><th align="center" colspan="7">&nbsp;</th></tr></tfoot>
	</table>

	<input type="hidden" name="option" value="<?php echo $option; ?>">
	<input type="hidden" name="task" value="viewTypes">
	<input type="hidden" name="boxchecked" value="0">
	</form>
	<?php
	}

	function editType( &$row, $option ) {
		JFilterOutput::objectHTMLSafe( $row, ENT_QUOTES, 'desc' );
		$editor = &JFactory::getEditor();
		
		$font_color_rgb = html2rgb($row->font_color);
		$font_background_rgb = html2rgb($row->font_background);
		
		$document	=& JFactory::getDocument();
		$document->addStyleSheet(JURI::root(true)."/media/com_hotproperty_avl/css/mooRainbow.css");
		$document->addScript(JURI::root(true)."/media/com_hotproperty_avl/js/mooRainbow.js");
		$document->addScriptDeclaration("		
		window.addEvent('domready', function() {
			$('preview').style.backgroundColor = '".$row->font_background."';
			$('preview').style.color = '".$row->font_color."';
			var font_color = new MooRainbow('font_color', {
				id: 'font_color',
				'startColor': [".(is_array($font_color_rgb)?implode(',',$font_color_rgb):'0,0,0')."],
				'onChange': function(color) {
					$('font_color').value = color.hex;
					$('preview').style.color = color.hex;
				}
			});
			var font_background = new MooRainbow('font_background', {
				id: 'font_background',
				'startColor': [".(is_array($font_background_rgb)?implode(',',$font_background_rgb):'0,0,0')."],
				'onChange': function(color) {
					$('font_background').value = color.hex;
					$('preview').style.backgroundColor = color.hex;
				}
			});
		});
		");
?>
	<script language="javascript">
	<!--
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == 'cancelType') {
				submitform( pressbutton );
				return;
			}
			// do field validation
			if (form.name.value == "") {
				alert( JText::_( 'HOTPROPERTY_AVL_FILL_IN_THE_AVAILABILITY_TYPE_NAME' ) );
			} else {
				submitform( pressbutton );
			}
		}
	//-->
	</script>
	<form action="index2.php" method="POST" name="adminForm">
	<table cellpadding="4" cellspacing="1" border="0" width="100%" class="adminform">
		<tr>
			<td align="left" width="10%"><?php echo JText::_( 'HOTPROPERTY_AVL_PREVIEW' ); ?>:</td>
			<td align="left"><span id="preview" style="padding:0 6px;"><?php echo $row->name; ?></td>
		</tr>
		<tr>
			<td align="left" width="10%"><?php echo JText::_( 'HOTPROPERTY_AVL_NAME' ); ?>:</td>
			<td align="left"><input class="inputbox" type="text" name="name" size="30" valign="top" value="<?php echo $row->name; ?>" onkeydown="$('preview').innerHTML=this.value"></td>
		</tr>
		<tr>
			<td align="left" valign="top"><?php echo JText::_( 'HOTPROPERTY_AVL_DESCRIPTION' ); ?>:</td>
			<td align="left"><?php // echo $editor->display( 'desc',  $row->desc , '70%', '250', '75', '10' ) ; ?>
				<textarea name="desc" rows="5" cols="50"><?php echo $row->desc; ?></textarea>
			</td>
		</tr>
		<tr>
			<td align="left" width="10%"><?php echo JText::_( 'HOTPROPERTY_AVL_FONT_COLOR' ); ?>:</td>
			<td align="left"><input class="inputbox" type="text" name="font_color" id="font_color" size="10" valign="top" value="<?php echo $row->font_color; ?>"></td>
		</tr>
		<tr>
			<td align="left" width="10%"><?php echo JText::_( 'HOTPROPERTY_AVL_FONT_BACKGROUND_COLOR' ); ?>:</td>
			<td align="left"><input class="inputbox" type="text" name="font_background" id="font_background" size="10" valign="top" value="<?php echo $row->font_background; ?>"></td>
		</tr>
	</table>
	<input type="hidden" name="option" value="<?php echo $option; ?>">
	<input type="hidden" name="avl_type_id" value="<?php echo $row->avl_type_id; ?>">
	<input type="hidden" name="task" value="saveType">
	</form>
<?php }

/********************************
 * About Availability Extension *
 *******************************/
	function showAbout() {
		$database =& JFactory::getDBO();
	?>
	<center><img width="153" height="103" src="components/com_hotproperty/assets/images/logo.png" alt="Hot Property"></center>
	<br />
	<table width="50%" border="0" align="center" cellpadding="10" cellspacing="0" class="adminlist">
		<thead>
		<tr>
			<th colspan="2"><?php echo JText::_( 'HOTPROPERTY_AVL_AVAILABILITY_EXTENSION_FOR_HOT_PROPERTY' ); ?></th>
		</tr>
		</thead>
		<tr>
			<td align="center" colspan="2"><?php
				$database->setQuery("SELECT COUNT(*) FROM #__components WHERE `option`='com_hotproperty'");
				$installed = $database->loadResult();
				if ($installed > 0) {
					echo JText::_( 'HOTPROPERTY_AVL_HOT_PROPERTY_IS_INSTALLED' );
				} else {
					echo JText::_( 'HOTPROPERTY_AVL_HOT_PROPERTY_IS_NOT_INSTALLED' );
				}
			?></td>
		</tr>
		<tr>
			<td width="20%" align="right"><strong><?php echo JText::_( 'HOTPROPERTY_AVL_EXTENSION' ); ?></strong></td>
			<td width="80%" align="left"><?php echo JText::_( 'HOTPROPERTY_AVL_AVAILABILITY' ); ?></td>
		</tr>
		<tr>
			<td width="20%" align="right"><strong><?php echo JText::_( 'HOTPROPERTY_AVL_VERSION' ); ?></strong></td>
			<td width="80%" align="left"><?php echo hp_avl_version; ?></td>
		</tr>
		<tr>
			<td width="20%" align="right"><strong><?php echo JText::_( 'HOTPROPERTY_AVL_DESCRIPTION' ); ?></strong></td>
			<td width="80%" align="left"><?php echo JText::_( 'HOTPROPERTY_AVL_AVAILABILITY_DESCRIPTION' ); ?></td>
		</tr>
		<tr>
			<td width="20%" align="right"><strong><?php echo JText::_( 'HOTPROPERTY_AVL_WEBSITE' ); ?></strong></td>
			<td width="80%" align="left"><a target="_blank" href="http://www.mosets.com/">http://www.mosets.com/</a></td>
		</tr>
		<tr>
			<td width="20%" align="right"><strong><?php echo JText::_( 'HOTPROPERTY_AVL_AUTHOR' ); ?></strong></td>
			<td width="80%" align="left">Lee Cher Yeong</td>
		</tr>
		<tr>
			<td width="20%" align="right"><strong><?php echo JText::_( 'HOTPROPERTY_AVL_EMAIL' ); ?></strong></td>
			<td width="80%" align="left"><a href="mailto:hotproperty@mosets.com">hotproperty@mosets.com</a></td>
		</tr>
		<tr>
			<td width="20%" align="right"><strong><?php echo JText::_( 'HOTPROPERTY_AVL_LICENSE' ); ?></strong></td>
			<td width="80%" align="left"><a href="index.php?option=com_hotproperty&amp;view=about">License</a></td>
		</tr>
	</table>
	<br />
	<center>Mosets Consulting &copy; 2009 All rights reserved.</center>
	<?php
	}

}

?>
