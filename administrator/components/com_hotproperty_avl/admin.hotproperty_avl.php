<?php
/**
 * @version		$Id: admin.hotproperty_avl.php 4 2009-11-04 07:58:45Z cy $
 * @package		Hotproperty Availability
 * @copyright	(C) 2004 - 2009 Mosets Consulting
 * @url			http://www.mosets.com/
 */

defined('_JEXEC') or die('Restricted access');

require_once( 'components/com_hotproperty_avl/admin.hotproperty_avl.class.php' );
require_once( $mainframe->getPath( 'admin_html' ) );

$id	= JRequest::getVar( 'id', array(), 'post', 'array' );

if (!is_array( $id )) {
	$id = array(0);
}

switch ($task) {
	# Extension Event
	case "ext_main":
		ext_viewyear( $option );
		break;
	case "ext_viewmonth":
		ext_main( $option );
		break;
	case "ext_viewyear":
		ext_viewyear( $option );
		break;
	case "ext_save":
		ext_save( $option );
		break;

	# Type Event
	case "newType":
		editType( 0, $option );
		break;
	case "editType":
		editType( $id[0], $option );
		break;
	case "saveType":
		saveType( $option );
		break;
	case "orderup_type":
		orderType( $id[0], -1, $option );
		break;
	case "orderdown_type":
		orderType( $id[0], 1, $option );
		break;
	case "removeType":
		removeType( $id, $option );
		break;
	case "cancelType":
		cancelType( $option );
		break;

	# Config
	case "config":
		config( $option );
		break;
	case "saveConfig":
		saveConfig( $option );
		break;

	# About
	case "about":
		HTML_hp_avl::showAbout();
		break;

	case "viewTypes":
	default:
		viewTypes( $option );
}

function ext_main( $option ) {
	$database =& JFactory::getDBO();

	$today = getdate();
	$year = JRequest::getInt( 'year', $today['year'] );
	$month = JRequest::getInt( 'month', $today['mon'] );
	$property_id = JRequest::getInt( 'property_id', 0 );
	
	if ($property_id == 0) {
	
		HTML_hp_avl::ext_error( "<center><br /><h2>Unavailable</h2>Please save the property first before setting up availability's option</center>" );		

	} else {

		$cal = new backend_Calendar();
		$cal->property_id = $property_id;

		# Get Availability Type
		$database->setQuery('SELECT * FROM #__hp_avl_types');
		$avl_types = $database->loadObjectList('avl_type_id');
		$cal->avl_types = $avl_types;

		# Retrieve assigned Availability
		$database->setQuery("SELECT avl_type_id, date FROM #__hp_avl WHERE property_id = '".$property_id."' AND MONTH(`date`) = '".$month."' AND YEAR(`date`) = '".$year."'");
		$cal->avl = $database->loadObjectList('date');

		$html['calendar'] = $cal->getMonthHTML( $month, $year );

		HTML_hp_avl::ext_main( $html, $avl_types, $month, $year, $property_id, $option );

	}
}

function ext_save( $option ) {
	global $mainframe;
	$database =& JFactory::getDBO();
	
	$post		= JRequest::get('post');
	$year = JRequest::getInt( 'year' );
	$month = JRequest::getInt( 'month' );
	$property_id = JRequest::getInt( 'property_id' );
	
	$row = new hp_avl($database);

	$row->clear_avl($property_id, $month, $year);

	foreach($post AS $k => $v) {
		if (substr($k,0,3) == "day" && $v > 0) {
			$date = date('Y-m-d', strtotime(substr($k,3,2).' '.date('F',mktime(0, 0, 0, $month, 1, 2004)).' '.$year));
			$row->add_avl($property_id, $date, $v);
		}
	}
	$mainframe->redirect( "index.php?option=$option&task=ext_viewmonth&month=$month&year=$year&property_id=$property_id&tmpl=component", JText::_( 'HOTPROPERTY_AVL_AVAILABILITIES_HAVE_BEEN_SAVED' )  );
  
}

function ext_viewyear( $option ) {
	$database =& JFactory::getDBO();

	$today = getdate();
	$year = JRequest::getInt( 'year', $today['year'] );
	$month = JRequest::getInt( 'month', $today['mon'] );
	$property_id = JRequest::getInt( 'property_id', 0 );

	if ($property_id == 0) {
		HTML_hp_avl::ext_error( "<center><br /><h2>".JText::_( 'HOTPROPERTY_AVL_UNAVAILABLE' )."</h2>".JText::_( 'HOTPROPERTY_AVL_ERROR_01' )."</center>" );
	} else {
		
		$cal = new backend_Calendar();
		$cal->edit = 0;
		$cal->property_id = $property_id;

		# Get Availability Type
		$database->setQuery('SELECT * FROM #__hp_avl_types');
		$avl_types = $database->loadObjectList('avl_type_id');
		$cal->avl_types = $avl_types;

		# Retrieve assigned Availability
		$database->setQuery("SELECT `avl_type_id`, `date` FROM #__hp_avl WHERE property_id = '".$property_id."' AND YEAR(`date`) = '".$year."'");
		$cal->avl = $database->loadObjectList('date');

		$html = $cal->getYearHTML( $year );

		HTML_hp_avl::ext_viewyear( $html, $option );
	}
}

function viewTypes($option) {
	global $mainframe;

	$database =& JFactory::getDBO();

	$limit = $mainframe->getUserStateFromRequest( "viewlistlimit", 'limit', 10 );
	$limitstart = $mainframe->getUserStateFromRequest( "viewcli{$option}limitstart", 'limitstart', 0 );

	# get the total number of records
	$database->setQuery( "SELECT count(*) FROM #__hp_avl_types" );
	$total = $database->loadResult();

	jimport('joomla.html.pagination');
	$pageNav = new JPagination($total, $limitstart, $limit);
	
	$sql = "SELECT *"
		. "\nFROM #__hp_avl_types"
		. "\nORDER BY ordering ASC"
		. "\nLIMIT $pageNav->limitstart,$pageNav->limit";

	$database->setQuery($sql);

	if(!$result = $database->query()) {
		return false;
	}
	$rows = $database->loadObjectList();

	HTML_hp_avl::viewTypes( $rows, $pageNav, $option );
}

function editType($id, $option) {
	$database =& JFactory::getDBO();

	$row = new hp_avl_types($database);
	$row->load($id);

	HTML_hp_avl::editType( $row, $option );
}

function saveType( $option ) {
	global $mainframe;
	$database =& JFactory::getDBO();

	$post = JRequest::get( 'post' );

	$row = new hp_avl_types( $database );

	if (!$row->bind( $post )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	# Put new item to last
	if($row->avl_type_id < 1) $row->ordering = 9999;

	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	# Update order
	$row->reorder();
	$mainframe->redirect( "index2.php?option=$option&task=viewTypes");
}

function orderType( $id, $inc, $option ) {
	global $mainframe;
	$database =& JFactory::getDBO();

	$row = new hp_avl_types( $database  );
	$row->load( $id );
	$row->move( $inc);
	
	$mainframe->redirect( "index2.php?option=$option&task=viewTypes" );
}

function removeType( $id, $option ) {
	global $mainframe;
	$database =& JFactory::getDBO();

	for ($i = 0; $i < count($id); $i++) {
			# Clear all Availability of this type
			$query="DELETE FROM #__hp_avl WHERE `avl_type_id`='".$id[$i]."'";
			$database->setQuery($query);
			$database->query();
			
			# Delete the Availability Type
			$query="DELETE FROM #__hp_avl_types WHERE `avl_type_id`='".$id[$i]."'";
			$database->setQuery($query);
			$database->query();
	}
	$mainframe->redirect("index2.php?option=$option&task=viewTypes");
}

function cancelType( $option ) {
	global $mainframe;
	$mainframe->redirect( "index2.php?option=$option&task=viewTypes" );
}

?>