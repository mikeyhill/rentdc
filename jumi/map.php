<?php
// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

global $_VERSION;

// Add the code you want to include below the next line

?>
<img src="dc_peninsula_map.jpg" alt="Door County Map" width="350" height="375" border="0" usemap="#Map" longdesc="http://www.rentdoorcounty.com/images/dc_peninsula_map.jpg" />
<map name="Map" id="Map">
<area shape="rect" coords="135,61,230,81" href="http://www.rentdoorcounty.com/index.php?option=com_hotproperty&view=searches&layout=results&Itemid=61&Field[Town2]=Gills+Rock" />
<area shape="rect" coords="116,90,205,110" href="http://www.rentdoorcounty.com/index.php?option=com_hotproperty&view=searches&layout=results&Itemid=61&Field[Town2]=Ellison+Bay" />
<area shape="rect" coords="94,116,184,136" href="http://www.rentdoorcounty.com/index.php?option=com_hotproperty&view=searches&layout=results&Itemid=61&Field[Town2]=Sister+Bay" />
<area shape="rect" coords="85,140,170,159" href="http://www.rentdoorcounty.com/index.php?option=com_hotproperty&view=searches&layout=results&Itemid=61&Field[Town2]=Ephraim" />
<area shape="rect" coords="65,168,149,187" href="http://www.rentdoorcounty.com/index.php?option=com_hotproperty&view=searches&layout=results&Itemid=61&Field[Town2]=Fish+Creek" />
<area shape="rect" coords="45,198,133,221" href="http://www.rentdoorcounty.com/index.php?option=com_hotproperty&view=searches&layout=results&Itemid=61&Field[Town2]=Egg+Harbor" />
<area shape="rect" coords="217,160,332,182" href="http://www.rentdoorcounty.com/index.php?option=com_hotproperty&view=searches&layout=results&Itemid=61&Field[Town2]=Baileys+Harbor" />
<area shape="rect" coords="201,207,308,227" href="http://www.rentdoorcounty.com/index.php?option=com_hotproperty&view=searches&layout=results&Itemid=61&Field[Town2]=Jacksonport" />
<area shape="rect" coords="194,232,303,253" href="http://www.rentdoorcounty.com/index.php?option=com_hotproperty&view=searches&layout=results&Itemid=61&Field[Town2]=Whitefish+Bay" />
<area shape="rect" coords="174,280,271,310" href="http://www.rentdoorcounty.com/index.php?option=com_hotproperty&view=searches&layout=results&Itemid=61&Field[Town2]=+Sturgeon+Bay+Lakeside" />
<area shape="rect" coords="13,226,112,261" href="http://www.rentdoorcounty.com/index.php?option=com_hotproperty&view=searches&layout=results&Itemid=61&Field[Town2]=Sturgeon+Bay+Bayside" />
<area shape="rect" coords="217,186,328,206" href="http://www.rentdoorcounty.com/index.php?option=com_hotproperty&view=searches&layout=results&Itemid=61&Field[Town2]=Kangaroo+Lake" />
</map>